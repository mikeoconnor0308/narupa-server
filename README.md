# README #

## What is this repository for? ##

This repository contains the implementation of the simulations and server for Narupa, a virtual reality enabled real-time interactive molecular dynamics framework for use in nanotech research applications. It is an active fork of the iMD-VR framework [first described in this paper](http://advances.sciencemag.org/content/4/6/eaat2731).  

## Why is it called Narupa?

Narupa is a portmanteau of the prefix 'nano' and suffix 'arupa'. [Wikipedia](https://en.wikipedia.org/wiki/Arupa) explains how arūpa is a Sanskrit word describing non-physical and non-material objects. It seemed to us a good concept for describing what it's like to interact with simulated nanoscale objects in VR. 

## How do I get set up? ##

This page provides details on how to clone and build the repository from source. For more detailed guidance, check out the [documentation](https://glowackigroup.gitlab.io/narupa/). Documentation on the VR client can be found
[here](https://glowackigroup.gitlab.io/narupaXR/).

### All Platforms 

Clone this repository to a folder called `narupa-server`. It is generally advisable to create a folder structure for all the repos in the Narupa family, (e.g. [narupaXR](https://gitlab.com/glowackigroup/narupaxr), [narupa-openmm](https:/gitlab.com/glowackigroup/narupa-openmm)), as it will make linking much easier:


```
Narupa
+-- narupa-server
+-- narupaXR
+-- plugins
|   +-- narupa-openmm
```

#### Windows

You will need to have [Visual Studio 2017](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx) (Community is fine), or [Rider](https://www.jetbrains.com/rider/) installed. If you want to develop the OpenMM plugin, be sure to install the C++ support. 
Open the narupa solution `(narupa-server/narupa.sln)`.

Press play.

To build the documentation, run the `build_docs.bat` script in the root of the repository. Doxygen (version > 1.8) must be installed and on your PATH. 

#### Linux / OS X

To build from source on Linux, mono 5.0+ is required. Download using instructions from the [mono website](https://www.mono-project.com/download/stable/#download-lin). *Note:* We will soon be supporting dotnet standard. If you can handle not using vim, we recommend using the cross platform IDE [Rider](https://www.jetbrains.com/rider/). 
In Rider, open the narupa solution `narupa-server/narupa.sln)`. Press play.

Otherwise, open a terminal, navigate to the `narupa-server` folder and build the solution with `msbuild`: 
```
msbuild narupa.sln
```

To build the documentation, run the `build_docs.sh` script in the root of the repository. Doxygen (version > 1.8) must be installed and on your PATH. 

**Note:** When running with plugins that interface with native code (e.g. OpenMM), you may need to run the server from the terminal, depending on your setup. Open a terminal, navigate to the narupa-server.exe binary location (by default this is `narupa-server/bin/Debug/`) and run:

```
#!bash
mono /path/to/narupa/narupa-server/bin/Debug/narupa-server.exe
```

If you experience crashes while loading plugins, it may be helpful to run mono in Debug mode, as it will provide detailed information as the mono runtime attempts to load native libaries. To do this, set the MONO_LOG_LEVEL variable to DEBUG, e.g:
```#!bash
export MONO_LOG_LEVEL=DEBUG
mono narupa-server.exe
```

## Getting Started

To change the simulation that is run, edit the `server.xml` file (in the location of the executable) to point at a prepared simulation. There are many examples in the `Assets/Simulations` folder, alternatively, from a terminal, you can specify a simulation with:

```
[mono] narupa-server.exe -S /path/to/simulation.xml
```

Here, `mono` may be required.

## Plugins

Narupa functionality can be extended with Plugins. We currently support an [OpenMM plugin](https://gitlab.com/glowackigroup/narupa-openmm), which enables GPU-accelerated simulation of biomolecules. 

### Windows

Create a new folder called Plugins in Narupa binary folder (typically `narupa-server/bin/Plugins`) folder (the root of the repository folder), and place plugins in separate folders within that directory. Rebuild the server and it will copy them to the correct output directory
(i.e. debug/release).
**Note:** On Windows, before extracting you may need to "unblock" the download link. In order to do this, right click on the zip file, click Properties, and check the Unblock box at the bottom of the window.

### Linux

Copy the managed (i.e. C#) folder structure downloaded or generated during build to the a folder called Plugins in the Narupa build directory (typically `narupa-server/bin/Plugins`).
If native binaries are also supplied with the binary (e.g. C++ binaries, .so or .a files) place these in `/usr/lib`, run the command `ldconfig` or add the paths to your `LD_LIBRARY_PATH` (see [here](http://www.mono-project.com/docs/advanced/pinvoke/) for more documentation).

## Acknowledgements 

Some portions of the molecular dynamics codebase have been based on the excellent OpenMM application and library layers, and are distributed under the [MIT license](http://docs.openmm.org/latest/userguide/library.html#license). We also support the VMD IMD API, which is distributed under the [UIUC Open Source License](https://www.ks.uiuc.edu/Research/vmd/plugins/pluginlicense.html). We thank them for their efforts. 