﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;

namespace Nano.Science.Simulation
{
    /// <summary>
    ///     Loads and stores the data structures required to define a simulation.
    /// </summary>
    public class SimulationFile : ILoadable
    {

        /// <summary>
        ///     The reporter
        /// </summary>
        private IReporter reporter;

        /// <summary>
        ///     Name of this simulation.
        /// </summary>
        public string SimulationName;

        private XmlDocument xmlDoc;


        /// <inheritdoc />
        public SimulationFile()
        {
            Topology = new Topology();
        }

        /// <inheritdoc />
        public SimulationFile(IReporter reporter)
        {
            Topology = new Topology();
            this.reporter = reporter;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Simulation.SimulationFile" /> class.
        /// </summary>
        /// <param name="simulationFileName">Name of the simulation file.</param>
        /// <param name="reporter">The reporter the instance will use.</param>
        public SimulationFile(string simulationFileName, IReporter reporter)
        {
            Topology = new Topology();

            SimulationFileName = simulationFileName;
            this.reporter = reporter;
        }

        /// <inheritdoc />
        public SimulationFile(XmlDocument xmlDoc, IReporter reporter)
        {
            Topology = new Topology();
            this.reporter = reporter;
            this.xmlDoc = xmlDoc;
        }

        /// <inheritdoc />
        public SimulationFile(IReporter reporter, Topology topology, SystemProperties simulationSettings = null)
        {
            Topology = topology;
            SimulationSettings = simulationSettings;
            this.reporter = reporter;
        }

        /// <summary>
        ///     Name of simulation file.
        /// </summary>
        public string SimulationFileName { get; }

        /// <summary>
        ///     Whether the simulation has been loaded from file.
        /// </summary>
        public bool LoadedSimulation { get; private set; }

        /// <summary>
        ///     The physical settings of the simulation.
        /// </summary>
        public SystemProperties SimulationSettings { get; set; }

        /// <summary>
        ///     The topology of the simulation.
        /// </summary>
        public Topology Topology { get; set; }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            if (SimulationSettings == null)
            {
                SimulationSettings = new SystemProperties();
                SimulationSettings.InitialiseDefaultProperties();

                var atomPositions = Topology.GetAtomPositions();
                //@review This was for builder....
                //SimulationSettings.SimBoundary.FitBoundaryToPositions(atomPositions);
            }
            element.AppendChild(Loader.SaveObject(context, element, SimulationSettings));

            element.AppendChild(Loader.SaveObject(context, element, Topology));
            return element;
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            SimulationName = Helper.GetAttributeValue(node, "Name", "Unnamed Simulation");
            SimulationSettings =
                Loader.LoadObject<SystemProperties>(context, node, new[] {typeof(IReporter)}, new object[] {reporter});

            if (SimulationSettings == null)
                context.Error("Missing element 'SystemProperties' in simulation file", true);

            Topology = Loader.LoadObject<Topology>(context, node);

            LoadedSimulation = true;
            reporter = context.Reporter;
        }

        /// <summary>
        /// Instantiates the <see cref="Topology"/> into a full topology, spawning all templates.
        /// </summary>
        /// <returns>Topology ready for simulation.</returns>
        public InstantiatedTopology InstantiateTopology()
        {
            var topology = new InstantiatedTopology();
            var boundary = SimulationSettings.SimBoundary;

            try
            {
                Topology.Instantiate(topology, ref boundary, reporter);
            }
            catch (Exception e)
            {
                reporter.PrintException(e, "Failed to spawn topology into simulation.");
                throw;
            }

            topology.GenerateMolecules();

            return topology;
        }

        /// <summary>
        ///     Loads the simulation from file.
        /// </summary>
        /// <returns><c>true</c> if simulation successfully loaded, <c>false</c> otherwise.</returns>
        /// <exception cref="System.Exception">Missing element 'SystemProperties' in simulation file ' + SimulationFileName + '</exception>
        /// <autogeneratedoc />
        public bool LoadSimulation()
        {
            var context = new LoadContext(reporter);

            if (xmlDoc == null)
            {
                xmlDoc = new XmlDocument();

                try
                {
                    xmlDoc.Load(Helper.ResolvePath(SimulationFileName));
                }
                catch (Exception e)
                {
                    reporter.PrintError("Exception thrown while attempting to load simulation: {0}", e.ToString());
                    throw;
                }
            }

            XmlNode simulationNode = xmlDoc.DocumentElement;

            Load(context, simulationNode);

            if (context.Errors.Count > 0)
            {
                reporter.PrintError("Errors were encountered while attempting to load simulation: ");
                context.ReportErrors();
                if (context.HasHadCriticalError)
                    throw new Exception("Critical error encountered while attempting to load simulation.");
            }

            LoadedSimulation = true;

            return true;
        }
    }
}