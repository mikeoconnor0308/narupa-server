﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents an exception in the simulation file.
    /// </summary>
    [Serializable] 
    public class FlexibleFileFormatException : Exception
    {
        /// <summary>
        /// XML string associated with this exception.
        /// </summary>
        public string Xml { get; set; }  

        /// <summary>
        /// Constructs an exception for the given xml. 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="xml"></param>
        public FlexibleFileFormatException(string message, string xml) : base(message)
        {
            Xml = xml; 
        }

        /// <summary>
        /// Cosntructs an exception for the given xml.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="xml"></param>
        /// <param name="innerException"></param>
        public FlexibleFileFormatException(string message, string xml, Exception innerException) : base(message, innerException)
        {
            Xml = xml;
        }
    }
}
