﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using SlimMath;
using System.Collections.Generic;
using System.Xml;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents a molecular topology, prior to being spawned into the simulation as a <see cref="InstantiatedTopology"/>.
    /// </summary>
    /// <remarks>
    /// The topology here is represented as a series of templates (groups of atoms, molecules etc) and spawners. Upon
    /// instantiated, this topology will be converted into a concrete series of atoms in an <see cref="IAtomicSystem"/>. 
    /// </remarks>
    [XmlName("Topology")]
    public class Topology : ILoadable
    {
        /// <summary>
        /// The templates in this topology.
        /// </summary>
        public readonly ResidueCollection Templates = new ResidueCollection();
        /// <summary>
        /// The spawners associated with the templates in this topology.
        /// </summary>
        public readonly List<ISpawner> Spawners = new List<ISpawner>();
        /// <summary>
        /// The external forcefields applied to all templates in this topology.
        /// </summary>
        public readonly List<IExternalForceFieldTerms> ExternalForceFieldTerms = new List<IExternalForceFieldTerms>();
        
        //TODO @review: Should a selection be a part of a Residue or Template? 
        /// <summary>
        /// JSON string for selection data that the user wants to pass to the front end. 
        /// </summary>
        public string SelectionData { get; private set; }

        //TODO @review This should live at the residue / template level, or be a front end representational choice.
        /// <summary>
        /// The dynamic bond implementation for this system.
        /// </summary>
        public IDynamicBonds DynamicBonds;

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            if (node.SelectSingleNode("Templates") != null)
            {
                Templates.Clear();
                Templates.AddRange(Loader.LoadObjects<IResidue>(context, node.SelectSingleNode("Templates")));

                Spawners.Clear();
                Spawners.AddRange(Loader.LoadObjects<ISpawner>(context, node.SelectSingleNode("Spawners")));
                if(Spawners.Count == 0)
                {
                    context.Error("Missing spawner node, will spawn using instance spawner. ");
                    foreach (IResidue residue in Templates)
                    {
                        Spawners.Add(new InstanceSpawner(residue.Name, residue.Name, Vector3.Zero, Quaternion.Identity));
                    }
                }


            }
            //If templates node doesn't exist, assume we're spawning straight from a residue definition.
            else
            {
                Templates.Clear();
                var template = Loader.LoadObject<IResidue>(context, node);
                Templates.Add(template);
                Spawners.Clear();
                Spawners.Add(new InstanceSpawner(template.Name, template.Name, Vector3.Zero, Quaternion.Identity));
            }
            ExternalForceFieldTerms.Clear();
            ExternalForceFieldTerms.AddRange(
                Loader.LoadObjects<IExternalForceFieldTerms>(context,
                    node.SelectSingleNode("ExternalForceFields")));
            DynamicBonds = Loader.LoadObject<IDynamicBonds>(context, node);

            //TODO @review: Should selection data exist in its own (ILoadable) class? 
            if (node.SelectSingleNode("SelectionData") != null &&
                node.SelectSingleNode("SelectionData").Attributes["FilePath"] != null)
            {
                string json_path = node.SelectSingleNode("SelectionData").Attributes["FilePath"].Value;
                context.Reporter.PrintNormal("Loading in JSON selection file: " + json_path);
                using (System.IO.StreamReader r = new System.IO.StreamReader(json_path))
                {
                    SelectionData = r.ReadToEnd();
                }
            }
            else SelectionData = string.Empty;
        }

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns></returns>
        public Topology Clone()
        {
            Topology t = new Topology();
            t.ExternalForceFieldTerms.AddRange(ExternalForceFieldTerms);
            t.Spawners.AddRange(Spawners);
            t.Templates.AddRange(Templates);
            if (DynamicBonds != null)
                t.DynamicBonds = DynamicBonds;
            return t;
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            element.AppendChild(Loader.SaveObjects(context, element, Templates, "Templates"));
            /*
            if (Spawners.Count == 0)
            {
                if (context.Reporter != null) context.Reporter.PrintDebug("Generating default spawners...");
                GenerateDefaultSpawners();
            }
            */
            element.AppendChild(Loader.SaveObjects(context, element, Spawners, "Spawners"));
            element.AppendChild(Loader.SaveObjects(context, element, ExternalForceFieldTerms, "ExternalForceFieldTerms"));
            if (DynamicBonds != null) element.AppendChild(Loader.SaveObject(context, element, DynamicBonds));
            return element;
        }

        /// <summary>
        /// Instantiates this topology.
        /// </summary>
        /// <param name="topology">The topology to instantiate.</param>
        /// <param name="boundry">The boundary to use for spawning.</param>
        /// <param name="reporter">Reporter to print information to while spawning.</param>
        public void Instantiate(InstantiatedTopology topology, ref ISimulationBoundary boundry, IReporter reporter)
        {
            List<BoundingSphere> boundingSpheres = new List<BoundingSphere>();

            foreach (ISpawner spawner in Spawners)
            {
                SpawnContext spawnContext = SpawnContext.Identity;
                spawnContext.Reporter = reporter;
                List<ISpawnable> spawnables = GetSpawnables(spawner, true);

                spawner.Spawn(ref boundry, ref boundingSpheres, topology, ref spawnContext, spawnables.ToArray());
                topology.Spawners.Add(spawner.Name);
            }
            if (DynamicBonds != null) topology.DynamicBonds = DynamicBonds;
            FlattenExternalForceFields(topology);

            if (SelectionData != null) topology.SelectionData = this.SelectionData;
            else topology.SelectionData = string.Empty;
        }

        private void FlattenExternalForceFields(InstantiatedTopology topology)
        {
            foreach (IExternalForceFieldTerms forceFieldTerms in ExternalForceFieldTerms)
            {
                forceFieldTerms.AddToTopology(topology);
            }
        }

        private List<ISpawnable> GetSpawnables(ISpawner spawner, bool throwExceptions = false)
        {
            List<ISpawnable> spawnables = new List<ISpawnable>();

            foreach (string templateName in spawner.Templates)
            {
                IResidue residue;
                //TODO @review @ptew Terrible hack due to assumption of unique residue names!
                if (Templates.TryGet(templateName + "-0", out residue) == false)
                {
                    if (throwExceptions)
                    {
                        throw new FlexibleFileFormatException(string.Format("Spawner \"{0}\" references unknown residue template \"{1}\".", spawner, templateName), string.Empty);
                    }

                    continue;
                }

                spawnables.Add(residue);
            }

            return spawnables;
        }

        internal List<Vector3> GetAtomPositions()
        {
            List<Vector3> atomPositions = new List<Vector3>();
            foreach (IResidue residue in Templates)
            {
                foreach (IAtom atom in residue.Atoms)
                {
                    atomPositions.Add(atom.Position);
                }
            }
            return atomPositions;
        }
    }
}