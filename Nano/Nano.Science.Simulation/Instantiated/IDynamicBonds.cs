// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;

namespace Nano.Science.Simulation.Instantiated
{
    /// <summary>
    /// Interface for a dynamic bond algorithm.
    /// </summary>
    public interface IDynamicBonds : ILoadable
    {
        /// <summary>
        /// Given current state of atomic system, calculates bonds based on the update frequency.
        /// </summary>
        /// <param name="system"></param>
        /// <param name="bondsChanged"></param>
        /// <param name="forceCalculation"></param>
        /// <returns>The current list of bonds.</returns>
        BondList CalculateBonds(IAtomicSystem system, out bool bondsChanged, bool forceCalculation = false);
    }
}