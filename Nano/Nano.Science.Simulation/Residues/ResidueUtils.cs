﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Nano.Science.Simulation.Spawning;
using SlimMath;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using Narupa.Trajectory.Load.PDB;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Helper methods for spawning groups and residues.
    /// </summary>
    public static class ResidueUtils
    {
        /// <summary>
        /// Load the specified residue from file.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static IResidue LoadFromFile(LoadContext context, string filePath)
        {
            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.Load(Helper.ResolvePath(filePath));

            XmlNode templateNode = xmlDocument.DocumentElement;

            return Loader.LoadObject<IResidue>(context, templateNode);
        }

        private static bool TryGetAtom(IResidue residue, ref AtomPathPart bondPathPart, out IAtom atom, out int atomIndex, bool throwExceptions = false)
        {
            atom = null;
            atomIndex = -1;

            if (bondPathPart.Type == AtomPathPartType.String)
            {
                for (int i = 0; i < residue.Atoms.Count; i++)
                {
                    IAtom residueAtom = residue.Atoms[i];

                    if (bondPathPart.String.Equals(residueAtom.Name) == true)
                    {
                        atomIndex = i;

                        atom = residueAtom;

                        return true;
                    }
                }

                if (throwExceptions == true)
                {
                    throw new FlexibleFileFormatException(
                        $"No atom with the name \"{bondPathPart.String}\" exists within residue \"{residue.Name}\".", string.Empty);
                }
            }
            else
            {
                if (bondPathPart.Index >= 0 && bondPathPart.Index < residue.Atoms.Count)
                {
                    atomIndex = bondPathPart.Index;

                    atom = residue.Atoms[atomIndex];

                    return true;
                }

                if (throwExceptions == true)
                {
                    throw new FlexibleFileFormatException(
                        $"The atom index \"{bondPathPart.Index}\" is outside the bounds of the residue \"{residue.Name}\" atoms collection.", string.Empty);
                }
            }

            return false;
        }

        internal static void LoadFromExternalFile(LoadContext context, string filePath, IResidue parent, bool centre = false)
        {
            filePath = Helper.ResolvePath(filePath);
            string ext = Path.GetExtension(filePath)?.ToLower();
            if (ext == ".pdb") LoadFromPdbFile(context, filePath, parent, centre);
            else if (ext == ".xyz")
            {
                if (centre) context.Reporter.PrintNormal("Warning: Centering xyz files around 0 not implemented. Using coordinates from file...");
                LoadFromXyzFile(context, filePath, parent);
            }
            else context.Error($"Unsupported file type to load topology from {ext}, check input.", true);
        }

        private static void LoadFromXyzFile(LoadContext context, string filePath, IResidue parent)
        {
            List<IAtom> atoms = new List<IAtom>();
            var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                int lineNumber = 0;
                int numAtoms = 0;
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (lineNumber == 0)
                    {
                        bool success = int.TryParse(line, out numAtoms);
                        if (success == false)
                            context.Error(
                                $"Invalid xyz file, expected first line to be number of atoms {filePath}, saw {line}");
                    }
                    else if (lineNumber == 1)
                    {
                    }
                    else if ((lineNumber - 2) > numAtoms)
                    {
                        context.Reporter.PrintWarning(ReportVerbosity.Normal,
                            $"XYZ file {filePath} contained more atoms than declared, ignoring them!");
                        break;
                    }
                    else
                        atoms.Add(ReadAtomFromXyzString(line));
                    lineNumber++;
                }
            }
            parent.Atoms.AddRange(atoms);
            //TODO add support for bonds detection.
            context.Reporter.PrintWarning(ReportVerbosity.Normal, "Reading from xyz files does not currently support automatic bond definitions. Ensure you are running with DynamicBonds");
        }

        private static void LoadFromPdbFile(LoadContext context, string filePath, IResidue parent, bool centre = false)
        {
            PdbFile pdbFileStructure = new PdbFile(Helper.ResolvePath(filePath), context.Reporter, centre);
            //TODO run pdb file through pdbfixer?

            //Copy contents from PDBFile over to Nano Simbox residues.
            if (pdbFileStructure.Models.Count == 0)
                context.Error($"PDB File {filePath} did not have any models or valid atoms in it!", true, true);
            PdbModel model = pdbFileStructure.Models[0];
            foreach (PdbChain chain in model.Chains)
            {
                //TODO @review Residue naming convention doesn't really make sense, maybe something more generic like AtomGroup??
                Residue chainResidue = new Residue();
                chainResidue.Name = chain.Letter;
                chainResidue.Parent = parent;
                //Loop over the residues in the chain.
                foreach (PdbResidue res in chain.Residues)
                {
                    Residue residue = new Residue();
                    //TODO @review how best to represent residues and residue numbers in the address? residue names in pdb files are not unique, but the IDs are. We want both!
                    residue.Name = $"{res.Name}-{res.Number}-{res.AbsoluteIndex}";
                    residue.Id = res.Number;
                    residue.Parent = chainResidue;
                    foreach (PdbAtom atom in res.Atoms)
                    {
                        AtomMetaData simboxAtom = new AtomMetaData();
                        simboxAtom.Name = $"{atom.Name}-{atom.SerialNumber}-{atom.AbsoluteIndex}";
                        simboxAtom.Element = (Element)PeriodicTable.GetElementPropertiesBySymbol(atom.Element).Number;
                        simboxAtom.Position = atom.Position * Units.NmPerAngstrom;
                        //TODO would it be good for simboxAtoms to have more data? E.g. link to the residue they are in etc.
                        residue.Atoms.Add(simboxAtom);
                    }
                    chainResidue.Children.Add(residue);
                }
                parent.Children.Add(chainResidue);
            }

            //Loop over all the bonds in the pdb file and add to the residue.
            foreach (PdbBond pdbBond in pdbFileStructure.Bonds)
            {
                BondMetaData simboxBond = new BondMetaData();
                PdbAtom atomA = pdbBond.From;
                PdbAtom atomB = pdbBond.To;
                string pathToA = $"{atomA.ChainIdentifier}/{atomA.ResidueName}-{atomA.ResidueId}-{atomA.AbsoluteResidueIndex}/{atomA.Name}-{atomA.SerialNumber}-{atomA.AbsoluteIndex}";
                string pathToB = $"{atomB.ChainIdentifier}/{atomB.ResidueName}-{atomB.ResidueId}-{atomB.AbsoluteResidueIndex}/{atomB.Name}-{atomB.SerialNumber}-{atomB.AbsoluteIndex}";
                simboxBond.PathToA = new AtomPath(pathToA);
                simboxBond.PathToB = new AtomPath(pathToB);
                parent.Bonds.Add(simboxBond);
            }
        }

        private static IAtom ReadAtomFromXyzString(string line)
        {
            AtomMetaData atom = new AtomMetaData();
            var fields = line.Trim().Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            if (fields.Length < 4)
                throw new Exception($"Invalid line for XYZ file {line}");
            Element element = (Element)PeriodicTable.GetElementPropertiesBySymbol(fields[0]).Number;
            Vector3 position = Vector3.Zero;
            for (int i = 1; i < 4; i++)
            {
                float x = 0f;
                bool success = float.TryParse(fields[i], NumberStyles.Any, CultureInfo.InvariantCulture, out x);
                if (success == false)
                    throw new Exception($"Invalid position field {fields[i]} in line {line}");

                position[i - 1] = x;
            }
            atom.Element = element;
            atom.Position = position * Units.NmPerAngstrom;
            //@review don't use atom name as it is not unique!
            //atom.Name = fields[0];
            return atom;
        }

        private static bool TryGetChildResidue(IResidue root, ref AtomPathPart bondPathPart, out IResidue residue)
        {
            foreach (IResidue child in root.Children)
            {
                if (bondPathPart.String.Equals(child.Name) == true)
                {
                    residue = child;
                    return true;
                }
            }

            if (bondPathPart.Type == AtomPathPartType.Index)
            {
                if (bondPathPart.Index >= 0 && bondPathPart.Index < root.Children.Count)
                {
                    residue = root.Children[bondPathPart.Index];
                    return true;
                }
            }

            residue = root;

            return false;
        }

        /// <summary>
        /// Creates a bounding sphere around the atom based on its VDW radius.
        /// </summary>
        /// <param name="atom">Atom to create bounding sphere around.</param>
        /// <param name="context">Spawn context to base bounding sphere around.</param>
        /// <param name="minimumBufferDistance">Additional distance to add around atom.</param>
        /// <returns>Bounding sphere 3/4 the VDW radius of the atom plus the optional buffer distance, centered at the atoms position within the spawn context.</returns>
        public static BoundingSphere CreateBoundingSphere(IAtom atom, ref SpawnContext context, float minimumBufferDistance = 0f)
        {
            Vector3 point = Vector3.Transform(atom.Position, context.Rotation) + context.Offset;

            BoundingSphere b = new BoundingSphere(point, PeriodicTable.GetElementProperties(atom.Element).VDWRadius * 0.75f + minimumBufferDistance);
            return b;
        }

        /// <summary>
        /// Attempts to resolve the index of an atom within the topology.
        /// </summary>
        /// <param name="root">Residue atom belongs to.</param>
        /// <param name="linkage">Linkage of atom.</param>
        /// <param name="index">Index of atom.</param>
        /// <param name="throwExceptions">Whether to throw exception.</param>
        /// <returns><c>true</c> if atom index resolved, <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="FlexibleFileFormatException"></exception>
        public static bool TryResolveAtomIndex(IResidue root, AtomLinkage linkage, out int index, bool throwExceptions)
        {
            if (root == null)
            {
                throw new ArgumentNullException("root");
            }

            index = -1;

            if (linkage.Residue.Atoms.Contains(linkage.Atom) == false)
            {
                if (throwExceptions == true)
                {
                    throw new FlexibleFileFormatException(string.Format("Atom \"{0}\" does not exist in the residue \"{1}\".", linkage.Atom, linkage.Residue.ToString()), string.Empty);
                }
                else
                {
                    return false;
                }
            }

            List<IResidue> residues = new List<IResidue>();

            IResidue current = linkage.Residue;

            // walk down from child to parent until we find the root or hit null
            while (current != root)
            {
                if (current == null)
                {
                    if (throwExceptions == true)
                    {
                        throw new FlexibleFileFormatException(string.Format("Linkage \"{0}\" does not exist in the same hierarchy as root residue \"{1}\".", linkage.ToString(), root.ToString()), string.Empty);
                    }
                    else
                    {
                        return false;
                    }
                }

                residues.Add(current);

                current = current.Parent;
            }

            index = 0;

            for (int i = residues.Count - 1; i >= 0; i--)
            {
                index += residues[i].FirstAtomIndexWithParent;
            }

            index += linkage.AtomIndex;

            return true;
        }

        internal static BoundingBox CreateBoundingBox(IAtom atom, ref SpawnContext context)
        {
            Vector3 point = Vector3.Transform(atom.Position, context.Rotation) + context.Offset;
            float vdwRadius = PeriodicTable.GetElementProperties(atom.Element).VDWRadius * 0.5f;
            return new BoundingBox(point - new Vector3(vdwRadius), point + new Vector3(vdwRadius));
        }

        /// <summary>
        /// Attempts to resolve the path of an atom in a bond.
        /// </summary>
        /// <param name="root">Residue atom belongs to.</param>
        /// <param name="linkage">Linkage of atom.</param>
        /// <param name="bondPath">Path to atom.</param>
        /// <param name="throwExceptions"></param>
        /// <returns><c>true</c> if bond path resolved, <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="FlexibleFileFormatException"></exception>
        public static bool TryResolveBondPath(IResidue root, AtomLinkage linkage, out AtomPath bondPath, bool throwExceptions)
        {
            if (root == null)
            {
                throw new ArgumentNullException("root");
            }

            bondPath = default(AtomPath);

            if (linkage.Residue.Atoms.Contains(linkage.Atom) == false)
            {
                if (throwExceptions == true)
                {
                    throw new FlexibleFileFormatException(string.Format("Atom \"{0}\" does not exist in the residue \"{1}\".", linkage.Atom, linkage.Residue.ToString()), string.Empty);
                }
                else
                {
                    return false;
                }
            }

            List<IResidue> residues = new List<IResidue>();

            IResidue current = linkage.Residue;

            // walk down from child to parent until we find the root or hit null
            while (current != root)
            {
                if (current == null)
                {
                    if (throwExceptions == true)
                    {
                        throw new FlexibleFileFormatException(string.Format("Linkage \"{0}\" does not exist in the same hierarchy as root residue \"{1}\".", linkage.ToString(), root.ToString()), string.Empty);
                    }
                    else
                    {
                        return false;
                    }
                }

                residues.Add(current);

                current = current.Parent;
            }

            StringBuilder pathBuilder = new StringBuilder();

            for (int i = residues.Count - 1; i >= 0; i--)
            {
                pathBuilder.Append(residues[i].Name + "/");
            }

            if (string.IsNullOrEmpty(linkage.Atom.Name) == false)
            {
                pathBuilder.Append(linkage.Atom.Name);
            }
            else
            {
                pathBuilder.Append(linkage.AtomIndex);
            }

            bondPath = new AtomPath(pathBuilder.ToString());

            return true;
        }

        /// <summary>
        /// Resolve the indices of a pair of atoms.
        /// </summary>
        /// <param name="baseIndex"></param>
        /// <param name="root"></param>
        /// <param name="bond"></param>
        /// <param name="indexA"></param>
        /// <param name="indexB"></param>
        public static void ResolveAtomIndices(int baseIndex, IResidue root, IBond bond, out int indexA, out int indexB)
        {
            ResolveAtomIndices(baseIndex, root, bond.A, out indexA);

            ResolveAtomIndices(baseIndex, root, bond.B, out indexB);
        }

        /// <summary>
        /// Resolve the index of an atom.
        /// </summary>
        /// <param name="baseIndex"></param>
        /// <param name="root"></param>
        /// <param name="linkage"></param>
        /// <param name="index"></param>
        public static void ResolveAtomIndices(int baseIndex, IResidue root, AtomLinkage linkage, out int index)
        {
            TryResolveAtomIndex(root, linkage, out index, true);

            index += baseIndex;
        }

        /// <summary>
        /// Attempts to resolve a <see cref="AtomLinkage"/> for a given atom path.
        /// </summary>
        /// <param name="root">Root residue.</param>
        /// <param name="path">Path to atom.</param>
        /// <param name="linkage">Resolved atom linkage.</param>
        /// <param name="throwExceptions">Indicates whether exceptiosn will be thrown.</param>
        /// <returns><c>true</c> if atom linkage resolved, <c>false</c> otherwise.</returns>
        public static bool TryResolveLinkage(IResidue root, AtomPath path, out AtomLinkage linkage, bool throwExceptions)
        {
            linkage = default(AtomLinkage);

            IResidue residue;
            int atomIndex;
            IAtom atom;

            if (TryResolveResidue(root, path, out residue, out atom, out atomIndex, throwExceptions) == false)
            {
                return false;
            }

            linkage = new AtomLinkage(residue, atomIndex, atom);

            return true;
        }

        /// <summary>
        /// Attempts to resolve the parent residue, atom and atom index of an atom path.
        /// </summary>
        /// <param name="root">Root residue.</param>
        /// <param name="path">Atom path.</param>
        /// <param name="residue">The resolved residue.</param>
        /// <param name="atom">The resolved aotm.</param>
        /// <param name="atomIndex">The resolved atom index.</param>
        /// <param name="throwExceptions">Indicates whether exceptions will be thrown.</param>
        /// <returns><c>true</c> if residue resolved, <c>false</c> otherwise.</returns>
        /// <exception cref="FlexibleFileFormatException"></exception>
        public static bool TryResolveResidue(IResidue root, AtomPath path, out IResidue residue, out IAtom atom, out int atomIndex, bool throwExceptions = false)
        {
            residue = null;
            atom = null;
            atomIndex = -1;

            int i = 0;
            IResidue partResidue = root;

            while (i < path.Count - 1)
            {
                AtomPathPart bondPathPart = path[i];

                if (TryGetChildResidue(partResidue, ref bondPathPart, out partResidue) == false)
                {
                    if (throwExceptions == true)
                    {
                        throw new FlexibleFileFormatException(
                            $"The bond path '{root.Name}/{path.ToString()}' is not valid. Could not find child residue \"{path[i].ToString()}\".", string.Empty);
                    }

                    return false;
                }
                i++;
            }

            AtomPathPart atomPathPart = path.AtomPart;

            if (TryGetAtom(partResidue, ref atomPathPart, out atom, out atomIndex) == false)
            {
                if (throwExceptions == true)
                {
                    throw new FlexibleFileFormatException(
                        $"The bond path '{root.Name}/{path.ToString()}' is not valid. Could not find child residue \"{path[i].ToString()}\".", string.Empty);
                }

                return false;
            }

            residue = partResidue;

            return true;
        }

    }
}