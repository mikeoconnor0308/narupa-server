﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Loading;
using Nano.Science.Simulation.Spawning;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Represents a group of atoms.
    /// </summary>
    public interface IResidue : ILoadable, ISpawnable
    {
        /// <summary>
        /// The atoms in this group.
        /// </summary>
        List<IAtom> Atoms { get; }

        /// <summary>
        /// The bonds in this group.
        /// </summary>
        List<IBond> Bonds { get; }

        /// <summary>
        /// The children of this group.
        /// </summary>
        ResidueCollection Children { get; }

        /// <summary>
        /// Name of this group.
        /// </summary>
        new string Name { get; set; }

        /// <summary>
        /// ID of this group, if applicable.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Parent of this group.
        /// </summary>
        IResidue Parent { get; set; }

        /// <summary>
        /// The total number of atoms in this group and all children.
        /// </summary>
        int TotalAtomCount { get; }

        /// <summary>
        /// The first atom index with respect to the parent of this group.
        /// </summary>
        int FirstAtomIndexWithParent { get; set; }

        /// <summary>
        /// Ensures that the linkage between atoms is consistent.
        /// </summary>
        void CheckLinkage();

  }
}