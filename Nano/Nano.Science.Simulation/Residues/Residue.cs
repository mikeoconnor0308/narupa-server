﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Xml;
using Nano.Loading;
using Nano.Science.Simulation.ForceField;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Represents a high level topology of the simulation, including element list
    /// and list of connected components.
    /// </summary>
    [XmlName("Residue")]
    public class Residue : ResidueBase
    {
        /// <inheritdoc />
        public Residue()
        {
        }

        /// <inheritdoc />
        public override void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", string.Empty);

            if (string.IsNullOrEmpty(Name) == true)
            {
                throw new FlexibleFileFormatException("Residues must have a name.", node.OuterXml);
            }

            Children.Clear();
            Children.AddRange(Loader.LoadObjects<IResidue>(context, node));

            Atoms.Clear();

            Bonds.Clear();
            var file = node.SelectSingleNode("File");
            if (file != null)
            {
                string filePath = Helper.GetAttributeValue(file, "Path", "");
                bool centre = Helper.GetAttributeValue(file,"Centre",false);
                ResidueUtils.LoadFromExternalFile(context, filePath, this, centre);
            }

            Atoms.AddRange(Loader.LoadObjects<IAtom>(context, node.SelectSingleNode("Atoms")));

            Bonds.AddRange(Loader.LoadObjects<IBond>(context, node.SelectSingleNode("Bonds")));

            CheckLinkage();

            ForceFieldTerms.Clear();
            ForceFieldTerms.AddRange(Loader.LoadObjects<IForceFieldTerms>(context, node.SelectSingleNode("ForceFields")));
        }

        /// <inheritdoc />
        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);
            element.AppendChild(Loader.SaveObjects(context, element, Atoms, "Atoms", null));
            element.AppendChild(Loader.SaveObjects(context, element, Bonds, "Bonds", null));

            if (Children != null)
            {
                Loader.SaveObjects<IResidue>(context, element, Children.ToArray());
            }

            element.AppendChild(Loader.SaveObjects(context, element, ForceFieldTerms, "ForceFields", null));
            return element;
        }
    }
}