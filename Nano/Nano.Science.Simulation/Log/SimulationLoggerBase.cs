﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Xml;
using Nano.Loading;

namespace Nano.Science.Simulation.Log
{
    /// <summary>
    /// Base class for a simulation logger.
    /// </summary>
    public abstract class SimulationLoggerBase : ISimulationLogger
    {
        /// <summary>
        /// The path in which this logger will create files
        /// </summary>
        protected string LogPath = "^/Logs/Trajectories";

        /// <summary>
        /// The data this logger will store.
        /// </summary>
        /// <remarks>
        /// What would a logger log if a logger could log logs?
        /// </remarks>
        protected LogOptions LogOptions = LogOptions.Positions;

        /// <summary>
        /// How often, in time steps, will logs be written.
        /// </summary>
        protected int WriteFrequency = 1;

        /// <summary>
        /// If set to true, a date time stamp will be created with each log file.
        /// </summary>
        /// <remarks>
        /// If set to false, logs from previous runs may be overwritten.
        /// </remarks>
        protected bool UseDateTimeStamp = true;

        protected long Step;
        protected float TimeStep;
        protected float SimulationTime; 
        
        /// <summary>
        /// Identifier for this log.
        /// </summary>
        public abstract string Identifier { get; set; }

        /// <summary>
        /// Closes this log.
        /// </summary>
        public abstract void CloseLog();


        /// <inheritdoc />
        public abstract void Dispose();

        /// <inheritdoc />
        public virtual void Load(LoadContext context, XmlNode node)
        {
            LogPath = Helper.ResolvePath(Helper.GetAttributeValue(node, "LogPath", LogPath));
            LogOptions = LoadLogOptions(context, node);
            WriteFrequency = Helper.GetAttributeValue(node, "WriteFrequency", WriteFrequency);
            UseDateTimeStamp = Helper.GetAttributeValue(node, "UseDateTimeStamp", UseDateTimeStamp);
        }

        private LogOptions LoadLogOptions(LoadContext context, XmlNode node)
        {
            //If the user understands bitmasks, they can dump a value directly in
            LogOptions logOptions = Helper.GetAttributeValue(node, "LogOptions", LogOptions.None);
            //Otherwise, if an option is set then add it to the bitmask.
            logOptions |= Helper.GetAttributeValue(node, "Positions", false) ? LogOptions.Positions : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "Velocities", false) ? LogOptions.Velocities : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "Forces", false) ? LogOptions.Forces : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "Interactions", false) ? LogOptions.Interactions : logOptions;
            return logOptions;
        }

        /// <inheritdoc />
        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "LogPath", LogPath);
            Helper.AppendAttributeAndValue(element, "LogOptions", LogOptions);
            Helper.AppendAttributeAndValue(element, "WriteFrequency", WriteFrequency);
            Helper.AppendAttributeAndValue(element, "UseDateTimeStamp", UseDateTimeStamp);
            return element;
        }

        /// <inheritdoc />
        public virtual void CreateLog(bool append = false, params string[] logIdentifiers)
        {
            Helper.EnsurePathExists(Path.Combine(Helper.ResolvePath(LogPath), "dummy"));
        }

        /// <summary>
        /// Create a log file path.
        /// </summary>
        /// <param name="path">Folder to store log in.</param>
        /// <param name="name">Name of file.</param>
        /// <param name="identifiers">Additional identifiers for the log file.</param>
        /// <param name="appendDateTimeStamp">Whether to append a date time stamp.</param>
        /// <param name="fileExtension">File extension to use.</param>
        /// <returns></returns>
        public static string CreateLogFilePath(string path, string name, string[] identifiers, bool appendDateTimeStamp, string fileExtension)
        {
            string file = Path.Combine(Helper.ResolvePath(path), name);
            foreach (string identifier in identifiers)
            {
                if (identifier != string.Empty && identifier != "")
                    file += "_" + identifier;
            }
            if (appendDateTimeStamp)
            {
                file += "_" + $"{DateTime.Now:yyyy-MM-dd_hh-mm-ss-tt}";
            }
            file += "." + fileExtension;
            return file;
        }

        /// <summary>
        /// Returns true if this logger is due to perform a logging.
        /// </summary>
        /// <param name="step"></param>
        /// <param name="timeStep"></param>
        /// <returns></returns>
        public virtual bool IsDueToLog(long step, float timeStep)
        {
            Step = step;
            SimulationTime = step * timeStep;
            TimeStep = timeStep;
            
            if (step % WriteFrequency == 0)
                return true;
            else return false;
        }

        /// <summary>
        /// Logs the current atomic state.
        /// </summary>
        /// <param name="system"></param>
        public abstract void LogState(IAtomicSystem system);

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public abstract ISimulationLogger Clone();
    }
}