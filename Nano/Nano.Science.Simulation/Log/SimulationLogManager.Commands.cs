﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Reflection;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using Rug.Osc;

namespace Nano.Science.Simulation.Log
{
    /// <summary>
    /// Manages simulation logs. 
    /// </summary>
    public partial class SimulationLogManager : ICommandProvider
    {
        private TransportContext transportContext;

        /// <inheritdoc />
        public string Address
        {
            get
            {
                return "/log";
            }
        }

        /// <summary>
        /// Creates an XYZ logger upon command.
        /// </summary>
        /// <param name="message"></param>
        /// <exception cref="Exception"></exception>
        [OscCommand(Help = "Starts a new XYZ logger with the specified playerID, update frequency and LogOptions ", ExampleArguments = "1, 10, 0", TypeTagString = "s,i,i")]
        public void StartXyzLog(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());

            string playerId;
            int updateFrequency;
            LogOptions logOptions;
            try
            {
                playerId = (string)message[0];
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid playerID string.");
                return;
            }
            try
            {
                updateFrequency = (int)message[1];
                if (updateFrequency <= 0)
                    throw new Exception("Invalid update frequency!");
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid update frequency. Expected integer > 0.");
                return;
            }
            try
            {
                logOptions = (LogOptions)message[2];
                if (updateFrequency <= 0)
                    throw new Exception("Invalid update frequency!");
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid log Options. Expected integer > 0 corresponding to LogOptions bitmask.");
                return;
            }

            string logPath = Helper.ResolvePath("^/Logs/Trajectories/Players/" + playerId);

            XYZLogger logger = new XYZLogger(logPath, logOptions, updateFrequency, true);
            RemoveLogger(logger.Identifier);

            logger.CreateLog();

            try
            {
                AddLogger(logger);
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "A conflicting log already exists, this will be removed.");
                RemoveLogger(logger.Identifier);
                AddLogger(logger);
                return;
            }

            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, message);
        }

        /// <summary>
        /// Halts an XYZ logger upon command.
        /// </summary>
        /// <param name="message"></param>
        [OscCommand(Help = "Stops an existing XYZ logger with the specified playerID", ExampleArguments = "1", TypeTagString = "s")]
        public void StopXyzLog(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());

            string playerId = "";
            try
            {
                playerId = (string)message[0];
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid playerID string.");
                return;
            }
            string identifier = Helper.ResolvePath("^/Logs/Trajectories/Players/" + playerId + "/xyz");
            RemoveLogger(identifier);
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, message);
        }

        /// <inheritdoc />
        public void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        /// <inheritdoc />
        public void DetachTransportContext(TransportContext transportContext)
        {
            this.transportContext = null;
        }
    }
}