﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Loading;

namespace Nano.Science.Simulation.Log
{
    [XmlName("Logging")]
    public partial class SimulationLogManager
    {

        private readonly Dictionary<string, ISimulationLogger> activeLogs = new Dictionary<string, ISimulationLogger>();
        private readonly object locker = new object();

        /// <summary>
        /// Adds the given logger to the set of currently active logs.
        /// </summary>
        /// <param name="logger"></param>
        /// <returns><c>True</c> if logger added successfully, <c>False</c> otherwise.</returns>
        /// <exception cref="Exception"></exception>
        public bool AddLogger(ISimulationLogger logger)
        {
            lock (locker)
            {
                if (activeLogs.ContainsKey(logger.Identifier))
                {
                    throw new Exception(
                        $"Attempt to create logger with the identifier {logger.Identifier} that already exists. To avoid potential file conflicts this logger will not be added.");
                }
                activeLogs.Add(logger.Identifier, logger);
            }
            return true;
        }

        /// <summary>
        /// Removes the given logger.
        /// </summary>
        /// <param name="identifier"></param>
        public void RemoveLogger(string identifier)
        {
            lock (locker)
            {
                if (activeLogs.ContainsKey(identifier))
                    activeLogs[identifier].Dispose();
                activeLogs.Remove(identifier);
            }
        }

        
        /// <summary>
        /// Removes the given logger.
        /// </summary>
        /// <param name="logger"></param>
        public void RemoveLogger(ISimulationLogger logger)
        {
            lock (locker)
            {
                RemoveLogger(logger.Identifier);
            }
        }

        /// <summary>
        /// Perform logging for each logger.
        /// </summary>
        /// <param name="system">The atomic system.</param>
        /// <param name="step">The current simulation step.</param>
        /// <param name="timeStep">The time step used by the simulation.</param>
        public void Log(IAtomicSystem system, long step, float timeStep)
        {
            lock (locker)
            {
                foreach (var logger in activeLogs)
                {
                    if (logger.Value.IsDueToLog(step, timeStep))
                        logger.Value.LogState(system);
                }
            }
        }

        /// <summary>
        /// Initialise the active logs.
        /// </summary>
        /// <param name="simulationName"></param>
        /// <param name="reporter"></param>
        public void InitialiseActiveLogs(string simulationName = "", IReporter reporter = null)
        {
            lock (locker)
            {
                foreach (var logger in activeLogs.Values)
                {
                    try
                    {
                        logger.CreateLog(false, simulationName);
                    }
                    catch (Exception e)
                    {
                        reporter?.PrintException(e, $"Exception thrown while creating log {logger.Identifier}.");
                    }
                }
            }
        }

        /// <summary>
        /// Dispose of all logs.
        /// </summary>
        public void Dispose()
        {
            lock (locker)
            {
                foreach (ISimulationLogger logger in activeLogs.Values)
                {
                    logger.Dispose();
                }
            }
        }
    }
}