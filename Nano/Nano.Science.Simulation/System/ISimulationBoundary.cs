// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Loading;
using Nano.Transport.Variables;
using SlimMath;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents a boundary on the simulation.
    /// </summary>
    public interface ISimulationBoundary : IVariableProvider, ILoadable, IEquatable<ISimulationBoundary>
    {
        /// <summary> Whether this simulation is using periodic boundary conditions. </summary>
        bool PeriodicBoundary { get; set; }

        /// <summary> Whether this simulation boundary is currently undergoing resizing. </summary>
        bool Resizing { get; set; }

        /// <summary>
        /// Whether this boundary is variable (it can change size/shape during a simulation).
        /// </summary>
        bool IsVariableBox { get; set; }

        /// <summary> 3D dimensions of box, in Angstroms. </summary>
        BoundingBox SimulationBox { get; set; }

        /// <summary>
        /// Occurs when [box change].
        /// </summary>
        event EventHandler BoxChange;

        /// <summary>
        /// Gets the periodic box vector.
        /// </summary>
        /// <returns>Vector3.</returns>
        /// <exception cref="System.Exception">Attempted to get periodic box vector when periodic booundary is not enforced!</exception>
        Vector3 GetBoxLengths();

        /// <summary>
        /// Gets the periodic box vector recipricol.
        /// </summary>
        /// <returns>Vector3.</returns>
        /// <exception cref="System.Exception">Attempted to get periodic box vector when periodic booundary is not enforced!</exception>
        Vector3 GetBoxLengthsRecipricol();

        /// <summary>
        /// Sets the simulation box to a specific bounding box.
        /// </summary>
        /// <param name="box">The box.</param>
        void SetSimulationBox(BoundingBox box);

        /// <summary>
        /// Sets the simulation box according to lattice vectors a, b and c.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="centerAboutZero"></param>
        void SetSimulationBox(Vector3 a, Vector3 b, Vector3 c, bool centerAboutZero = true);

        /// <summary>
        /// Sets the periodic box vectors.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        void SetPeriodicBoxVectors(Vector3 a, Vector3 b, Vector3 c);

        /// <summary>
        /// Gets the periodic box vectors.
        /// </summary>
        /// <returns>Vector3[].</returns>
        Vector3[] GetPeriodicBoxVectors();

        /// <summary>
        /// Gets the periodic difference of a given vector.
        /// </summary>
        /// <param name="diff">The difference.</param>
        void GetPeriodicDifference(ref Vector3 diff);

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns></returns>
        ISimulationBoundary Clone();

        /// <summary>
        /// Update the bounding box based on current atomic system state.
        /// </summary>
        /// <remarks>
        /// This is for handling resizing boxes.
        /// @review Do we actually need the resizing box logic any more?
        /// </remarks>
        /// <param name="system"></param>
        void UpdateBoundingBox(IAtomicSystem system);
    }
}