﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the ^GPL. See License.txt in the project root for license information.

using System;

namespace Nano.Rainbow.Private.v1_0
{
    public class ApplicationKey
    {
        public DateTime Created { get; set; }

        public DateTime Expires { get; set; }

        public Guid Organization { get; set; }

        public string Token { get; set; }

        public string User { get; set; }
    }
}