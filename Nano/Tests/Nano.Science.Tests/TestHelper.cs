﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Nano.Loading;

namespace Nano.Science.Tests
{
    public class TestHelper
    {
        private static readonly Random Random = new Random();

        public readonly List<string> SimulationFiles = new List<string>();

        public IReporter Reporter = new Util.CmdReporter();

        public int Seed { get; }

        private string AssemblyLocation()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Uri codebase = new Uri(assembly.CodeBase);
            string path = codebase.LocalPath;

            return new FileInfo(path).DirectoryName + @"\";
        }

        public TestHelper(string simulationDirectoryPath = "^/Assets/Simulations")
        {
            //string workDir = Helper.ResolvePath(NUnit.Framework.TestContext.CurrentContext.WorkDirectory + "/");
            Helper.ApplicationRootPath = AssemblyLocation();
            //Helper.UseCurrentDirectoryForRoot = true;
            List<string> files = RecursiveFileSearch(Helper.ResolvePath(simulationDirectoryPath), "*.xml");
            SimulationFiles.Clear();
            SimulationFiles.AddRange(files);

            LoadManager.ScanAssembly(typeof(AssemblyRoot).Assembly);
            LoadManager.ScanAssembly(typeof(Nano.Science.Simulation.AssemblyRoot).Assembly);
            //LoadManager.ScanAssembly(typeof(Simbox.MD.AssemblyRoot).Assembly);

            Seed = DateTime.Now.Millisecond;
            MathUtilities.Rand = new Random(Seed);

            Reporter.PrintEmphasized("The seed used for this test was: {0} ", Seed);
        }

        private List<string> RecursiveFileSearch(string directory, string searchPattern)
        {
            List<string> results = new List<string>();
            foreach (string f in Directory.EnumerateFiles(directory, searchPattern))
            {
                results.Add(f);
            }
            foreach (string d in Directory.GetDirectories(Helper.ResolvePath(directory)))
            {
                /*
                foreach (string f in Directory.EnumerateFiles(d, searchPattern))
                {
                    results.Add(f);
                }
                */
                results.AddRange(RecursiveFileSearch(d, searchPattern));
            }

            return results;
        }

        public string GetRandomSimulationFile()
        {
            int index = Random.Next(SimulationFiles.Count);
            return SimulationFiles[index];
        }
    }
}