﻿using System;
using Rug.Osc;
using TestHelper.Util;

namespace Nano.Science.Simulation.Tests.Util
{
    internal static class Cmd
    {
        private static readonly object synclock = new object();

        public static bool ShowFullErrors { get; set; }

        static Cmd()
        {
            ShowFullErrors = true;
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        private static void Write_Inner(ConsoleColor color, string str)
        {
            ConsoleColor fore = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.Write(str);

            Console.ForegroundColor = fore;
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        private static void WriteLine_Inner(ConsoleColor color, string str)
        {
            ConsoleColor fore = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.WriteLine(str);

            Console.ForegroundColor = fore;
        }

        /// <summary>
        /// Write a 2 part item line to the console with colors.
        /// </summary>
        /// <param name="color1">Color to write part 1 with.</param>
        /// <param name="str1">String to write.</param>
        /// <param name="color2">Color to write part 2 with.</param>
        /// <param name="str2">String to write.</param>
        private static void WriteItem_Inner(ConsoleColor color1, string str1, ConsoleColor color2, string str2)
        {
            ConsoleColor fore = Console.ForegroundColor;

            Console.ForegroundColor = color1;
            Console.Write(str1);

            Console.ForegroundColor = color2;
            Console.WriteLine(str2);

            Console.ForegroundColor = fore;
        }

        private static void WriteItem_Inner(ConsoleColor color1, string str1, int max, ConsoleColor dotColor, ConsoleColor color2, string str2)
        {
            ConsoleColor fore = Console.ForegroundColor;

            Console.ForegroundColor = color1;
            Console.Write(str1);

            if (str1.Length < max)
            {
                Console.ForegroundColor = dotColor;
                Console.Write("".PadLeft(max - str1.Length, '.'));
            }

            Console.ForegroundColor = color1;
            Console.Write(": ");

            Console.ForegroundColor = color2;
            Console.WriteLine(str2);

            Console.ForegroundColor = fore;
        }

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        private static void WriteStackTrace_Inner(string trace)
        {
            if (String.IsNullOrEmpty(trace) == false)
            {
                ConsoleColor col = Console.ForegroundColor;

                Console.WriteLine();
                Console.ForegroundColor = Colors.ErrorDetail;
                Console.WriteLine(new string('=', 80));

                Console.ForegroundColor = Colors.Normal;
                Console.WriteLine("  " + trace.Replace("\n", "\n  "));
                Console.WriteLine();

                Console.ForegroundColor = Colors.ErrorDetail;
                Console.WriteLine(new string('=', 80));
                Console.WriteLine();

                Console.ForegroundColor = col;
            }
        }

        public static void WriteAndReset(ConsoleColor color, string str)
        {
            lock (synclock)
            {
                Write_Inner(color, str);
                Console.CursorLeft = 0;
            }
        }

        public static void WriteAndReset(ConsoleColor color1, string str1, ConsoleColor color2, string str2)
        {
            lock (synclock)
            {
                Write_Inner(color1, str1);
                Write_Inner(color2, str2);
                Console.CursorLeft = 0;
            }
        }

        public static void WriteLine()
        {
            lock (synclock)
            {
                Console.WriteLine();
            }
        }

        public static void ClearLine(int index)
        {
            lock (synclock)
            {
                int backupX = Console.CursorLeft, backupY = Console.CursorTop;

                Console.SetCursorPosition(0, index);

                Console.Write(new string(' ', Console.BufferWidth));

                Console.SetCursorPosition(backupX, backupY);
            }
        }

        public static void ClearLineAndMoveToStart(int index)
        {
            lock (synclock)
            {
                Console.SetCursorPosition(0, index);

                Console.Write(new string(' ', Console.BufferWidth));

                Console.SetCursorPosition(0, index);
            }
        }

        public static void Write(ConsoleColor color, string str)
        {
            lock (synclock)
            {
                Write_Inner(color, str);
            }
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        public static void WriteLine(ConsoleColor color, string str)
        {
            lock (synclock)
            {
                WriteLine_Inner(color, str);
            }
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        /// <param name="maxLength">The maximum length.</param>
        public static void WriteLine(ConsoleColor color, string str, int maxLength)
        {
            lock (synclock)
            {
                WriteLine_Inner(color, MaxLengthPadded(str, maxLength, ' ', " .."));
            }
        }

        /// <summary>
        /// Write a 2 part item line to the console with colors.
        /// </summary>
        /// <param name="color1">Color to write part 1 with.</param>
        /// <param name="str1">String to write.</param>
        /// <param name="color2">Color to write part 2 with.</param>
        /// <param name="str2">String to write.</param>
        public static void WriteItem(ConsoleColor color1, string str1, ConsoleColor color2, string str2)
        {
            lock (synclock)
            {
                WriteItem_Inner(color1, str1, color2, str2);
            }
        }

        public static void WriteItem(ConsoleColor color1, string str1, int max, ConsoleColor dotColor, ConsoleColor color2, string str2)
        {
            lock (synclock)
            {
                WriteItem_Inner(color1, str1, max, dotColor, color2, str2);
            }
        }

        public static void WriteInfo(string message)
        {
            lock (synclock)
            {
                WriteLine_Inner(Colors.Success, message);
            }
        }

        public static void WriteInfo(string message, string detail)
        {
            lock (synclock)
            {
                if (ShowFullErrors == true &&
                    String.IsNullOrEmpty(detail) == false)
                {
                    Write_Inner(Colors.Success, message);
                    WriteLine_Inner(Colors.Normal, " (" + detail + ")");
                }
                else
                {
                    WriteLine_Inner(Colors.Success, message);
                }
            }
        }

        public static void WriteError(string message)
        {
            lock (synclock)
            {
                WriteLine_Inner(Colors.Error, message);
            }
        }

        internal static void WriteError(string message, string detail)
        {
            lock (synclock)
            {
                if (ShowFullErrors == true &&
                    String.IsNullOrEmpty(detail) == false)
                {
                    Write_Inner(Colors.Error, message);
                    WriteLine_Inner(Colors.ErrorDetail, " (" + detail + ")");
                }
                else
                {
                    WriteLine_Inner(Colors.Error, message);
                }
            }
        }

        public static void WriteException(Exception ex)
        {
            if (ShowFullErrors == true)
            {
                lock (synclock)
                {
                    WriteException_Inner(ex);
                }
            }
            else
            {
                WriteError(ex.Message);
            }
        }

        public static void WriteException(string message, Exception ex)
        {
            lock (synclock)
            {
                WriteLine_Inner(Colors.Error, message);

                if (ShowFullErrors == true)
                {
                    WriteException_Inner(ex);
                }
                else
                {
                    WriteLine_Inner(Colors.ErrorDetail, ex.Message);
                }
            }
        }

        /// <summary>
        /// Write a exception to the console.
        /// </summary>
        /// <param name="ex">exception object</param>
        private static void WriteException_Inner(Exception ex)
        {
            WriteLine_Inner(Colors.Error, ex.Message);

            WriteStackTrace_Inner(ex.StackTrace);
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, string message)
        {
            WriteMessage(direction, timeTag, Colors.Message, message, -1);
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, ConsoleColor messageColor, string message)
        {
            WriteMessage(direction, timeTag, messageColor, message, -1);
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, string message, int maxLength)
        {
            WriteMessage(direction, timeTag, Colors.Message, message, maxLength);
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, ConsoleColor messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (synclock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Write_Inner(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        Write_Inner(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        Write_Inner(Colors.Action, "   ");
                        length += 3;
                        break;

                    default:
                        break;
                }

                if (timeTag != null)
                {
                    string timeTagString = timeTag.ToString() + " ";
                    Write_Inner(Colors.Normal, timeTagString);

                    length += timeTagString.Length;
                }

                WriteLine_Inner(messageColor, maxLength > 0 ? MaxLength(message, maxLength - length) : message);
            }
        }

        public static void WriteMessage(Direction direction, ConsoleColor prefixColor, string prefix, ConsoleColor messageColor, string message)
        {
            WriteMessage(direction, prefixColor, prefix, messageColor, message, -1);
        }

        public static void WriteMessage(Direction direction, ConsoleColor prefixColor, string prefix, ConsoleColor messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (synclock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Write_Inner(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        Write_Inner(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        Write_Inner(Colors.Action, "   ");
                        length += 3;
                        break;

                    default:
                        break;
                }

                if (string.IsNullOrEmpty(prefix) == false)
                {
                    Write_Inner(prefixColor, prefix + " ");

                    length += prefix.Length + 1;
                }

                WriteLine_Inner(messageColor, maxLength > 0 ? MaxLengthPadded(message, maxLength - length, ' ', " ..") : message);
            }
        }

        public static string MaxLength(string str, int max)
        {
            if (str.Length > max)
            {
                if (max - 3 > str.Length)
                {
                    return "...";
                }
                else
                {
                    return str.Substring(0, max - 3) + "...";
                }
            }
            else
            {
                return str;
            }
        }

        public static string GetMemStringFromBytes(long bytes, bool space)
        {
            decimal num = bytes / 1024M;
            string str = "KB";
            while (num >= 1000M)
            {
                if (str == "KB")
                {
                    num /= 1024M;
                    str = "MB";
                }
                else
                {
                    if (str == "MB")
                    {
                        num /= 1024M;
                        str = "GB";
                        continue;
                    }
                    if (str == "GB")
                    {
                        num /= 1024M;
                        str = "TB";
                        continue;
                    }
                    if (str == "TB")
                    {
                        num /= 1024M;
                        str = "PB";
                        continue;
                    }
                    if (str == "PB")
                    {
                        num /= 1024M;
                        str = "XB";
                        continue;
                    }
                    if (str == "XB")
                    {
                        num /= 1024M;
                        str = "ZB";
                        continue;
                    }
                    if (str == "ZB")
                    {
                        num /= 1024M;
                        str = "YB";
                        continue;
                    }
                    if (str == "YB")
                    {
                        num /= 1024M;
                        str = "??";
                        continue;
                    }
                    num /= 1024M;
                }
            }
            return (num.ToString("N2") + (space ? (" " + str) : str));
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength)
        {
            return GetMemStringFromBytes(bytes, false).PadLeft(maxLength, ' ');
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength, bool space)
        {
            return GetMemStringFromBytes(bytes, space).PadLeft(maxLength, ' ');
        }

        public static string MaxLengthLeftPadded(string str, int totalWidth, char paddingChar, string appendIfCut)
        {
            if (str.Length > totalWidth)
            {
                return (str.Substring(0, totalWidth - appendIfCut.Length) + appendIfCut);
            }
            return str.PadLeft(totalWidth, paddingChar);
        }

        public static string MaxLengthPadded(string str, int totalWidth, char paddingChar, string appendIfCut)
        {
            if (str.Length > totalWidth)
            {
                return (str.Substring(0, totalWidth - appendIfCut.Length) + appendIfCut);
            }
            return str.PadRight(totalWidth, paddingChar);
        }
    }
}