// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.IO;
using Rug.Cmd;
using Rug.Osc;

namespace Nano.Console.Utils
{
    public static class RugCmd
    {
        private static readonly object SyncLock = new object();
        
        public static bool InsetActions { get; set; } = true; 
        
        public static bool CursorVisible
        {
            set
            {
                try
                {
                    System.Console.CursorVisible = value;
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch { }
            }
        }

        public static bool ShowFullErrors { get; set; }

        static RugCmd()
        {
            ShowFullErrors = true;
        }

        public static IReporter CreateDefaultConsoleContext()
        {
            RC.Sys = ConsoleExt.SystemConsole;
            RC.App = RC.Sys;
            //RC.Theme = Rug.Cmd.Colors.ConsoleColorTheme.Load(System.Console.ForegroundColor, System.Console.BackgroundColor, ConsoleColorDefaultThemes.Colorful);
            RC.Theme = Rug.Cmd.Colors.ConsoleColorTheme.Load(
                (ConsoleColor) (Math.Min((int) ConsoleColor.White,
                        Math.Max((int) ConsoleColor.Black,
                            (int) System.Console.BackgroundColor
                        )
                    )
                ), GetThemeStream());

            Colors.Heading = (ConsoleColor)RC.Theme[ConsoleThemeColor.TitleText];
            Colors.Emphasized = (ConsoleColor)RC.Theme[ConsoleThemeColor.TitleText2];
            Colors.UserInput = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text];
            Colors.Ident = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text2];
            Colors.Normal = (ConsoleColor)RC.Theme[ConsoleThemeColor.TitleText];
            Colors.Success = (ConsoleColor)RC.Theme[ConsoleThemeColor.TextGood];
            Colors.Error = (ConsoleColor)RC.Theme[ConsoleThemeColor.ErrorColor1];

            Colors.ErrorDetail = (ConsoleColor)RC.Theme[ConsoleThemeColor.ErrorColor2];

            Colors.Transmit = (ConsoleColor)RC.Theme[ConsoleThemeColor.SubTextGood];
            Colors.Receive = (ConsoleColor)RC.Theme[ConsoleThemeColor.SubTextBad];
            Colors.Message = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text];
            Colors.Action = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text3];
            Colors.Debug = (ConsoleColor)RC.Theme[ConsoleThemeColor.PromptColor1];
            Colors.Warning = (ConsoleColor)RC.Theme[ConsoleThemeColor.WarningColor1];

            RugCmdReporter reporter = new RugCmdReporter();

            return reporter;
        }

        public static string GetMemStringFromBytes(long bytes, bool space)
        {
            decimal num = bytes / 1024M;
            string str = "KB";
            while (num >= 1000M)
            {
                switch (str)
                {
                    case "KB":
                        num /= 1024M;
                        str = "MB";
                        continue;
                    case "MB":
                        num /= 1024M;
                        str = "GB";
                        continue;
                    case "GB":
                        num /= 1024M;
                        str = "TB";
                        continue;
                    case "TB":
                        num /= 1024M;
                        str = "PB";
                        continue;
                    case "PB":
                        num /= 1024M;
                        str = "XB";
                        continue;
                    case "XB":
                        num /= 1024M;
                        str = "ZB";
                        continue;
                    case "ZB":
                        num /= 1024M;
                        str = "YB";
                        continue;
                    case "YB":
                        num /= 1024M;
                        str = "??";
                        continue;
                }
                num /= 1024M;
            }
            return (num.ToString("N2") + (space ? (" " + str) : str));
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength)
        {
            return GetMemStringFromBytes(bytes, false).PadLeft(maxLength, ' ');
        }

        public static string GetMemStringFromBytes(long bytes, int maxLength, bool space)
        {
            return GetMemStringFromBytes(bytes, space).PadLeft(maxLength, ' ');
        }

        public static string MaxLength(string str, int max)
        {
            if (str.Length > max)
            {
                if (max - 3 > str.Length)
                {
                    return "...";
                }
                else
                {
                    return str.Substring(0, max - 3) + "...";
                }
            }
            else
            {
                return str;
            }
        }

        public static string MaxLengthPadded(string str, int totalWidth, char paddingChar, string appendIfCut)
        {
            if (str.Length > totalWidth)
            {
                return (str.Substring(0, totalWidth - appendIfCut.Length) + appendIfCut);
            }
            return str.PadRight(totalWidth, paddingChar);
        }

        public static void WriteError(string message)
        {
            lock (SyncLock)
            {
                WriteLine_Inner(Colors.Error, message);
            }
        }

        public static void WriteException(string message, Exception ex)
        {
            lock (SyncLock)
            {
                WriteLine_Inner(Colors.Error, message);

                if (ShowFullErrors == true)
                {
                    WriteException_Inner(ex);
                }
                else
                {
                    WriteLine_Inner(Colors.ErrorDetail, ex.Message);
                }
            }
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        public static void WriteLine(ConsoleColor color, string str)
        {
            lock (SyncLock)
            {
                WriteLine_Inner(color, str);
            }
        }

        public static void WriteMessage(Direction direction, OscTimeTag? timeTag, ConsoleColor messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (SyncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Write_Inner(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        Write_Inner(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        if (InsetActions)
                        {
                            Write_Inner(Colors.Action, "   ");
                            length += 3;
                        }
                        break;
                }

                if (timeTag != null)
                {
                    string timeTagString = timeTag.ToString() + " ";
                    Write_Inner(Colors.Normal, timeTagString);

                    length += timeTagString.Length;
                }

                WriteLine_Inner(messageColor, maxLength > 0 ? MaxLength(message, maxLength - length) : message);
            }
        }

        public static void WriteMessage(Direction direction, ConsoleColor prefixColor, string prefix, ConsoleColor messageColor, string message)
        {
            WriteMessage(direction, prefixColor, prefix, messageColor, message, -1);
        }

        public static void WriteMessage(Direction direction, ConsoleColor prefixColor, string prefix, ConsoleColor messageColor, string message, int maxLength)
        {
            int length = 0;
            lock (SyncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Write_Inner(Colors.Transmit, "TX ");
                        length += 3;
                        break;

                    case Direction.Receive:
                        Write_Inner(Colors.Receive, "RX ");
                        length += 3;
                        break;

                    case Direction.Action:
                        if (InsetActions)
                        {
                            Write_Inner(Colors.Action, "   ");
                            length += 3;
                        }
                        break;
                }

                if (string.IsNullOrEmpty(prefix) == false)
                {
                    Write_Inner(prefixColor, prefix + " ");

                    length += prefix.Length + 1;
                }

                WriteLine_Inner(messageColor, maxLength > 0 ? MaxLengthPadded(message, maxLength - length, ' ', " ..") : message);
            }
        }

        public static MemoryStream GetThemeStream()
        {
            return new MemoryStream(Convert.FromBase64String(
                @"AA8NCw4HBQMGCAUDBgwEDgYPCQoCDgYMBAgPDw8HAAABDgoNCw8KDQsHCg0LDAwODg8LCgoODgwM
CA4ODw8AAAIOCwEKDwsBCg8LAQoMDA4ODwkKCg4ODAwDDw4PBwAAAwEBAQEPBQ4LBwUOCwQEDg4P
DwoKDw8EBAEPDg8HAAAEDg4ODg8NCwIHDQsCDAwODg8PCgIPDwwMAA8ODwYAAAUKCgoKDw0LDg4N
Cw4MDA4ODw8KCg8PDAwNDw4ACQAABg4ODg4PBAoLAQQKCwQEDg4PDwoKDw8EBAEPDg8OAAAHAwMD
Aw8CBgkAAgYJDAQODgMPAgIOBgwEAw8ADwAAAAgPDw8PBwULCgAFCwoEBA4OCw8KCg4OBAQACA8P
CAAACQ8PDw8ICgsNDgoLDQwMDg4PDwoKCAgMDAgPDg8OAAAKAwMDAwEFBA4CBQQODAwGBgEDAwMO
BgwMDwIBAgMAAAsNDQ0NBQYCCQ8GAgkEBA4ODw8CAgUFDAQFDQ8PDQAADAQPDw8HAQoLDgEKCw4E
DgQPDwoKBwcOBAQODw4HAAANBQsLAw8FDgsDBQ4LBAQODg8PCgoPDwQEBQMLDwMAAA4BAAAABgID
DQkCAw0EBAYGCQECAgYGDAQBDg4PDgAADwIAAAAIBQMBBgUDAQwEBgYBAQICCAgMBAIPDw8OAAA="
            ));
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        private static void Write_Inner(ConsoleColor color, string str)
        {
            RC.Write((ConsoleColorExt)color, str);
        }

        /// <summary>
        /// Write a exception to the console.
        /// </summary>
        /// <param name="ex">exception object</param>
        private static void WriteException_Inner(Exception ex)
        {
            WriteLine_Inner(Colors.Error, ex.Message);

            WriteStackTrace_Inner(ex.StackTrace);
        }

        /// <summary>
        /// Write a line to the console with colors.
        /// </summary>
        /// <param name="color">Color to write with.</param>
        /// <param name="str">String to write.</param>
        private static void WriteLine_Inner(ConsoleColor color, string str)
        {
            RC.WriteLine((ConsoleColorExt)color, str);
        }

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        private static void WriteStackTrace_Inner(string trace)
        {
            if (String.IsNullOrEmpty(trace) == false)
            {
                RC.WriteLine();
                RC.WriteLine((ConsoleColorExt)Colors.ErrorDetail, new string('=', 80));
                RC.WriteLine((ConsoleColorExt)Colors.Normal, "  " + trace.Replace("\n", "\n  "));
                RC.WriteLine();

                RC.WriteLine((ConsoleColorExt)Colors.ErrorDetail, new string('=', 80));
                RC.WriteLine();
            }
        }
    }
}