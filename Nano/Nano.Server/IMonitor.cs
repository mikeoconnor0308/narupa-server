﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Threading;
using System.Threading.Tasks;

namespace Nano.Server
{
    public interface IMonitor : IServerComponent
    {
        Task Start(IServer serverContext, CancellationToken cancel);
    }
}