﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Nano.Transport.Comms;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Nano.Server
{
    [XmlName("ServerConfig")]
    public class ServerConfig : ILoadable
    {
        public readonly List<ServerInstanceInfo> ServerInfos = new List<ServerInstanceInfo>();

        private const string configFile = "^/ServerConfig.xml";

        public ServerConfig()
        {
        }

        public void Load(IReporter reporter)
        {
            Load(reporter, configFile);
        }

        public void Load(IReporter reporter, string configFile)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(Helper.ResolvePath(configFile));

            LoadContext loadContext = new LoadContext(reporter);

            Load(loadContext, xmlDocument.DocumentElement);

            if (loadContext.Errors.Count > 0)
            {
                throw new Exception($"{loadContext.Errors.Count} errors while loading server config file \"{configFile}\".");
            }
        }

        public void Load(LoadContext context, XmlNode node)
        {
            ServerInfos.Clear();
            ServerInfos.AddRange(Loader.LoadObjects<ServerInstanceInfo>(context, node));
        }

        public void Save(IReporter reporter)
        {
            Save(reporter, configFile);
        }

        public void Save(IReporter reporter, string configFile)
        {
            XmlDocument xmlDocument = new XmlDocument();

            LoadContext loadContext = new LoadContext(reporter);

            XmlElement node = Helper.CreateElement(xmlDocument, "Config");

            XmlElement simulationNode = Loader.SaveObject(loadContext, node, this);

            xmlDocument.AppendChild(simulationNode);

            if (loadContext.Errors.Count > 0)
            {
                throw new Exception($"{loadContext.Errors.Count} errors while saving server config file \"{configFile}\".");
            }

            xmlDocument.Save(Helper.ResolvePath(configFile));
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Loader.SaveObjects<ServerInstanceInfo>(context, element, ServerInfos);

            return element;
        }
    }

    [XmlName("Server")]
    public class ServerInstanceInfo : ILoadable
    {
        public List<string> Options = new List<string>();
        public bool CompressStreams { get; set; }
        public ConnectionType ConnectionType { get; set; }
        public string FilePath { get; set; }
        public string Name { get; set; }
        public string Namespace { get; set; }
        public ServerOnConnectionState OnConnectionState { get; set; }
        public ServiceDiscoveryType ServiceDiscoveryType { get; set; }

        public ServerInstanceInfo Clone()
        {
            return new ServerInstanceInfo()
            {
                Namespace = Namespace,

                Name = Name,

                FilePath = FilePath,

                OnConnectionState = OnConnectionState,
            };
        }

        public bool IsOptionDefined(string name)
        {
            return Options.Contains(name);
        }

        public void Load(LoadContext context, XmlNode node)
        {
            Namespace = Helper.GetAttributeValue(node, nameof(Namespace), null);
            Name = Helper.GetAttributeValue(node, nameof(Name), string.Empty);
            FilePath = Helper.GetAttributeValue(node, nameof(FilePath), string.Empty);
            OnConnectionState = Helper.GetAttributeValue(node, nameof(OnConnectionState), ServerOnConnectionState.Play);
            CompressStreams = Helper.GetAttributeValue(node, nameof(CompressStreams), true);
            ConnectionType = Helper.GetAttributeValue(node, nameof(ConnectionType), ConnectionType.Tcp);
            ServiceDiscoveryType = Helper.GetAttributeValue(node, nameof(ServiceDiscoveryType), ServiceDiscoveryType.None);

            if (string.IsNullOrEmpty(Name) == true || Name.Trim().Length == 0)
            {
                context.Error($"Server node without \"{nameof(Name)}\".");
            }

            if (string.IsNullOrEmpty(FilePath) == true || Name.Trim().Length == 0)
            {
                context.Error($"Server node without \"{nameof(FilePath)}\".");
            }

            Options.Clear();

            string options = Helper.GetAttributeValue(node, nameof(Options), string.Empty);

            if (string.IsNullOrEmpty(options) == false)
            {
                foreach (string option in options.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    Options.Add(option.Trim());
                }
            }
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Namespace), Namespace);
            Helper.AppendAttributeAndValue(element, nameof(Name), Name);
            Helper.AppendAttributeAndValue(element, nameof(ConnectionType), ConnectionType);
            Helper.AppendAttributeAndValue(element, nameof(FilePath), FilePath);
            Helper.AppendAttributeAndValue(element, nameof(OnConnectionState), OnConnectionState);
            Helper.AppendAttributeAndValue(element, nameof(ServiceDiscoveryType), ServiceDiscoveryType);

            if (CompressStreams != true)
            {
                Helper.AppendAttributeAndValue(element, nameof(CompressStreams), CompressStreams);
            }

            if (Options.Count > 0)
            {
                Helper.AppendAttributeAndValue(element, nameof(Options), string.Join(", ", Options.ToArray()));
            }

            return element;
        }

        public void SetOption(string name, bool value)
        {
            Options.Remove(name);

            if (value == false)
            {
                return;
            }

            Options.Add(name);
        }
    }
}