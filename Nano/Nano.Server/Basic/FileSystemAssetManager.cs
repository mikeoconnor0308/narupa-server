﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using Nano.Loading;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("FileSystemAssetManager")] 
    public class FileSystemAssetManager : IAssetManager
    {
        public string Folder { get; set; }         

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Folder), Folder); 

            return element; 
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            Folder = Helper.GetAttributeValue(node, nameof(Folder), "^/Assets"); 
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {

        }

        /// <inheritdoc />
        public void ValidateArguments(IReporter reporter)
        {
            reporter.PrintDetail(Direction.Action, "assets folder", Helper.ResolvePath(Folder)); 
        }

        /// <inheritdoc />
        public async Task<Stream> GetAsset(Uri assetUri, IReporter reporter = null)
        {
            FileInfo file = new FileInfo(Path.Combine(Helper.ResolvePath(Folder), assetUri.OriginalString.TrimStart('/', '\\')));

            if (file.Exists == false)
            {
                throw new FileNotFoundException($"Asset file could not be found. {file}", file.ToString()); 
            }

            return file.OpenRead(); 
        }

        /// <inheritdoc />
        public async Task<bool> AssetExists(Uri assetUri, IReporter reporter = null)
        {
            try
            {
                FileInfo file = new FileInfo(Path.Combine(Helper.ResolvePath(Folder), assetUri.ToString().TrimStart('/', '\\')));

                return file.Exists;
            }
            catch
            {
                return false; 
            }
        }
    }
}