﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nano.Server.Basic
{
    /// <summary>
    /// A set in which the equality operator is based on element-wise comparison.
    /// </summary>
    public class EquatableSet : HashSet<int>, IEquatable<EquatableSet>
    {
        /// <inheritdoc />
        public bool Equals(EquatableSet other)
        {
            if (other == null) return false;
            return SetEquals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.Sum(index => index.GetHashCode());
        }

        /// <inheritdoc />
        public EquatableSet()
        {
        }

        /// <inheritdoc />
        public EquatableSet(ICollection<int> hashSet)
        {
            Clear();
            UnionWith(hashSet);
        }
    }
}