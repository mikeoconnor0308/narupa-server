﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Xml;
using Nano.Loading;

namespace Nano.Server.Basic
{
    // <StreamOptions Type="StreamOptions" VRServer="true" />

    [XmlName("StreamOptions")]
    public class StreamOptions : ILoadable
    {
        public bool CompressStreams { get; set; } = true;
        public bool KinectServer { get; set; } = false; 

        public int MaxNumberOfParticles { get; set; } = 40000;
        public bool NoCollisions { get; set; } = true; 

        public bool NoForces { get; set; } = true; 
        public bool NoSelectedAtoms { get; set; } 
        public bool NoVelocities { get; set; } = true; 
        public bool VRServer { get; set; } = true; 

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            MaxNumberOfParticles = Helper.GetAttributeValue(node, nameof(MaxNumberOfParticles), MaxNumberOfParticles);

            CompressStreams = Helper.GetAttributeValue(node, nameof(CompressStreams), CompressStreams);
            NoForces = Helper.GetAttributeValue(node, nameof(NoForces), NoForces);
            NoSelectedAtoms = Helper.GetAttributeValue(node, nameof(NoSelectedAtoms), NoSelectedAtoms);
            NoCollisions = Helper.GetAttributeValue(node, nameof(NoCollisions), NoCollisions);
            NoVelocities = Helper.GetAttributeValue(node, nameof(NoVelocities), NoVelocities);
            VRServer = Helper.GetAttributeValue(node, nameof(VRServer), VRServer);
            KinectServer = Helper.GetAttributeValue(node, nameof(KinectServer), KinectServer);
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(MaxNumberOfParticles), MaxNumberOfParticles);

            Helper.AppendAttributeAndValue(element, nameof(CompressStreams), CompressStreams);
            Helper.AppendAttributeAndValue(element, nameof(NoForces), NoForces);
            Helper.AppendAttributeAndValue(element, nameof(NoSelectedAtoms), NoSelectedAtoms);
            Helper.AppendAttributeAndValue(element, nameof(NoCollisions), NoCollisions);
            Helper.AppendAttributeAndValue(element, nameof(NoVelocities), NoVelocities);
            Helper.AppendAttributeAndValue(element, nameof(VRServer), VRServer);
            Helper.AppendAttributeAndValue(element, nameof(KinectServer), KinectServer);

            return element;
        }
    }
}