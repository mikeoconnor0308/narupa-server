﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using Rug.Osc;
using System;
using System.IO;
using System.Net;

namespace Nano.Client
{
    public class TrajectoryRecorder : ITransportPacketReceiver, IDisposable
    {
        private readonly object syncLock = new object();
        private readonly ITransportPacketReceiver transportPacketReceiver;
        private Stream stream;
        private BinaryWriter writer;

        public TrajectoryRecorder(ITransportPacketReceiver transportPacketReceiver, Stream stream)
        {
            this.transportPacketReceiver = transportPacketReceiver;

            this.stream = stream;

            writer = new BinaryWriter(stream);
        }

        public void Dispose()
        {
            lock (syncLock)
            {
                writer?.Close();
                writer = null;
                stream?.Dispose();
                stream = null;
            }
        }

        public void OnDataPacket(IPEndPoint origin, ushort id, byte[] data, int index, int length)
        {            
            transportPacketReceiver?.OnDataPacket(origin, id, data, index, length);
            
            lock (syncLock)
            {
                writer?.Write(data, index, length);
                //writer?.Flush();
                //stream?.Flush();
            }            
        }

        public void OnOscPacket(IPEndPoint origin, OscBundle bundle)
        {            
            transportPacketReceiver?.OnOscPacket(origin, bundle);
            
            lock (syncLock)
            {
                writer?.Write((ushort)TransportPacketType.OscBundle);

                byte[] bundleBytes = bundle.ToByteArray();

                writer?.Write(bundleBytes.Length);
                writer?.Write(bundleBytes);
                //writer?.Flush();
                //stream?.Flush();
            }            
        }
    }
}