﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Rug.Osc.Ahoy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Nano.Client
{
    public class SearchForConnections : IDisposable, IEnumerable<SimboxConnectionInfo>
    {
        public readonly int SendInterval;

        public readonly int Timeout;

        private readonly List<SimboxConnectionInfo> autoConnectionInfoList = new List<SimboxConnectionInfo>();

        private readonly object autoConnectionInfoListSyncLock = new object();

        private readonly List<IAhoyQuery> queriesList = new List<IAhoyQuery>();

        private readonly object queriesListSyncLock = new object();

        private DateTime startTime;

        public event Action<SimboxConnectionInfo> DeviceDiscovered;

        public event Action<SimboxConnectionInfo> DeviceExpired;

        public int Count => autoConnectionInfoList.Count;

        public SimboxConnectionInfo this[int index] => autoConnectionInfoList[index];

        public SearchForConnections(int timeout = 0, int sendInterval = 1000)
        {
            Timeout = timeout;
            SendInterval = sendInterval;
        }

        public static SimboxConnectionInfo[] EnumerateConnections(int timeout = 1000, int interval = 300)
        {
            using (SearchForConnections autoConnector2 = new SearchForConnections(timeout, interval))
            {
                autoConnector2.Search();

                autoConnector2.Sort();

                return autoConnector2.ToArray();
            }
        }

        public void BeginEndSearch()
        {
            lock (queriesListSyncLock)
            {
                BeginEndSearchUdp();
            }

            startTime = DateTime.MinValue;
        }

        public void BeginSearch()
        {
            EndSearch();

            startTime = DateTime.Now;

            lock (autoConnectionInfoListSyncLock)
            {
                autoConnectionInfoList.Clear();
            }

            lock (queriesListSyncLock)
            {
                BeginSearchUdp();
            }
        }

        public void Dispose()
        {
            EndSearch();
        }

        public void EndSearch()
        {
            lock (queriesListSyncLock)
            {
                EndSearchUdp();
            }

            startTime = DateTime.MinValue;
        }

        public IEnumerator<SimboxConnectionInfo> GetEnumerator()
        {
            return autoConnectionInfoList.GetEnumerator();
        }

        public int IndexOf(SimboxConnectionInfo info)
        {
            lock (autoConnectionInfoListSyncLock)
            {
                return autoConnectionInfoList.IndexOf(info);
            }
        }

        public void Search()
        {
            if (Timeout <= 0)
            {
                throw new Exception("Cannot search with an infinite timeout.");
            }

            BeginSearch();

            WaitForCompletion();
        }

        public void Sort()
        {
            lock (autoConnectionInfoListSyncLock)
            {
                autoConnectionInfoList.Sort();
            }
        }

        public SimboxConnectionInfo[] ToArray()
        {
            lock (autoConnectionInfoListSyncLock)
            {
                return autoConnectionInfoList.ToArray();
            }
        }

        public void WaitForCompletion()
        {
            if (Timeout <= 0)
            {
                throw new Exception("Cannot wait with an infinite timeout.");
            }

            try
            {
                DateTime endTime = startTime.AddMilliseconds(Timeout);

                double timeDelta = (endTime - startTime).TotalSeconds;

                int waitInterval = (int)(timeDelta * 1000d);

                if (waitInterval > 0)
                {
                    Thread.CurrentThread.Join(waitInterval);
                }
            }
            finally
            {
                EndSearch();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (autoConnectionInfoList as IEnumerable).GetEnumerator();
        }

        private void BeginEndSearchUdp()
        {
            foreach (IAhoyQuery query in queriesList)
            {
                query.BeginEndSearch();
            }

            queriesList.Clear();
        }

        private void BeginSearchUdp()
        {
            queriesList.Add(AhoyQuery.CreateQuery());

            foreach (IAhoyQuery query in queriesList)
            {
                query.ServiceDiscovered += delegate (AhoyServiceInfo serviceInfo)
                {
                    NetworkConnectionInfo info = new NetworkConnectionInfo(
                        serviceInfo.Descriptor,
                        serviceInfo.NetworkAdapterIPAddress,
                        serviceInfo.Address,
                        serviceInfo.SendPort);

                    lock (autoConnectionInfoListSyncLock)
                    {
                        autoConnectionInfoList.Add(info);
                    }

                    DeviceDiscovered?.Invoke(info);
                };

                query.ServiceExpired += delegate (AhoyServiceInfo serviceInfo)
                {
                    NetworkConnectionInfo info = new NetworkConnectionInfo(
                        serviceInfo.Descriptor,
                        serviceInfo.NetworkAdapterIPAddress,
                        serviceInfo.Address,
                        serviceInfo.SendPort);

                    lock (autoConnectionInfoListSyncLock)
                    {
                        autoConnectionInfoList.Remove(info);
                    }

                    DeviceExpired?.Invoke(info);
                };

                query.BeginSearch(SendInterval);
            }
        }

        private void EndSearchUdp()
        {
            foreach (IAhoyQuery query in queriesList)
            {
                query.EndSearch();
            }

            queriesList.Clear();
        }
    }
}