﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Client.Console.Simbox
{
    public class FrameHistory<TFrame>
    {
        public FrameHistory(int limit = 4096)
        {
            Resize(limit);
        }

        public void Resize(int limit)
        {
            buffer = new TFrame[limit];

            Reset();
        }

        private TFrame[] buffer;

        private int bufferHead;
        private int bufferTail;

        public int DiscardFramesCount { get; private set; }
        public int FirstFrame { get; private set; }
        public int FinalFrame { get; private set; }

        public int Count
        {
            get
            {
                if (bufferTail == -1)
                {
                    return 0;
                }

                if (bufferHead <= bufferTail)
                {
                    return bufferTail - bufferHead + 1;
                }
                else
                {
                    return bufferTail + 1
                           + buffer.Length - bufferHead;
                }
            }
        }

        public bool Full
        {
            get
            {
                return Count == buffer.Length;
            }
        }

        public TFrame this[int frame]
        {
            get
            {
                int index = (bufferHead + frame - FirstFrame) % buffer.Length;

                return buffer[index];
            }
        }

        public void Reset()
        {
            FirstFrame = -1;
            FinalFrame = -1;

            bufferHead = 0;
            bufferTail = -1;

            DiscardFramesCount = 0;
        }

        public void AddNewestFrame(TFrame frame)
        {
            int next;

            if (FirstFrame == -1)
            {
                FirstFrame = 0;
                next = 0;
            }
            else
            {
                next = (bufferTail + 1) % buffer.Length;

                if (next == bufferHead)
                {
                    bufferHead = (bufferHead + 1) % buffer.Length;
                    FirstFrame += 1;

                    DiscardFramesCount += 1;
                }
            }

            buffer[next] = frame;
            bufferTail = next;
            FinalFrame += 1;
        }

        public override string ToString()
        {
            return string.Format("FrameHistory: {0}-{1} ({2} frames)", FirstFrame, FinalFrame, Count);
        }
    }
}