﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Variables;
using SlimMath;
using System;

namespace Nano.Client.Console.Simbox
{
    public class NsbFrame
    {
        /// <summary>
        /// Map from atom indices in frontend-frame to the underlying absolute
        /// atom indices in the simulation.
        /// </summary>
        public int[] VisibleAtomMap;

        public long FrameNumber;
        public int AtomCount;
        public int BondCount;

        public ushort[] AtomTypes;
        public Vector3[] AtomPositions;
        public Vector3[] AtomVelocities;

        public BondPair[] BondPairs;

        public ushort[] SelectedAtomIndicies;
        public ushort[] CollidedAtomIndicies;

        public VRInteraction[] VRInteractions;

        public static void Clear(NsbFrame frame)
        {
            frame.AtomCount = 0;
            frame.BondCount = 0;

            frame.AtomTypes = null;
            frame.AtomPositions = null;
            frame.BondPairs = null;

            frame.SelectedAtomIndicies = null;
            frame.VRInteractions = null;
        }

        public static void Interpolate(NsbFrame prevFrame,
            NsbFrame nextFrame,
            NsbFrame resultFrame,
            float u)
        {
            resultFrame.VisibleAtomMap = prevFrame.VisibleAtomMap;

            resultFrame.AtomCount = Math.Min(prevFrame.AtomCount, nextFrame.AtomCount);
            resultFrame.BondCount = Math.Min(prevFrame.BondCount, nextFrame.BondCount);

            resultFrame.AtomTypes = prevFrame.AtomTypes;
            resultFrame.BondPairs = prevFrame.BondPairs;
            resultFrame.VisibleAtomMap = prevFrame.VisibleAtomMap;

            resultFrame.SelectedAtomIndicies = prevFrame.SelectedAtomIndicies;

            resultFrame.SelectedAtomIndicies = prevFrame.SelectedAtomIndicies;
            if (resultFrame.AtomPositions == null
                || resultFrame.AtomPositions.Length < resultFrame.AtomCount)
            {
                resultFrame.AtomPositions = new Vector3[resultFrame.AtomCount];
            }

            int velocityCount = Math.Min(prevFrame.AtomVelocities.Length,
                nextFrame.AtomVelocities.Length);

            if (resultFrame.AtomVelocities == null
                || resultFrame.AtomVelocities.Length < resultFrame.AtomCount)
            {
                resultFrame.AtomVelocities = new Vector3[resultFrame.AtomCount];
            }

            for (int id = 0; id < resultFrame.AtomCount; ++id)
            {
                Vector3 prevPos = prevFrame.AtomPositions[id];
                Vector3 nextPos = nextFrame.AtomPositions[id];

                nextPos.X = prevPos.X * (1 - u) + nextPos.X * u;
                nextPos.Y = prevPos.Y * (1 - u) + nextPos.Y * u;
                nextPos.Z = prevPos.Z * (1 - u) + nextPos.Z * u;

                resultFrame.AtomPositions[id] = nextPos;
            }

            for (int id = 0; id < velocityCount; ++id)
            {
                Vector3 prevVel = prevFrame.AtomVelocities[id];
                Vector3 nextVel = nextFrame.AtomVelocities[id];

                nextVel.X = prevVel.X * (1 - u) + nextVel.X * u;
                nextVel.Y = prevVel.Y * (1 - u) + nextVel.Y * u;
                nextVel.Z = prevVel.Z * (1 - u) + nextVel.Z * u;

                resultFrame.AtomVelocities[id] = nextVel;
            }
        }
    }
}