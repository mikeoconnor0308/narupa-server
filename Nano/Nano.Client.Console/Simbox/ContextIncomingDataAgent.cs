﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using SlimMath;

namespace Nano.Client.Console.Simbox
{
    public class ContextIncomingDataAgent : IncomingDataAgent
    {
        public ContextIncomingDataAgent(TransportContext transportContext)
            : base(transportContext)
        {
        }

        protected override void CreateDataStream(TransportDataStreamReceiver receiver, ref TransportStreamFormatDescriptor descriptor)
        {
            //@review @mike @ptew Terrible hacks to get interaction structs working.
            if (receiver.ID == (ushort)StreamID.Interaction)
            {
                receiver.CreateStream<Interaction>(ref descriptor);
                return;
            }

            if (receiver.ID == (ushort)StreamID.InteractionForceInfo)
            {
                receiver.CreateStream<InteractionForceInfo>(ref descriptor);
                return;
            }

            if (receiver.ID == (ushort)StreamID.VRInteraction || receiver.ID == (ushort)StreamID.VRPositions)
            {
                receiver.CreateStream<VRInteraction>(ref descriptor);
                return;
            }

            if (receiver.ID == (ushort)StreamID.VisibleAtomMap)
            {
                receiver.CreateStream<int>(ref descriptor);
                return;
            }

            if (descriptor.Type == VariableType.Float && descriptor.ChannelCount == 4)
            {
                receiver.CreateStream<Vector4>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Float && descriptor.ChannelCount == 3)
            {
                receiver.CreateStream<Vector3>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Float && descriptor.ChannelCount == 2)
            {
                receiver.CreateStream<Vector2>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Float && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<float>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Half && descriptor.ChannelCount == 4)
            {
                receiver.CreateStream<Half4>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Half && descriptor.ChannelCount == 3)
            {
                receiver.CreateStream<Half3>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Half && descriptor.ChannelCount == 2)
            {
                receiver.CreateStream<Half2>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Half && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<Half>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.UInt16 && descriptor.ChannelCount == 2)
            {
                receiver.CreateStream<BondPair>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.UInt16 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<ushort>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Int64 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<long>(ref descriptor);
            }
            else if (descriptor.Type == VariableType.Int32 && descriptor.ChannelCount == 1)
            {
                receiver.CreateStream<int>(ref descriptor);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}