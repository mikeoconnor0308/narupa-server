﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Variables
{
    /// <summary>
    /// Variable types.
    /// </summary>
    public enum VariableType : byte
    {

        /// <summary>
        /// Unknown variable type.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Half precision float.
        /// </summary>
        Half = 1,

        /// <summary>
        /// Single precision float.
        /// </summary>
        Float = 2,

        /// <summary>
        /// Double precision float.
        /// </summary>
        Double = 3,

        /// <summary>
        /// Boolean.
        /// </summary>
        Bool = 4,

        /// <summary>
        /// Signed byte.
        /// </summary>
        Int8 = 5,

        /// <summary>
        /// Unsigned byte.
        /// </summary>
        UInt8 = 6,

        /// <summary>
        /// Signed short.
        /// </summary>
        Int16 = 7,

        /// <summary>
        /// Unsigned short.
        /// </summary>
        UInt16 = 8,

        /// <summary>
        /// Signed 32 bit integer.
        /// </summary>
        Int32 = 9,

        /// <summary>
        /// Unsigned 32 bit integer.
        /// </summary>
        UInt32 = 10,

        /// <summary>
        /// Signed 64 bit integer.
        /// </summary>
        Int64 = 11,

        /// <summary>
        /// Unsigned 64 bit integer.
        /// </summary>
        UInt64 = 12,

        /// <summary>
        /// String.
        /// </summary>
        String = 13,

        /// <summary>
        /// 2D floating point vector.
        /// </summary>
        Vector2 = 14,

        /// <summary>
        /// 3D floating point vector.
        /// </summary>
        Vector3 = 15,

        /// <summary>
        /// Axis aligned 3D bounding box.
        /// </summary>
        BoundingBox = 16,

        Byte = 17
    }
}