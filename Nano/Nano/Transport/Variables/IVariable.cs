﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Rug.Osc;

namespace Nano.Transport.Variables
{
    public delegate void VariableChangedEvent(IVariable variable, VariableChangeSource changeSource);

    /// <summary>
    /// The source of a variable change.
    /// </summary>
    /// <autogeneratedoc />
    public enum VariableChangeSource
    {
        /// <summary>
        /// The variable change was from a local source.
        /// </summary>
        /// <autogeneratedoc />
        Local,

        /// <summary>
        /// The variable change was from a remote source.
        /// </summary>
        /// <autogeneratedoc />
        Remote,
    }

    /// <summary>
    /// A single variable.
    /// </summary>
    public interface IVariable
    {
        /// <summary>
        /// Gets the imitable identifier for this variable.
        /// </summary>
        /// <value>The name.</value>
        VariableName Name { get; }

        /// <summary>
        /// Gets the variable type of this variable.
        /// </summary>
        /// <value>The type of the variable.</value>
        VariableType VariableType { get; }

        /// <summary>
        /// Gets a value indicating if this variable is readonly to the user.
        /// </summary>
        /// <value><c>true</c> if readonly; otherwise, <c>false</c>.</value>
        bool Readonly { get; }

        /// <summary>
        /// The timestamp of the last message.
        /// </summary>
        DateTime Timestamp { get; set; } 

        /// <summary>
        /// Reads this variables current and desired value from a OSC message.
        /// </summary>
        /// <param name="message">An OSC message.</param>
        void FromMessage(OscMessage message);

        /// <summary>
        /// Writes this variables current and desired value to a OSC message.
        /// </summary>
        /// <returns>An OSC message.</returns>
        OscMessage ToMessage();

        /// <summary>
        /// Occurs when the value has changed.
        /// </summary>
        /// <autogeneratedoc />
        event VariableChangedEvent ValueChanged;

        /// <summary>
        /// Occurs when the desired value has changed.
        /// </summary>
        /// <autogeneratedoc />
        event VariableChangedEvent DesiredValueChanged;

        /// <summary>
        /// Occurs when any change is made.
        /// </summary>
        /// <autogeneratedoc />
        event VariableChangedEvent Changed;
    }

    /// <summary>
    /// A single variable.
    /// </summary>
    public interface IVariable<T> : IVariable // where T : struct
    {
        /// <summary>
        /// Gets or sets the current value of this variable.
        /// </summary>
        /// <remarks>
        /// If in the context of a client then the <see cref="IVariable"/> will be transmitted immediately.
        /// </remarks>
        T Value { get; set; }

        /// <summary>
        /// Gets or sets the desired value of this variable.
        /// </summary>
        /// <remarks>
        /// If in the context of a client then the <see cref="IVariable"/> will be transmitted immediately.
        /// </remarks>
        T DesiredValue { get; set; }
    }
}