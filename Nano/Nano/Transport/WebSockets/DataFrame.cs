﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;

namespace Nano.WebSockets
{
    public enum Opcode : byte
    {
        /// <summary>
        /// Indicates continuation frame.
        /// </summary>
        Cont = 0x0,

        /// <summary>
        /// Indicates text frame.
        /// </summary>
        Text = 0x1,

        /// <summary>
        /// Indicates binary frame.
        /// </summary>
        Binary = 0x2,

        /// <summary>
        /// Indicates connection close frame.
        /// </summary>
        Close = 0x8,

        /// <summary>
        /// Indicates ping frame.
        /// </summary>
        Ping = 0x9,

        /// <summary>
        /// Indicates pong frame.
        /// </summary>
        Pong = 0xa
    }

    public enum DataFrameReadState
    {
        Failed,
        NotEnoughBytesForHeader,
        IncompletePayload,
        DataFrameTooLarge, 
        Complete,
    }

    public class DataFrame
    {
        public bool Fin;

        public bool HasMask;
        public byte[] Mask = new byte[4];
        public Opcode Opcode;
        public byte[] PayloadData = new byte[0];
        public int PayloadLength;

        public void CreateMask()
        {
            Keys.RandomNumber.GetBytes(Mask);

            HasMask = true;
        }

        public void ClearMask()
        {
            HasMask = false;

            Array.Clear(Mask, 0, 4);
        }

        public DataFrameReadState Read(BinaryReader reader)
        {
            long avalibleBytes = reader.BaseStream.Length - reader.BaseStream.Position;

            if (avalibleBytes < 1)
            {
                return DataFrameReadState.NotEnoughBytesForHeader;
            }

            byte header = reader.ReadByte();
            avalibleBytes -= 1;

            // read FIN bit
            Fin = (header & 0x80) == 0x80;

            // read OPCODE nibble
            Opcode = (Opcode)(header & 0x0F);

            if (avalibleBytes < 1)
            {
                return DataFrameReadState.NotEnoughBytesForHeader;
            }

            header = reader.ReadByte();
            avalibleBytes -= 1;

            // read MASK bit
            HasMask = (header & 0x80) == 0x80;

            // read little length 
            ulong length = unchecked((ulong)(header & 0x7F));

            // read extended lengths
            if (length == 126)
            {
                if (avalibleBytes < 2)
                {
                    return DataFrameReadState.NotEnoughBytesForHeader;
                }

                length = (ushort) System.Net.IPAddress.NetworkToHostOrder(reader.ReadInt16());
                
                avalibleBytes -= 2;
            }
            else if (length == 127)
            {
                if (avalibleBytes < 8)
                {
                    return DataFrameReadState.NotEnoughBytesForHeader;
                }

                length = unchecked((ulong) System.Net.IPAddress.NetworkToHostOrder(reader.ReadInt64()));
                avalibleBytes -= 8;
            }

            if (length > int.MaxValue)
            {
                return DataFrameReadState.DataFrameTooLarge; 
            }

            if (HasMask == true)
            {
                if (avalibleBytes < 4)
                {
                    return DataFrameReadState.NotEnoughBytesForHeader;
                }

                Mask = reader.ReadBytes(4);
                avalibleBytes -= 4;
            }

            PayloadLength = (int)length;

            if (avalibleBytes < PayloadLength)
            {
                return DataFrameReadState.IncompletePayload;
            }

            PayloadData = reader.ReadBytes(PayloadLength);

            if (HasMask == true)
            {
                MaskPayload();
            }

            return DataFrameReadState.Complete; 
        }

        public void Write(BinaryWriter writer)
        {
            byte header;

            header = 0;
            header |= (byte)(Fin ? 0x80 : 0x00);
            header |= (byte)(Opcode);

            writer.Write(header);

            header = 0;
            header |= (byte)(HasMask ? 0x80 : 0x00);

            long length = PayloadData.LongLength;

            if (length > ushort.MaxValue)
            {
                header |= (byte)(127);

                writer.Write(header);
                writer.Write(System.Net.IPAddress.HostToNetworkOrder(unchecked((long)length)));
            }
            else if (length >= 126)
            {
                header |= (byte)(126);

                ushort lengthU16 = (ushort) length; 
                
                writer.Write(header);
                writer.Write(System.Net.IPAddress.HostToNetworkOrder(unchecked((short)lengthU16)));
            }
            else
            {
                header |= (byte)(length);

                writer.Write(header);
            }

            if (HasMask)
            {
                writer.Write(Mask);

                MaskPayload();
            }

            writer.Write(PayloadData);
        }

        private void MaskPayload()
        {
            for (int i = 0, ie = PayloadData.Length; i < ie; i++)
            {
                PayloadData[i] = (byte)(PayloadData[i] ^ Mask[i % 4]);
            }
        }
    }
}