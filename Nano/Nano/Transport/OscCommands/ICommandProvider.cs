﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;

namespace Nano.Transport.OSCCommands
{
    /// <summary>
    /// Interface enabling client-side commands to be run.
    /// </summary>
    public interface ICommandProvider
    {
        /// <summary>
        /// Gets the address of the command provider.
        /// </summary>
        /// <value>The address.</value>
        string Address { get; }

        /// <summary>
        /// Attaches the transport context.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        void AttachTransportContext(TransportContext transportContext);

        /// <summary>
        /// Detaches the transport context.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        void DetachTransportContext(TransportContext transportContext);
    }
}