﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Nano.Transport.Agents;
using Nano.Transport.Comms.Security;
using Nano.WebSockets;

namespace Nano.Transport.Comms.ServerSocketImplementations
{
    class WebSocketImplementation : IServerSocketImplementation
    {
        public IPEndPoint EndPoint { get; }

        private const int BufferSize = 65536;
        private static uint ImportantQueueThreshold = 256; 
        private static uint NormalQueueThreshold = 64;

        private readonly byte[] buffer = new byte[BufferSize];
        private readonly DataFrameHandler dataFrameHandler;
        private readonly ManualResetEvent doneEvent = new ManualResetEvent(true);
        private readonly ManualResetEvent errorEvent = new ManualResetEvent(true);
        private readonly Http http;
        private readonly MemoryStream memoryStream = new MemoryStream();
        private readonly TransportPacketParser packetParser;
        private readonly PacketStatistics packetStatistics;
        private readonly IReporter reporter;
        private readonly ManualResetEvent sendEvent = new ManualResetEvent(true);
        private readonly object syncLock = new object();
        private readonly TcpClient tcpClient;
        private readonly TransportSecuritySessionToken securitySessionToken;
        private ConnectionState state = ConnectionState.NotConnected;

        public event EventHandler Ended;
        public event EventHandler Started;

        public bool BufferOutput { get; set; } = true;
        public int QueuedPackets { get; private set; }

        public WebSocketImplementation(TransportSecuritySessionToken securitySessionToken, TcpClient client, PacketStatistics packetStatistics, IReporter reporter, ITransportPacketReceiver receiver)
        {
            this.securitySessionToken = securitySessionToken; 

            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            if (packetStatistics == null)
            {
                throw new ArgumentNullException(nameof(packetStatistics));
            }

            if (reporter == null)
            {
                throw new ArgumentNullException(nameof(reporter));
            }

            if (receiver == null)
            {
                throw new ArgumentNullException(nameof(receiver));
            }

            tcpClient = client;

            EndPoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;

            this.packetStatistics = packetStatistics;
            this.reporter = reporter;

            dataFrameHandler = new DataFrameHandler(EndPoint.ToString(), false, reporter);
            dataFrameHandler.BinaryFrame += DataFrameHandler_BinaryFrame;
            dataFrameHandler.TextFrame += DataFrameHandler_TextFrame;
            dataFrameHandler.FrameComplete += DataFrameHandler_FrameComplete;

            http = new Http(EndPoint.ToString(), reporter);
            http.HttpComplete += Http_HttpComplete;

            packetParser = new TransportPacketParser(receiver, packetStatistics);
        }

        public void Dispose()
        {
            packetParser.Dispose();
        }

        public bool Flush()
        {
            if (BufferOutput == false)
            {
                return false;
            }

            if (state == ConnectionState.NotConnected ||
                state == ConnectionState.Disconnected)
            {
                return false;
            }

            try
            {
                sendEvent.Reset();

                lock (memoryStream)
                {
                    if ((int)memoryStream.Position == 0)
                    {
                        return false;
                    }

                    QueuedPackets++;

                    // Begin sending the data to the remote device.
                    dataFrameHandler.SendBinary(memoryStream.ToArray(), 0, (int)memoryStream.Position);

                    memoryStream.Position = 0;
                }
            }
            catch (SocketException ex)
            {
                sendEvent.Set();

                if (CheckConnection() == false)
                {
                    return false;
                }

                reporter.PrintException(ex, Strings.Error_Client_FailedToLeaveStreams);

                EndSession();
            }
            catch (Exception ex)
            {
                sendEvent.Set();

                if (CheckConnection() == false)
                {
                    return false;
                }

                reporter.PrintException(ex, Strings.ClientSession_Exception_Send_General, EndPoint);

                EndSession();
            }

            return true;
        }

        public bool Send(TransportPacketPriority priority, byte[] data, int index, int count)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (index < 0 || index >= data.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            if (count < 0 || count + index > data.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (state == ConnectionState.NotConnected ||
                state == ConnectionState.Disconnected)
            {
                return false;
            }

            switch (priority)
            {
                case TransportPacketPriority.Critical:
                    break;

                case TransportPacketPriority.Important:
                    if (QueuedPackets > ImportantQueueThreshold)
                    {
                        return false;
                    }
                    break;

                case TransportPacketPriority.Normal:
                    if (QueuedPackets > NormalQueueThreshold)
                    {
                        return false;
                    }
                    break;

                default:
                    break;
            }

            try
            {
                sendEvent.Reset();

                if (BufferOutput == false)
                {
                    QueuedPackets++;

                    // Begin sending the data to the remote device.
                    dataFrameHandler.SendBinary(data, index, count);
                }
                else
                {
                    lock (memoryStream)
                    {
                        memoryStream.Write(data, index, count);
                    }
                }
            }
            catch (SocketException ex)
            {
                sendEvent.Set();

                if (CheckConnection() == false)
                {
                    return false;
                }

                reporter.PrintException(ex, Strings.Error_Client_FailedToLeaveStreams);

                EndSession();
            }
            catch (Exception ex)
            {
                sendEvent.Set();

                if (CheckConnection() == false)
                {
                    return false;
                }

                reporter.PrintException(ex, Strings.ClientSession_Exception_Send_General, EndPoint);

                EndSession();
            }

            return true;
        }

        public void Start()
        {
            state = ConnectionState.Handshake;

            reporter.PrintEmphasized(Direction.Action, EndPoint, Strings.ClientSession_Start);

            BeginReceive(tcpClient.Client);
        }

        public void Stop()
        {
            lock (syncLock)
            {
                EndSession();

                packetParser.Dispose();
            }
        }

        private void BeginReceive(Socket client)
        {
            try
            {
                // Begin receiving the data from the remote device.
                client.BeginReceive(buffer, 0, BufferSize, 0, EndReceive, null);
            }
            catch (Exception ex)
            {
                if (CheckConnection() == false)
                {
                    return;
                }

                reporter.PrintException(ex, Strings.ClientSession_Exception_Receive, EndPoint);

                EndSession();
            }
        }

        private bool CheckConnection()
        {
            if (state == ConnectionState.NotConnected ||
                state == ConnectionState.Disconnected)
            {
                return false;
            }

            errorEvent.WaitOne();

            if (state == ConnectionState.NotConnected ||
                state == ConnectionState.Disconnected)
            {
                return false;
            }

            errorEvent.Reset();

            return true;
        }

        private void DataFrameHandler_BinaryFrame(DataFrameHandler handler, byte[] buffer, int index, int count)
        {
            packetParser.Process(EndPoint, buffer, index, count);
        }

        private void DataFrameHandler_FrameComplete(DataFrameHandler handler, byte[] buffer, int index, int count)
        {
            tcpClient.Client.BeginSend(buffer, index, count, SocketFlags.None, SendCallback, null);
        }

        private void DataFrameHandler_TextFrame(DataFrameHandler handler, string text)
        {
            reporter.PrintNormal(text);
        }

        private void EndReceive(IAsyncResult ar)
        {
            try
            {
                Socket client = tcpClient.Client;

                // Read data from the remote device.
                int bytesRead = client.EndReceive(ar);

                packetStatistics.IncrementReceiveBytes(bytesRead);

                if (bytesRead == 0)
                {
                    EndSession();

                    return;
                }

                if (state == ConnectionState.Handshake)
                {
                    if (http.ProcessClientHandshake(buffer, 0, bytesRead, securitySessionToken?.SecurityKey) == false)
                    {
                        reporter.PrintError("Handshake failed");

                        EndSession();

                        return;
                    }

                    state = ConnectionState.Connected;

                    Started?.Invoke(this, EventArgs.Empty);
                }
                else
                {
                    dataFrameHandler.Process(buffer, 0, bytesRead);
                }

                client.BeginReceive(buffer, 0, BufferSize, 0, EndReceive, null);
            }
            catch (Exception ex)
            {
                if (CheckConnection() == false)
                {
                    return;
                }

                reporter.PrintException(ex, Strings.ClientSession_Exception_ReceiveCallback, EndPoint);

                EndSession();
            }
        }

        private void EndSession()
        {
            if (state == ConnectionState.NotConnected ||
                state == ConnectionState.Disconnected)
            {
                return;
            }

            state = ConnectionState.Disconnected;

            reporter.PrintEmphasized(Direction.Action, EndPoint, Strings.ClientSession_Stopping);

            tcpClient.Close();

            const int timeout = 5000;

            doneEvent.WaitOne(timeout);
            sendEvent.WaitOne(timeout);

            reporter.PrintEmphasized(Direction.Action, EndPoint, Strings.ClientSession_Stopped);

            errorEvent.Set();

            reporter.PrintEmphasized(Direction.Action, EndPoint, Strings.ClientSession_SessionEnded);

            Ended?.Invoke(this, EventArgs.Empty);
        }

        private void Http_HttpComplete(Http http1, byte[] buffer, int index, int count)
        {
            tcpClient.Client.BeginSend(buffer, index, count, SocketFlags.None, SendCallback, null);
        }

        private void SendCallback(IAsyncResult result)
        {
            try
            {
                QueuedPackets--;

                // Complete sending the data to the remote device.
                int bytesSent = tcpClient.Client.EndSend(result); 

                packetStatistics.IncrementSentBytes(bytesSent);

                // Signal that all bytes have been sent.
                sendEvent.Set();
            }
            catch (Exception ex)
            {
                sendEvent.Set();

                if (CheckConnection() == false)
                {
                    return;
                }

                reporter.PrintException(ex, Strings.ClientSession_Exception_SendCallback, EndPoint);

                EndSession();
            }
        }
    }
}
