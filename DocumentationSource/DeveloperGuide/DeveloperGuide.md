# Developer Guide

This page is a guide for developers seeking to extend Narupa.

@section introduction Introduction

This developer guide provides detailed information about the structure and architecture of Narupa code base,
aimed at helping developers in extending the core functionality of Narupa or in developing plugins. 

If you are intending to simply run simulations with the Narupa server, then the \ref UserGuide "user guide" should provide
you with all the information that you need. 

For developers, the documentation in this guide is intended to help provide a starting point for getting involved in 
development, but is by no means a comprehensive guide of all the things Narupa can be made to do. Between this guide
and the \ref Namespaces "API Documentation", you should be able to get going. 


@section style Style Guidelines 

The following conventions are used in the codebase and should be followed to maintain consistency. It is recommended to use plugins for your IDE to simplify conforming to these standards. We use Rider and Resharper. 

* Naming Conventions: 
    * Methods, Class Names, Public Fields and Public Properties: `UpperCamelCase`. 
    * Private fields, local variables: `lowerCamelCase`. 
    * Namespaces: Should match the folder layout e.g. `Assets.NSB.VR.Control` 
* Documentation: All public classes, fields and methods should have XML documentation. Code examples are encouraged. 

@section documentation Contributing to the Documentation 

One of the easiest and most useful contributions to make is to edit and add to this documentation. 

## Building the Documentation 

To build the documentation the following software is required: 

* Doxygen (>1.8) 

There are two scripts in the root of the repository, `build_docs.sh` and `build_docs.bat`. The shell script is for Linux and Mac OS X users, while the BAT script is for Windows. 
Run the appropriate script for your system, and the documentation will be generated in the folder Documentation.

## Documentation Structure

The documentation is generated from XML comments on the code, as well as Markdown files for the main page and guides.
The documentation has the following structure: 

~~~
DocumentationSource
+-- Doxyfile
+-- UserGuide
|   +-- UserGuide.md
+-- DeveloperGuide
|   +-- DeveloperGuide.md
~~~

Additional pages can be added by editing the `INPUT` field of the `Doxyfile`. The look of the documentation page can be changed by editing the `doxygen-narupa.css` style.

@section architecture Narupa Server Architecture

The Narupa server is made of up of two major components, the Narupa namespace, consisting of the server and molecular
simulation details, and the Nano API, consisting of the transport details and base classes to assist creating simulations. 

In the Nano namespace, we have the following projects:

* Nano - Core utilities and the low level transport API. 
* Nano.Client - Base classes for creating clients using the Nano transport API. 
* Nano.Client.Console - Base classes and helper utilities for console based clients such as a python or command line client. 
* Nano.Server - Base classes for creating servers using the Nano transport API.
* Nano.Science.Simulation - Base classes for performing molecular simulations. 

In the Narupa namespace, we have the following projects: 

* narupa-server - The main program that runs the server and hosts the simulation. 
* Narupa.MD - The basic library for performing molecular dynamics simulations. 

@section Writing Plugins for the Narupa Server

A convenient way to extend the functionality of the server without having to write any code directly in Narupa is through
writing plugins. 

A plugin consists of a C Sharp library that implements one (or more) of the interfaces that are loaded by a simulation,
using the ILoadable interface. There are lots of components that are loaded in this manner and all can be customized, 
however the most useful ones for plugins are the IForcefield, ISimulation and IAtomicSystem and IIntegrator. Using these,
you can easily write your own forcefields, or entirely new simulation engines. 

A plugin, once built, has the following folder structure: 

~~~
PluginName
+-- PluginManifest.xml
+-- Managed
|   +-- Plugin.dll 
|   +-- PluginDependency.dll
+-- Native
|   +-- libplugin.so (or plugin.dll on Windows)

~~~

Within the plugin folder, there are 3 items: the plugin manifest, a folder for managed binaries, and a folder for native
binaries. 

The plugin manifest details the name of the plugin, which version of Narupa it is compatible with and which libraries 
should be scanned for ILoadable. An example is given below: 

~~~{xml}
<?xml version="1.0" encoding="utf-8" ?>
<Plugin Name="OpenMMForceField" APIVersion="1.0.*" Version="2.1.*">
  <LoadableAssemblies>
    <Assembly Name="Narupa.OpenMM.dll"/>
  </LoadableAssemblies>
</Plugin>
~~~

In the Managed folder, the .NET libraries of the plugin are placed. Any compiled binaries that are
not part of the .NET framework (i.e. c++, FORTRAN etc) are stored. On Windows, binaries in this path will be added to 
the PATH variable at runtime. On Linux and OS X systems, the path to these binaries must be added to the relevant 
path variable or the binaries should be installed in the appropriate manner. See the \ref UserGuide for more details. 

Below are C++ and C# templates to demonstrate a simple interop between C# and C/C++ that can be used to represent the meat of a forcefield plugin. 

~~~{c++}
/***
 * Example exposing c++ code in a form compatible with C#
 */
 //This prevents c++ name mangling.
extern "C"
{
	/*
	 * Initialise system example. Make sure export is included.
	 * element IDs and atoms, as well as topology information etc can be provided.
	 */
	__declspec(dllexport) void Init(int* elements, int natoms)
	{
		//Do some stuff;
	}

	/*
	* Example forcefield calculation.
	* Given positions, returns forces and potential energy.
	* Positions in Narupa are in nm, forces are in kJ/(mol*nm) and energy is in kJ/mol.
	*/
	__declspec(dllexport) float CalculateForceField(float* positions, float* forcesOut, int natoms)
	{
		for (int i = 0; i < natoms; i++)
		{
			forcesOut[natoms * 3 + 0] = i;
			forcesOut[natoms * 3 + 1] = i;
			forcesOut[natoms * 3 + 2] = i;
		}

		return 0;
	}

	__declspec(dllexport) void Dispose()
	{
		//Gracefully dispose of any allocated memory.
	}
}
~~~

~~~{cs}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NanoNarupaPluginExampleCSharp
{
    /// <summary>
    /// Demonstration of linking against a library with unmanaged C functions.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Calls Init in our library.
        /// </summary>
        /// <param name="elements">Array of element numbers.</param>
        /// <param name="natoms">Number of atoms in system.</param>
        [DllImport("nsbpluginc")]
        private static extern void Init([In] int[] elements, int natoms);

        /// <summary>
        /// Calls CalculateForceField in the plugin.
        /// </summary>
        /// <param name="positions">Positions in C style array.</param>
        /// <param name="forces">Forces in C style array.</param>
        /// <param name="natoms">Number of atoms.</param>
        /// <returns></returns>
        [DllImport("nsbpluginc")]
        private static extern float CalculateForceField([In] float[] positions, [Out] float[] forces, int natoms);

        /// <summary>
        /// Calls Dispose in the plugin.
        /// </summary>
        [DllImport("nsbpluginc")]
        private static extern void Dispose();

        private static void Main(string[] args)
        {
            int natoms = 3;
            int[] elements = new int[natoms];
            for (int i = 0; i < natoms; i++)
            {
                elements[i] = i;
            }

            float[] positions = new float[3 * natoms];
            for (int i = 0; i < natoms; i++)
            {
                positions[i * 3 + 0] = i;
                positions[i * 3 + 1] = i;
                positions[i * 3 + 2] = i;
            }

            //Test calling all the methods.
            Init(elements, natoms);

            float[] forces = new float[3 * natoms];
            //Call the forcefield.
            float energy = CalculateForceField(positions, forces, natoms);
            Console.WriteLine($"Plugin calculated energy of {energy} kJ/mol");

            Dispose();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
~~~

The [OpenMM plugin](https://gitlab.org/intangiblerealities/narupa-openmm) is an example of a forcefield where the C Sharp code is autogenerated using SWIG, and can be used as a guide. 

@section md Developing Molecular Simulations

The Narupa server provides a modular framework for developing molecular simulations. As explained in the previous 
section, with plugins it is possible to develop for a wide variety of applications. In this section, the simulation architecture is described in detail. 

## Loading a Simulation 

The main interface for a molecular simulation is the \ref Nano.Science.Simulation.ISimulation "ISimulation". A Narupa server loads this interface based on the input file (described in the [UserGuide](../UserGuide/UserGuide.md)), and drives the simulation by calling the \ref Nano.Science.Simulation.ISimulation.Run "Run" method every frame. 

A simulation is instantiated as follows. The \ref Nano.Science.Simulation.SimulationFile "SimulationFile" class loads the \ref Nano.Science.Simulation.SystemProperties "SystemProperties" and the \ref "Nano.Science.Simulation.Topology" Topology by recursively loading using the \ref Nano.ILoadable "ILoadable" interface. 

At this point, we have a set of templates that need to be spawned into a true simulation. The Topology is then instantiated into the aptly named \ref Nano.Science.Simulation.InstantiatedTopology InstantiatedTopology. During this process, the templates defined in the topology are spawned into the system, along with the accompanying forcefields. 

Finally, the \ref Nano.Science.Simulation.IAtomicSystem "AtomicSystem" is constructed from the topology, instantiating the initial \ref Nano.Science.Simulation.ParticleCollection "ParticleCollection" state, the integrator, thermostat and forcefields. 

Since it is modular by design, it is easy to swap out the various components. Implementing \ref Nano.Science.Simulation.IForceField forcefields are the best place to start, and there are plenty of examples such as the \ref Narupa.MD.ForceField.LennardJonesForceField, or adding a trajectory format in \ref Narupa.Trajectory.

@section transport Adding New Communication Schemes

**Note:** The transport layer going to be overhauled soon to make it easier to use. 

The Narupa server's primary purpose is to transmit simulation data to clients. Data from the server takes three forms: \ref Nano.Transport.Streams.IDataStream "Streams", \ref Nano.Transport.Variables.IVariable "Variables" and OSC messages. Streams are used to transmit bulk data such as atom positions and interactions. Variables are essentially OSC messages communicating important simulation properties such as potential energy and temperature, wrapped up in an easily accessible manner. OSC messages are a generic transport protocol that can be used to control the behaviour of the server.

## Creating a New Stream 

Suppose you want to add a new stream to the Narupa server. A stream can (currently) only transmit arrays of structs of a predetermined size and type. Let us suppose the new stream will transmit `float` values representing the kinetic energy of each atom. 
To create this stream, you will need to add it to the server:

~~~{cs}
private IDataStream<float> atomKineticEnergyStream;
...
atomKineticEnergyStream = OutgoingStreams.DefineStream<int>((int) 321, "Atom Kinetic Energy", VariableType.Float, 1, MaxNumberOfParticles, TransportDataStreamTransmitMode.Continuous);

~~~

Here, 321 is the stream ID, see \ref `Nano.Transport.Streams.StreamID` for the current set of reserved stream IDs.

To publish data on the stream, you fill the Data values and flush it: 

~~~{cs}
for(int i=0; i < numParticles; i++)
{
    atomKineticEnergyStream.Data[i] = atomEnergy[i];
}
atomKineticEnergyStream.Count = numParticles; 
OutgoingStreams.Flush(atomKineticEnergyStream.ID);
~~~

## Setting up an OSC Command

Another way of controlling the server is through Osc commands. The \ref Nano.Transport.OscCommands.ICommandProvider "ICommandProvider" interface provides a convenient way of setting up commands that can be triggered by clients. Adding the `OscCommand` attribute to a method will result in it being added to the set of 
OSC addresses that are listened to. The example below from the \ref Narupa.MD.ForceField.External.ForceField.ParticleRestraintForce "ParticleRestraintForce" class results in a command with the address `/restraint/addRestraint` that takes an atom index, target position and force constant and creates a restraint.

~~~{cs}
public class ParticleRestraintForce : ICommandProvider
{
   
    public string Address => "/restraint";

    public List<ParticleRestraint> Restraints = new List<ParticleRestraint>();
    private TransportContext transportContext;
    private object locker = new object();

    public void AttachTransportContext(TransportContext transportContext)
    {
        this.transportContext = transportContext;
    }

    public void DetachTransportContext(TransportContext transportContext)
    {
    }

    [OscCommand(ExampleArguments = "2, 1000, 0, 0, 0", Help = "Adds a restraint to the specified atom", TypeTagString = "i,f,f,f,f")]
    public void AddRestraint(OscMessage message)
    {
        OscCommandAttribute attributes = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>();
        OscMessage reply;
        int atomIndex = -1;
        float force = 1000;
        Vector3 position = Vector3.Zero;
        try
        {
            atomIndex = (int)message[0];
            force = (float)message[1];
            position = new Vector3((float)message[2], (float)message[3], (float)message[4]);
        }
        catch
        {
            OscCommandHelper.SendTypeTagString(transportContext, message.Origin, attributes);
            return;
        }

        lock (locker)
        {
            ParticleRestraint restraint = new ParticleRestraint()
            {
                AtomIndex = atomIndex,
                K = force,
                Pos0 = position,
                RestraintDimension = RestraintDimensions.XYZ
            };
            Restraints.Add(restraint);
        }
    }
   
}

~~~

Consult the Narupa client documentation for an example of sending OSC messages to the server.

## Broadcasting an OSC Message

It can sometimes be useful to transmit a message to all clients indicating to them that something has happened in the server. This can be done with an OSC message. For example, to indicate that a bond has broken the following message can be sent from the server, using a \ref Nano.Transport.Comms.TranportContext "TransportContext":

~~~{cs}
TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage("/sim/bondBroken", atomIndexA, atomIndexB);
~~~

Consult the Narupa client documentation for an example of listening to OSC messages sent from the server.

