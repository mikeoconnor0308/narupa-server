
This website documents the Narupa Engine, a software package for running interactive molecular simulations. Checkout the code on [gitlab](gitlab.com/intangiblerealities/). 

For documentation on the VR client, go [here](https://intangiblerealities.gitlab.io/narupaXR/).

To get started with using Narupa, check out the [user guide](UserGuide/UserGuide.md).

To contribute to development, head over to the [developer guide](DeveloperGuide/DeveloperGuide.md).

