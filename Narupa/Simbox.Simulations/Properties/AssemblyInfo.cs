﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Simbox.Simulations")]
[assembly: AssemblyDescription("Temporary NanoSimbox Simulations Database")]

[assembly: ComVisible(false)]
[assembly: Guid("17326721-e39e-4fd4-8e55-5ab2bb16c0ed")]
