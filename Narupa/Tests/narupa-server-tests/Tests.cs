﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MDServer;
using Nano;
using Nano.Client;
using Nano.Client.Console.Simbox;
using Nano.Loading;
using Nano.Server;
using NUnit.Framework;
using Rug.Osc;
using AssemblyRoot = Nano.Server.AssemblyRoot;
using CmdReporter = Simbox.MD.Tests.Util.CmdReporter;

namespace Narupa.Server.Tests
{
    [TestFixture]
    public class Tests
    {
        //TODO switch to async. 
        ManualResetEvent manualResetEvent = new ManualResetEvent(false);
        
        // ReSharper disable once NotAccessedField.Local
        private Simbox.MD.Tests.TestHelper helper;
        private ManualResetEvent broadcastTestEvent = new ManualResetEvent(false);

        [SetUp]
        public void Initialize()
        {
            helper = new Simbox.MD.Tests.TestHelper();
        }
       
        /// <summary>
        /// Tests that a client can connect to a basic running simulation. 
        /// </summary>
        [Test]
        public void ClientServerConnectionTest()
        {
            //This is where all assemblies are scanned for ILoadable content.
            LoadManager.ScanAssembly(typeof(ServerConfig).Assembly);

            LoadManager.ScanAssembly(typeof(AssemblyRoot).Assembly);
            LoadManager.ScanAssembly(typeof(Nano.Server.AssemblyRoot).Assembly);

            LoadManager.ScanAssembly(typeof(SimulationService).Assembly);
            
            CmdReporter reporter = new CmdReporter();
           
            
           // load config 

            var serverName = "Test Server";
            var server = new Server(serverName);

            try
            {
                server.Load(reporter, configFile:"^/Assets/ServerConfigs/test_server.xml");
            }
            catch (Exception ex)
            {
                Assert.Fail("Failed to instantiate server", ex);
            }
            CancellationTokenSource cancelSource = new CancellationTokenSource();

            CancellationToken cancel = cancelSource.Token;

            Task serverStartTask; 
            
            
            server.ReportManager.Initialize(server, reporter);
            server.SecurityProvider.Initialize(server, reporter);

            try
            {
                List<Task> allTasks = new List<Task>
                {
                    server.Monitor.Start(server, cancel),
                    server.Service.Start(server, cancel),
                    server.SecurityProvider.Start(server, cancel),
                    server.Discovery.Start(server, cancel)
                };
            }
            catch (Exception e)
            {
                Assert.Fail("Exception thrown starting server");
            }
            

            //Give the server a little time to spin up.
            //TODO Add a async await for when the service has started.
            Task.Delay(1000);
           
            //set up a client and connect to the server. 
            var connections = SearchForConnections.EnumerateConnections(5000);
            //find the test server. 
            bool foundServer = false;
            SimboxConnectionInfo testConnection = null;
            foreach (var connection in connections)
            {
                if (connection.Descriptor == serverName)
                {
                    foundServer = true;
                    testConnection = connection;
                    break;
                }
            }

            if (!foundServer)
            {
                Assert.Fail("Was unable to find server.");
            }
            NSBFrameReader reader = new NSBFrameReader(); 
            reader.OnReady += OnReady;
            reader.TryEnterServer(testConnection);
            bool success = manualResetEvent.WaitOne(2000);

            if(success == false)
                Assert.Fail("Connection timed out.");
            int numAtoms = 120;
            Assert.AreEqual(numAtoms, reader.LatestFrame.AtomCount);
            
            cancelSource.Cancel();

        }

        
        
            
        /// <summary>
        /// Tests that a client can broadcast a message.
        /// </summary>
        [Test]
        public void ClientTestBroadcast()
        {
            //This is where all assemblies are scanned for ILoadable content.
            LoadManager.ScanAssembly(typeof(ServerConfig).Assembly);

            LoadManager.ScanAssembly(typeof(AssemblyRoot).Assembly);
            LoadManager.ScanAssembly(typeof(Nano.Server.AssemblyRoot).Assembly);

            LoadManager.ScanAssembly(typeof(SimulationService).Assembly);
            
            CmdReporter reporter = new CmdReporter();
           
            
           // load config 

            var serverName = "Test Server";
            Server server = new Server(serverName);

            try
            {
                server.Load(reporter, configFile:"^/Assets/ServerConfigs/test_server.xml");
            }
            catch (Exception ex)
            {
                Assert.Fail("Failed to instantiate server", ex);
            }
            CancellationTokenSource cancelSource = new CancellationTokenSource();

            CancellationToken cancel = cancelSource.Token;

            Task serverStartTask; 
            
            
            server.ReportManager.Initialize(server, reporter);
            server.SecurityProvider.Initialize(server, reporter);

            try
            {              
                List<Task> allTasks = new List<Task>
                {
                    server.Monitor.Start(server, cancel),
                    server.Service.Start(server, cancel),
                    server.SecurityProvider.Start(server, cancel),
                    server.Discovery.Start(server, cancel)
                };
            }
            catch (Exception e)
            {
                Assert.Fail("Exception thrown starting server");
            }
            

            //Give the server a little time to spin up.
            //TODO Add a async await for when the service has started.
            Task.Delay(1000);
           
            //set up a client and connect to the server. 
            var connections = SearchForConnections.EnumerateConnections(5000);
            //find the test server. 
            bool foundServer = false;
            SimboxConnectionInfo testConnection = null;
            foreach (var connection in connections)
            {
                if (connection.Descriptor == serverName)
                {
                    foundServer = true;
                    testConnection = connection;
                    break;
                }
            }

            if (!foundServer)
            {
                Assert.Fail("Was unable to find server.");
            }
            NSBFrameReader reader = new NSBFrameReader(); 
            reader.OnReady += OnReady;
            reader.TryEnterServer(testConnection);
            bool success = manualResetEvent.WaitOne(3000);

            if(success == false)
                Assert.Fail("Connection timed out.");


            if (!reader.Client.TransportContext.Transmitter.IsConnected)
            {
                Assert.Fail();
            }
            
            //TODO why do we have to wait for a second sometimes?
            Task.Delay(10000);
            reader.Client.TransportContext.OscManager.Attach("/broadcast/test", OnBroadcastTest);
            reader.Client.Send(new OscMessage("/broadcast/test", "hi"));
            success = manualResetEvent.WaitOne(2000);
            if(success == false)
                Assert.Fail("Did not receive response.");
            cancelSource.Cancel();
        }

        private void OnBroadcastTest(OscMessage message)
        {
            broadcastTestEvent.Set();
        }
        private void OnReady()
        {
            manualResetEvent.Set();
        }
    }
}