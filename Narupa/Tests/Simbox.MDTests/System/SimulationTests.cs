﻿using System;
using System.Collections.Generic;
using Nano.Science.Simulation;
using NUnit.Framework;
using SlimMath;
using Nano.Science;
using System.Linq;
using Nano;

namespace Simbox.MD.Tests
{
    [TestFixture]
    public class SimulationTests
    {
        private TestHelper helper;

        [SetUp]
        public void Initialize()
        {
            helper = new TestHelper();
        }

        [Test]
        public void TestCalculationOfKineticEnergyAndTemperature()
        {
            // path to simulation a file
            string simulationFile = "^/Assets/Simulations/TestSims/Buckyballs.xml";
            Simulation sim = new Simulation(simulationFile, helper.Reporter);

            // run simulation for one step
            sim.Run(1);

            // calculate KE TEmp
            Vector2 keTemp = sim.ActiveSystem.CalculateKineticEnergyAndTemperature();
            float ke = keTemp[0];
            float temp = keTemp[1];
            float tolerance = 1E-5f;

            //Calculate ke and temp manually and compare - velocities in nm/ps. mass in amu.
            //sim.ActiveSystem.Particles.Velocity[i];
            //sim.ActiveSystem.Particles.Mass[i];
            // 1 amu * nm^2 / ps^2 = 1e3 J / mol = 1 kJ / mol
            float verifyKE = 0f;
            for (int i = 0; i < sim.ActiveSystem.NumberOfParticles; i++)
            {
                verifyKE += 0.5f * sim.ActiveSystem.Particles.Mass[i] * (sim.ActiveSystem.Particles.Velocity[i]).LengthSquared;
            }

            float verifyTemp = 0f;
            verifyTemp = 2 * verifyKE / (sim.ActiveSystem.GetDegreesOfFreedom() * 0.001f * PhysicalConstants.MolarGasConstantR);

            // assert within single point accuracy please note it allows for zero temperature
            if (temp == 0.0f)
            {
                Assert.AreEqual(verifyTemp, temp, tolerance);
            }
            else
            {
                float ratio = verifyTemp / temp;
                Assert.AreEqual(ratio, 1.0f, tolerance);
            }
            // kinetic energy
            if (ke == 0.0f)
            {
                Assert.AreEqual(verifyKE, ke, tolerance);
            }
            else
            {
                float ratio = verifyKE / ke;
                Assert.AreEqual(ratio, 1.0f, tolerance);
            }
        }

        // TODO These tests are too long for CI. Is there a quick test we could do?
        /*
        [Test]
        public void TestBerendsenThermostat()
        {
            string simulationFile = "^/Assets/Simulations/TestSims/Buckyballs.xml";
            Simulation sim = new Simulation(simulationFile, helper.Reporter);
            int nsteps = 5000;

            // temperature storage
            float[] instantaneousTemperatures = new float[nsteps];

            // define a set of equilibrium temperatures to test the system at
            float[] equlibriumTemps = new float[3] { 50.0f, 300.0f,  1000.0f};

            // tolerance within two number are condisdered equal
            float tolerance = 1E-2f;

            foreach (float equTemp in equlibriumTemps)
            {
                // equlibrate system
                (sim.ActiveSystem as AtomicSystem).Thermostat.EquilibriumTemperature = equTemp;
                sim.Run(nsteps);

                for (int i = 0; i < nsteps; i++)
                {
                    sim.Run(1);
                    Vector2 keTemp = sim.ActiveSystem.CalculateKineticEnergyAndTemperature();
                    instantaneousTemperatures[i] = keTemp[1];
                }
                // calculate avarage
                float verifyTemp = instantaneousTemperatures.Average();

                // calculate deviation (we know the temperature is not supposed to be zero but for sake of completeness this case is included)
                if (equTemp == 0.0f)
                {
                    Assert.AreEqual(equTemp, verifyTemp, tolerance);
                }
                else
                {
                    float ratio = verifyTemp / equTemp;
                    Assert.AreEqual(1.0f, ratio, tolerance);
                }            
            }
        }
        
        [Test]
        public void TestAndersenThermostat()
        {
            string simulationFile = "^/Assets/Simulations/TestSims/BuckyballsAndersen.xml";
            Simulation sim = new Simulation(simulationFile, helper.Reporter);
            int nsteps = 5000;

            // temperature storage
            float[] instantaneousTemperatures = new float[nsteps];

            // define a set of equilibrium temperatures to test the system at
            float[] equilibriumTemperatures = new float[3] { 50.0f, 300.0f,  1000.0f};

            // deviation allowed 
            float tolerance = 0.05f;

            
            foreach (float equTemp in equilibriumTemperatures)
            {
                helper.Reporter?.PrintNormal($"Testing temperature {equTemp}");
                (sim.ActiveSystem as AtomicSystem).Thermostat.EquilibriumTemperature = equTemp;
                // equilibrate system
                sim.Run(nsteps);

                for (int i = 0; i < nsteps; i++)
                {
                    sim.Run(1);
                    Vector2 keTemp = sim.ActiveSystem.CalculateKineticEnergyAndTemperature();
                    instantaneousTemperatures[i] = keTemp[1];
                }
                // calculate avarage
                float verifyTemp = instantaneousTemperatures.Average();

                float ratio = verifyTemp / equTemp;
                Assert.AreEqual(1.0f, ratio, tolerance);
            }
        }
        */

        /// <summary>
        /// Tests that Simbox can successfully load a small MM3 structure.
        /// </summary>
        [Test]
        public void LoadingOfMm3SimulationTest()
        {
            string simFile = "^/Assets/Simulations/TestSims/Methane.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            float energy = sim.ActiveSystem.CalculateForceFields();
            //Check that the MM3 energy is as expected.
            Assert.AreEqual(4.844f, energy, 0.001f);
            sim.Dispose();
        }

        /// <summary>
        /// Tests that Simbox can successfully load a small protein structure.
        /// </summary>
        [Test]
        public void LoadingOfProteinFromPdbTest()
        {
            string simFile = "^/Assets/Simulations/TestSims/2ala.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            //Check that we get the right number of atoms and bonds.
            Assert.AreEqual(21, sim.ActiveTopology.Bonds.Count);
            Assert.AreEqual(22, sim.ActiveSystem.NumberOfParticles);
        }

        /// <summary>
        /// Tests that Simbox can successfully load the openmm plugin and calculate the correct energy.
        /// </summary>
        //TODO move this to openmm plugin. 
        /*
        [Test]
        public void OpenMMTest()
        {
            string simFile = "^/Assets/Simulations/TestSims/XML/2ala.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);

            float energy = sim.ActiveSystem.CalculateForceFields();
            double diff = Math.Abs(-114.0986 - energy);
            if(diff > 0.001)
                Assert.Fail($"Energy is not the same as the reference value, difference of {diff} kJ/mol.");
        }
        */
        
        /// <summary>
        /// Tests that we can successfully load a structure with subgroups.
        /// </summary>
        [Test]
        public void LoadingOfSubResiduesTest()
        {
            string simFile = "^/Assets/Simulations/TestSims/Heroin.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            //Check we get the right number of bonds for heroin.
            Assert.AreEqual(54, sim.ActiveTopology.Bonds.Count);
            sim.Dispose();
        }

		/// <summary>
		/// Tests that we can successfully load a structure that uses non unique atom names.
		/// </summary>
        /// <remarks>
        /// This is a test for backwards compatibility.
        /// </remarks>
		[Test]
		public void LoadingOfNonUniqueAtomNamesTest()
		{
			string simFile = "^/Assets/Simulations/TestSims/AceticAcid.xml";
			Simulation sim = new Simulation(simFile, helper.Reporter);
            //TODO check number of bonds/potential energy.
			sim.Dispose();
		}

        [Test]
        public void ResetTest()
        {
            string simulationFile = "^/Assets/Simulations/TestSims/Heroin.xml";

            Simulation sim = new Simulation(simulationFile, helper.Reporter);

            SystemProperties originalProperties = sim.SimulationFile.SimulationSettings.Clone();
            ParticleCollection originalState = sim.ActiveSystem.Particles.Clone();

            sim.Reset();

            Assert.IsTrue(originalProperties.IntegratorProperties.Equals(sim.ActiveSystem.Integrator.GetProperties()));
            Dictionary<string, IThermostatProperties> newStats = new Dictionary<string, IThermostatProperties>();

            if (sim.ActiveSystem is AtomicSystem system)
            {
                if (system.Thermostat != null) newStats.Add(system.Thermostat.GetProperties().Name, system.Thermostat.GetProperties());
                if (system.Barostat != null) newStats.Add(system.Barostat.GetProperties().Name, system.Barostat.GetProperties());
            }

            foreach (string originalStat in originalProperties.ThermostatProperties.Keys)
            {
                Assert.IsTrue(originalProperties.ThermostatProperties[originalStat].Equals(newStats[originalStat]));
            }

            Assert.IsTrue(originalProperties.SimBoundary.Equals(sim.ActiveSystem.Boundary));
        }
        
        [Test]
        public void TestExternalPotentialEnergy()
        {
            //spawn a simulation in which the methane is always in the same spot. 
            string simFile = "^/Assets/Simulations/TestSims/MethaneInstance.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            float energy = sim.ActiveSystem.CalculateForceFields();
            //Check that the MM3 energy is as expected.
            Assert.AreEqual(4.8440485f, energy, 0.0001f);
            float externalEnergy = ((AtomicSystem) sim.ActiveSystem).ExternalPotentialEnergy; 
            Assert.AreEqual(0f, externalEnergy, 0.0000001f);
            Interaction interaction = new Interaction(Vector3.Zero);
            List<Interaction> interactions = new List<Interaction>();
            interactions.Add(interaction);
            sim.ActiveSystem.Interactions.SetInteractions(interactions);
            energy = sim.ActiveSystem.CalculateForceFields();
            Assert.AreEqual(4.8440485f, energy, 0.0001f);
            externalEnergy = ((AtomicSystem) sim.ActiveSystem).ExternalPotentialEnergy; 
            Assert.AreEqual(-12010.7002, externalEnergy, 0.1f);
            sim.Dispose();
            
            
        }
    }
}