﻿using Nano;
using NUnit.Framework;
using SlimMath;

namespace Simbox.MD.Tests
{
    [TestFixture]
    public class SimulationBoundaryTests
    {
        [Test]
        public void EqualsTest()
        {
            SimulationBoundary b1 = new SimulationBoundary(new BoundingBox(new Vector3(-2f), new Vector3(2f)), true, true, 1f);
            SimulationBoundary b2 = new SimulationBoundary(new BoundingBox(new Vector3(-2f), new Vector3(2f)), true, true, 1f);

            Assert.IsTrue(b1.Equals(b2));

            b2.SetSimulationBox(new BoundingBox(new Vector3(-3f), new Vector3(3f)));
            Assert.IsFalse(b1.Equals(b2));

            b2 = new SimulationBoundary(new BoundingBox(new Vector3(-2f), new Vector3(2f)), false, false);
            Assert.IsFalse(b1.Equals(b2));
        }

        [Test]
        public void SimulationBoundaryCloneTest()
        {
            SimulationBoundary b1 = new SimulationBoundary(new BoundingBox(new Vector3(-2f), new Vector3(2f)), true, true, 1f);
            SimulationBoundary b2 = (SimulationBoundary)b1.Clone();
            Assert.IsTrue(b1.Equals(b2));
        }

        [Test]
        public void GetPeriodicDifferenceTest()
        {
            SimulationBoundary b = new SimulationBoundary(new BoundingBox(new Vector3(-2f), new Vector3(2f)), true);
            Vector3 pos = new Vector3(0.5f, 0.5f, 0.5f);
            b.GetPeriodicDifference(ref pos);
            Assert.AreEqual(pos, new Vector3(0.5f, 0.5f, 0.5f));

            pos = new Vector3(2.5f, 2.5f, 2.5f);
            b.GetPeriodicDifference(ref pos);
            Assert.AreEqual(pos, new Vector3(-1.5f, -1.5f, -1.5f));

            pos = new Vector3(-2.5f, -2.5f, -2.5f);
            b.GetPeriodicDifference(ref pos);
            Assert.AreEqual(pos, new Vector3(1.5f, 1.5f, 1.5f));
        }

        [Test]
        public void GetPeriodicBoxVectorsTest()
        {
            SimulationBoundary b = new SimulationBoundary(new BoundingBox(new Vector3(-3f, -2f, -1f), new Vector3(3f, 2f, 1f)), true);
            Vector3[] periodicBoxVectors = b.GetPeriodicBoxVectors();
            Assert.AreEqual(new Vector3(6, 0, 0), periodicBoxVectors[0]);
            Assert.AreEqual(new Vector3(0, 4, 0), periodicBoxVectors[1]);
            Assert.AreEqual(new Vector3(0, 0, 2), periodicBoxVectors[2]);
        }

        [Test]
        public void SetDefaultsTest()
        {
            SimulationBoundary b = new SimulationBoundary(new BoundingBox(-new Vector3(1f), new Vector3(1f)), true);
            b.SetDefaults();
            BoundingBox defaultBox = new BoundingBox(-10000f, -10000f, -10000f, 10000f, 10000f, 10000f);
            SimulationBoundary b2 = new SimulationBoundary(defaultBox);
            Assert.IsTrue(b.Equals(b2));
        }

        [Test]
        public void SetSimulationBoxTest()
        {
            SimulationBoundary b = new SimulationBoundary(new BoundingBox(-new Vector3(1f), new Vector3(1f)), false, true, 1f);

            //Resize the box.
            b.SetSimulationBox(new BoundingBox(new Vector3(-2f), new Vector3(2f)));

            //Create a new box of the new size and check they are the same.
            SimulationBoundary b2 = new SimulationBoundary(new BoundingBox(-new Vector3(2f), new Vector3(2f)), false, true, 1f);
            Assert.IsTrue(b.Equals(b2));

        }

        [Test]
        public void SetDesiredBoxTest()
        {
            SimulationBoundary b = new SimulationBoundary(new BoundingBox(-new Vector3(1f), new Vector3(1f)), false, true, 1f);

            BoundingBox newBox = new BoundingBox(new Vector3(-0.5f), new Vector3(0.5f));
            b.SetDesiredBox(newBox);
            Assert.AreEqual(newBox, b.DesiredSimulationBox);

            //Trying to set the box smaller than the minimum volume should just clamp to minimum.
            newBox = new BoundingBox(new Vector3(-0.1f), new Vector3(0.1f));
            b.SetDesiredBox(newBox);
            BoundingBox minimumBox = MathUtilities.CreateCube(1f);
            Assert.AreEqual(minimumBox, b.DesiredSimulationBox);
        }
    }
}