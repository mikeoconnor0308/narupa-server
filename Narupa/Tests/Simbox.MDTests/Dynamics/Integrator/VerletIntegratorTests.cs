﻿using System;
using Nano;
using NUnit.Framework;
using Simbox.MD.Dynamics.Integrator;
using SlimMath;

namespace Simbox.MD.Tests.Dynamics.Integrator
{
    [TestFixture()]
    public class VerletIntegratorTests
    {
        private TestHelper testHelper;        

        [SetUp]
        public void Initialize()
        {
            testHelper = new TestHelper();
        }
        
        //TODO figure out how what needs to be done for this to pass. It's just oscillating. 
        /*
        [Test()]
        public void EnergyConservationTest()
        {
            Simulation sim = new Simulation(Helper.ResolvePath("^/Assets/Simulations/TestSims/Buckyballs_NVE.xml"), testHelper.Reporter);
            sim.ActiveSystem.Integrator = new VerletIntegrator(sim.ActiveSystem as AtomicSystem, 0.0005f, false, null);
            var integrator = sim.ActiveSystem.Integrator;
            var system = sim.ActiveSystem;
            int numSteps = 1000;
            var peList = new System.Collections.Generic.List<float>(numSteps);
            var keList = new System.Collections.Generic.List<float>(numSteps);
            float pe = system.CalculateForceFields();
            float ke = system.CalculateKineticEnergyAndTemperature()[0];
            peList.Add(pe);
            keList.Add(ke);

            float resultPE = 0;
            float resultKE = 0;
            for (int i = 0; i < numSteps; i++)
            {
                integrator.Propagate(1);
                resultPE = system.CalculateForceFields();
                resultKE = ((AtomicSystem) system).CalculateKineticEnergyAndTemperature()[0];
                peList.Add(resultPE);
                keList.Add(resultKE);
                System.Console.WriteLine($"{resultPE}, {resultKE}, {resultPE + resultKE}");
            }

            float total = pe + ke;
            float resultTotal = resultKE + resultPE;
            float diff = Math.Abs(total - resultTotal) / Math.Abs(total);
            if (diff  > 0.001f)
            {
                Assert.Fail($"Energy not conserved, change of {diff * 100f}%");
            }

        }
        */
        
    }
}