﻿using System;
using Nano;
using NUnit.Framework;
using Simbox.MD.Dynamics.Integrator;
using SlimMath;

namespace Simbox.MD.Tests.Dynamics.Integrator
{
    [TestFixture()]
    public class VelocityVerletTests
    {
        private TestHelper testHelper;        

        [SetUp]
        public void Initialize()
        {
            testHelper = new TestHelper();
        }

        [Test()]
        public void PropagatePositionNaNTest()
        {
            Simulation sim = new Simulation(Helper.ResolvePath("^/Assets/Simulations/TestSims/XML/Methane.xml"), testHelper.Reporter);

            VelocityVerletIntegrator v = new VelocityVerletIntegrator();
            Vector3 pos = sim.ActiveSystem.Particles.Position[0];
            sim.ActiveSystem.Particles.Position[0] = new Vector3(float.NaN);
            try
            {
                v.Propagate(1);
                Assert.Fail("Exception should have been thrown!");
            }
            catch (Exception)
            {
            }
            sim.ActiveSystem.Particles.Position[0] = new Vector3(float.PositiveInfinity);
            try
            {
                v.Propagate(1);
                Assert.Fail("Exception should have been thrown!");
            }
            catch
            {
            }
            sim.ActiveSystem.Particles.Position[0] = new Vector3(float.NegativeInfinity);
            try
            {
                v.Propagate(1);
                Assert.Fail("Exception should have been thrown!");
            }
            catch
            {
            }
            sim.ActiveSystem.Particles.Position[0] = pos;
            sim.ActiveSystem.Particles.Force[1] = new Vector3(float.NaN);
            try
            {
                v.Propagate(1);
                Assert.Fail("Exception should have been thrown!");
            }
            catch
            {
            }
        }
        
        [Test()]
        public void EnergyConservationTest()
        {
            Simulation sim = new Simulation(Helper.ResolvePath("^/Assets/Simulations/TestSims/Buckyballs_NVE.xml"), testHelper.Reporter);

            var system = sim.ActiveSystem;
            int numSteps = 1000;
            var peList = new System.Collections.Generic.List<float>(numSteps);
            var keList = new System.Collections.Generic.List<float>(numSteps);
            float pe = system.CalculateForceFields();
            float ke = system.CalculateKineticEnergyAndTemperature()[0];
            
            peList.Add(pe);
            keList.Add(ke);

            float resultPE = 0;
            float resultKE = 0;
            for (int i = 0; i < numSteps; i++)
            {
                sim.Run(1);
                resultPE = system.PotentialEnergy;
                resultKE = system.CalculateKineticEnergyAndTemperature()[0];
                peList.Add(resultPE);
                keList.Add(resultKE);
            }

            float total = pe + ke;
            float resultTotal = resultKE + resultPE;
            float diff = Math.Abs(total - resultTotal) / Math.Abs(total);
            if (diff  > 0.001f)
            {
                Assert.Fail($"Energy not conserved, change of {diff * 100f}%");
            }

        }
        
    }
}