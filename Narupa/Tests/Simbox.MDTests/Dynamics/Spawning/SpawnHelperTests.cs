﻿using Nano.Science.Simulation.Spawning;
using NUnit.Framework;
using SlimMath;

namespace Simbox.MD.Tests.Dynamics.Spawning
{
    [TestFixture()]
    public class SpawnHelperTests
    {
        [Test()]
        public void BoxContainsBoxTest()
        {
            BoundingBox A = new BoundingBox(Vector3.Zero, new Vector3(2, 2, 2));
            BoundingBox B = new BoundingBox(new Vector3(1, 1, 1), new Vector3(1.5f, 1.5f, 1.5f));
            Assert.IsTrue(SpawnHelper.BoxContainsBox(A, B));
            Assert.IsFalse(SpawnHelper.BoxContainsBox(B, A));
        }
    }
}