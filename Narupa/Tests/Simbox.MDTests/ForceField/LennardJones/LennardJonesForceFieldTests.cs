﻿using System;
using System.Collections.Generic;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using NUnit.Framework;
using Simbox.MD.ForceField.LennardJones;
using SlimMath;

namespace Simbox.MD.Tests.ForceField.LennardJones
{
    [TestFixture()]
    public class LennardJonesForceFieldTests
    {
        private TestHelper Helper;
        private BoundingBox testBox = new BoundingBox(Vector3.One * -1000f, Vector3.One * 1000f);

        [SetUp]
        public void Initialize()
        {
            Helper = new TestHelper();
        }

        private const double tolerance = 1.0e-03;

        [Test()]
        public void CalculateForceFieldTest()
        {
            List<float> particle_sigmas = new List<float>();
            particle_sigmas.Add(0.182f);
            particle_sigmas.Add(0.204f);
            particle_sigmas.Add(0.182f);
            particle_sigmas.Add(0.162f);

            List<float> particle_epsilons = new List<float>();
            particle_epsilons.Add(0.059f * Units.KJPerKCal);
            particle_epsilons.Add(0.027f * Units.KJPerKCal);
            particle_epsilons.Add(0.059f * Units.KJPerKCal);
            particle_epsilons.Add(0.02f * Units.KJPerKCal);

            List<LennardJonesTerm> ljTerms = new List<LennardJonesTerm>();
            ljTerms.Add(new LennardJonesTerm(1.82f * Units.NmPerAngstrom, 0.059f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(2.04f * Units.NmPerAngstrom, 0.027f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(1.82f * Units.NmPerAngstrom, 0.059f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(1.62f * Units.NmPerAngstrom, 0.02f * Units.KJPerKCal));

            List<Vector3> positions = new List<Vector3>();
            positions.Add(new Vector3(-0.2f, 0f, 0f));
            positions.Add(new Vector3(0f, 0f, 0f));
            positions.Add(new Vector3(0.2f, 0f, 0f));
            positions.Add(new Vector3(0, 0.2f, 0f));

            List<Element> elements = new List<Element>();
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);

            InstantiatedTopology flatTopology = new InstantiatedTopology();
            flatTopology.Positions.AddRange(positions);
            flatTopology.Elements.AddRange(elements);
            SystemProperties properties = new SystemProperties();
            properties.InitialiseDefaultProperties();
            properties.SimBoundary = new SimulationBoundary(testBox);
            AtomicSystem system = new AtomicSystem(flatTopology, properties, Helper.Reporter, false);

            LennardJonesForceField forcefield = new LennardJonesForceField(flatTopology, properties, ljTerms, 0.5f);

            List<Vector3> forces = new List<Vector3>(positions.Count);
            List<Vector3> reference_forces = new List<Vector3>(positions.Count);
            for (int ii = 0; ii < positions.Count; ii++)
            {
                forces.Add(Vector3.Zero);
                reference_forces.Add(Vector3.Zero);
            }

            float ref_V = 0f;
            Vector3 diff;
            float epsilon;
            float sigma;
            float r;
            float dVdr;
            Vector3 force;
            float tempA;
            float tempB;
            for (int ii = 0; ii < positions.Count; ii++)
            {
                for (int jj = 0; jj < positions.Count; jj++)
                {
                    if (ii == jj) continue;

                    diff = positions[ii] - positions[jj];

                    epsilon = (float)Math.Sqrt(particle_epsilons[ii] * particle_epsilons[jj]);
                    sigma = (particle_sigmas[ii] + particle_sigmas[jj]) / 2;

                    r = diff.Length;
                    tempA = 4f * epsilon * (float)Math.Pow(sigma, 12);
                    tempB = 4f * epsilon * (float)Math.Pow(sigma, 6);
                    ref_V += 0.5f * (tempA / (float)Math.Pow(r, 12) - tempB / (float)Math.Pow(r, 6));
                    dVdr = -12f * (tempA / (float)Math.Pow(r, 13)) + 6f * (tempB / (float)Math.Pow(r, 7));
                    //Apply chain rule
                    force = -diff / r * dVdr;

                    reference_forces[ii] += force;
                }
            }

            float V = forcefield.CalculateForceField(system, forces);
            Assert.AreEqual(ref_V, V, tolerance);

            for (int ii = 0; ii < positions.Count; ii++)
            {
                Assert.AreEqual(reference_forces[ii].X, forces[ii].X, tolerance);
                Assert.AreEqual(reference_forces[ii].Y, forces[ii].Y, tolerance);
                Assert.AreEqual(reference_forces[ii].Z, forces[ii].Z, tolerance);
            }
        }

        [Test()]
        public void CalculateForceFieldLJCutOffTest()
        {
            List<float> particle_sigmas = new List<float>();
            particle_sigmas.Add(1.82f * Units.NmPerAngstrom);
            particle_sigmas.Add(2.04f * Units.NmPerAngstrom);
            particle_sigmas.Add(1.82f * Units.NmPerAngstrom);
            particle_sigmas.Add(1.62f * Units.NmPerAngstrom);

            List<float> particle_epsilons = new List<float>();
            particle_epsilons.Add(0.059f * Units.KJPerKCal);
            particle_epsilons.Add(0.027f * Units.KJPerKCal);
            particle_epsilons.Add(0.059f * Units.KJPerKCal);
            particle_epsilons.Add(0.02f * Units.KJPerKCal);

            List<LennardJonesTerm> ljTerms = new List<LennardJonesTerm>();
            ljTerms.Add(new LennardJonesTerm(1.82f * Units.NmPerAngstrom, 0.059f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(2.04f * Units.NmPerAngstrom, 0.027f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(1.82f * Units.NmPerAngstrom, 0.059f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(1.62f * Units.NmPerAngstrom, 0.02f * Units.KJPerKCal));

            List<Vector3> positions = new List<Vector3>();
            positions.Add(new Vector3(-0.2f, 0f, 0f));
            positions.Add(new Vector3(0f, 0f, 0f));
            positions.Add(new Vector3(0.2f, 0f, 0f));
            positions.Add(new Vector3(0, 0.55f, 0f)); ;

            List<Element> elements = new List<Element>();
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);

            InstantiatedTopology flatTopology = new InstantiatedTopology();
            flatTopology.Positions.AddRange(positions);
            flatTopology.Elements.AddRange(elements);
            SystemProperties properties = new SystemProperties();
            properties.InitialiseDefaultProperties();
            properties.SimBoundary = new SimulationBoundary(testBox);
            properties.SimBoundary = new SimulationBoundary(testBox);
            AtomicSystem system = new AtomicSystem(flatTopology, properties, Helper.Reporter, false);

            LennardJonesForceField forcefield = new LennardJonesForceField(flatTopology, properties, ljTerms, 0.5f);

            List<Vector3> forces = new List<Vector3>(positions.Count);
            List<Vector3> reference_forces = new List<Vector3>(positions.Count);
            for (int ii = 0; ii < positions.Count; ii++)
            {
                forces.Add(Vector3.Zero);
                reference_forces.Add(Vector3.Zero);
            }

            float ref_V = 0f;
            Vector3 diff;
            float epsilon;
            float sigma;
            float r;
            float dVdr;
            Vector3 force;
            float tempA;
            float tempB;
            for (int ii = 0; ii < positions.Count; ii++)
            {
                for (int jj = 0; jj < positions.Count; jj++)
                {
                    if (ii == jj) continue;

                    diff = positions[ii] - positions[jj];

                    epsilon = (float)Math.Sqrt(particle_epsilons[ii] * particle_epsilons[jj]);
                    sigma = (particle_sigmas[ii] + particle_sigmas[jj]) / 2;

                    r = diff.Length;
                    if (r > 0.5f)
                        continue;
                    tempA = 4f * epsilon * (float)Math.Pow(sigma, 12);
                    tempB = 4f * epsilon * (float)Math.Pow(sigma, 6);
                    ref_V += 0.5f * (tempA / (float)Math.Pow(r, 12) - tempB / (float)Math.Pow(r, 6));
                    dVdr = -12f * (tempA / (float)Math.Pow(r, 13)) + 6f * (tempB / (float)Math.Pow(r, 7));
                    //Apply chain rule
                    force = -diff / r * dVdr;

                    reference_forces[ii] += force;
                }
            }

            float V = forcefield.CalculateForceField(system, forces);
            Assert.AreEqual(ref_V, V, tolerance);

            for (int ii = 0; ii < positions.Count; ii++)
            {
                Assert.AreEqual(reference_forces[ii].X, forces[ii].X, tolerance);
                Assert.AreEqual(reference_forces[ii].Y, forces[ii].Y, tolerance);
                Assert.AreEqual(reference_forces[ii].Z, forces[ii].Z, tolerance);
            }
        }

        [Test()]
        public void CalculateForceFieldVDWExceptionTest()
        {
            List<float> particle_sigmas = new List<float>();
            particle_sigmas.Add(1.82f * Units.NmPerAngstrom);
            particle_sigmas.Add(2.04f * Units.NmPerAngstrom);
            particle_sigmas.Add(1.82f * Units.NmPerAngstrom);
            particle_sigmas.Add(1.62f * Units.NmPerAngstrom);

            List<float> particle_epsilons = new List<float>();
            particle_epsilons.Add(0.059f * Units.KJPerKCal);
            particle_epsilons.Add(0.027f * Units.KJPerKCal);
            particle_epsilons.Add(0.059f * Units.KJPerKCal);
            particle_epsilons.Add(0.02f * Units.KJPerKCal);

            List<LennardJonesTerm> ljTerms = new List<LennardJonesTerm>();
            ljTerms.Add(new LennardJonesTerm(1.82f * Units.NmPerAngstrom, 0.059f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(2.04f * Units.NmPerAngstrom, 0.027f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(1.82f * Units.NmPerAngstrom, 0.059f * Units.KJPerKCal));
            ljTerms.Add(new LennardJonesTerm(1.62f * Units.NmPerAngstrom, 0.02f * Units.KJPerKCal));

            List<Vector3> positions = new List<Vector3>();
            positions.Add(new Vector3(-0.2f, 0f, 0f));
            positions.Add(new Vector3(0f, 0f, 0f));
            positions.Add(new Vector3(0.2f, 0f, 0f));
            positions.Add(new Vector3(0, 0.2f, 0f));

            List<Element> elements = new List<Element>();
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);
            elements.Add(Element.Hydrogen);

            InstantiatedTopology flatTopology = new InstantiatedTopology();
            flatTopology.Positions.AddRange(positions);
            flatTopology.Elements.AddRange(elements);
            SystemProperties properties = new SystemProperties();
            properties.InitialiseDefaultProperties();
            properties.SimBoundary = new SimulationBoundary(testBox);
            AtomicSystem system = new AtomicSystem(flatTopology, properties, Helper.Reporter, false);

            LennardJonesForceField forcefield = new LennardJonesForceField(flatTopology, properties, ljTerms, 0.5f);

            //Make particle 0 nonreactive.
            forcefield.AddException(0, 1);
            forcefield.AddException(0, 2);
            forcefield.AddException(0, 3);

            List<Vector3> forces = new List<Vector3>(positions.Count);
            List<Vector3> reference_forces = new List<Vector3>(positions.Count);
            for (int ii = 0; ii < positions.Count; ii++)
            {
                forces.Add(Vector3.Zero);
                reference_forces.Add(Vector3.Zero);
            }

            float ref_V = 0f;
            Vector3 diff;
            float epsilon;
            float sigma;
            float r;
            float dVdr;
            Vector3 force;
            float tempA;
            float tempB;
            //Reference skips atom 0.
            for (int ii = 1; ii < positions.Count; ii++)
            {
                for (int jj = 1; jj < positions.Count; jj++)
                {
                    if (ii == jj) continue;

                    diff = positions[ii] - positions[jj];

                    epsilon = (float)Math.Sqrt(particle_epsilons[ii] * particle_epsilons[jj]);
                    sigma = (particle_sigmas[ii] + particle_sigmas[jj]) / 2;

                    r = diff.Length;
                    tempA = 4f * epsilon * (float)Math.Pow(sigma, 12);
                    tempB = 4f * epsilon * (float)Math.Pow(sigma, 6);
                    ref_V += 0.5f * (tempA / (float)Math.Pow(r, 12) - tempB / (float)Math.Pow(r, 6));
                    dVdr = -12f * (tempA / (float)Math.Pow(r, 13)) + 6f * (tempB / (float)Math.Pow(r, 7));
                    //Apply chain rule
                    force = -diff / r * dVdr;

                    reference_forces[ii] += force;
                }
            }

            float V = forcefield.CalculateForceField(system, forces);
            Assert.AreEqual(ref_V, V, tolerance);

            for (int ii = 0; ii < positions.Count; ii++)
            {
                Assert.AreEqual(reference_forces[ii].X, forces[ii].X, tolerance);
                Assert.AreEqual(reference_forces[ii].Y, forces[ii].Y, tolerance);
                Assert.AreEqual(reference_forces[ii].Z, forces[ii].Z, tolerance);
            }
        }
    }
}