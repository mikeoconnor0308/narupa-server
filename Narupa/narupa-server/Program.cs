﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MDServer;
using Nano;
using Nano.Console.Utils;
using Nano.Loading;
using Nano.Server;
using Rug.Cmd;
using AssemblyRoot = Nano.AssemblyRoot;

namespace Narupa.Server
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            IReporter reporter;

            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                case PlatformID.MacOSX:
                    reporter = AnsiCmd.CreateDefaultConsoleContext();
                    AnsiCmd.ShowFullErrors = true;
                    break; 
                    
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.Win32NT:
                case PlatformID.WinCE:
                    reporter = RugCmd.CreateDefaultConsoleContext();
                    RugCmd.ShowFullErrors = true;
                    break; 

                case PlatformID.Xbox:
                default:
                    throw new ArgumentOutOfRangeException();
            }
            

            try
            {
                RugCmd.CursorVisible = true;                

                //string version = $"v{typeof(Program).Assembly.GetName().Version.Major}.{typeof(Program).Assembly.GetName().Version.Minor}";
                string version = $"v{typeof(Program).Assembly.GetName().Version.Major}.{typeof(Program).Assembly.GetName().Version.Minor} ({typeof(Program).Assembly.GetName().Version.Build})";
                string title = $"{typeof(Program).Assembly.GetName().Name} {version}";

                // Load plugins 
                try
                {
                    //This is where all assemblies are scanned for ILoadable content.
                    LoadManager.ScanAssembly(typeof(Server).Assembly);
                    LoadManager.ScanAssembly(typeof(ServerConfig).Assembly);

                    LoadManager.ScanAssembly(typeof(Nano.AssemblyRoot).Assembly);
                    LoadManager.ScanAssembly(typeof(Nano.Server.AssemblyRoot).Assembly);
                    LoadManager.ScanAssembly(typeof(Nano.Science.Simulation.AssemblyRoot).Assembly);
                    LoadManager.ScanAssembly(typeof(Simbox.MD.AssemblyRoot).Assembly);

                    LoadManager.ScanAssembly(typeof(AssemblyRoot).Assembly);
                            
                    LoadManager.ScanAssembly(typeof(SimulationService).Assembly);
                    
                    Nano.Server.Plugins.PluginLoader.ScanPluginDirectory(reporter);
                }
                catch (Exception ex)
                {
                    reporter.PrintException(ex, "Exception while enumerating loadable types");

                    return;
                }

                // load config 

                Server server = new Server(title);

                try
                {
                    server.Load(reporter);
                }
                catch (Exception ex)
                {
                    reporter.PrintException(ex, "Exception while loading server configuration");

                    return;
                }

                try
                {
                    // create command line arguments
                    ArgumentParser parser = new ArgumentParser(typeof(Program).Assembly.GetName().Name, title);

                    EnumSwitch verbositySwitch = new EnumSwitch("Verbosity", "Console output verbosity.", typeof(ReportVerbosity));

                    server.RegisterArguments(parser);

                    parser.Add("-", "Verbosity", verbositySwitch);

                    // parse command line                
                    reporter.ReportVerbosity = ReportVerbosity.Debug;

                    parser.Parse(args);

                    if (verbositySwitch.Defined == true)
                    {
                        reporter.ReportVerbosity = (ReportVerbosity) verbositySwitch.Value;
                    }

                    server.ValidateArguments(reporter);
                }
                catch (Exception ex)
                {
                    reporter.PrintError(ex.Message);
                }

                CancellationTokenSource cancelSource = new CancellationTokenSource();

                CancellationToken cancel = cancelSource.Token;

                Console.CancelKeyPress += (sender, e) =>
                {
                    e.Cancel = true;
                    cancelSource.Cancel();
                };

                reporter.PrintWarning(ReportVerbosity.ExceptionsOnly, "Press CTRL+C to exit");

                // run 
                try
                {
                    server.ReportManager.Initialize(server, reporter);
                    server.SecurityProvider.Initialize(server, reporter);

                    List<Task> allTasks = new List<Task>
                    {
                        server.Monitor.Start(server, cancel),
                        server.Service.Start(server, cancel),
                        server.SecurityProvider.Start(server, cancel),
                        server.Discovery.Start(server, cancel)
                    };


                    Task.WaitAny((from task in allTasks where task != null select task).ToArray());
                }
                catch (Exception ex)
                {
                    reporter.PrintException(ex, "Server exception");
                }
                finally
                {
                    if (cancelSource.IsCancellationRequested == false)
                    {
                        cancelSource.Cancel();
                    }
                }
            }
            finally
            {
                Task.Delay(1000).Wait();
            }
        }
    }
}