﻿/*  Copyright (C) 2011 by David R. Glowacki and contributors
This file is a part of
fOOmd: Molecular Dynamics package

fOOmd is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fOOmd is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.

You should have received a copy of the GNU Lesser Public License
along with fOOmd.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using Nano;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Simbox.MD.ForceField.MM3.Load;
using SlimMath;

namespace Simbox.MD.ForceField.MM3
{
    /// <summary>
    /// Class implementing the MM3 force field.
    /// </summary>
    public partial class MM3ForceField : IForceField
    {
        #region Physical Constants

        private const float BondUnit = 71.94f;
        private const float BondCubic = -2.55f;
        private const float BondQuartic = 3.793125f;
        private const float AngleUnit = 0.02191418f;
        private const float AngleCubic = -0.014f;
        private const float AngleQuartic = 0.000056f;
        private const float AnglePentic = -0.0000007f;
        private const float AngleSextic = 0.000000022f;
        private const float RadToDegrees = 57.2957795f;
        private const float OPBUnit = 0.043844f;
        private const float TorsionUnit = 0.5f;

        /// <summary> Maximum allowed energy in VDW interaction. </summary>
        private const float MAX_ENERGY = 1.0e8f;

        #endregion Physical Constants

        #region Public Properties

        /// <summary>
        /// Name of ForceField
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return "MM3"; }
        }

        /// <summary>
        /// Gets the out of plane bend energy.
        /// </summary>
        /// <value>The out of plane bend energy.</value>
        public float OutOfPlaneBendEnergy { get; private set; }

        //TODO these could be moved into the structs?
        /// <summary> Energy in all bonds </summary>
        public float BondEnergy { get; private set; }

        /// <summary> Energy in all angles </summary>
        public float AngleEnergy { get; private set; }

        /// <summary> Energy in all torsions </summary>
        public float TorsionEnergy { get; private set; }

        /// <summary> Energy in all VDW interactions </summary>
        public float VDWEnergy { get; private set; }

        /// <summary> Energy in all morse potentials </summary>
        public float MorseEnergy { get; private set; }

        /// <summary> Cartesian forces for this forcefield </summary>
        public List<Vector3> Forces;

        /// <summary> Total Energy for this forcefield </summary>
        public float TotalEnergy { get; private set; }

        internal MM3Topology Topology;
        internal IReporter Reporter;

        /// <summary>
        /// Whether this forcefield is applying periodic boundary conditions.
        /// </summary>
        public bool PeriodicBoundary;

        /// <summary>
        /// The simulation boundary.
        /// </summary>
        public ISimulationBoundary SimulationBoundary;

        private int numberOfParticles;

        #endregion Public Properties

        /// <summary>
        /// Calculates the force field, and returns the potential energy.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="forces">The forces.</param>
        /// <returns>System.Single.</returns>
        /// <exception cref="ArgumentException">The number of particles in the system and in the forcefield do not match!
        /// or
        /// Force vector argument is not same length as position vector</exception>
        public float CalculateForceField(IAtomicSystem system, List<Vector3> forces)
        {
            SimulationBoundary = system.Boundary;
            numberOfParticles = system.NumberOfParticles;

            ResetForcesAndEnergy();
            List<Vector3> positions = system.Particles.Position;
            TotalEnergy += CalculateBondForces(positions, Topology.Bonds);
            TotalEnergy += CalculateMorseForces(positions, Topology.MorseTerms);
            TotalEnergy += CalculateAngleForces(positions, Topology.Angles);
            TotalEnergy += CalculateTorsionForces(positions, Topology.Torsions);
            TotalEnergy += CalculateOPBendForces(positions, Topology.OPBends);
            AddForces(forces);
            //Convert from MM3's kcal/mol to our kJ/mol energy unit.
            return TotalEnergy * Units.KJPerKCal;
        }

        /// <summary>
        /// Disposes this instance.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MM3ForceField"/> class.
        /// </summary>
        public MM3ForceField()
        {
            Forces = new List<Vector3>();
            return;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MM3ForceField"/> class.
        /// </summary>
        /// <param name="topology">The topology.</param>
        public MM3ForceField(MM3Topology topology)
        {
            Topology = topology;
            Forces = new List<Vector3>(topology.NumberOfParticles);
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                Forces.Add(new Vector3(0));
            }
        }
    }
}