﻿/*  Copyright (C) 2011 by David R. Glowacki and contributors
This file is a part of
fOOmd: Molecular Dynamics package

fOOmd is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fOOmd is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.

You should have received a copy of the GNU Lesser Public License
along with fOOmd.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using MathNet.Numerics.Distributions;
using Nano;
using Nano.Science;
using SlimMath;
// ReSharper disable InconsistentNaming

namespace Simbox.MD.ForceField.MM3
{
    partial class MM3ForceField
    {
        #region Reference MM3 Implementation

        ///<summary> Sets forces and energies to zero </summary>
        private void ResetForcesAndEnergy()
        {
            for (int i = 0; i < Forces.Count; i++)
            {
                Forces[i] = Vector3.Zero;
            }
            BondEnergy = 0f;
            AngleEnergy = 0f;
            TorsionEnergy = 0f;
            VDWEnergy = 0f;
            TotalEnergy = 0f;
            OutOfPlaneBendEnergy = 0f;
        }

        /// <summary>
        /// Applies the forces calculated by this forcefield to the atomic system, and converts them to kJ/mol*nm
        /// </summary>
        private void AddForces(List<Vector3> outputForces)
        {
            for (int i = 0; i < Forces.Count; i++)
            {
                //convert from kcal / (mol * angstrom) to kJ / (mol * nm).
                outputForces[i] += Forces[i] * Units.KJPerKCal * Units.AngstromsPerNm;
            }
        }

        /// <summary>
        /// Calculates the MM3 bond energy and forces and accumulates it to
        /// the atomic system.
        /// </summary>
        /// <param name="position">The positions.</param>
        /// <param name="bonds">MM3 Topology</param>
        /// <returns>System.Single.</returns>
        private float CalculateBondForces(List<Vector3> position, BondStruct bonds)
        {
            int i, j;
            float r, dr, V, dV, dVdr, drSquared, r0;

            Vector3 posi, posj, dist;

            for (int ii = 0; ii < bonds.Count; ii++)
            {
                //get indices of the atoms in the bond
                i = bonds.IndexI[ii];
                j = bonds.IndexJ[ii];

                posi = position[i];
                posj = position[j];
                //get distance between atoms in bonds
                dist = posi - posj;
                dist *= Units.AngstromsPerNm;

                //calculate the interatomic distance, r, and displacement from equilibrium, dr
                r = dist.Length;
                r0 = bonds.R0[ii];
                dr = r - r0;

                drSquared = dr * dr;

                V = BondUnit * bonds.FC[ii] * drSquared * (1.0f + BondCubic * dr + BondQuartic * drSquared);

                //calculate the gradient, dV/dr
                dVdr = 2.0f * BondUnit * bonds.FC[ii] * dr * (1.0f + 1.5f * BondCubic * dr + 2.0f * BondQuartic * drSquared);
                dV = dVdr / r;

                Forces[i] += -1.0f * dV * dist;
                Forces[j] += dV * dist;
                //keep track of the total bond energy in all the bonds
                BondEnergy += V;
            }
            return BondEnergy;
        }

        /// <summary>
        /// Morse potential calculation.
        /// </summary>
        /// <param name="position">Atomic positions</param>
        /// <param name="morse">Morse parameters</param>
        /// <returns></returns>
        private float CalculateMorseForces(List<Vector3> position, MorseStruct morse)
        {
            int i, j;
            float r, dr, energy, dV, dVdr, drSquared, alpha, r0;

            Vector3 posi, posj, dist;

            for (int morseIndex = 0; morseIndex < morse.Count; morseIndex++)
            {
                //get indices of the atoms in the bond
                i = morse.IndexI[morseIndex];
                j = morse.IndexJ[morseIndex];

                posi = position[i];
                posj = position[j];
                //get distance between atoms in morse
                dist = posi - posj;
                if (PeriodicBoundary)
                    SimulationBoundary.GetPeriodicDifference(ref dist);
                dist *= Units.AngstromsPerNm;
                //calculate the interatomic distance, r, and displacement from equilibrium, dr
                r = dist.Length;
                r0 = morse.R0[morseIndex];
                dr = r - r0;
                alpha = morse.Alpha[morseIndex];

                var exp = (float) Math.Exp(-alpha * dr);
                energy = BondUnit * morse.De[morseIndex] * (1 - 2f * exp + exp * exp);

                //calculate the gradient, wrt to distance.
                dVdr = 2.0f * BondUnit * alpha * morse.De[morseIndex] * (exp - exp * exp);
                dV = dVdr / r;

                Forces[i] += -1.0f * dV * dist;
                Forces[j] += dV * dist;
                //keep track of the total bond energy in all the morse
                MorseEnergy += energy;
            }
            return MorseEnergy;
        }

        /// <summary>
        /// Calculates the MM3 angle energy and forces and accumulates it to
        /// the atomic system.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="angles">MM3 angles</param>
        /// <returns>System.Single.</returns>
        private float CalculateAngleForces(List<Vector3> position, AngleStruct angles)
        {
            int i, j, k;
            for (int angleIndex = 0; angleIndex < angles.Count; ++angleIndex)
            {
                float V = 0;

                //get indices of the atoms in the bond
                i = angles.IndexI[angleIndex];
                j = angles.IndexJ[angleIndex];
                k = angles.IndexK[angleIndex];

                var ipos = position[i];
                var jpos = position[j];
                var kpos = position[k];

                Vector3 diffIJ = ipos - jpos;
                Vector3 diffKJ = kpos - jpos;
                diffIJ *= Units.AngstromsPerNm;
                diffKJ *= Units.AngstromsPerNm;

                diffIJ.Z = diffIJ.Z;

                diffKJ.X = diffKJ.X;
                diffKJ.Y = diffKJ.Y;
                diffKJ.Z = diffKJ.Z;

                float distIJSqr = diffIJ.LengthSquared;
                float distIKSqr = diffKJ.LengthSquared;


                Vector3 product = Vector3.Cross(diffKJ, diffIJ);
                float rp = product.Length; 
                float dot = Vector3.Dot(diffIJ, diffKJ);
                float cosine = dot / (float)Math.Sqrt(distIJSqr * distIKSqr);
                cosine = (float)Math.Min(1.0, Math.Max(-1.0, cosine));
                float angle = RadToDegrees * (float)Math.Acos(cosine);

                //Calculate the energy and derivative wrt time.
                float dt = angle - angles.Theta0[angleIndex];
                float dt2 = dt * dt;
                float dt3 = dt2 * dt;
                float dt4 = dt2 * dt2;
                V = AngleUnit * angles.FC[angleIndex] * dt2 * (1.0f + AngleCubic * dt + AngleQuartic * dt2 + AnglePentic * dt3 + AngleSextic * dt4);
                float dVddt = AngleUnit * angles.FC[angleIndex] * dt * RadToDegrees * (2.0f + 3.0f * AngleCubic * dt + 4.0f * AngleQuartic * dt2 + 5.0f * AnglePentic * dt3 + 6.0f * AngleSextic * dt4);

                float termi = -1.0f * dVddt / (distIJSqr * rp);
                float termk = dVddt / (distIKSqr * rp);
                
                //derivatives for each atom oin the angle.
                Vector3 derivI = termi * Vector3.Cross(diffIJ, product);
                Vector3 derivK = termk * Vector3.Cross(diffKJ, product);
                Vector3 derivJ = -1.0f * derivI - derivK;

                Forces[i] += -1f * derivI;
                Forces[j] += -1f * derivJ;
                Forces[k] += -1f * derivK;
                AngleEnergy += V;



            }
            return AngleEnergy;
        }

        private float CalculateTorsionForces(List<Vector3> position, TorsionStruct torsions)
        {
            int i, j, k, l;

            Vector3 f = new Vector3(0f);

            for (int ii = 0; ii < torsions.Count; ii++)
            {

                //get indices of the atoms in the bond
                i = torsions.IndexI[ii];
                j = torsions.IndexJ[ii]; ;
                k = torsions.IndexK[ii];
                l = torsions.IndexL[ii];

                Vector3 ba = position[j] - position[i];
                Vector3 cb = position[k] - position[j];
                Vector3 dc = position[l] - position[k];
                ba *= Units.AngstromsPerNm;
                cb *= Units.AngstromsPerNm;
                dc *= Units.AngstromsPerNm;

                Vector3 crossABC = Vector3.Cross(ba, cb);
                Vector3 crossBCD = Vector3.Cross(cb, dc);

                Vector3 crossAll = Vector3.Cross(crossABC, crossBCD);
                var crossABCLenSqr = crossABC.LengthSquared;
                var crossBCDLenSqr = crossBCD.LengthSquared;
                var crossLength = (float)Math.Sqrt(crossABCLenSqr * crossBCDLenSqr);

                var cbDist = (float)Math.Sqrt(cb.X * cb.X + cb.Y * cb.Y + cb.Z * cb.Z);
                var cosine = Vector3.Dot(crossABC, crossBCD) / crossLength;
                var sine = Vector3.Dot(cb, crossAll) / (cbDist * crossLength);

                Vector3 torsionTerm = torsions.V[ii];
                Vector3 cosTerm = new Vector3(1.0f, -1.0f, 1.0f);
                Vector3 sinTerm = new Vector3(0.0f, 0.0f, 0.0f);

                Vector3 cosVector;
                Vector3 sineVector; 
                cosVector.X = cosine;
                cosVector.Y = cosine * cosine - sine * sine;

                sineVector.X = sine;
                sineVector.Y = 2.0f * cosVector.X * sineVector.X;

                cosVector.Z = cosVector.X * cosVector.Y - sineVector.X * sineVector.Y;
                sineVector.Z = cosVector.X * sineVector.Y + sineVector.X * cosVector.Y; 
                              
               

                Vector3 phi = Vector3.One + Vector3.Modulate(cosVector, cosTerm) +
                              Vector3.Modulate(sineVector, sinTerm);

                Vector3 scalarDerivs = new Vector3(1f, 2f, 3f);
                Vector3 phiDeriv =  Vector3.Modulate(scalarDerivs, (Vector3.Modulate(cosVector, sinTerm) - Vector3.Modulate(sineVector, cosTerm)));

                var V = TorsionUnit * Vector3.Dot(torsionTerm, phi);
                float Vderiv = TorsionUnit * Vector3.Dot(torsionTerm, phiDeriv);

                Vector3 ca = position[k] - position[i];
                Vector3 db = position[l] - position[j];

                Vector3 VderivABC = Vderiv * Vector3.Cross(crossABC, cb) / (crossABCLenSqr * cbDist);
                Vector3 VderivBCD = -Vderiv * Vector3.Cross(crossBCD, cb) / (crossBCDLenSqr * cbDist);

                Vector3 derivA = Vector3.Cross(VderivABC, cb);
                Vector3 derivB = Vector3.Cross(ca, VderivABC) + Vector3.Cross(VderivBCD, dc);
                Vector3 derivC = Vector3.Cross(VderivABC, ba) + Vector3.Cross(db, VderivBCD);
                Vector3 derivD = Vector3.Cross(VderivBCD, cb); 

                Forces[i] += -1.0f * derivA;
                Forces[j] += -1.0f * derivB;
                Forces[k] += -1.0f * derivC;
                Forces[l] += -1.0f * derivD;
                TorsionEnergy += V;
            }
            return TorsionEnergy;
        }
        
        private float CalculateOPBendForces(List<Vector3> position, OPBendTerms opBends)
        {
            

            for (int i = 0; i < opBends.Count; i++)
            {
                int a, b, c, d;
                a = opBends.IndexA[i];
                b = opBends.IndexB[i];
                c = opBends.IndexC[i];
                d = opBends.IndexD[i];

                //differences
                Vector3 ab = position[a] - position[b];
                Vector3 cb = position[c] - position[b];
                Vector3 db = position[d] - position[b];
                Vector3 ad = position[a] - position[d];
                Vector3 cd = position[c] - position[d];
                ab *= Units.AngstromsPerNm;
                cb *= Units.AngstromsPerNm;
                db *= Units.AngstromsPerNm;
                cd *= Units.AngstromsPerNm;
                ad *= Units.AngstromsPerNm;

                var adLengthSquared = ad.LengthSquared;
                var cdLengthSquared = cd.LengthSquared;
                var dot = Vector3.Dot(ad, cd);
                var cc = adLengthSquared * cdLengthSquared - dot * dot;

                var outOfPlaneEnergy = Vector3.Dot(db, Vector3.Cross(ab, cb));
                var dbLengthSquared = db.LengthSquared;

                var bkk2 = dbLengthSquared - outOfPlaneEnergy * outOfPlaneEnergy / cc;
                var cosine = (float) Math.Sqrt(bkk2 / dbLengthSquared);
                cosine = Math.Min(1.0f, Math.Max(-1.0f, cosine));
                var angle = RadToDegrees * (float) Math.Acos(cosine);
                var dt = angle;
                var dt2 = dt * dt;
                var dt3 = dt2 * dt;
                var dt4 = dt2 * dt2;

                float cOPB = -0.014f;
                float qOPB = -0.000056f;
                float pOPB = -0.0000007f;
                float sOPB = 0.000000022f;
                float force = opBends.ForceConstants[i];

                var e = OPBUnit * force * dt2 * (1.0f + cOPB * dt + qOPB * dt2 + pOPB * dt3 + sOPB * dt4);
                var deddt = OPBUnit * force * dt * RadToDegrees *
                        (2.0f + 3.0f * cOPB * dt + 4.0f * qOPB * dt2 + 5.0f * pOPB * dt3 + 6.0f * sOPB * dt4);

                float dedcos; 
                try
                {
                    dedcos = -deddt * Math.Sign(outOfPlaneEnergy) / (float) Math.Sqrt(cc * bkk2);
                }
                catch (Exception ex)
                {
                    Reporter?.PrintException(ex,
                        "Exception thrown during out of plane bend calculation between atoms {0}, {1} ,{2} and {3}", a,
                        b, c, d);
                    throw;
                }

                float derivTerm = outOfPlaneEnergy / cc;
                Vector3 derivA = derivTerm * (cdLengthSquared * ad - cd * dot);
                Vector3 derivC = derivTerm * (adLengthSquared * cd - ad * dot);
                Vector3 derivD = -derivA - derivC; 

                derivTerm = outOfPlaneEnergy / dbLengthSquared;
                Vector3 VderivA = Vector3.Cross(db, cb);
                Vector3 VderivC = Vector3.Cross(ab, db);
                Vector3 VdericD = Vector3.Cross(cb, ab) - Vector3.Cross(cb, ab) + db * derivTerm;

                Vector3 fa = dedcos * (derivA + VderivA);
                Vector3 fc = dedcos * (derivC + VderivC);
                Vector3 fd = dedcos * (derivD + VdericD);
                Vector3 fb = -fa - fc - fd;

                Forces[a] += -1.0f * fa;
                Forces[b] += -1.0f * fb;
                Forces[c] += -1.0f * fc;
                Forces[d] += -1.0f * fd; 
                OutOfPlaneBendEnergy += e;
            }

            return OutOfPlaneBendEnergy;
        }

        #endregion Reference MM3 Implementation
    }
}