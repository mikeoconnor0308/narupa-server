﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Simbox.MD.ForceField.MM3.Load
{
    internal class InstantiatedMM3Terms : IInstantiatedForceFieldTerms
    {
        public MM3Data MM3Data;
        public readonly List<int> AtomTypes = new List<int>();
        public readonly List<OPBend> OPBends = new List<OPBend>();

        public IForceField GenerateForceField(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter)
        {
            reporter?.PrintNormal("Generating MM3 bonded force field for topology.");
            OPBendTerms opBendList = new OPBendTerms();
            foreach (OPBend opBend in OPBends)
            {
                opBendList.AddTerm(opBend);
            }

            MM3Topology topology = new MM3Topology(parentTopology.Molecules, AtomTypes, MM3Data, reporter, opBendList);
            MM3ForceField forceField = new MM3ForceField()
            {
                Topology = topology,
                Reporter = reporter,
                SimulationBoundary = properties.SimBoundary,
                PeriodicBoundary = properties.SimBoundary.PeriodicBoundary,
                Forces = new List<Vector3>(parentTopology.NumberOfParticles)
            };

            for (int i = 0; i < parentTopology.NumberOfParticles; i++)
            {
                forceField.Forces.Add(Vector3.Zero);
            }

            reporter?.PrintNormal("MM3 force field generated with {0} bonds, {1} angles, {2} torsions and {3} out of plane bends.", topology.Bonds.Count, topology.Angles.Count, topology.Torsions.Count, topology.OPBends.Count);

            return forceField;
        }
    }
}