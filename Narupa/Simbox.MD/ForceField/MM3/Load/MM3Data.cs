﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Simbox.MD.ForceField.MM3.Load
{
    /// <summary>
    /// Class for storing all of the MM3 forcefield data, for reference.
    /// This allows for simpler input files where only MM3 atom types
    /// and connectivity need to be specified.
    /// </summary>
    public class MM3Data
    {
        /// <summary> Struct for storing MM3 Atom Data</summary>
        public struct Atom
        {
            //TODO Change everything to readonly
            /// <summary>
            /// The element of atom.
            /// </summary>
            public string Element;

            /// <summary>
            /// The type of atom.
            /// </summary>
            public string Type;

            /// <summary>
            /// The Lennard-Jones sigma parameter for atom
            /// </summary>
            public float Sigma;

            /// <summary>
            /// The Lennard-Jones epsilon parameter for atom
            /// </summary>
            public float Epsilon;
        }

        /// <summary> Struct for storing MM3 Bond Data </summary>
        public struct Bond
        {
            /// <summary> Harmonic potential equilibrium distance </summary>
            public float r0;

            /// <summary> Harmonic potential force constant </summary>
            public float FC;

            /// <summary> Morse potential D</summary>
            public float De;

            /// <summary> Morse potential alpha </summary>
            public float alpha;
        }

        /// <summary> Struct for storing MM3 Angle Data </summary>
        public struct Angle
        {
            /// <summary>
            /// The equilibrium angle.
            /// </summary>
            public float theta0;

            /// <summary>
            /// The force constant.
            /// </summary>
            public float FC;
        }

        /// <summary> Struct for storing MM3 Torsion Data </summary>
        public struct Torsion
        {
            /// <summary>
            /// The torsion vector
            /// </summary>
            public Vector3 V;
        }

        private string mm3File;

        /// <summary> Dictionary of Atom Type IDs and information </summary>
        public Dictionary<int, Atom> Atoms;

        /// <summary> Dictionary of pairs of MM3 atom IDs and bond information </summary>
        public Dictionary<BondPair, Bond> Bonds;

        /// <summary> Dictionary with key triplets of MM3 atom IDs and value angle information </summary>
        public Dictionary<AngleIdx, Angle> Angles;

        /// <summary> Dictionary with key tuple of torsion atom IDs and value torsion information </summary>
        public Dictionary<TorsionIdx, Torsion> Torsions;

        /// <summary>
        /// Reads the general MM3 forcefield data from an XML file.
        /// </summary>
        /// <param name="mm3File"> XML file containing mm3 forcefield parameters </param>
        public MM3Data(string mm3File)
        {
            this.mm3File = mm3File;

            XmlDocument mm3Doc = new XmlDocument();
            mm3Doc.Load(Helper.ResolvePath(mm3File));

            XmlNode mm3Node = mm3Doc.DocumentElement;

            XmlNode atomArrayNode = mm3Node.SelectSingleNode("atomArray");
            if (atomArrayNode == null)
                throw new Exception("Missing element 'atomArray' in node 'mm3' while reading MM3 data from file: " + mm3File);
            LoadAtoms(atomArrayNode);

            XmlNode vdwArrayNode = mm3Node.SelectSingleNode("vdwArray");
            if (vdwArrayNode == null)
                throw new Exception("Missing element 'vdwArray' in node 'mm3' while reading MM3 data from file: " + mm3File);
            LoadVDW(vdwArrayNode);

            XmlNode bondArrayNode = mm3Node.SelectSingleNode("bondArray");
            if (bondArrayNode == null)
                throw new Exception("Missing element 'bondArray' in node 'mm3' while reading MM3 data from file: " + mm3File);
            LoadBonds(bondArrayNode);

            XmlNode angleArrayNode = mm3Node.SelectSingleNode("angleArray");
            if (angleArrayNode == null)
                throw new Exception("Missing element 'angleArray' in node 'mm3' while reading MM3 data from file: " + mm3File);
            LoadAngles(angleArrayNode);

            XmlNode torsionArrayNode = mm3Node.SelectSingleNode("torsionArray");
            if (torsionArrayNode == null)
                throw new Exception("Missing element 'torsionArray' in node 'mm3' while reading MM3 data from file: " + mm3File);
            LoadTorsions(torsionArrayNode);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MM3Data"/> class.
        /// </summary>
        public MM3Data()
        {
        }

        private void LoadAtoms(XmlNode atomArrayNode)
        {
            Atoms = new Dictionary<int, Atom>();
            XmlNodeList AtomNodeList = atomArrayNode.SelectNodes("atom");
            foreach (XmlNode atomNode in AtomNodeList)
            {
                int index = Helper.GetAttributeValue(atomNode, "index", 0);
                if (index == 0)
                    throw new Exception("Missing attribute 'index' in atom during read of MM3 data:" + atomNode.OuterXml);
                string element = Helper.GetAttributeValue(atomNode, "element", "");
                if (element == "")
                    throw new Exception("Missing attribute 'element' in atom during read of MM3 data:" + atomNode.OuterXml);
                string type = Helper.GetAttributeValue(atomNode, "type", "");
                if (type == "")
                    throw new Exception("Missing attribute 'type' in atom during read of MM3 data:" + atomNode.OuterXml);
                Atom atom = new Atom();
                atom.Element = element;
                atom.Type = type;
                Atoms[index] = atom;
            }
        }

        private void LoadBonds(XmlNode bondArrayNode)
        {
            Bonds = new Dictionary<BondPair, Bond>();
            XmlNodeList BondNodeList = bondArrayNode.SelectNodes("bond");
            foreach (XmlNode bondNode in BondNodeList)
            {
                int type1 = Helper.GetAttributeValue(bondNode, "type1", 0);
                if (type1 == 0)
                    throw new Exception("Missing attribute 'type1' in bond during read of MM3 data:" + bondNode.OuterXml);
                int type2 = Helper.GetAttributeValue(bondNode, "type2", 0);
                if (type2 == 0)
                    throw new Exception("Missing attribute 'type2' in bond during read of MM3 data:" + bondNode.OuterXml);
                float r_e = Helper.GetAttributeValue(bondNode, "r_e", 0f);
                if (r_e == 0)
                    throw new Exception("Missing attribute 'r_e' in bond during read of MM3 data:" + bondNode.OuterXml);
                float force = Helper.GetAttributeValue(bondNode, "force", 0f);
                if (force == 0)
                    throw new Exception("Missing attribute 'force' in bond during read of MM3 data:" + bondNode.OuterXml);
                //Morse parameters are optional.
                float D_e = Helper.GetAttributeValue(bondNode, "D_e", 0f);
                float alpha = Helper.GetAttributeValue(bondNode, "alpha", 0f);
                Bond bond = new Bond();
                BondPair pair = new BondPair(type1, type2);
                bond.r0 = r_e;
                bond.FC = force;
                bond.De = D_e;
                bond.alpha = alpha;
                Bonds[pair] = bond;
            }
        }

        private void LoadAngles(XmlNode angleArrayNode)
        {
            Angles = new Dictionary<AngleIdx, Angle>();
            XmlNodeList AngleNodeList = angleArrayNode.SelectNodes("angle");
            foreach (XmlNode angleNode in AngleNodeList)
            {
                int type1 = Helper.GetAttributeValue(angleNode, "type1", 0);
                if (type1 == 0)
                    throw new Exception("Missing attribute 'type1' in angle during read of MM3 data:" + angleNode.OuterXml);
                int type2 = Helper.GetAttributeValue(angleNode, "type2", 0);
                if (type2 == 0)
                    throw new Exception("Missing attribute 'type2' in angle during read of MM3 data:" + angleNode.OuterXml);
                int type3 = Helper.GetAttributeValue(angleNode, "type3", 0);
                if (type3 == 0)
                    throw new Exception("Missing attribute 'type3' in angle during read of MM3 data:" + angleNode.OuterXml);
                float theta_0 = Helper.GetAttributeValue(angleNode, "theta_0", float.NaN);
                if (Single.IsNaN(theta_0))
                    throw new Exception("Missing attribute 'theta_0' in angle during read of MM3 data:" + angleNode.OuterXml);
                float k_0 = Helper.GetAttributeValue(angleNode, "k_0", 0f);
                if (k_0 == 0)
                    throw new Exception("Missing attribute 'k_0' in angle during read of MM3 data:" + angleNode.OuterXml);
                Angle angle = new Angle();
                AngleIdx idx = new AngleIdx(type1, type2, type3);
                angle.theta0 = theta_0;
                angle.FC = k_0;
                Angles[idx] = angle;
            }
        }

        private void LoadTorsions(XmlNode torsionArrayNode)
        {
            Torsions = new Dictionary<TorsionIdx, Torsion>();
            XmlNodeList TorsionNodeList = torsionArrayNode.SelectNodes("torsion");
            foreach (XmlNode torsionNode in TorsionNodeList)
            {
                int type1 = Helper.GetAttributeValue(torsionNode, "type1", 0);
                if (type1 == 0)
                    throw new Exception("Missing attribute 'type1' in torsion during read of MM3 data:" + torsionNode.OuterXml);
                int type2 = Helper.GetAttributeValue(torsionNode, "type2", 0);
                if (type2 == 0)
                    throw new Exception("Missing attribute 'type2' in torsion during read of MM3 data:" + torsionNode.OuterXml);
                int type3 = Helper.GetAttributeValue(torsionNode, "type3", 0);
                if (type3 == 0)
                    throw new Exception("Missing attribute 'type3' in torsion during read of MM3 data:" + torsionNode.OuterXml);
                int type4 = Helper.GetAttributeValue(torsionNode, "type4", 0);
                if (type4 == 0)
                    throw new Exception("Missing attribute 'type4' in torsion during read of MM3 data:" + torsionNode.OuterXml);
                float V1 = Helper.GetAttributeValue(torsionNode, "V1", float.NaN);
                if (float.IsNaN(V1))
                    throw new Exception("Missing attribute 'V1' in torsion during read of MM3 data:" + torsionNode.OuterXml);
                float V2 = Helper.GetAttributeValue(torsionNode, "V2", float.NaN);
                if (float.IsNaN(V2))
                    throw new Exception("Missing attribute 'V2' in torsion during read of MM3 data:" + torsionNode.OuterXml);
                float V3 = Helper.GetAttributeValue(torsionNode, "V3", float.NaN);
                if (float.IsNaN(V3))
                    throw new Exception("Missing attribute 'V3' in torsion during read of MM3 data:" + torsionNode.OuterXml);
                Torsion torsion = new Torsion();
                TorsionIdx idx = new TorsionIdx(type1, type2, type3, type4);
                torsion.V = new Vector3(V1, V2, V3);
                Torsions[idx] = torsion;
            }
        }

        internal MM3Data Clone()
        {
            MM3Data d = new MM3Data();
            d.Angles = Angles;
            d.Atoms = Atoms;
            d.Bonds = Bonds;
            d.mm3File = mm3File;
            d.Torsions = Torsions;
            return d;
        }

        private void LoadVDW(XmlNode vdwArrayNode)
        {
            XmlNodeList vdwNodeList = vdwArrayNode.SelectNodes("vdw");
            int index = 0;
            try
            {
                foreach (XmlNode vdwNode in vdwNodeList)
                {
                    index = Helper.GetAttributeValue(vdwNode, "type1", 0);
                    if (index == 0)
                        throw new Exception("Missing attribute 'type1' in vdw during read of MM3 data:" + vdwNode.OuterXml);
                    float sigma = Helper.GetAttributeValue(vdwNode, "sigma", 0f);
                    if (sigma == 0)
                        if (Atoms[index].Element != "*")
                            throw new Exception("Missing attribute 'sigma' in vdw during read of MM3 data:" + vdwNode.OuterXml);
                    float epsilon = Helper.GetAttributeValue(vdwNode, "epsilon", 0f);
                    if (epsilon == 0)
                        if (Atoms[index].Element != "*")
                            throw new Exception("Missing attribute 'epsilon' in vdw during read of MM3 data:" + vdwNode.OuterXml);
                    Atom a = Atoms[index];
                    a.Sigma = sigma;
                    a.Epsilon = epsilon;
                    Atoms[index] = a;
                }
            }
            catch(Exception e)
            {
                throw new Exception("Exception thrown trying to assign LJ types.", e);
            }

        }
    }
}