﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Simbox.MD.ForceField.MM3.Load
{
    /// <summary>
    /// MM3 Topology class, which includes bonds, angles and torsions
    /// </summary>
    public class MM3Topology
    {
        #region Public Fields

        /// <summary>
        /// The angles
        /// </summary>
        public readonly AngleStruct Angles = new AngleStruct();

        /// <summary>
        /// The atom types
        /// </summary>
        public readonly List<int> AtomTypes = new List<int>();

        /// <summary>
        /// The bonds
        /// </summary>
        public readonly BondStruct Bonds = new BondStruct();

        /// <summary>
        /// The morse terms
        /// </summary>
        public readonly MorseStruct MorseTerms = new MorseStruct();

        /// <summary>
        /// Gets the number of particles.
        /// </summary>
        /// <value>The number of particles.</value>
        public int NumberOfParticles { get { return AtomTypes.Count; } }

        /// <summary>
        /// The out-of-plane bends.
        /// </summary>
        public readonly OPBendTerms OPBends = new OPBendTerms();

        /// <summary>
        /// The torsions
        /// </summary>
        public readonly TorsionStruct Torsions = new TorsionStruct();

        /// <summary>
        /// Dictionary of atom type info present in this simulation.
        /// <remarks>
        /// Intended purely for initialisation and occasional reference.
        /// </remarks>
        /// </summary>
        public readonly SortedDictionary<int, MM3Data.Atom> TypeInfo = new SortedDictionary<int, MM3Data.Atom>();

        #endregion Public Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MM3Topology"/> class.
        /// </summary>
        public MM3Topology()
        {
            AtomTypes = new List<int>();
            Bonds = new BondStruct();
            Angles = new AngleStruct();
            Torsions = new TorsionStruct();
            MorseTerms = new MorseStruct();
            TypeInfo = new SortedDictionary<int, MM3Data.Atom>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MM3Topology" /> class.
        /// </summary>
        /// <param name="numberOfParticles">The number of particles.</param>
        /// <param name="atomTypes">The atom types.</param>
        /// <param name="bonds">The bonds.</param>
        /// <param name="angles">The angles.</param>
        /// <param name="torsions">The torsions.</param>
        /// <param name="typeInfo">The type information.</param>
        /// TODO make tests not require this.
        /// <remarks>Used for testing.</remarks>
        public MM3Topology(int numberOfParticles, List<int> atomTypes = null, BondStruct bonds = null, AngleStruct angles = null, TorsionStruct torsions = null, SortedDictionary<int, MM3Data.Atom> typeInfo = null)
        {
            if (atomTypes == null)
            {
                for (int i = 0; i < numberOfParticles; i++)
                {
                    AtomTypes.Add(1);
                }
            }
            else
                AtomTypes = atomTypes;
            if (bonds != null)
            {
                Bonds = bonds;
            }

            if (angles != null)
            {
                Angles = angles;
            }
            if (torsions != null)
            {
                Torsions = torsions;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MM3Topology" /> class.
        /// </summary>
        /// <param name="components">The components.</param>
        /// <param name="atomTypes">The atom types.</param>
        /// <param name="referenceData">The reference data.</param>
        /// <param name="opbends">The out-of-plane bends.</param>
        /// <param name="reporter">The reporter.</param>
        public MM3Topology(List<Molecule> components, List<int> atomTypes, MM3Data referenceData, IReporter reporter, OPBendTerms opbends = null)
        {
            AtomTypes = atomTypes;
            Reporter = reporter;

            ReferenceData = referenceData;
            TypeInfo = new SortedDictionary<int, MM3Data.Atom>();
            foreach (int type in atomTypes)
            {
                if (type != -1)
                    TypeInfo[type] = referenceData.Atoms[type];
            }

            //Add additional terms if required
            if (opbends?.Count > 0)
            {
                OPBends = opbends;
            }

            Generate(components, referenceData);
        }

        #endregion Public Constructors

        #region Private Fields

        private IReporter Reporter;

        #endregion Private Fields

        #region Private Methods

        private void Generate(List<Molecule> Components, MM3Data data)
        {
            //For each component, generate bonds, angles and torsions
            Reporter?.PrintDetail("Generating MM3 force field terms for {0} molecules.", Components.Count);
            foreach (Molecule c in Components)
            {
                GenerateTopology(c, data, Reporter);
            }
        }

        private void GenerateTopology(Molecule component, MM3Data referenceData, IReporter reporter = null)
        {
            //Generate the MM3 bond type
            for (int i = 0; i < component.Bonds.Count; i++)
            {
                BondPair bond = new BondPair(component.Bonds.BondIndexes[i].A, component.Bonds.BondIndexes[i].B);
                bool morse = component.Bonds.CanBeBroken[i];
                try
                {
                    AddBond(bond, morse);
                }
                catch (Exception e)
                {
                    reporter?.PrintException(e, "Failed to add bond.");
                }
            }
            //Generate the angles
            HashSet<int> a = new HashSet<int>();
            HashSet<int> b = new HashSet<int>();
            HashSet<int> intersection = new HashSet<int>();

            List<AngleIdx> angles = new List<AngleIdx>();

            for (int ii = 0; ii < component.Bonds.Count; ii++)
            {
                BondPair bond_A = component.Bonds.BondIndexes[ii];
                a.Clear();
                a.Add(bond_A.A);
                a.Add(bond_A.B);

                for (int jj = ii + 1; jj < component.Bonds.Count; jj++)
                {
                    BondPair bond_B = component.Bonds.BondIndexes[jj];

                    b.Clear();
                    b.Add(bond_B.A);
                    b.Add(bond_B.B);
                    intersection = new HashSet<int>(a.Intersect(b));
                    if (intersection.Count == 1)
                    {
                        int i = a.Except(intersection).First();
                        int j = intersection.First();
                        int k = b.Except(intersection).First();
                        angles.Add(new AngleIdx(i, j, k));
                        AddAngle(i, j, k);
                    }
                }
            }

            for (int ii = 0; ii < angles.Count; ii++)
            {
                AngleIdx angleA = angles[ii];
                a.Clear();
                a.Add(angleA.I);
                a.Add(angleA.J);
                a.Add(angleA.K);

                for (int jj = ii + 1; jj < angles.Count; jj++)
                {
                    AngleIdx angleB = angles[jj];
                    b.Clear();
                    b.Add(angleB.I);
                    b.Add(angleB.J);
                    b.Add(angleB.K);
                    intersection = new HashSet<int>(a.Intersect(b));
                    //A torsion is defined whenever 2 angles share
                    //2 common indices - as long as the middle atom isn't identical
                    //TODO there may be a better way to determine torsions
                    //   B   D                      A-B-D
                    //  / \ /  <- 2 shared atoms.     |   <- two shared atoms, but B
                    // A   C                          C      is central atom in both.
                    if (intersection.Count == 2 && angleA.J != angleB.J)
                    {
                        int i = angleA.I;
                        int j = angleA.J;
                        int k = angleA.K;

                        //Get indices the right way round
                        if (k != angleB.J)
                        {
                            Swap(ref i, ref k);
                        }

                        int l = b.Except(a).First();

                        try
                        {
                            AddTorsion(i, j, k, l);
                        }
                        catch (Exception e)
                        {
                            reporter?.PrintException(e, "Failed to add torsion");
                        }
                    }
                }
            }
        }

        private void LoadAdditionalTerms(XmlNode additionalTerms)
        {
            //TODO OPBends can be autogenerated from mm3 data (which needs updating)
            XmlNodeList OPBendList = additionalTerms.SelectNodes("OPBend");
            if (OPBendList.Count > 0)
            {
                foreach (XmlNode opBendNode in OPBendList)
                {
                    int a = Helper.GetAttributeValue(opBendNode, "a", -1);
                    if (a == -1)
                        throw new Exception("Missing attribute 'a' in OPBend");
                    int b = Helper.GetAttributeValue(opBendNode, "b", -1);
                    if (b == -1)
                        throw new Exception("Missing attribute 'b' in OPBend");
                    int c = Helper.GetAttributeValue(opBendNode, "c", -1);
                    if (c == -1)
                        throw new Exception("Missing attribute 'c' in OPBend");
                    int d = Helper.GetAttributeValue(opBendNode, "d", -1);
                    if (d == -1)
                        throw new Exception("Missing attribute 'd' in OPBend");
                    float fc = Helper.GetAttributeValue(opBendNode, "fc", float.NaN);
                    if (float.IsNaN(fc))
                        throw new Exception("Missing attribute 'fc' in OPBend");
                    OPBends.AddTerm(a, b, c, d, fc);
                }
            }
        }

        private void LoadAtomTypes(XmlNode typeNode, MM3Data data)
        {
            XmlNodeList ParticleNodeList = typeNode.SelectNodes("Particle");
            foreach (XmlNode particleNode in ParticleNodeList)
            {
                int type = Helper.GetAttributeValue(particleNode, "type", 0);
                if (type == 0)
                    throw new Exception("Missing attribute 'type' in Particle during read of MM3 data:" + particleNode.OuterXml);
                AtomTypes.Add(type);
                TypeInfo[type] = data.Atoms[type];
            }
        }

        private void Swap(ref int a, ref int b)
        {
            a = a + b;
            b = a - b;
            a = a - b;
        }

        #endregion Private Methods

        /// <summary>
        /// Adds the atom to the force field.
        /// </summary>
        /// <param name="type">The type.</param>
        public void AddAtom(int type)
        {
            AtomTypes.Add(type);
        }

        /// <summary>
        /// Adds the bond to the force field.
        /// </summary>
        /// <param name="bond">The bond.</param>
        /// <param name="morse">if set to <c>true</c> [morse].</param>
        public void AddBond(BondPair bond, bool morse)
        {
            int typeA = AtomTypes[bond.A];
            int typeB = AtomTypes[bond.B];

            if (typeA > typeB)
            {
                int tmp = typeB;
                typeB = typeA;
                typeA = tmp;
            }

            BondPair bondtypes = new BondPair(typeA, typeB);

            if (!morse)
            {
                MM3Data.Bond bondData;
                try
                {
                    bondData = ReferenceData.Bonds[bondtypes];
                }
                catch (KeyNotFoundException e)
                {
                    string error = $"Do not know how to handle bond between atoms {bond.A} and {bond.B}, with MM3 atom types {bondtypes.A} and {bondtypes.B}";
                    Reporter?.PrintError(error);
                    throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, error);
                }
                Bonds.IndexI.Add(bond.A);
                Bonds.IndexJ.Add(bond.B);
                Bonds.FC.Add(ReferenceData.Bonds[bondtypes].FC);
                Bonds.R0.Add(ReferenceData.Bonds[bondtypes].r0);
            }
            else
            {
                Reporter?.PrintDetail("Adding morse potential between atoms {0} and {1}", bond.A, bond.B);
                float r0 = ReferenceData.Bonds[bondtypes].r0;
                float d = ReferenceData.Bonds[bondtypes].De;
                float alpha = ReferenceData.Bonds[bondtypes].alpha;
                MorseTerms.AddTerm(bond.A, bond.B, r0, d, alpha);
            }
        }

        /// <summary>
        /// Adds the angle to the force field.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <param name="j">The j.</param>
        /// <param name="k">The k.</param>
        public void AddAngle(int i, int j, int k)
        {
            //Swap i and k if i is larger
            int typeI = AtomTypes[i];
            int typeJ = AtomTypes[j];
            int typeK = AtomTypes[k];
            if (typeI > typeK)
            {
                i = i + k;
                k = i - k;
                i = i - k;
                typeI = typeI + typeK;
                typeK = typeI - typeK;
                typeI = typeI - typeK;
            }
            AngleIdx angletypes = new AngleIdx(typeI, typeJ, typeK);

            MM3Data.Angle angleData;
            try
            {
                angleData = ReferenceData.Angles[angletypes];
            }
            catch (KeyNotFoundException e)
            {
                string error = $"Do not know how to handle angle between atoms {i}, {j}, {k}, with MM3 atom types {angletypes.I}, {angletypes.J} and {angletypes.K}.";
                Reporter?.PrintError(error);
                throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, error);
            }
            Angles.IndexI.Add(i);
            Angles.IndexJ.Add(j);
            Angles.IndexK.Add(k);
            Angles.Theta0.Add(ReferenceData.Angles[angletypes].theta0);
            Angles.FC.Add(ReferenceData.Angles[angletypes].FC);
        }

        /// <summary>
        /// Adds the torsion to the force field.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <param name="j">The j.</param>
        /// <param name="k">The k.</param>
        /// <param name="l">The l.</param>
        /// <exception cref="Exception">No torsion data found for torsion with indices: [ + i + ,  + j + ,  + k + ,  + l + ]</exception>
        public void AddTorsion(int i, int j, int k, int l)
        {
            int typeI = AtomTypes[i];
            int typeJ = AtomTypes[j];
            int typeK = AtomTypes[k];
            int typeL = AtomTypes[l];

            //Torsions are ordered such that type j is less than type k
            if (typeJ > typeK)
            {
                //Swap the types
                Swap(ref typeJ, ref typeK);
                Swap(ref typeI, ref typeL);
                //And swap the indices
                Swap(ref i, ref l);
                Swap(ref j, ref k);
            }
            TorsionIdx torsion = new TorsionIdx(AtomTypes[i], AtomTypes[j], AtomTypes[k], AtomTypes[l]);
            Vector3 V;
            try
            {
                V = ReferenceData.Torsions[torsion].V;
            }
            //If key not found, then try flipping the torsion the other way
            catch
            {
                //Swap the types
                Swap(ref typeJ, ref typeK);
                Swap(ref typeI, ref typeL);
                //And swap the indices
                Swap(ref i, ref l);
                Swap(ref j, ref k);

                torsion = new TorsionIdx(typeI, typeJ, typeK, typeL);
                try
                {
                    V = ReferenceData.Torsions[torsion].V;
                    Torsions.IndexI.Add(i);
                    Torsions.IndexJ.Add(j);
                    Torsions.IndexK.Add(k);
                    Torsions.IndexL.Add(l);
                    Torsions.V.Add(ReferenceData.Torsions[torsion].V);
                }
                catch (KeyNotFoundException e2)
                {
                    string error = $"No torsion data found for torsion with indices: [{i}, {j}, {k}, {l}] with atom types: {torsion.I} {torsion.J} {torsion.K} {torsion.L}";
                    Reporter?.PrintError(error);
                    //throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, error);
                }
            }


        }

        /// <summary>
        /// Adds the out of plane bend to the force field.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <param name="d">The d.</param>
        /// <param name="fc">The fc.</param>
        public void AddOPBend(int a, int b, int c, int d, float fc)
        {
            OPBends.AddTerm(a, b, c, d, fc);
        }

        /// <summary>
        /// Gets the reference MM3 data.
        /// </summary>
        /// <value>The reference data.</value>
        public MM3Data ReferenceData { get; }
    }
}