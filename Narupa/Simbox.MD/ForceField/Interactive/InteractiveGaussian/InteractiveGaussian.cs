﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using Nano;
using Nano.Science;
using Nano.Transport.Agents;
using Nano.Transport.Variables;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Transport.Variables.Interaction;
using SlimMath;
using Interaction = Nano.Science.Simulation.Interaction;

namespace Simbox.MD.ForceField.Interactive.InteractiveGaussian
{
    /// <summary>
    /// Force field for interactions using a gaussian potential.
    /// </summary>
    /// <remarks>
    /// Depending on the state of each InteractionType, it will either apply a force to all atoms within
    /// a specified cutoff distance or to the nearest atom.
    ///
    /// The width and height of the Gaussian can be tweaked using the GradientScaleFactor and Sigma variables.
    ///
    /// Computes the energy using the following formula:
    /// V = -1 * massFactor * height * exp(-r^2 / (2 * width^2))
    /// and force
    /// force_x = -dV/dx = - dr/dx * dV/dr = massFactor * height * r * dr/dx * exp(-r^2 / (2 * width^2))
    ///
    /// where:
    /// height = gradientScaleFactor * GlobalGradientScaleFactor * interactionGradientScaleFactor is the height of the gaussian,
    /// r is the distance between the particle and the interaction site
    /// dr/dx is the derivative of r with respect to x,
    /// width is the width of the gaussian
    /// and mass factor is a scaling factor used to account for a particle's mass.
    /// </remarks>
    public class InteractiveGaussian : IForceField, IExternalForceField, IVariableProvider
    {
        /// <summary> Strength of field applied to each particle. </summary>
        public readonly List<float> GradientScaleFactor = new List<float>();

        /// <summary>
        /// The width of the Gaussian.
        /// </summary>
        public float Width = 1f;

        /// <summary>
        /// Whether velocities will be reinitialized upon interaction ending. 
        /// </summary>
        public bool ReinitializeVelocities;
        /// <summary>
        /// The temperature scale factor to apply to the target temperature when reinitializing velocities. 
        /// </summary>
        /// <remarks>
        /// A lower scale factor (less than 1.0) will encourage stability.
        /// </remarks>
        public float TemperatureScaleFactor;
        
        private float widthSqr;

        private float globalGradientScaleFactor = 1f;

        private bool iVarsInitialised = false;
        internal bool ApplyMassFactor = true;
        private IVariable<float> widthVariable;
        private IVariable<float> gradientScaleFactorVariable;
        private const float Epsilon = 0.000001f;
        private int lastNumInteractions;

        /// <summary>
        /// Determines whether this force field will be calculated using the old exponential approximation or not.
        /// </summary>
        internal bool LegacyMode;

        public bool VariablesInitialised => iVarsInitialised;

        public string Name => "InteractiveGaussian";

        /// <summary>
        /// Whether to use a harmonic potential instead of the Gaussian based potential.
        /// </summary>
        public bool HarmonicForce = false;
  
        /// <summary>
        /// Calculates the energy and forces of the forcefield at the given positions.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="forces">The forces.</param>
        /// <returns>System.Single.</returns>
        /// <exception cref="Exception">Number of interactions greater than number supported!</exception>
        public float CalculateForceField(IAtomicSystem system, List<Vector3> forces)
        {
            widthSqr = Width * Width;
            float cutOffDistance = 2 * widthSqr;

            
            List<Interaction> interactions = system.Interactions;
            if (system.NumberOfParticles > GradientScaleFactor.Count)
            {
                FillGradientScaleFactors(system.NumberOfParticles - GradientScaleFactor.Count, GradientScaleFactor[0]);
            }


            if(ReinitializeVelocities && system is AtomicSystem atomicSystem && interactions.Count == 0 && lastNumInteractions > 0)
                system.InitializeVelocitiesToTemperature(atomicSystem.Thermostat.EquilibriumTemperature * TemperatureScaleFactor);
            
            lastNumInteractions = interactions.Count;
            
            List<Vector3> atomPositions = system.Particles.Position;
            Vector3 force;
            float V = 0;
            //Loop over all interaction points.
            for (int j = 0; j < interactions.Count; j++)
            {
                float interactionEnergy = 0f;
                interactions[j].Energy = 0f;
                interactions[j].NetForce = Vector3.Zero;
                interactions[j].ForceMagnitude = 0f;
                if (interactions[j].Type == InteractionType.NoInteraction)
                    continue;

                //If all atom interaction, then calculate the force between the point and all the atoms within the cutoff radius.
                if (interactions[j].Type == InteractionType.AllAtom)
                {
                    foreach (ushort i in system.AtomSelections.VisibleAtoms)
                    {
                        if (Math.Abs(GradientScaleFactor[i]) < Epsilon)
                            continue;
                        Vector3 diff = interactions[j].Position - atomPositions[i];
                        float dist2 = diff.LengthSquared;
                        if (dist2 >= cutOffDistance)
                            continue;
                        interactionEnergy += CalculateForce(diff, dist2, out force, GradientScaleFactor[i], interactions[j].GradientScaleFactor, system.Particles.Mass[i]);
                        forces[i] += force;
                        interactions[j].ForceMagnitude += force.Length;
                        interactions[j].NetForce += force;
                    }
                    interactions[j].Energy = interactionEnergy;
                    V += interactionEnergy;
                    continue;
                }
                else if (interactions[j].Type == InteractionType.Group)
                {
                    V += CalculateCOMInteraction(interactions[j], system, forces);
                    continue;
                }
                if (interactions[j].SelectedAtoms.Count > 1)
                    throw new Exception("More than one atom in the selected atoms for a single atom interaction.");

                int selectedAtom = interactions[j].SelectedAtoms.Count == 1 ? interactions[j].SelectedAtoms.First() : -1;

                //If single atom interaction, and no atom is selected, loop over all visible atoms to get the closest one.
                if (selectedAtom == -1)
                {
                    Vector3 selectedAtomDiff = new Vector3();
                    float selectedAtomDist2 = float.MaxValue;

                    foreach (ushort i in system.AtomSelections.VisibleAtoms)
                    {
                        if (Math.Abs(GradientScaleFactor[i]) < Epsilon)
                            continue;
                        Vector3 diff = interactions[j].Position - atomPositions[i];
                        float dist2 = diff.LengthSquared;

                        //If single atom mode, apply selection criteria.
                        if (dist2 < selectedAtomDist2)
                        {
                            selectedAtomDiff = diff;
                            selectedAtomDist2 = dist2;
                            selectedAtom = i;
                        }
                    }
                    if (selectedAtom != -1)
                    {
                        interactionEnergy = CalculateForce(selectedAtomDiff, selectedAtomDist2, out force, GradientScaleFactor[selectedAtom], interactions[j].GradientScaleFactor, system.Particles.Mass[selectedAtom]);
                        forces[selectedAtom] += force;
                        system.Interactions[j].SelectedAtoms.Add(selectedAtom);
                        interactions[j].ForceMagnitude += force.Length;
                        interactions[j].NetForce += force;
                        interactions[j].Energy = interactionEnergy;
                        interactions[j].SelectedAtomsCentreOfMass = system.Particles.Position[selectedAtom];
                        V += interactionEnergy;
                    }
                }
                else
                {
                    Vector3 diff = interactions[j].Position - atomPositions[selectedAtom];
                    float dist2 = diff.LengthSquared;
                    interactionEnergy = CalculateForce(diff, dist2, out force, GradientScaleFactor[selectedAtom], interactions[j].GradientScaleFactor, system.Particles.Mass[selectedAtom]);
                    forces[selectedAtom] += force;
                    interactions[j].ForceMagnitude += force.Length;
                    interactions[j].NetForce += force;
                    interactions[j].Energy = interactionEnergy;
                    interactions[j].SelectedAtomsCentreOfMass = system.Particles.Position[selectedAtom];
                    V += interactionEnergy;
                }
            }

            potentialEnergy = V;
            return V;
        }

        /// <summary>
        /// Calculates the centre of mass based interaction, applying equal force to all atoms in the selection.
        /// </summary>
        /// <param name="interaction"></param>
        /// <param name="system"></param>
        /// <param name="forces"></param>
        private float CalculateCOMInteraction(Interaction interaction, IAtomicSystem system, List<Vector3> forces)
        {
            //Get the center of mass of the atoms in the interaction.
            Vector3 com = new Vector3();
            float totalMass = 0f;
            float gradientScaleFactorAvg = 0f;
            foreach (int atomIndex in interaction.SelectedAtoms)
            {
                int absIndex = system.AtomSelections.VisibleAtoms[atomIndex];

                com += system.Particles.Position[absIndex] * system.Particles.Mass[absIndex];
                totalMass += system.Particles.Mass[absIndex];
                gradientScaleFactorAvg += GradientScaleFactor[absIndex];
            }
            com /= totalMass;
            gradientScaleFactorAvg /= interaction.SelectedAtoms.Count;

            //Populate the interaction's centre of mass so it can be sent to clients for visualisation.
            interaction.SelectedAtomsCentreOfMass = com;

            //Calculate the force to apply to the center of mass.
            Vector3 diff = interaction.Position - com;
            float distSqr = diff.LengthSquared;
            
            Vector3 forcePerAtom;
            float energyPerAtom = CalculateForce(diff, distSqr, out forcePerAtom, gradientScaleFactorAvg, interaction.GradientScaleFactor);

            forcePerAtom /= interaction.SelectedAtoms.Count;
            energyPerAtom /= interaction.SelectedAtoms.Count;
            
            float totalEnergy = 0f;
            //Apply the force to all atoms in the group, multiplying through by their mass so each atom will have same resulting velocity.
            foreach (int atomIndex in interaction.SelectedAtoms)
            {
                int absIndex = system.AtomSelections.VisibleAtoms[atomIndex];
                totalEnergy += energyPerAtom * system.Particles.Mass[absIndex];
                Vector3 force = system.Particles.Mass[absIndex] * forcePerAtom;
                forces[absIndex] += force;
                interaction.NetForce += force;
                interaction.ForceMagnitude += force.Length;
            }
            interaction.Energy += totalEnergy;
            return totalEnergy;
        }

        private void FillGradientScaleFactors(int count, float val = 1.0f)
        {
            for (int i = 0; i < count; i++)
            {
                GradientScaleFactor.Add(val);
            }
        }

        public void Dispose()
        {
        }

        /// <inheritdoc />
        public void InitialiseVariables(VariableManager variableManager)
        {
            gradientScaleFactorVariable = variableManager.Variables.Add(VariableName.GradientScaleFactor, VariableType.Float) as IVariable<float>;
            widthVariable = variableManager.Variables.Add(VariableName.Sigma, VariableType.Float) as IVariable<float>;
        }

        /// <inheritdoc />
        public void ResetVariableDesiredValues()
        {
            widthVariable.DesiredValue = Width;
            widthVariable.Value = Width;
            gradientScaleFactorVariable.Value = globalGradientScaleFactor;
            gradientScaleFactorVariable.DesiredValue = globalGradientScaleFactor;
        }

        /// <inheritdoc />
        public void UpdateVariableValues()
        {
            if (Math.Abs(widthVariable.DesiredValue - Width) > Epsilon)
                Width = widthVariable.DesiredValue;
            if (Math.Abs(gradientScaleFactorVariable.DesiredValue - globalGradientScaleFactor) > Epsilon)
            {
                globalGradientScaleFactor = gradientScaleFactorVariable.DesiredValue;
            }
        }

        /// <summary>
        /// Calculates the interaction force to apply to a particle.
        /// </summary>
        /// <returns>The energy of the interaction in kJ/mol.</returns>
        /// <param name="diff">The vector difference between the particle and the interaction site.</param>
        /// <param name="distSquared">The squared distance between the particle and the interaction site.</param>
        /// <param name="force">The calculated force in kJ/(mol*nm).</param>
        /// <param name="gradientScaleFactor">Gradient scale factor, controlling the scale of the interaction.</param>
        /// <param name="interactionGradientScaleFactor">Tuning parameter for this particular interaction.</param>
        /// <param name="massFactor">Tuning factor taking into account the particle's mass.</param>
        /// <remarks>
        /// Computes the energy using the following formula when using Gaussian based interaction:
        /// V = -1 * massFactor * height * exp(-r^2 / (2 * width^2))
        /// and force
        /// force_x = -dV/dx = - dr/dx * dV/dr = massFactor * height * r * dr/dx * exp(-r^2 / (2 * width^2))
        ///
        /// where:
        /// height = gradientScaleFactor * GlobalGradientScaleFactor * interactionGradientScaleFactor is the height of the gaussian,
        /// r is the distance between the particle and the interaction site
        /// dr/dx is the derivative of r with respect to x,
        /// width is the width of the gaussian
        /// and mass factor is a scaling factor used to account for a particle's mass.
        ///
        /// If HarmonicForce has been enabled, it instead calculates the interaction using a spring potential:
        /// V = massFactor * height * r^2
        /// and force
        /// force_x = -dV/dx = - dr/dx * dV/dr = -2 * massFactor * height * (x - x_i)
        /// The force is contrained such that it never exceeds a prespecified maximum force. 
        /// 
        /// </remarks>
        private float CalculateForce(Vector3 diff, float distSquared, out Vector3 force, float gradientScaleFactor = 1f, float interactionGradientScaleFactor = 1f, float massFactor = 1f)
        {
            //TODO Do we really need this many variables?
            float height = gradientScaleFactor * globalGradientScaleFactor * interactionGradientScaleFactor;
            float energy = 0f;
            if (HarmonicForce)
            {
                energy = -1f * (ApplyMassFactor ? massFactor : 1f)  * height * diff.LengthSquared;
                force = 2f * (ApplyMassFactor ? massFactor : 1f) * height * diff;
                const float maxForce = 10000f; 
                const float minForce = -1f * maxForce;
                
                for (int i = 0; i < 3; i++)
                {
                    force[i] = force[i] > maxForce ? maxForce : force[i];
                    force[i] = force[i] < minForce ? minForce : force[i];
                }
                return energy;
            }
            if (LegacyMode)
            {
                force = massFactor * height * diff * MathUtilities.ComputeFastExp((-1 * distSquared) / (2 * widthSqr));
            }
            else
            {
                energy = -1f * (ApplyMassFactor ? massFactor : 1f) * height * (float)Math.Exp((-1 * distSquared) / (2 * widthSqr));
                force = (ApplyMassFactor ? massFactor : 1f) * height * diff * (float)Math.Exp((-1 * distSquared) / (2 * widthSqr));
            }
            return energy;
        }
        
        // TODO review this stuff as a potential way to prevent momentum.
        private HashSet<int> lastStepAtoms = new HashSet<int>();

        private HashSet<int> thisStepAtoms = new HashSet<int>();
        private double maxHarmonicEnergy;
        private float potentialEnergy;


        private void ReinitializeInteractionVelocities(AtomicSystem system, ICollection<int> previousAtoms)
        {
            List<double> randomNoise = MathUtilities.GenerateListOfGaussianNoise(previousAtoms.Count * 3);

            //Reinitialize velocities of atoms.
            int i = 0;
            foreach(int atomId in previousAtoms)
            {
                if (Math.Abs(system.Particles.Mass[atomId]) < 0.0001f)
                    continue;
                //get molar gas constant in kJ / (K * mol)
                const float boltz = PhysicalConstants.MolarGasConstantR / 1000;
                double velocityScale = Math.Sqrt(boltz * TemperatureScaleFactor * system.Thermostat.EquilibriumTemperature / system.Particles.Mass[atomId]);
                Vector3 velocity = new Vector3((float)randomNoise[3 * i + 0], (float)randomNoise[3 * i + 1], (float)randomNoise[3 * i + 2]);
                system.Particles.Velocity[i] = velocity * (float)velocityScale;
                i++;
            }
        }

        // Updates the set of atoms that are no longer being interacted with. 
        private void UpdateLastInteractionAtoms(AtomicSystem system)
        {
            //Get all the atoms interacted with this step. 
            thisStepAtoms.Clear();
            foreach (Interaction interaction in system.Interactions)
            {
                thisStepAtoms.UnionWith(interaction.SelectedAtoms);
            }
            
            //Get the atoms from last step that are not still being interacted with. 
            lastStepAtoms.ExceptWith(thisStepAtoms);
            
            //Reinitialize the velocities of those atoms. 
            ReinitializeInteractionVelocities(system, lastStepAtoms);
            
            //Set last steps atoms to be this steps atoms, ready for the next step
            lastStepAtoms.Clear(); 
            lastStepAtoms.UnionWith(thisStepAtoms);
            
        }
        
        /// <inheritdoc />
        public float GetPotentialEnergy()
        {
            return potentialEnergy;
        }
    }
}