﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;

namespace Simbox.MD.ForceField.Interactive.InteractiveGaussian
{
    internal class InstantiatedInteractiveGaussianForceFieldTerms : IInstantiatedForceFieldTerms
    {
        internal readonly List<float> GradientScaleFactors = new List<float>();
        internal bool ApplyMassFactor;
        internal bool LegacyMode;
        public bool ReinitializeVelocities;
        public float TemperatureScaleFactor;
        public bool HarmonicForce;

        public IForceField GenerateForceField(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter)
        {
            InteractiveGaussian f = new InteractiveGaussian();
            f.GradientScaleFactor.Clear();
            f.GradientScaleFactor.AddRange(GradientScaleFactors);
            f.ApplyMassFactor = ApplyMassFactor;
            f.LegacyMode = LegacyMode;
            f.ReinitializeVelocities = ReinitializeVelocities;
            f.TemperatureScaleFactor = TemperatureScaleFactor;
            f.HarmonicForce = HarmonicForce;
            return f;
        }
    }
}