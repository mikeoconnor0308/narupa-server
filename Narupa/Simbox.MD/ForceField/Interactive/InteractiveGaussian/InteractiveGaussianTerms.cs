﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;

namespace Simbox.MD.ForceField.Interactive.InteractiveGaussian
{
    /// <summary>
    /// Represents a modifier term to particle interactions.
    /// </summary>

    /// <seealso cref="Nano.Loading.ILoadable" />
    [XmlName("ParticleInteraction")]
    internal class ParticleInteractionModifierTerm : ILoadable
    {
        /// <summary>
        /// The scale factor applied to modify the particle interaction.
        /// </summary>
        public float ScaleFactor;

        /// <summary>
        /// The atom path.
        /// </summary>

        public string AtomPath;

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            ScaleFactor = Helper.GetAttributeValue(node, "ScaleFactor", 1f);
            AtomPath = Helper.GetAttributeValue(node, "AtomPath", String.Empty);
            if (AtomPath == string.Empty)
            {
                context.Error("Missing AtomPath attribute for ParticleInteraction.");
            }

        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "ScaleFactor", ScaleFactor);
            return element;
        }
    }

    [XmlName("InteractiveGaussianForceField")]
    public class InteractiveGaussianTerms : IForceFieldTerms, IExternalForceFieldTerms
    {
        private float gradientScaleFactor;

        /// <summary>
        /// The particle modfiers
        /// </summary>
        private readonly List<ParticleInteractionModifierTerm> particleModfiers = new List<ParticleInteractionModifierTerm>();

        private bool massFactor;
        private bool legacyMode;

        
        /// <summary>
        /// Whether to reinitialize velocities after interacting. 
        /// </summary>
        private bool reinitializeVelocities;
        
        /// <summary>
        /// How much to adjust reinitialized velocities compared to equilibrium temperature. 
        /// </summary>
        /// <remarks>
        /// Reinitialising velocities straight to target temperature (i.e. a value of 1) may result in instabilities.
        /// A scale factor of around 0.5 may make the system more stable.
        /// </remarks>
        private float temperatureScaleFactor = 0.5f;

        private bool harmonicForce = false;


        /// <summary>
        /// Adds to topology.
        /// </summary>
        /// <param name="parentTopology">The parent topology.</param>
        /// <param name="residue">The residue.</param>
        /// <param name="context">The context.</param>
        public void AddToTopology(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context)
        {
            InstantiatedInteractiveGaussianForceFieldTerms flattenedTerms = null;
            bool termExists = false;

            foreach (IInstantiatedForceFieldTerms forceFieldTermType in parentTopology.ForceFieldTerms)
            {
                if (forceFieldTermType is InstantiatedInteractiveGaussianForceFieldTerms)
                {
                    flattenedTerms = forceFieldTermType as InstantiatedInteractiveGaussianForceFieldTerms;
                    termExists = true;
                    break;
                }
            }

            //If it doesn't exist, create it.
            if (termExists == false)
            {
                flattenedTerms = new InstantiatedInteractiveGaussianForceFieldTerms();
            }
            //Add gradient scale factors for each particle.
            flattenedTerms.GradientScaleFactors.Capacity = parentTopology.NumberOfParticles;
            while (flattenedTerms.GradientScaleFactors.Count < parentTopology.NumberOfParticles)
            {
                flattenedTerms.GradientScaleFactors.Add(gradientScaleFactor);
            }
            //Apply modifier for each particle.
            foreach (ParticleInteractionModifierTerm interaction in particleModfiers)
            {
                flattenedTerms.GradientScaleFactors[parentTopology.AddressToAtomIndexMap[(AtomPath.Combine(context.Address, interaction.AtomPath))]] *= interaction.ScaleFactor;
            }

            flattenedTerms.ApplyMassFactor |= massFactor;
            flattenedTerms.LegacyMode |= legacyMode;
            flattenedTerms.ReinitializeVelocities |= reinitializeVelocities;
            flattenedTerms.TemperatureScaleFactor = flattenedTerms.TemperatureScaleFactor > 0
                ? Math.Min(flattenedTerms.TemperatureScaleFactor, temperatureScaleFactor)
                : temperatureScaleFactor;
            flattenedTerms.HarmonicForce |= harmonicForce;
            if (termExists == false)
            {
                parentTopology.ForceFieldTerms.Add(flattenedTerms);
            }
        }

        /// <summary>
        /// Adds to topology.
        /// </summary>
        /// <param name="parentTopology">The parent topology.</param>
        public void AddToTopology(InstantiatedTopology parentTopology)
        {
            InstantiatedInteractiveGaussianForceFieldTerms flattenedTerms = null;
            bool termExists = false;

            foreach (IInstantiatedForceFieldTerms forceFieldTermType in parentTopology.ForceFieldTerms)
            {
                if (forceFieldTermType is InstantiatedInteractiveGaussianForceFieldTerms)
                {
                    flattenedTerms = forceFieldTermType as InstantiatedInteractiveGaussianForceFieldTerms;
                    termExists = true;
                    break;
                }
            }

            //If it doesn't exist, create it.
            if (termExists == false)
            {
                flattenedTerms = new InstantiatedInteractiveGaussianForceFieldTerms();
            }
            //Add gradient scale factors for each particle.
            flattenedTerms.GradientScaleFactors.Capacity = parentTopology.NumberOfParticles;
            while (flattenedTerms.GradientScaleFactors.Count < parentTopology.NumberOfParticles)
            {
                flattenedTerms.GradientScaleFactors.Add(gradientScaleFactor);
            }

            flattenedTerms.ApplyMassFactor |= massFactor;
            flattenedTerms.LegacyMode |= legacyMode;
            flattenedTerms.ReinitializeVelocities |= reinitializeVelocities;
            flattenedTerms.TemperatureScaleFactor = flattenedTerms.TemperatureScaleFactor > 0
                ? Math.Min(flattenedTerms.TemperatureScaleFactor, temperatureScaleFactor)
                : temperatureScaleFactor;
            flattenedTerms.HarmonicForce |= harmonicForce;
            
            if (termExists == false)
            {
                parentTopology.ForceFieldTerms.Add(flattenedTerms);
            }
        }

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            gradientScaleFactor = Helper.GetAttributeValue(node, "GradientScaleFactor", 0f);
            massFactor = Helper.GetAttributeValue(node, "ApplyMassFactor", true);
            legacyMode = Helper.GetAttributeValue(node, "LegacyMode", false);

            harmonicForce = Helper.GetAttributeValue(node, "HarmonicForce", false);
            reinitializeVelocities = Helper.GetAttributeValue(node, "ReinitializeVelocities", false);
            temperatureScaleFactor = Helper.GetAttributeValue(node, "TemperatureScaleFactor", 0.5f);
            particleModfiers.Clear();
            particleModfiers.AddRange(Loader.LoadObjects<ParticleInteractionModifierTerm>(context, node.SelectSingleNode("ParticleInteractions")));
            
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "GradientScaleFactor", gradientScaleFactor);
            Helper.AppendAttributeAndValue(element, "ReinitializeVelocities", reinitializeVelocities);
            Helper.AppendAttributeAndValue(element, "TemperatureScaleFactor", temperatureScaleFactor);
            Loader.SaveObjects(context, element, particleModfiers, "ParticleInteractions");

            return element;
        }
    }
}