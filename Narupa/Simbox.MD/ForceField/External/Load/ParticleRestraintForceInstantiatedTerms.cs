﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Simbox.MD.ForceField.External.ForceField;

namespace Simbox.MD.ForceField.External.Load
{
    internal class ParticleRestraintForceInstantiatedTerms : IInstantiatedForceFieldTerms
    {
        internal readonly Dictionary<string, IList<ParticleRestraint>> Restraints = new Dictionary<string, IList<ParticleRestraint>>();

        public IForceField GenerateForceField(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter)
        {
            ParticleRestraintForce force = new ParticleRestraintForce();
            foreach (var restraintList in Restraints)
            {
                force.Restraints.Add( restraintList.Key, restraintList.Value);
            }
            return force;
        }
    }
}