﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Xml;
using Nano.Loading;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Simbox.MD.ForceField.External.ForceField;

namespace Simbox.MD.ForceField.External.Load
{
    [XmlName("ParticleRestraintForce")]
    internal class ParticleRestraintForceTerms : IForceFieldTerms, IExternalForceFieldTerms
    {
        private readonly List<ParticleRestraint> particleRestraints = new List<ParticleRestraint>();
        
        public void AddToTopology(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context)
        {
            ParticleRestraintForceInstantiatedTerms instantiatedParticleRestraints = null;
            bool termExists = false;

            foreach (IInstantiatedForceFieldTerms forceFieldTermType in parentTopology.ForceFieldTerms)
            {
                if (forceFieldTermType is ParticleRestraintForceInstantiatedTerms)
                {
                    instantiatedParticleRestraints = forceFieldTermType as ParticleRestraintForceInstantiatedTerms;
                    termExists = true;
                    break;
                }
            }

            //If it doesn't exist, create it.
            if (termExists == false)
            {
                instantiatedParticleRestraints = new ParticleRestraintForceInstantiatedTerms();
            }

            //Add all the data for this residue into the flattened topology.
            foreach (ParticleRestraint restraint in particleRestraints)
            {
                ParticleRestraint instantiatedRestraint = new ParticleRestraint();
                instantiatedRestraint.AtomIndex = context.ResolveIndexOfAtom(parentTopology, new AtomPath(restraint.AtomIndex.ToString()));
                instantiatedRestraint.K = restraint.K;
                instantiatedRestraint.Pos0 = parentTopology.Positions[instantiatedRestraint.AtomIndex];
                instantiatedRestraint.RestraintDimension = restraint.RestraintDimension;

                if (instantiatedParticleRestraints.Restraints.ContainsKey(instantiatedRestraint.Name))
                {
                    instantiatedParticleRestraints.Restraints[instantiatedRestraint.Name].Add(instantiatedRestraint);
                }
                else
                {
                    var restraintList = new List<ParticleRestraint>(1);
                    restraintList.Add(instantiatedRestraint);
                    instantiatedParticleRestraints.Restraints.Add(instantiatedRestraint.Name, restraintList);
                }
            }

            //Add the terms to the parent topology
            if (termExists == false)
            {
                parentTopology.ForceFieldTerms.Add(instantiatedParticleRestraints);
            }
        }

        public void Load(LoadContext context, XmlNode node)
        {
            particleRestraints.AddRange(Loader.LoadObjects<ParticleRestraint>(context, node.SelectSingleNode("ParticleRestraints")));
        }

        public void AddToTopology(InstantiatedTopology parentTopology)
        {
            ParticleRestraintForceInstantiatedTerms instantiatedParticleRestraints = null;
            bool termExists = false;

            foreach (IInstantiatedForceFieldTerms forceFieldTermType in parentTopology.ForceFieldTerms)
            {
                if (forceFieldTermType is ParticleRestraintForceInstantiatedTerms)
                {
                    instantiatedParticleRestraints = forceFieldTermType as ParticleRestraintForceInstantiatedTerms;
                    termExists = true;
                    break;
                }
            }

            //If it doesn't exist, create it.
            if (termExists == false)
            {
                instantiatedParticleRestraints = new ParticleRestraintForceInstantiatedTerms();
            }

            //Add all the data for this residue into the flattened topology.
            foreach (ParticleRestraint restraint in particleRestraints)
            {
                ParticleRestraint instantiatedRestraint = new ParticleRestraint();
                instantiatedRestraint.AtomIndex = restraint.AtomIndex;
                instantiatedRestraint.K = restraint.K;
                instantiatedRestraint.Pos0 = parentTopology.Positions[instantiatedRestraint.AtomIndex];
                instantiatedRestraint.RestraintDimension = restraint.RestraintDimension;

                if (instantiatedParticleRestraints.Restraints.ContainsKey(instantiatedRestraint.Name))
                {
                    instantiatedParticleRestraints.Restraints[instantiatedRestraint.Name].Add(instantiatedRestraint);
                }
                else
                {
                    var restraintList = new List<ParticleRestraint>(1);
                    restraintList.Add(instantiatedRestraint);
                    instantiatedParticleRestraints.Restraints.Add(instantiatedRestraint.Name, restraintList);
                }
            }

            //Add the terms to the parent topology
            if (termExists == false)
            {
                parentTopology.ForceFieldTerms.Add(instantiatedParticleRestraints);
            } 
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            element.AppendChild(Loader.SaveObjects<ParticleRestraint>(context, element, particleRestraints, "ParticleRestraints"));
            return element;
        }
    }
}