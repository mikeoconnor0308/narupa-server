﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Residues;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using SlimMath;
using System.Linq;
using System.Reflection;
using Rug.Osc;

namespace Simbox.MD.ForceField.External.ForceField
{
    public enum RestraintDimensions { X, Y, Z, XY, XZ, YZ, XYZ };

    public class ParticleRestraint : ILoadable 
    {
        /// <summary>
        /// Restraint force constant.
        /// </summary>
        public float K;

        /// <summary>
        /// Atom index to apply constraint to.
        /// </summary>
        public int AtomIndex;

        /// <summary>
        /// Target position.
        /// </summary>
        public Vector3 Pos0;


        /// <summary>
        /// Name of this restraint.
        /// </summary>
        /// <remarks>
        /// Can be used to identify this restraint or group it with other restraints so it can
        /// be altered or removed it at runtime.
        /// </remarks>
        public string Name = "Ungrouped";
        
        public RestraintDimensions RestraintDimension;


        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "K", K);
            Helper.AppendAttributeAndValue(element, "AtomIndex", AtomIndex);
            Helper.AppendAttributeAndValue(element, "RestraintDimensions", RestraintDimension.ToString());
            Helper.AppendAttributeAndValue(element, "Name", Name);
            return element;
        }

        public void Load(LoadContext context, XmlNode node)
        {
            //Set K to be 200 kCal/mol
            K = Helper.GetAttributeValue(node, "K", 200 * Units.KJPerKCal * Units.AngstromsPerNm * Units.AngstromsPerNm);
            AtomIndex = Helper.GetAttributeValue(node, "AtomIndex", -1);
            RestraintDimension = Helper.GetAttributeValue(node, "RestraintDimensions", RestraintDimensions.XYZ);
            Name = Helper.GetAttributeValue(node, "Name", Name);
        }


    }

    /// <summary>
    /// Applies restraints to particles, holding them in place. 
    /// </summary>
    public class ParticleRestraintForce : IForceField, IExternalForceField, ICommandProvider
    {
        public string Name
        {
            get
            {
                return "Particle Restraint Force";
            }
        }

        private TransportContext transportContext;
        
        //TODO should the list be collapsed into a data structure?
        public Dictionary<string, IList<ParticleRestraint>> Restraints = new Dictionary<string, IList<ParticleRestraint>>();
        private IAtomicSystem sys;
        private object restraintLock = new object();
        private float potentialEnergy;

        public float CalculateForceField(IAtomicSystem system, List<Vector3> Forces)
        {
            Vector3 pos;
            sys = system;
            float V = 0f;
            foreach (var restraintList in Restraints.Values)
            {
                foreach(var restraint in restraintList)
                {
                    Vector3 diff = system.Particles.Position[restraint.AtomIndex] - restraint.Pos0;
                    switch (restraint.RestraintDimension)
                    {
                        case RestraintDimensions.X:
                            diff.Y = 0f;
                            diff.Z = 0f;
                            break;

                        case RestraintDimensions.Y:
                            diff.X = 0f;
                            diff.Z = 0f;
                            break;

                        case RestraintDimensions.Z:
                            diff.X = 0f;
                            diff.Y = 0f;
                            break;

                        case RestraintDimensions.XY:
                            diff.Z = 0f;
                            break;

                        case RestraintDimensions.XZ:
                            diff.Y = 0f;
                            break;

                        case RestraintDimensions.YZ:
                            diff.X = 0f;
                            break;

                        case RestraintDimensions.XYZ:
                            break;

                        default:
                            break;
                    }
                    V += restraint.K * diff.LengthSquared;

                    Forces[restraint.AtomIndex] += -2f * restraint.K * diff;
                }

            }

            potentialEnergy = V; 
            return V;
        }

        /// <inheritdoc />
        public string Address
        {
            get { return "/restraints"; }
        }

        /// <inheritdoc />
        public void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        /// <inheritdoc />
        public void DetachTransportContext(TransportContext transportContext)
        {
            this.transportContext = null;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            return;
        }

        private void AddRestraint(string name, float k,  List<int> atoms)
        {
                
            if (atoms.Count == 0)
                return;
            lock (restraintLock)
            {
                IList<ParticleRestraint> restraintList; 
                if (Restraints.ContainsKey(name))
                {
                    restraintList = Restraints[name];
                }
                else
                {
                    restraintList = new List<ParticleRestraint>(atoms.Count);
                    Restraints[name] = restraintList;
                }
                foreach (var index in atoms)
                {
                    ParticleRestraint restraint = new ParticleRestraint()
                    {
                        K = k,
                        AtomIndex = index,
                        Pos0 = sys.Particles.Position[index],
                        Name =  name,
                        RestraintDimension = RestraintDimensions.XYZ
                    };
                    restraintList.Add(restraint);
                }
            }
        }
        
        private void RemoveRestraints(string name)
        {
            lock (restraintLock)
            {
                if (Restraints.ContainsKey(name))
                {
                    Restraints.Remove(name);
                }
            }
        }

        private void RemoveRestraints(int atomIndex)
        {
            lock (restraintLock)
            {
                foreach (var restraintList in Restraints.Values)
                {
                    restraintList.ToList().RemoveAll(restraint => restraint.AtomIndex == atomIndex);
                }
            }
        }
        
        [OscCommand(ExampleArguments = "Selection 1, 100.5, 1, 2, 3", Help = "Adds restraints to the specified atoms at their current positions, with the given name for grouping, and force factor.", TypeTagString = "s,f,[i]")] 
        public void AddRestraints(OscMessage message) 
        { 
            OscCommandAttribute attributes = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>(); 
            OscMessage reply; 
            float force = 1000; 
            Vector3 position = Vector3.Zero;
            string name;
            List<int> atoms = new List<int>();
            try
            {
                name = (string) message[0];
                force = (float)message[1];
                for (int i = 2; i < message.Count; i++)
                {
                    atoms.Add((int)message[i]);
                }
            } 
            catch 
            { 
                return; 
            } 
    
            AddRestraint(name, force, atoms);    
            reply = new OscMessage(Address+"/addRestraints", "added restraint"); 
            transportContext.Transmitter.Transmit((TransportPacketPriority)TransportPacketPriority.Important, message.Origin, new[] { reply }); 
        } 
        
        [OscCommand(ExampleArguments = "Selection 1", Help = "Removes restraints applied to the specified named group.", TypeTagString = "s")] 
        public void RemoveRestraints(OscMessage message) 
        { 
            OscCommandAttribute attributes = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>();
            attributes.Address = Address + "/removeRestraints";
            OscMessage reply; 
            string name;
            try
            {
                name = (string) message[0];
            } 
            catch 
            { 
                OscCommandHelper.SendTypeTagString(transportContext, message.Origin, attributes); 
                return; 
            } 
    
            RemoveRestraints(name);
            reply = new OscMessage(attributes.Address, "removed restraint"); 
            transportContext.Transmitter.Transmit((TransportPacketPriority)TransportPacketPriority.Important, message.Origin, new[] { reply }); 
        } 
        
        /// <inheritdoc />
        public float GetPotentialEnergy()
        {
            return potentialEnergy;
        }
    }
}