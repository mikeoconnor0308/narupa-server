﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Simbox.MD.ForceField.LennardJones
{
    /// <summary>
    /// Represents the Lennard Jones Force Field for Non-bonded terms.
    /// </summary>
    /// <remarks>
    /// This implementation of Lennard Jones serves primarily as a reference implementation that is unoptimised and portable.
    /// Fancy optimisations such as neighbour/cell lists and parallelism have not been implemented.
    /// </remarks>
    /// <seealso cref="Nano.Science.Simulation.ForceField.IForceField" />
    public class LennardJonesForceField : IForceField
    {
        /// <summary>
        /// The LJ terms to calculate.
        /// </summary>
        public readonly List<LennardJonesTerm> LJTerms = new List<LennardJonesTerm>();

        /// <summary>
        /// The set of exceptions for each particle.
        /// </summary>
        public readonly List<HashSet<int>> Exceptions = new List<HashSet<int>>();

        /// <summary>
        /// The cut off distance.
        /// </summary>
        public float CutOffDistance;

        private float cutoffDistanceRecipricol;
        private int numberOfParticles;
        private const float MAX_ENERGY = 1.0e6f;
        //private OpenMMForceField openMMForceField;

        private IReporter Reporter;
        private bool runOpenMM;

        /// <summary>
        /// Name of ForceField.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return "Lennard Jones";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LennardJonesForceField" /> class.
        /// </summary>
        /// <param name="topology">The topology.</param>
        /// <param name="properties">The properties.</param>
        /// <param name="terms">The terms.</param>
        /// <param name="cutOffDistance">The cut off distance.</param>
        /// <param name="reporter">The reporter.</param>
        public LennardJonesForceField(InstantiatedTopology topology, SystemProperties properties, List<LennardJonesTerm> terms, float cutOffDistance, IReporter reporter = null)
        {
            Reporter = reporter;
            LJTerms.AddRange(terms);
            numberOfParticles = topology.NumberOfParticles;
            GenerateExceptions(topology.Molecules);
            CutOffDistance = cutOffDistance;
            cutoffDistanceRecipricol = 1.0f / CutOffDistance;

            //TODO Cell list??
            /*
            if (topology.NumberOfParticles > 100)
            {
                try
                {
                    if (CreateOpenMMSystem(properties.SimBoundary, topology))
                        runOpenMM = true;
                }
                catch (Exception e)
                {
                    Reporter?.PrintException(e, "Exception encountered while trying to run Lennard Jones force field with OpenMM. Switching back to reference implementation.");
                }
            }
            */
        }

        /*
        private bool CreateOpenMMSystem(SimulationBoundary boundary, InstantiatedTopology topology)
        {
            //Try to load the OpenMM libary, if it's not present, then just run with reference code.
            try
            {
                OpenMMForceField.LoadOpenMMLibraryAndPlugins("^/Native/OpenMM/Win32/");
            }
            catch (Exception e)
            {
                Reporter?.PrintException(e, "Attempted to run lennard jones forces with OpenMM, but could not load it. Switching back to (slow) reference build.");
                return false;
            }

            //Set up the force field.
            openMMForceField = new OpenMMForceField(boundary, Reporter);
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                openMMForceField.AddParticle(PeriodicTable.GetElementProperties(topology.Elements[i]).AtomicWeight);
            }

            //Create a nonbonded force that matches the reference one.
            OpenMMNET.NonbondedForce openMMNonBondedForce = new OpenMMNET.NonbondedForce();
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                openMMNonBondedForce.addParticle(0f, LJTerms[i].Sigma, LJTerms[i].Epsilon);
            }
            HashSet<BondPair> exceptions = new HashSet<BondPair>();

            for (int i = 0; i < Exceptions.Count; i++)
            {
                foreach (int j in Exceptions[i])
                {
                    if (i < j)
                        exceptions.Add(new BondPair(i, j));
                    else
                        exceptions.Add(new BondPair(j, i));
                }
            }
            foreach (BondPair b in exceptions)
            {
                openMMNonBondedForce.addException(b.A, b.B, 0f, 1f, 0f);
            }
            if (boundary.PeriodicBoundary)
                openMMNonBondedForce.setNonbondedMethod(OpenMMNET.NonbondedForce.NonbondedMethod.CutoffPeriodic);
            else
                openMMNonBondedForce.setNonbondedMethod(OpenMMNET.NonbondedForce.NonbondedMethod.CutoffNonPeriodic);
            openMMNonBondedForce.setCutoffDistance(CutOffDistance);

            //Create the openmm system and context and set everything up.
            openMMForceField.AddForce(openMMNonBondedForce);
            openMMForceField.CreateContext();
            openMMForceField.CreateParticleVectors(topology.NumberOfParticles);
            if (boundary.PeriodicBoundary)
            {
                openMMForceField.SetPeriodicBox(boundary.GetPeriodicBoxVectors());
            }

            return true;
        }
        */

        /// <summary>
        /// Initializes a new instance of the <see cref="LennardJonesForceField"/> class.
        /// </summary>
        public LennardJonesForceField()
        {
            //Set default CutOffDistance to be 0.5 nm;
            CutOffDistance = 0.3f;
            cutoffDistanceRecipricol = 1.0f / CutOffDistance;
        }

        /// <summary>
        /// Generates the exceptions to LJ for the given set of molecules.
        /// </summary>
        /// <param name="molecules">The molecules.</param>
        private void GenerateExceptions(List<Molecule> molecules)
        {
            Exceptions.Clear();
            int i;
            for (i = 0; i < numberOfParticles; i++)
            {
                Exceptions.Add(new HashSet<int>());
            }
            foreach (Molecule mol in molecules)
            {
                HashSet<BondPair> molExceptions = mol.GetFirstAndSecondNeighbours();
                foreach (BondPair bond in molExceptions)
                {
                    Exceptions[bond.A].Add(bond.B);
                    Exceptions[bond.B].Add(bond.A);
                }
            }
        }

        /// <summary>
        /// Calculates the force field.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="Forces">The forces.</param>
        /// <returns>System.Single.</returns>
        public float CalculateForceField(IAtomicSystem system, List<Vector3> Forces)
        {
            /*
            if (runOpenMM)
                return openMMForceField.CalculateForceField(system, Forces);
            */
            List<Vector3> position = system.Particles.Position;

            Vector3 ipos, diff, force;
            float rRec, r2Rec, r6Rec, V, dVdr;

            float energy = 0f;
            int mask;
            float virial = 0;

            for (int i = 0; i < numberOfParticles; i++)
            {
                ipos = position[i];
                float sigmaI = LJTerms[i].Sigma;
                float epsilonI = LJTerms[i].Epsilon;

                for (int j = 0; j < numberOfParticles; j++)
                {
                    if (j == i)
                        continue;
                    diff = ipos - position[j];
                    if (system.Boundary.PeriodicBoundary)
                        system.Boundary.GetPeriodicDifference(ref diff);
                    //Take the reciprocol of the difference
                    float r = diff.Length;
                    rRec = 1.0f / diff.Length;
                    r2Rec = rRec * rRec;

                    mask = Exceptions[i].Contains(j) ? 0 : 1;

                    // if r > cutoff then 1/r < 1/cutoff
                    mask &= rRec <= cutoffDistanceRecipricol ? 0 : 1;

                    if (mask == 0)
                        continue;

                    float sigma = (sigmaI + LJTerms[j].Sigma) * 0.5f;
                    float epsilon = (float)Math.Sqrt(epsilonI * LJTerms[j].Epsilon);
                    float sigma2 = sigma * sigma;
                    float sigma6 = sigma2 * sigma2 * sigma2;

                    float A = 4.0f * epsilon * sigma6 * sigma6;
                    float B = -4.0f * epsilon * sigma6;

                    r6Rec = r2Rec * r2Rec * r2Rec;

                    //Calculate energy: this lovely equation is simply (A/r^12) + (B/r^6)
                    V = ((A * r6Rec * r6Rec) + (B * r6Rec));
                    //Calculate dV/dr: this is -12(A/r^13)-6(A/r^7)
                    dVdr = (-12f * (A * r6Rec * r6Rec * rRec) + -6f * (B * r6Rec * rRec));

                    //Zero out energy and force if over MAX_ENERGY
                    mask &= V > MAX_ENERGY ? 0 : 1;

                    //Compare against NaNs and Infinities.
                    mask &= float.IsNaN(V) ? 0 : 1;
                    mask &= float.IsInfinity(V) ? 0 : 1;
                    mask &= float.IsNegativeInfinity(V) ? 0 : 1;

                    //Apply mask which will multiply by zero if
                    //any of the conditions have failed
                    V *= mask;

                    //force = -dV/dt =  -(ri - rj)/rij * dV/dr
                    force = -mask * diff * (dVdr * rRec);

                    //multiply energy by 0.5 since each particle is added twice.
                    energy += 0.5f * V;
                    Forces[i] += force;
                    virial += 0.5f * Vector3.Dot(diff, force);
                }
            }
            //var atomicSystem = system as AtomicSystem.AtomicSystem;
            //if (atomicSystem != null) atomicSystem.Virial += virial;
            return energy;
        }

        /// <summary>
        /// Adds an exception between the specified particles, so computation of LJ forces will be skipped between them.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <param name="j">The j.</param>
        public void AddException(int i, int j)
        {
            Exceptions[i].Add(j);
            Exceptions[j].Add(i);
        }

        /// <summary>
        /// Disposes this instance.
        /// </summary>
        public void Dispose()
        {
            /*
            if (runOpenMM)
                openMMForceField.Dispose();
            runOpenMM = false;
            */
        }
    }
}