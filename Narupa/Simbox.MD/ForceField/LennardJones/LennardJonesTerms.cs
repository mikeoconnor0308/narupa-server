﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Simbox.MD.ForceField.MM3.Load;

namespace Simbox.MD.ForceField.LennardJones
{
    /// <summary>
    /// Represents a mapping between a <see cref="AtomPath"/> and a Lennard Jones term.
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    [XmlName("LennardJonesAtomMapping")]
    public class LennardJonesAtomMapping : ILoadable
    {
        /// <summary>
        /// The atom path.
        /// </summary>
        public AtomPath AtomPath;

        /// <summary>
        /// Sigma value in nm.
        /// </summary>
        public float Sigma;

        /// <summary>
        /// Epsilon value in kJ/mol.
        /// </summary>
        public float Epsilon;

        /// <summary>
        /// (Optional) MM3 Type.
        /// </summary>
        public int MM3Type;

        public LennardJonesAtomMapping()
        {
        }

        public LennardJonesAtomMapping(string atomPath, int mm3Type)
        {
            AtomPath = new AtomPath(atomPath);
            MM3Type = mm3Type;
        }

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            AtomPath = new AtomPath(Helper.GetAttributeValue(node, "AtomPath", string.Empty));
            MM3Type = Helper.GetAttributeValue(node, "MM3Type", -1);
            if (MM3Type == -1)
            {
                Sigma = Helper.GetAttributeValue(node, "Sigma", 0f);
                if (Sigma == 0f)
                    context.Error("Missing or zero sigma value");
                Epsilon = Helper.GetAttributeValue(node, "Epsilon", 0f);
            }
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "AtomPath", AtomPath.ToString());
            Helper.AppendAttributeAndValue(element, "Sigma", Sigma);
            Helper.AppendAttributeAndValue(element, "Epsilon", Epsilon);
            if (MM3Type != -1) Helper.AppendAttributeAndValue(element, "MM3Type", MM3Type);
            return element;
        }
    }

    /// <summary>
    /// </summary>
    /// <seealso cref="Nano.Science.Simulation.ForceField.IForceFieldTerms" />
    [XmlName("LennardJonesForceField")]
    public class LennardJonesTerms : IForceFieldTerms
    {
        /// <summary>
        /// The mappings from atom paths to Lennard Jones terms.
        /// </summary>
        public readonly List<LennardJonesAtomMapping> LennardJonesMappings = new List<LennardJonesAtomMapping>();

        /// <summary>
        /// The cut off distance.
        /// </summary>
        public float CutOffDistance = 0.5f;

        private string mm3DataFile = "^/Assets/Data/mm3.xml";

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            XmlNode dataFileNode = node.SelectSingleNode("MM3DataFile");

            if (dataFileNode != null) mm3DataFile = Helper.GetAttributeValue(dataFileNode, "File", string.Empty);

            LennardJonesMappings.AddRange(Loader.LoadObjects<LennardJonesAtomMapping>(context, node.SelectSingleNode("LennardJonesAtomMappings")));

            CutOffDistance = Helper.GetAttributeValue(node, "CutOffDistance", CutOffDistance);
        }

        /// <summary>
        /// Parses the MM3 LJ data if it has been used.
        /// </summary>
        /// <param name="mm3DataFile">The MM3 data file.</param>
        public void ParseMM3LJTypes(string mm3DataFile)
        {
            //First loop over the lennard jones mappings and look for any reference to an MM3Type.
            bool useMM3 = false;
            foreach (LennardJonesAtomMapping ljmap in LennardJonesMappings)
            {
                if (ljmap.MM3Type != -1)
                {
                    useMM3 = true;
                    break;
                }
            }
            if (useMM3 == false)
                return;

            //if there are some mm3 types, dig the lj terms out of the mm3 data set.
            MM3Data mm3Data = new MM3Data(mm3DataFile);
            foreach (LennardJonesAtomMapping ljmap in LennardJonesMappings)
            {
                if (ljmap.MM3Type != -1)
                {
                    ljmap.Sigma = mm3Data.Atoms[ljmap.MM3Type].Sigma * Units.NmPerAngstrom;
                    ljmap.Epsilon = mm3Data.Atoms[ljmap.MM3Type].Epsilon * Units.KJPerKCal;
                }
            }
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            element.AppendChild(Loader.SaveObjects(context, element, LennardJonesMappings, "LennardJonesAtomMappings"));
            Helper.AppendAttributeAndValue(element, "CutOffDistance", CutOffDistance);
            return element;
        }

        /// <summary>
        /// Adds the Lennard Jones terms to the topology.
        /// </summary>
        /// <param name="parentTopology">The parent topology.</param>
        /// <param name="residue">The residue.</param>
        /// <param name="context">The context.</param>
        public void AddToTopology(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context)
        {
            InstantiatedLennardJonesTerms flattenedLJTerms = null;
            bool termExists = false;

            ParseMM3LJTypes(mm3DataFile);
            //Check for existing MM3FlattenedTerms in the flattened topology
            foreach (IInstantiatedForceFieldTerms forceFieldTermType in parentTopology.ForceFieldTerms)
            {
                if (forceFieldTermType is InstantiatedLennardJonesTerms)
                {
                    flattenedLJTerms = forceFieldTermType as InstantiatedLennardJonesTerms;
                    termExists = true;
                    break;
                }
            }

            //If it doesn't exist, create it.
            if (termExists == false)
            {
                flattenedLJTerms = new InstantiatedLennardJonesTerms();
            }

            //Add all the data for this residue into the flattened topology.
            flattenedLJTerms.LJTerms.Capacity = parentTopology.NumberOfParticles;
            while (flattenedLJTerms.LJTerms.Count < parentTopology.NumberOfParticles)
            {
                flattenedLJTerms.LJTerms.Add(new LennardJonesTerm());
            }
            //Fill atom types correctly. Doing it this way means input file does not have to be ordered.
            foreach (LennardJonesAtomMapping atomMapping in LennardJonesMappings)
            {
                flattenedLJTerms.LJTerms[context.ResolveIndexOfAtom(parentTopology, atomMapping.AtomPath)] = new LennardJonesTerm(atomMapping.Sigma, atomMapping.Epsilon);
            }

            //Add cutoff distance
            flattenedLJTerms.CutOffDistance = Math.Max(CutOffDistance, flattenedLJTerms.CutOffDistance);

            //Add the terms to the parent topology
            if (termExists == false)
            {
                parentTopology.ForceFieldTerms.Add(flattenedLJTerms);
            }
        }
    }
}