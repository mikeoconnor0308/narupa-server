﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Simbox.MD.ForceField.LennardJones
{
    /// <summary>
    /// Represents the terms required to compute the Lennard Jones potential.
    /// </summary>
    public struct LennardJonesTerm
    {
        /// <summary>
        /// The sigma value in nm.
        /// </summary>
        /// <remarks>
        /// This value represents the distance at which the inter-particle potential is zero.
        /// </remarks>
        public float Sigma;

        /// <summary>
        /// The epsilon value in kJ/mol.
        /// </summary>
        /// <remarks>
        /// This value represents the depth of the potential well, or how strong the interaction is.
        /// </remarks>
        public float Epsilon;

        /// <summary>
        /// Initializes a new instance of the <see cref="LennardJonesTerm"/> struct.
        /// </summary>
        /// <param name="sigma">The sigma.</param>
        /// <param name="epsilon">The epsilon.</param>
        public LennardJonesTerm(float sigma, float epsilon)
        {
            Sigma = sigma;
            Epsilon = epsilon;
        }
    }
}