﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;

namespace Simbox.MD.ForceField.LennardJones
{
    internal class InstantiatedLennardJonesTerms : IInstantiatedForceFieldTerms
    {
        public readonly List<LennardJonesTerm> LJTerms = new List<LennardJonesTerm>();
        internal float CutOffDistance;

        public IForceField GenerateForceField(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter)
        {
            reporter?.PrintDetail("Generating Lennard Jones terms for topology.");
            return new LennardJonesForceField(parentTopology, properties, LJTerms, CutOffDistance, reporter);
        }
    }
}