﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Simbox.MD.ForceField.EVB.State
{
    /// <summary>
    /// Represents an EVB state loaded into a topology.
    /// </summary>
    public class EvbStateInstantiated
    {
        /// <summary>
        /// The topology the EVB state represents.
        /// </summary>
        public InstantiatedTopology Topology;

        /// <summary>
        /// The energy shift applied to this state.
        /// </summary>
        public float EnergyShift;

        /// <summary>
        /// The name of this state.
        /// </summary>
        public string Name;

        /// <summary>
        /// Creates an <see cref="EvbState"/> from the terms of this instance.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <param name="reporter">The reporter.</param>
        /// <returns>EVBState.</returns>
        internal EvbState CreateEvbState(SystemProperties properties, IReporter reporter)
        {
            EvbState state = new EvbState()
            {
                Topology = Topology.Clone(),
                Energy = 0,
                EnergyShift = EnergyShift,
                Name = Name,
            };
            state.Forces.Clear();
            state.Forces.Capacity = Topology.NumberOfParticles;

            reporter?.PrintNormal("Creating EVB State.");

            for (int i = 0; i < Topology.NumberOfParticles; i++)
            {
                state.Forces.Add(Vector3.Zero);
            }

            state.Topology.GenerateMolecules();

            foreach (IInstantiatedForceFieldTerms forceFieldTerms in Topology.ForceFieldTerms)
            {
                IForceField forceField = forceFieldTerms.GenerateForceField(state.Topology, properties, reporter);
                state.ForceFields.Add(forceField);
            }

            return state;
        }
    }
}