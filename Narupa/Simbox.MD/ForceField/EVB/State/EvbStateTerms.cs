﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;

namespace Simbox.MD.ForceField.EVB.State
{
    /// <summary>
    /// Represents a bond removal from a topology when loaded.
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    [XmlName("RemoveBond")]
    class RemoveBond : ILoadable
    {
        /// <summary>
        /// The path of the first atom to remove.
        /// </summary>
        public AtomPath A;

        /// <summary>
        /// The path of the second atom to remove.
        /// </summary>
        public AtomPath B;

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            A = new AtomPath(Helper.GetAttributeValue(node, "A", null));
            B = new AtomPath(Helper.GetAttributeValue(node, "B", null));
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "A", A.ToString());
            Helper.AppendAttributeAndValue(element, "B", B.ToString());

            return element;
        }
    }

    /// <summary>
    /// Represents the addition of a bond in a topology when loaded.
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    [XmlName("AddBond")]
    class AddBond : ILoadable
    {
        /// <summary>
        /// The first atom of the bond to be added.
        /// </summary>
        public AtomPath A;

        /// <summary>
        /// The second atom of the bond to be added.
        /// </summary>
        public AtomPath B;

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            A = new AtomPath(Helper.GetAttributeValue(node, "A", null));
            B = new AtomPath(Helper.GetAttributeValue(node, "B", null));
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "A", A.ToString());
            Helper.AppendAttributeAndValue(element, "B", B.ToString());

            return element;
        }
    }
    /// <summary>
    /// Represents the terms that need to be instantiated in an EVB state.
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    [XmlName("EVBState")]
    internal class EvbStateTerms : ILoadable
    {
        
        /// The name of the state.
        private string name;

        /// The energy shift of the state.
        private float energyShift;

        /// The list of bonds to be removed from the parent topology.
        private readonly List<RemoveBond> removedBonds = new List<RemoveBond>();

        /// The list of bonds to be added to the parent topology.
        private readonly List<AddBond> addedBonds = new List<AddBond>();

        /// The list of force field terms required by the state.
        private readonly List<IForceFieldTerms> forceFieldTerms = new List<IForceFieldTerms>();

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            name = Helper.GetAttributeValue(node, "Name", string.Empty);

            energyShift = Helper.GetAttributeValue(node, "EnergyShift", 0f);

            removedBonds.Clear();
            removedBonds.AddRange(Loader.LoadObjects<RemoveBond>(context, node.SelectSingleNode("Bonds")));

            addedBonds.Clear();
            addedBonds.AddRange(Loader.LoadObjects<AddBond>(context, node.SelectSingleNode("Bonds")));

            forceFieldTerms.Clear();
            forceFieldTerms.AddRange(Loader.LoadObjects<IForceFieldTerms>(context, node.SelectSingleNode("ForceFields")));
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", name);
            Helper.AppendAttributeAndValue(element, "EnergyShift", energyShift);

            List<ILoadable> bondModifications = new List<ILoadable>();

            bondModifications.AddRange(removedBonds);
            bondModifications.AddRange(addedBonds);

            Loader.SaveObjects(context, element, bondModifications, "Bonds");

            Loader.SaveObjects(context, element, forceFieldTerms, "ForceFields");

            return element;
        }

        /// <summary>
        /// Given the parent topology, instantiates an instance of <see cref="EvbStateInstantiated"/> in the topology.
        /// </summary>
        /// <param name="parentTopology">The parent topology.</param>
        /// <param name="residue">The residue.</param>
        /// <param name="context">The context.</param>
        /// <returns>EVBStateFlattened.</returns>
        /// <exception cref="System.Exception"></exception>
        public EvbStateInstantiated FlattenEvbStateTerms(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context)
        {
            EvbStateInstantiated state = new EvbStateInstantiated()
            {
                Topology = parentTopology.Clone(),
                EnergyShift = energyShift,
                Name = name,
            };

            foreach (RemoveBond removed in removedBonds)
            {
                try
                {
                    int indexA = state.Topology.Addresses.IndexOf(AtomPath.Combine(context.Address, removed.A.ToString()));
                    int indexB = state.Topology.Addresses.IndexOf(AtomPath.Combine(context.Address, removed.B.ToString()));

                    state.Topology.RemoveBond(new BondPair(indexA, indexB));
                }
                catch (Exception ex)
                {
                    throw new Exception($"Exception while trying to remove the bond between \"{removed.A}\" and \"{removed.B}\".", ex);
                }
            }

            foreach (AddBond added in addedBonds)
            {
                int indexA = state.Topology.Addresses.IndexOf(AtomPath.Combine(context.Address, added.A.ToString()));
                int indexB = state.Topology.Addresses.IndexOf(AtomPath.Combine(context.Address, added.B.ToString()));

                state.Topology.AddBond(new BondPair(indexA, indexB));
            }
            state.Topology.GenerateMolecules();

            //Remove any forcefields in the topology, as we only care about the ones that evb needs to know about.
            state.Topology.ForceFieldTerms.Clear();
            foreach (IForceFieldTerms terms in forceFieldTerms)
            {
                terms.AddToTopology(state.Topology, residue, ref context);
            }

            return state;
        }
    }
}