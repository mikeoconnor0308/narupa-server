﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Simbox.MD.ForceField.EVB.State;

namespace Simbox.MD.ForceField.EVB
{
    internal class EvbInstantiatedTerms : IInstantiatedForceFieldTerms
    {
        internal readonly List<CouplingElement> CouplingElements = new List<CouplingElement>();
        internal readonly List<EvbStateInstantiated> EvbStateFlattenedTerms = new List<EvbStateInstantiated>();

        public IForceField GenerateForceField(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter)
        {
            List<EvbState> evbStates = new List<EvbState>();

            reporter?.PrintEmphasized("Generating EVB force field consisting of {0} states.", EvbStateFlattenedTerms.Count);
            foreach (EvbStateInstantiated stateTemplate in EvbStateFlattenedTerms)
            {
                evbStates.Add(stateTemplate.CreateEvbState(properties, reporter));
            }

            EvbForceField forceField = new EvbForceField(parentTopology, CouplingElements, evbStates);
            return forceField;
        }
    }
}