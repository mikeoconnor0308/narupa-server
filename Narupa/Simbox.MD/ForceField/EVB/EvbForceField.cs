﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Loading;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Factorization;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Simbox.MD.ForceField.EVB.State;
using SlimMath;
// ReSharper disable RedundantAssignment
// ReSharper disable UnusedMember.Local

namespace Simbox.MD.ForceField.EVB
{
    /// <summary>
    /// Class storing all the data and methods associated with EVB
    /// </summary>
    /// <seealso cref="Nano.Science.Simulation.ForceField.IForceField" />
    [XmlName("EVBForceField")]
    public class EvbForceField : IForceField
    {
        #region Private Members

        private const float MaxEnergy = 1.0e08f;
        private float[] eigenvals;
        private float[] eigenvecs;
        private Vector3[] forces;
        private float[] hamiltonian;
        private int numberOfParticles;

        private List<EvbState> evbStates = new List<EvbState>();
        private InstantiatedTopology topology;

        #endregion Private Members

        #region Public Properties

        /// <summary>
        /// Gets the current state index.
        /// </summary>
        /// <value>The current state index.</value>
        public int CurrentState { get; private set; }

        /// <summary>
        /// Gets the current topology.
        /// </summary>
        /// <value>The current topology.</value>
        public InstantiatedTopology CurrentTopology { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value><c>true</c> if this instance is active; otherwise, <c>false</c>.</value>
        public bool IsActive { get; set; }

        /// <summary>
        /// Name of ForceField
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return "EVB"; }
        }

        /// <summary>
        /// Gets the number of states.
        /// </summary>
        /// <value>The number of states.</value>
        public int NumberOfStates { get { return evbStates.Count; } }

        #endregion Public Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EvbForceField"/> class.
        /// </summary>
        public EvbForceField()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EvbForceField"/> class.
        /// </summary>
        /// <param name="parentTopology">The parent topology.</param>
        /// <param name="couplingElements">The coupling elements.</param>
        /// <param name="evbStates1">The evb states1.</param>
        public EvbForceField(InstantiatedTopology parentTopology, List<CouplingElement> couplingElements, List<EvbState> evbStates1)
        {
            topology = parentTopology.Clone();
            evbStates.AddRange(evbStates1);

            numberOfParticles = parentTopology.NumberOfParticles;

            //Set up matrix
            hamiltonian = new float[NumberOfStates * NumberOfStates];
            foreach (CouplingElement coupling in couplingElements)
            {
                hamiltonian[coupling.State1 * NumberOfStates + coupling.State2] = coupling.Value;
                hamiltonian[coupling.State2 * NumberOfStates + coupling.State1] = coupling.Value;
            }
            eigenvals = new float[NumberOfStates];
            eigenvecs = new float[NumberOfStates * NumberOfStates];
            forces = new Vector3[numberOfParticles];
        }

        #endregion Constructor

        #region CalculateForceField

        /// <summary>
        /// EVB calculate forcefield function. Computes the energies on each state, constructs
        /// EVB Hamiltonian and computes overall force acting upon system
        /// </summary>
        public float CalculateForceField(IAtomicSystem system, List<Vector3> outputForces)
        {
            //Catch dynamic error
            if (numberOfParticles != system.NumberOfParticles)
            {
                throw new Exception("Error: EVB Forcefield requires the number of particles to remain constant. The number of particles has changed since initialisation");
            }

            //Calculate the energy and forces of each state
            CalculateStateEnergiesAndForces(system);

            //Then diagonalize the state matrix to find the eigenvalues and eigenvectors
            Diagonalize(hamiltonian, ref eigenvals, ref eigenvecs, NumberOfStates);

            //Find the smallest eigenvalue and associated eigenvector
            Tuple<float, float[]> smallestEigenPair = FindSmallestEigenvalue(eigenvals, eigenvecs, NumberOfStates);
            float energy = smallestEigenPair.Item1;
            float[] smevec = smallestEigenPair.Item2;
            int mostLikelyState = 0;
            for (int i = 0; i < NumberOfStates; i++)
            {
                if (Math.Abs(smevec[i]) > Math.Abs(smevec[mostLikelyState]))
                {
                    mostLikelyState = i;
                }
            }
            if (mostLikelyState != CurrentState)
            {
                CurrentState = mostLikelyState;
                CurrentTopology = evbStates[mostLikelyState].Topology;
                system.SetTopology(CurrentTopology);
            }
            else
            {
            }

            //Write output to file

            //Then calculate the forces using the smallest eigenvector and the forces computed in each configuration
            CalculateForces(smevec, system.Particles.Force);
            return energy;
        }

        private void CalculateForces(float[] smevec, List<Vector3> outputForces)
        {
            //Reference serial implementation

            /*
             *  The force acting on a particle is found following the following relation:
             *  d/dq D = U^t (d/dq H) U
             *
             * where D is the diagonal matrix of eigenvalues, U is the matrix of eigenvectors, H is the state matrix
             * and q is the direction of the force. This amounts to evaluating the above equation for every particle in
             * each direction x,y,z. However we only need the result on the smallest eigenvalue so we use the smallest eigenvector
             * rather than all of U
             */

            //First compute forces locally
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                forces[i] = Vector3.Zero;
            }

            for (int j = 0; j < evbStates.Count; j++)
            {
                for (int i = 0; i < topology.NumberOfParticles; i++)
                {
                    forces[i] += (float)Math.Pow(smevec[j], 2) * evbStates[j].Forces[i];
                }
            }
            //Apply forces to output
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                outputForces[i] += forces[i];
            }
        }

        /// <summary>
        /// Calculates the energy and forces acting upon each state and constructs the
        /// EVB Hamiltonian (StateMatrix)
        /// </summary>
        private void CalculateStateEnergiesAndForces(IAtomicSystem system)
        {
            int index = 0;
            foreach (EvbState state in evbStates)
            {
                state.CalculateEnergyAndForces(system, index == CurrentState);

                if (state.Energy >= MaxEnergy)
                {
                    hamiltonian[index * NumberOfStates + index] = MaxEnergy;
                }
                else
                {
                    hamiltonian[index * NumberOfStates + index] = state.Energy + state.EnergyShift;
                }

                index++;
            }

            // TODO: Here would be off-diagonal couplings, except they are constant so are applied only in LoadCouplingElements
        }

        #endregion CalculateForceField

        #region Matrix Diagonalization

        /// <summary>
        /// Returns the eigenvectors and eigenvalues of a square matrix
        /// </summary>
        /// <param name="matrix">The input matrix.</param>
        /// <param name="eigenvalues">The eigenvalues.</param>
        /// <param name="eigenvectors">The eigenvectors.</param>
        /// <param name="n">Size of the matrix.</param>
        private void Diagonalize(float[] matrix, ref float[] eigenvalues, ref float[] eigenvectors, int n)
        {
            if (n == 1)
            {
                eigenvalues[0] = matrix[0];
                eigenvectors[0] = 1;
            }
            //TODO THIS IS A HACK.
            float[][] twodMatrix = Create2DSquareMatrix(matrix, n);

            Matrix<float> m = Matrix<float>.Build.DenseOfRowArrays(twodMatrix);

            Evd<float> eigenvectorDecomp = m.Evd(Symmetricity.Hermitian);

            eigenvalues = eigenvectorDecomp.EigenValues.Real().ToSingle().ToArray();
            eigenvectors = eigenvectorDecomp.EigenVectors.ToRowWiseArray();
        }

        /// <summary>
        /// Returns the smallest eigenvalue along with its eigenvector for a given list of eigenvalues and eigenvectors
        /// </summary>
        /// <param name="eigenvalues"></param>
        /// <param name="eigenvectors"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        private Tuple<float, float[]> FindSmallestEigenvalue(float[] eigenvalues, float[] eigenvectors, int n)
        {
            float smallest = eigenvalues[0];
            int indexSmallest = 0;
            float[] smevec = new float[n];

            for (int i = 1; i < n; i++)
            {
                if (eigenvalues[i] < smallest)
                {
                    smallest = eigenvalues[i];
                    indexSmallest = i;
                }
            }

            for (int i = 0; i < n; i++)
            {
                smevec[i] = eigenvectors[i * n + indexSmallest];
            }
            return new Tuple<float, float[]>(smallest, smevec);
        }

        private float[][] Create2DSquareMatrix(float[] inMatrix, int n)
        {
            float[][] matrix = new float[n][];
            for (int i = 0; i < n; i++)
            {
                matrix[i] = new float[n];
                for (int j = 0; j < n; j++)
                {
                    matrix[i][j] = inMatrix[i * n + j];
                }
            }
            return matrix;
        }

        /// <summary>
        /// Takes the state matrix as input and outputs a possibly smaller matrix by removing any elements over maxEnergy
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="maxEnergy"></param>
        /// <param name="n"></param>
        /// <param name="resultMatrix"></param>
        /// <param name="resultN"></param>
        /// <param name="resultIndices"></param>
        /// <returns></returns>
        // ReSharper disable once UnusedMember.Local
        private float[] ReduceMatrix(float[] matrix, float maxEnergy, int n, ref float[] resultMatrix, ref int resultN, ref List<int> resultIndices)
        {
            resultIndices.Clear();
            for (int i = 0; i < n; i++)
            {
                if (matrix[i * n + i] < maxEnergy)
                {
                    resultIndices.Add(i);
                }
            }
            resultN = resultIndices.Count;
            resultMatrix = new float[resultN * resultN];
            //Fill in resulting diagonal elements of result matrx
            for (int ii = 0; ii < resultN; ii++)

            {
                resultMatrix[ii * resultN + ii] = matrix[resultIndices[ii] * NumberOfStates + resultIndices[ii]];
            }
            //Fill in offdiagonal elements
            for (int ii = 0; ii < resultN; ii++)
            {
                for (int jj = 0; jj < resultN; jj++)
                {
                    if (ii != jj)
                    {
                        resultMatrix[ii * resultN + jj] = matrix[resultIndices[ii] * n + resultIndices[jj]];
                    }
                }
            }
            return resultMatrix;
        }

        #region 2 x 2 Matrix Diagonalization

        /// <summary>
        /// Analytically solve two by two matrix for its eigenvalues
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        private float[] TwoByTwoEigenvalues(float[] matrix)
        {
            float[] eigenvalues = new float[2];
            float a = matrix[0 * NumberOfStates + 0];
            float b = matrix[0 * NumberOfStates + 1];
            float c = matrix[1 * NumberOfStates + 0];
            float d = matrix[1 * NumberOfStates + 1];

            eigenvalues[0] = 0.5f * (a + d - (float)Math.Sqrt(Math.Pow(a, 2) + Math.Pow(d, 2) + 4 * b * c - 2 * a * d));
            eigenvalues[1] = 0.5f * (a + d + (float)Math.Sqrt(Math.Pow(a, 2) + Math.Pow(d, 2) + 4 * b * c - 2 * a * d));

            return eigenvalues;
        }

        private float[] TwoByTwoEigenvectors(float[] matrix, float[] eigenvalues)
        {
            float[] eigenvectors = new float[2 * 2];
            //First solve for eigenvector associated with first eigenvalue
            eigenvectors[0] = matrix[0 * NumberOfStates + 0] - eigenvalues[1];
            eigenvectors[2 * 1 + 0] = matrix[1 * NumberOfStates + 0];

            eigenvectors[1] = matrix[0 * NumberOfStates + 1];
            eigenvectors[2 * 1 + 1] = matrix[1 * NumberOfStates + 1] - eigenvalues[0];

            //Normalize
            float norm1 = 1f / (float)(Math.Sqrt(Math.Pow(eigenvectors[0], 2) + Math.Pow(eigenvectors[2], 2)));
            float norm2 = 1f / (float)(Math.Sqrt(Math.Pow(eigenvectors[1], 2) + Math.Pow(eigenvectors[3], 2)));
            eigenvectors[0] *= norm1;
            eigenvectors[1] *= norm2;
            eigenvectors[2] *= norm1;
            eigenvectors[3] *= norm2;

            return eigenvectors;
        }

        /// <summary>
        /// Disposes this instance.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion 2 x 2 Matrix Diagonalization

        #endregion Matrix Diagonalization
    }

    /// <summary>
    /// Class for the off-diagonal coupling elements in EVB matrix. Currently constant, but eventual could take functional forms
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    [XmlName("EVBCouplingElement")]
    public class CouplingElement : ILoadable
    {
        /// <summary>
        /// The first state.
        /// </summary>
        public int State1;

        /// <summary>
        /// The second state.
        /// </summary>
        public int State2;

        /// <summary>
        /// The value of the coupling between state1 and state2.
        /// </summary>
        public float Value;

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            State1 = Helper.GetAttributeValue(node, "State1", 0);
            State2 = Helper.GetAttributeValue(node, "State2", 0);
            Value = Helper.GetAttributeValue(node, "Value", 0);
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "State1", State1);
            Helper.AppendAttributeAndValue(element, "State2", State2);
            Helper.AppendAttributeAndValue(element, "Value", Value);

            return element;
        }
    }
}