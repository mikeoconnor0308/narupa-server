﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Xml;
using Nano.Loading;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Simbox.MD.ForceField.EVB.State;

namespace Simbox.MD.ForceField.EVB
{
    [XmlName("EVBForceField")]
    internal class EvbForceFieldTerms : IForceFieldTerms
    {
        public readonly List<CouplingElement> CouplingElements = new List<CouplingElement>();
        public readonly List<EvbStateTerms> EvbStateTemplates = new List<EvbStateTerms>();

        public void AddToTopology(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context)
        {
            EvbInstantiatedTerms flattenedEvbTerms = null;
            bool termExists = false;
            foreach (IInstantiatedForceFieldTerms forceFieldTermType in parentTopology.ForceFieldTerms)
            {
                if (forceFieldTermType is EvbInstantiatedTerms)
                {
                    flattenedEvbTerms = forceFieldTermType as EvbInstantiatedTerms;
                    termExists = true;
                    break;
                }
            }

            if (termExists == false)
            {
                flattenedEvbTerms = new EvbInstantiatedTerms();
                flattenedEvbTerms.CouplingElements.Clear();
                flattenedEvbTerms.CouplingElements.AddRange(CouplingElements);
            }

            foreach (EvbStateTerms stateTerms in EvbStateTemplates)
            {
                flattenedEvbTerms.EvbStateFlattenedTerms.Add(stateTerms.FlattenEvbStateTerms(parentTopology, residue, ref context));
            }

            if (termExists == false)
            {
                parentTopology.ForceFieldTerms.Add(flattenedEvbTerms);
            }
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Loader.SaveObjects(context, element, CouplingElements, "CouplingElements");
            Loader.SaveObjects(context, element, EvbStateTemplates, "States");

            return element;
        }

        public void Load(LoadContext context, XmlNode node)
        {
            CouplingElements.Clear();
            CouplingElements.AddRange(Loader.LoadObjects<CouplingElement>(context, node.SelectSingleNode("CouplingElements")));

            EvbStateTemplates.Clear();
            EvbStateTemplates.AddRange(Loader.LoadObjects<EvbStateTerms>(context, node.SelectSingleNode("States")));
        }
    }
}