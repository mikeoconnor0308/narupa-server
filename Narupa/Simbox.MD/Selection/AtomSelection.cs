﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using Nano.Science;
using Rug.Osc;

namespace Simbox.MD.Selection
{
    public enum SelectionType { OscAddress, Regex }

    public class AtomSelectionString : IEquatable<AtomSelectionString>
    {
        public string Pattern;
        public SelectionType SelectionStyle;

        public AtomSelectionString(string pattern, SelectionType selectionStyle)
        {
            Pattern = pattern;
            SelectionStyle = selectionStyle;
        }

        public bool Equals(AtomSelectionString other)
        {
            if (Pattern == other.Pattern && SelectionStyle == other.SelectionStyle)
                return true;
            return false;
        }
    }

    /// <summary>
    /// Represents a selection of atoms.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List{ushort}" />
    /// <seealso cref="ICommandProvider" />
    public class AtomSelection : List<ushort>, ICommandProvider
    {
        /// <summary>
        /// The name of this selection.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// The address of this selection.
        /// </summary>
        public readonly OscAddress Address;

        /// <summary>
        /// Occurs when [atom selection change].
        /// </summary>
        public event AtomSelectionChangeHandler AtomSelectionChange;

        /// <summary>
        /// Delegate AtomSelectionChangeHandler
        /// </summary>
        /// <param name="selectedAtoms">The selected atoms.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        public delegate void AtomSelectionChangeHandler(AtomSelection selectedAtoms, EventArgs e);

        /// <summary>
        /// The patterns selected in this selection.
        /// </summary>
        public readonly List<AtomSelectionString> Patterns = new List<AtomSelectionString>();

        private TransportContext transportContext;
        private List<string> atomAddresses;

        /// <summary>
        /// Gets a value indicating whether [to render selection].
        /// </summary>
        /// <value><c>true</c> if [render selection]; otherwise, <c>false</c>.</value>
        public bool RenderSelection { get; private set; }

        string ICommandProvider.Address
        {
            get
            {
                return Address.ToString();
            }
        }

        /// <summary>
        /// Sets whether to render the selection.
        /// </summary>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public void SetRenderSelection(bool value)
        {
            RenderSelection = value;
            AtomSelectionChange(this, null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AtomSelection" /> class.
        /// </summary>
        /// <param name="parentAddress"></param>
        /// <param name="name">The name.</param>
        /// <param name="atomAddresses">The topology.</param>
        /// <param name="hide"></param>
        /// <param name="patterns"></param>
        public AtomSelection(OscAddress parentAddress, string name, List<string> atomAddresses, bool hide = false, params AtomSelectionString[] patterns)
        {
            Name = name;
            Address = new OscAddress($"{parentAddress}/{name}");

            this.atomAddresses = atomAddresses;
            Patterns = new List<AtomSelectionString>();
            RenderSelection = !hide;

            if (patterns == null)
            {
                return;
            }

            foreach (AtomSelectionString pattern in patterns)
            {
                AddPattern(pattern);
            }
        }

        /// <summary>
        /// Adds the Osc address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>System.Int32.</returns>
        public int AddAddress(string address)
        {
            AtomSelectionString patternSelection = new AtomSelectionString(address, SelectionType.OscAddress);
            return AddPattern(patternSelection);
        }

        /// <summary>
        /// Adds the regex pattern to the selection.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <returns>System.Int32.</returns>
        public int AddRegex(string pattern)
        {
            AtomSelectionString selectionString = new AtomSelectionString(pattern, SelectionType.Regex);
            return AddPattern(selectionString);
        }

        /// <summary>
        /// Adds the pattern to the selection.
        /// </summary>
        /// <param name="selectionString">The selection string.</param>
        /// <returns>System.Int32.</returns>
        public int AddPattern(AtomSelectionString selectionString)
        {
            Patterns.Add(selectionString);
            int numAdded = AddPatternToSelection(selectionString);
            AtomSelectionChange?.Invoke(this, null);
            return numAdded;
        }

        /// <summary>
        /// Adds the address to selection.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>System.Int32.</returns>
        private int AddPatternToSelection(AtomSelectionString pattern)
        {
            HashSet<ushort> atomsToAdd = new HashSet<ushort>();

            atomsToAdd.UnionWith(pattern.SelectionStyle == SelectionType.OscAddress ? 
                AtomSelectionHelper.SelectAtomsByOscAddress(atomAddresses, new OscAddress(pattern.Pattern)) : 
                AtomSelectionHelper.SelectAtomsByRegex(atomAddresses, pattern.Pattern)
            );

            int numAdded = 0;
            foreach (ushort atom in atomsToAdd)
            {
                if (Contains(atom))
                {
                    continue;
                }

                numAdded++;
                Add(atom);
            }

            return numAdded;
        }

        /// <summary>
        /// Removes the address from selection.
        /// </summary>
        /// <param name="pattern">The selection pattern.</param>
        /// <returns>System.Int32.</returns>
        private int RemovePatternFromSelection(AtomSelectionString pattern)
        {
            HashSet<ushort> atomsToRemove = new HashSet<ushort>();
            atomsToRemove.UnionWith(pattern.SelectionStyle == SelectionType.OscAddress ? 
                AtomSelectionHelper.SelectAtomsByOscAddress(atomAddresses, new OscAddress(pattern.Pattern)) : 
                AtomSelectionHelper.SelectAtomsByRegex(atomAddresses, pattern.Pattern)
            );

            return atomsToRemove.Count(Remove);
        }

        /// <summary>
        /// Removes atoms matching the specified address from the selection.
        /// </summary>
        /// <param name="selection">The selection pattern..</param>
        /// <returns>System.Int32.</returns>
        public int RemovePattern(AtomSelectionString selection)
        {
            Patterns.Remove(selection);
            int numRemoved = RemovePatternFromSelection(selection);
            AtomSelectionChange(this, null);
            return numRemoved;
        }

        /// <summary>
        /// Pops the address from the selection.
        /// </summary>
        /// <returns>System.Int32.</returns>
        public int PopPattern()
        {
            return RemovePattern(Patterns[Patterns.Count - 1]);
        }

        /// <summary>
        /// Detaches the transport context.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void DetachTransportContext(TransportContext transportContext)
        {
            this.transportContext = null;
        }

        /// <summary>
        /// Attaches the transport context.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        [OscCommand(Help = "Renders the current selection")]
        public void Show(OscMessage message)
        {
            RenderSelection = true;

            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage(commandAddress));
            AtomSelectionChange(this, null);
        }

        [OscCommand(Help = "Hides the current selection")]
        public void Hide(OscMessage message)
        {
            RenderSelection = false;
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod()); ;
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage(commandAddress));
            AtomSelectionChange(this, null);
        }

        [OscCommand(Help = "Lists the current selection")]
        public void List(OscMessage message)
        {
            string selectionString = "";
            foreach (AtomSelectionString selection in Patterns)
            {
                selectionString += selection + "\n\t";
            }
            transportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, new[] {new OscMessage(Address.ToString(), selectionString)});
        }

        [OscCommand(Help = "Adds the specified path to selection by regex matching", ExampleArguments = ".*/CO2/.*", TypeTagString = "s")]
        public void AddRegex(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());

            string address;
            try
            {
                //example: /boundaryForce/select //CO2 scaleFactor
                address = (string)message[0];
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid selection string.");
                return;
            }

            int numAtomsAdded = AddRegex(address);

            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage(commandAddress, string.Format("Added {0} atoms.", numAtomsAdded)));
        }

        [OscCommand(Help = "Adds specified path to selection by OSC Address matching", ExampleArguments = "//CO2", TypeTagString = "s")]
        public void AddPath(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());
            string helpMessage = string.Format("Example usage: {0}, .*/CO2/.*", commandAddress);

            string address;
            try
            {
                //example: /boundaryForce/select //CO2 scaleFactor
                address = (string)message[0];
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid OSC address string");
                return;
            }

            int numAtomsAdded = 0;
            try
            {
                numAtomsAdded = AddAddress(address);
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid OSC address for selection.");
            }
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage(commandAddress, string.Format("Added {0} atoms.", numAtomsAdded)));
        }

        [OscCommand(Help = "Removes specified path to selection. ", ExampleArguments = "//CO2", TypeTagString = "s")]
        public void RemovePath(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());
            string helpMessage = string.Format("Example usage: {0}, .*/CO2/.*", commandAddress);

            string address;
            try
            {
                //example: /boundaryForce/select //CO2 scaleFactor
                address = (string)message[0];
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid OSC address string");
                return;
            }

            int numAtomsRemoved = 0;
            try
            {
                numAtomsRemoved = RemovePattern(new AtomSelectionString(address, SelectionType.OscAddress));
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid OSC address for selection.");
            }
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage(commandAddress, $"Removed {numAtomsRemoved} atoms."));
        }

        [OscCommand(Help = "Pops the last pattern added to the selection")]
        public void PopPattern(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());
            PopPattern();

            OscMessage reply = new OscMessage(commandAddress);

            transportContext.Transmitter.Broadcast(TransportPacketPriority.Important, reply);
        }
    }
}