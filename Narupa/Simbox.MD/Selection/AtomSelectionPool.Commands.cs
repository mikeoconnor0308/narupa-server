﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Reflection;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using Nano.Science;
using Rug.Osc;

namespace Simbox.MD.Selection
{
    partial class AtomSelectionPool
    {
        public void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
            AddDefaultSelections();
            UpdateVisibleAtoms();
        }

        [OscCommand(Help = "Resets all selections to be displayed.")]
        public void ShowEverything(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage(commandAddress));
            foreach (AtomSelection selection in selections.Values)
            {
                selection.SetRenderSelection(true);
            }
        }

        [OscCommand(Help = "Hides all selections")]
        public void HideEverything(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage(commandAddress));
            foreach (AtomSelection selection in selections.Values)
            {
                selection.SetRenderSelection(false);
            }
        }

        [OscCommand(Help = "Creates a new selection consisting of the hydrogen atoms in the specified selection.", ExampleArguments = "DNA", TypeTagString = "s")]
        public void GetHydrogens(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());

            string name;
            try
            {
                name = (string)message[0];
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid selection string.");
                return;
            }

            AtomSelection selection;
            if (selections.TryGetValue(name, out selection) == false)
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid selection.");
                return;
            }

            List<AtomSelectionString> hydrogenAddress = new List<AtomSelectionString>();
            foreach (ushort atomIndex in selection)
            {
                if (topology.Elements[atomIndex] == Element.Hydrogen)
                {
                    hydrogenAddress.Add(new AtomSelectionString(topology.Addresses[atomIndex], SelectionType.OscAddress));
                }
            }
            AtomSelection hydrogenSelection = new AtomSelection(selection.Address, "H", topology.Addresses, false, hydrogenAddress.ToArray());

            AddSelection(hydrogenSelection);

            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, message);
        }

        [OscCommand(Help = "For a given residue name, will show only the backbone", ExampleArguments = "10-ALA", TypeTagString = "s")]
        public void ShowBackboneOnly(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());

            string name;
            try
            {
                name = (string)message[0];
            }
            catch
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, "Invalid selection string.");
                return;
            }

            AtomSelection selection;
            if (selections.TryGetValue(name, out selection) == false)
            {
                OscCommandHelper.TransmitError(transportContext, message, attribute, string.Format("Could not find {0} selection to render backbone.", name));
                return;
            }
            //Hide the residue.
            selection.SetRenderSelection(false);

            CreateBackboneSelection(selection);
            UpdateVisibleAtoms();
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, message);
        }

        [OscCommand(Help = "Removes the specified selection", ExampleArguments = "CO2", TypeTagString = "s")]
        public void Remove(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());
            string name;
            try
            {
                name = (string)message[0];
            }
            catch
            {
                return;
            }

            AtomSelection selection;
            if (selections.TryGetValue(name, out selection))
            {
                RemoveSelection(selection);
            }

            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, message); ;
        }

        [OscCommand(Help = "Creates the specified selection address space", ExampleArguments = "CO2", TypeTagString = "s")]
        public void Create(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());

            string name;
            try
            {
                name = (string)message[0];
            }
            catch
            {
                return;
            }

            AtomSelection selection = new AtomSelection(Address, name, topology.Addresses);

            AddSelection(selection);

            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, message);
        }

        [OscCommand(Help = "Lists the current set of selections")]
        public void List(OscMessage message)
        {
            OscCommandAttribute attribute = OscCommandHelper.GetCustomAttribute<OscCommandAttribute>(MethodBase.GetCurrentMethod());
            string commandAddress = OscCommandHelper.GetAddressOfMethod(this, MethodBase.GetCurrentMethod());

            foreach (AtomSelection selection in selections.Values)
            {
                OscMessage reply = new OscMessage(message.Address, $"Name: {selection.Name}, Address: {selection.Address}, Count: {selection.Count}");
                transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, reply);
            }
        }

        public void DetachTransportContext(TransportContext transportContext)
        {
            foreach (AtomSelection selection in selections.Values)
            {
                selection.DetachTransportContext(transportContext);
            }
        }
    }
}