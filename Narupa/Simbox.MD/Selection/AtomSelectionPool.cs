﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using Nano.Science.Simulation.Instantiated;
using Nano;
using Rug.Osc;

namespace Simbox.MD.Selection
{
    /// <summary>
    /// Implementation of server side Atom Selection.
    /// </summary>
    public partial class AtomSelectionPool : IAtomSelectionPool
    {
        /// <summary>
        /// The number of particles that can be in the system before which system will default to just rendering the backbone if it is a protein.
        /// </summary>
        public static int BackboneParticleThreshold = 1500;

        /// <inheritdoc />
        public OscAddress Address => address;
        
        private readonly OscAddress address;

        private readonly Dictionary<string, AtomSelection> selections = new Dictionary<string, AtomSelection>();
        private TransportContext transportContext;

        /// <inheritdoc />
        public List<int> VisibleAtoms { get; set; }
        private InstantiatedTopology topology;

        string ICommandProvider.Address => Address.ToString();

        /// <summary>
        /// Occurs when [atom selection change].
        /// </summary>
        public event EventHandler VisibleAtomsChanged;

        private IReporter reporter;

        /// <inheritdoc />
        public AtomSelectionPool(OscAddress address, InstantiatedTopology topology, IReporter reporter = null)
        {
            this.address = address;
            this.topology = topology;
            VisibleAtoms = new List<int>();
            this.reporter = reporter;
            UpdateVisibleAtoms();
        }

        /// <summary>
        /// Adds the given selection of atoms to this instance. 
        /// </summary>
        /// <param name="selection">Atom selection to be added. </param>
        /// <exception cref="Exception"> Exception thrown when a selection with the same name already in the pool. </exception>
        public void AddSelection(AtomSelection selection)
        {
            if (selections.ContainsKey(selection.Name))
            {
                throw new Exception($"Selection with name {selection.Name} already exists!");
            }

            OscCommandContext oscContext = new OscCommandContext(transportContext);

            if (transportContext != null)
            {
                OscCommandManager.LoadCommands(oscContext, selection);
            }

            selection.AtomSelectionChange += new AtomSelection.AtomSelectionChangeHandler(UpdateAtomSelections);
            selections.Add(selection.Name, selection);
        }

        private void AddDefaultSelections()
        {
            bool hideWater = true;
            foreach (string spawner in topology.Spawners)
            {
                AtomSelection selection = new AtomSelection(Address, spawner, topology.Addresses, false, new AtomSelectionString(spawner, SelectionType.Regex));
                AddSelection(selection);
                //get rid of default to backbone for now.
                /*
                bool defaultToBackBone = false;
                if (selection.Count > BackboneParticleThreshold && IsProtein(selection))
                {
                    //reporter?.PrintEmphasized(string.Format("There are more than {0} particles in the selection and it appears to be a protein, will default to just transmitting the backbone."), selection.Count);
                    defaultToBackBone = true;
                }
                //If it's a protein, add a backbone selection, and switch to just rendering the backbone by default.
                if (defaultToBackBone)
                {
                    selection.SetRenderSelection(false);
                    AtomSelection backboneSelection = CreateBackboneSelection(selection);

                    if (backboneSelection != null)
                        backboneSelection.SetRenderSelection(true);
                }
                */


                //If selection is entirely water then don't render it.
                if (IsWater(selection))
                {
                    selection.SetRenderSelection(false);
                }
            }

            if (hideWater)
            {
                AtomSelection waterSelection = new AtomSelection(Address, "Water", topology.Addresses, true,new AtomSelectionString(".*/(WAT)|(HOH)-", SelectionType.Regex));
                AddSelection(waterSelection);
            }
            
        }

        /// <summary>
        /// Removes the given selection. 
        /// </summary>
        /// <param name="selection">The selection.</param>
        public void RemoveSelection(AtomSelection selection)
        {
            selection.DetachTransportContext(transportContext);
            selection.AtomSelectionChange -= UpdateAtomSelections;
            selections.Remove(selection.Name);
        }

        /// <summary>
        /// Updates the pool given a change in a particular selection. 
        /// </summary>
        /// <param name="selection">The selection that's changed. </param>
        /// <param name="e"></param>
        public void UpdateAtomSelections(AtomSelection selection, EventArgs e)
        {
            UpdateVisibleAtoms();
        }

        /// <summary>
        /// Updates the currently visible atoms based on the state of atom selections.
        /// </summary>
        public void UpdateVisibleAtoms()
        {
            HashSet<int> atomsToRender = new HashSet<int>();
            for (int i = 0; i < topology.NumberOfParticles; i++)
            {
                atomsToRender.Add(i);
            }
            foreach (AtomSelection selection in selections.Values)
            {
                foreach (int atom in selection)
                {
                    if (selection.RenderSelection)
                    {
                        atomsToRender.Add(atom);
                    }
                    else
                    {
                        atomsToRender.Remove(atom);
                    }
                }
            }
            VisibleAtoms.Clear();
            VisibleAtoms.AddRange(atomsToRender);
            VisibleAtomsChanged?.Invoke(this, null);
        }
    }
}