﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Xml;
using Nano.Loading;
using System.IO;
using Nano.Science.Simulation;
using SlimMath;
using Nano;
using Nano.Science.Simulation.Log;

namespace Simbox.MD.Log
{
    [Flags]
    public enum StateLogOptions
    {
        Time = 1,
        PotentialEnergy = 2,
        KineticEnergy = 4,
        Temperature = 8
    }

    [XmlName("StateLogger")]
    internal class StateLogger : ISimulationLogger
    {
        /// <summary>
        /// The path in which this logger will create files
        /// </summary>
        protected string LogPath = "^/Logs/Trajectories";

        public StateLogOptions LogOptions = StateLogOptions.Time | StateLogOptions.PotentialEnergy | StateLogOptions.KineticEnergy | StateLogOptions.Temperature;

        /// <summary>
        /// How often, in time steps, will logs be written.
        /// </summary>
        protected int WriteFrequency = 1;

        /// <summary>
        /// If set to true, a date time stamp will be created with each log file.
        /// </summary>
        /// <remarks>
        /// If set to false, logs from previous runs may be overwritten.
        /// </remarks>
        protected bool UseDateTimeStamp = true;

        private bool logCreated;

        //TODO possibly generalise to CSVHelper?
        private StreamWriter fileWriter;

        private long step;
        private float timeStep;

        public string Identifier
        {
            get
            {
                return Helper.ResolvePath(LogPath + "/state");
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public ISimulationLogger Clone()
        {
            StateLogger newLogger = new StateLogger
            {
                LogOptions = LogOptions,
                LogPath = LogPath,
                UseDateTimeStamp = UseDateTimeStamp,
                WriteFrequency = WriteFrequency
            };

            return newLogger;
        }

        public void CloseLog()
        {
            fileWriter?.Close();
        }

        public void CreateLog(bool append, params string[] logIdentifiers)
        {
            Helper.EnsurePathExists(Path.Combine(Helper.ResolvePath(LogPath), "dummy"));

            if (logCreated)
            {
                throw new Exception("Log already created!");
            }

            string csvPath = SimulationLoggerBase.CreateLogFilePath(LogPath, "trajectory", logIdentifiers, UseDateTimeStamp, "csv");

            fileWriter = new StreamWriter(csvPath, append);

            if (append == false)
                WriteCSVHeader();

            logCreated = true;
        }

        private void WriteCSVHeader()
        {
            string headerLine = "";
            headerLine += "timeStep";
            headerLine += (LogOptions & StateLogOptions.Time) == StateLogOptions.Time ? ", time (ps)" : "";
            headerLine += (LogOptions & StateLogOptions.PotentialEnergy) == StateLogOptions.PotentialEnergy ? ", potentialEnergy (kJ/mol)" : "";
            headerLine += (LogOptions & StateLogOptions.KineticEnergy) == StateLogOptions.KineticEnergy ? ", kineticEnergy (kJ/mol)" : "";
            headerLine += (LogOptions & StateLogOptions.Temperature) == StateLogOptions.Temperature ? ", temperature (K)" : "";
            fileWriter.WriteLine(headerLine);
        }

        public void Dispose()
        {
            if (fileWriter != null)
                fileWriter.Close();
        }

        public bool IsDueToLog(long step, float timeStep)
        {
            this.step = step;
            this.timeStep = timeStep;
            if (step % WriteFrequency == 0)
                return true;
            else return false;
        }

        public void LogState(IAtomicSystem system)
        {
            string line = "";

            line += step.ToString();
            line += (LogOptions & StateLogOptions.Time) == StateLogOptions.Time ? $",{(step * timeStep)}" : "";
            line += (LogOptions & StateLogOptions.PotentialEnergy) == StateLogOptions.PotentialEnergy ? $",{system.PotentialEnergy}" : "";
            bool printKE = (LogOptions & StateLogOptions.KineticEnergy) == (StateLogOptions.KineticEnergy);
            bool printTemp = (LogOptions & StateLogOptions.Temperature) == (StateLogOptions.Temperature);
            if (printKE || printTemp)
            {
                Vector2 keTemp = system.CalculateKineticEnergyAndTemperature();
                if (printKE)
                    line += $",{keTemp[0]}";
                if (printTemp)
                    line += $",{keTemp[1]}";
            }

            fileWriter.WriteLine(line);
            fileWriter.Flush();
        }

        public virtual void Load(LoadContext context, XmlNode node)
        {
            LogPath = Helper.ResolvePath(Helper.GetAttributeValue(node, "LogPath", LogPath));
            LogOptions = LoadStateLogOptions(context, node);
            WriteFrequency = Helper.GetAttributeValue(node, "WriteFrequency", WriteFrequency);
            UseDateTimeStamp = Helper.GetAttributeValue(node, "UseDateTimeStamp", UseDateTimeStamp);
        }

        private StateLogOptions LoadStateLogOptions(LoadContext context, XmlNode node)
        {
            //If the user understands bitmasks, they can dump a value directly in
            StateLogOptions logOptions = Helper.GetAttributeValue(node, "StateLogOptions", StateLogOptions.PotentialEnergy);
            //Otherwise, if an option is set then add it to the bitmask.
            logOptions |= Helper.GetAttributeValue(node, "Time", true) ? StateLogOptions.Time : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "KineticEnergy", false) ? StateLogOptions.KineticEnergy : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "PotentialEnergy", true) ? StateLogOptions.PotentialEnergy : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "Temperature", true) ? StateLogOptions.Temperature : logOptions;
            return logOptions;
        }

        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "LogPath", LogPath);
            Helper.AppendAttributeAndValue(element, "StateLogOptions", LogOptions);
            Helper.AppendAttributeAndValue(element, "WriteFrequency", WriteFrequency);
            Helper.AppendAttributeAndValue(element, "UseDateTimeStamp", UseDateTimeStamp);
            return element;
        }
    }
}