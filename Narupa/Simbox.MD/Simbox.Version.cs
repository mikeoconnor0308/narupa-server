// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Reflection; 
 
[assembly: AssemblyCompany("Interactive Scientific")] 
[assembly: AssemblyProduct("NanoSimbox Simulation Engine")] 
[assembly: AssemblyCopyright("Copyright © Interactive Scientific LTD")]
[assembly: AssemblyTrademark("")] 
[assembly: AssemblyCulture("")] 
[assembly: AssemblyVersion("1.1.3.0")] 
[assembly: AssemblyFileVersion("1.1")]