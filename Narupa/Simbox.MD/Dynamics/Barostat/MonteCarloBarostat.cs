﻿/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2008-2010 Stanford University and the Authors.      *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

using System;
using System.Collections.Generic;
using Nano;
using Nano.Science;
using Nano.Science.Simulation;
using SlimMath;

namespace Simbox.MD.Dynamics.Barostat
{
    internal class MonteCarloBarostat : Nano.Science.Simulation.Barostat
    {
        private int numberAccepted;
        private int numberRejected;

        //Initial scale by 1% of box volume
        private float volumeScale = 0.01f;

        /// <summary>
        /// How many rescalings to do before re-evaluating the volumeScale.
        /// </summary>
        private int sampleSize = 10;

        /// <summary>
        /// The minimum acceptance probability, with the aim to get an acceptance probability of 50%.
        /// </summary>
        private float minimumAcceptanceProbability = 0.25f;

        private float rescaleFrequency;
        private float step;

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public override string Name => "Montecarlo Barostat";

        /// <summary>
        /// Initializes a new instance of the <see cref="MonteCarloBarostat"/> class.
        /// </summary>
        /// <param name="equilibriumPressure">The equilibrium pressure.</param>
        /// <param name="rescaleFrequency">The rescale frequency.</param>
        /// <param name="minimumPressure">The minimum pressure.</param>
        /// <param name="maximumPressure">The maximum pressure.</param>
        public MonteCarloBarostat(float equilibriumPressure, float rescaleFrequency, float minimumPressure = 0.01f, float maximumPressure = 100000000f)
        {
            EquilibriumPressure = equilibriumPressure;
            this.rescaleFrequency = rescaleFrequency;
            MinimumPressure = minimumPressure;
            MaximumPressure = maximumPressure;
        }

        /// <summary>
        /// Applies the stat.
        /// </summary>
        /// <param name="system">The system.</param>
        public override void ApplyStat(IAtomicSystem system)
        {
            if (system is AtomicSystem == false)
            {
                throw new Exception("MonteCarloBarostat is not compatible with this AtomicSystem implementation!");
            }
            if (step < rescaleFrequency)
            {
                step++;
                return;
            }
            step = 0;
            float originalEnergy = system.CalculateForceFields();
            //Create new box, save old one
            BoundingBox originalBox = system.Boundary.SimulationBox;
            float originalVolume = MathUtilities.CalculateVolume(system.Boundary.SimulationBox);
            float originalLength = originalBox.Maximum.X - originalBox.Minimum.X;

            float deltaVolume = volumeScale * 2 * (float)(MathUtilities.Rand.NextDouble() - 0.5);
            float newVolume = deltaVolume + originalVolume;
            float lengthScale = (float)Math.Pow(newVolume / originalVolume, 1.0 / 3.0);

            BoundingBox candidateBox = new BoundingBox(new Vector3(-(lengthScale * originalLength) / 2f), new Vector3((originalLength * lengthScale) / 2f));

            //Make backup of old positions.
            List<Vector3> originalPositions = new List<Vector3>(system.Particles.Position);

            system.Boundary.SetSimulationBox(candidateBox);
            (system as AtomicSystem).ScaleMoleculeCenters(lengthScale);
            float candidateEnergy = system.CalculateForceFields();
            float deltaEnergy = candidateEnergy - originalEnergy;

            //Convert from bars to kJ/mol/nm^3
            float pressure = EquilibriumPressure * (PhysicalConstants.AvogadroC * 1e-25f);
            double kT = PhysicalConstants.MolarGasConstantR / 1000 * (system as AtomicSystem).Thermostat.EquilibriumTemperature;

            double weight = deltaEnergy + pressure * deltaVolume - system.Topology.Molecules.Count * kT * Math.Log(newVolume / originalVolume);

            bool accept = true;
            if (weight > 0)
            {
                float acceptProbability = (float)Math.Exp(-weight / kT);
                accept = MathUtilities.Rand.NextDouble() < acceptProbability;
            }
            if (accept)
            {
                numberAccepted++;
            }
            else
            {
                numberRejected++;
                system.Particles.Position = originalPositions;
                system.Boundary.SetSimulationBox(originalBox);
            }
            EvaluatePerformance(originalVolume);
        }

        /// <summary>
        /// Evaluates the performance of the barostat and adjusts the scale factor accordingly.
        /// </summary>
        /// <remarks>
        /// The barostat aims to have around 50% acceptance probability.
        /// </remarks>
        private void EvaluatePerformance(float currentVolume)
        {
            if (numberAccepted + numberRejected == sampleSize)
            {
                float acceptanceRatio = (float)numberAccepted / sampleSize;
                if (acceptanceRatio < minimumAcceptanceProbability)
                {
                    volumeScale *= 0.9f;
                }
                else if (acceptanceRatio > (1.0f - minimumAcceptanceProbability))
                {
                    volumeScale = Math.Min(volumeScale * 1.1f, currentVolume * 0.1f);
                }
                numberRejected = 0;
                numberAccepted = 0;
            }
        }

        /// <summary>
        /// Gets the properties of this thermostat.
        /// </summary>
        /// <returns>IThermostatProperties.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override IThermostatProperties GetProperties()
        {
            return new MonteCarloBarostatProperties()
            {
                RescaleFreq = rescaleFrequency,
                EquilibriumPressure = EquilibriumPressure,
                MaximumPressure = MaximumPressure,
                MinimumPressure = MinimumPressure
            };
        }
    }
}