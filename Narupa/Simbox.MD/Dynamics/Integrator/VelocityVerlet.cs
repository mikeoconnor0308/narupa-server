﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano;
using Nano.Transport.Agents;
using Nano.Transport.Variables;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Integrator;
using SlimMath;

namespace Simbox.MD.Dynamics.Integrator
{
    /// <summary>
    /// Velocity Verlet Integrator.
    /// </summary>
    /// <seealso cref="IIntegrator" />
    /// <seealso cref="Nano.Transport.Variables.IVariableProvider" />
    public class VelocityVerletIntegrator : IIntegrator, IVariableProvider
    {
        #region Public Properties

        bool IVariableProvider.VariablesInitialised
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// The collisions against a wall encountered in a step.
        /// </summary>
        public readonly List<ushort> WallCollisions = new List<ushort>();

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VelocityVerletIntegrator"/> class.
        /// </summary>
        public VelocityVerletIntegrator()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VelocityVerletIntegrator" /> class.
        /// </summary>
        /// <param name="atomicSystem">The atomic system.</param>
        /// <param name="timeStep">The time step.</param>
        /// <param name="removeCmMotion">if set to <c>true</c> [remove centre of mass motion].</param>
        /// <param name="reporter">The reporter.</param>
        public VelocityVerletIntegrator(AtomicSystem atomicSystem, float timeStep, bool removeCmMotion, IReporter reporter)
        {
            system = atomicSystem;
            this.timeStep = timeStep;
            this.reporter = reporter;
            removeCMMotion = removeCmMotion;
            periodicBoundVector = system.Boundary.GetPeriodicBoxVectors();
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns> IIntegrator.</returns>
        public IIntegrator Clone()
        {
            VelocityVerletIntegrator v = new VelocityVerletIntegrator();
            v.reporter = reporter;
            v.step = step;
            v.timeStep = timeStep;
            return v;
        }

        /// <summary>
        /// Gets the time step.
        /// </summary>
        /// <returns>System.Single.</returns>
        public float GetTimeStep()
        {
            return timeStep;
        }

        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }

        void IVariableProvider.InitialiseVariables(VariableManager variableManager)
        {
            timeStepIVar = variableManager.Variables.Add(VariableName.TimeStep, VariableType.Float) as IVariable<float>;

            iVarsInitialised = true;
            (this as IVariableProvider).ResetVariableDesiredValues();
        }

        void IVariableProvider.ResetVariableDesiredValues()
        {
            if (!iVarsInitialised) return;
            timeStepIVar.Value = timeStep;
            timeStepIVar.DesiredValue = timeStep;
        }

        void IVariableProvider.UpdateVariableValues()
        {
            if (!iVarsInitialised) return;
            if (timeStepIVar.Readonly == false)
            {
                if (Math.Abs(timeStepIVar.Value - timeStepIVar.DesiredValue) > 0.001f)
                {
                    timeStep = timeStepIVar.DesiredValue;
                    timeStepIVar.Value = timeStepIVar.DesiredValue;
                }
            }
        }

        /// <summary>
        /// Propagates the specified number of steps.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Propagate(int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                RunStep();
            }
        }

        /// <summary>
        /// Resets the integrator.
        /// </summary>
        /// <param name="activeSystem">The active system.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void ResetIntegrator(IAtomicSystem activeSystem)
        {
            system = (AtomicSystem)activeSystem;
            step = 0;
        }

        /// <summary>
        /// Propagates the system one step under Velocity Verlet integration.
        /// </summary>
        /// <exception cref="Exception">Berendsen Rescale factor set to NaN. Check input!
        /// or
        /// NaN in Positions for atom  + nan_index
        /// or
        /// NaN in Forces for atom  + nan_index
        /// or
        /// NaN in Velocity for atom  + nan_index</exception>
        public void RunStep()
        {
            //Update bounding box
            if (system.Boundary.IsVariableBox)
            {
                system.Boundary.UpdateBoundingBox(system);
                periodicBoundVector = system.Boundary.GetPeriodicBoxVectors();
            }


            //Apply thermostat.
            system.Thermostat?.ApplyStat(system);
            system.Barostat?.ApplyStat(system);
            system.CalculateInstantaneousPressure();


            //Propagate positions based on current velocities.
            PropagatePositions(system);
            if (system.Boundary.PeriodicBoundary && system.Boundary.Resizing == false)
                IntegratorHelper.WrapMoleculesIntoPeriodicBox(system, periodicBoundVector);

            int indexOfNaN = CheckForNaNs(system.Particles.Position);
            if (indexOfNaN != -1)
                throw new Exception("Propagation of positions resulted in 'not a number' exception in Positions for atom: " + indexOfNaN + " " + system.Topology.Addresses[indexOfNaN]);

            //Apply second half of last timesteps forces.
            ApplyForces(system.Particles.Velocity, system.Particles.Force, system.Particles.Mass, timeStep);

            system.CalculateForceFields();
            indexOfNaN = CheckForNaNs(system.Particles.Force);
            if (indexOfNaN != -1)
                throw new Exception("Calculation of forces resulted in 'not a number' exception in Forces for atom: " + indexOfNaN + " " + system.Topology.Addresses[indexOfNaN]);

            ApplyForces(system.Particles.Velocity, system.Particles.Force, system.Particles.Mass, timeStep);

            indexOfNaN = CheckForNaNs(system.Particles.Velocity);
            if (indexOfNaN != -1)
                throw new Exception("Velocity update resulted in 'not a number' exception in Velocities for atom: " + indexOfNaN + " " + system.Topology.Addresses[indexOfNaN]);

            if (removeCMMotion)
                system.Particles.RemoveCentreOfMassMotion();

            step++;
            simulationTime += timeStep;
            system.SimulationLogManager.Log(system, step, timeStep);

        }

        #endregion Public Methods

        #region Private Fields

        private bool iVarsInitialised;
        private IReporter reporter;
        private long step;
        private AtomicSystem system;
        private float timeStep;

        private IVariable<float> timeStepIVar;
        private bool removeCMMotion;
        private bool processCollisions = true;
        private float simulationTime;
        private Vector3[] periodicBoundVector;

        #endregion Private Fields

        #region Private Methods

        /// <summary>
        /// Applies the forces.
        /// </summary>
        /// <param name="velocity">The velocity.</param>
        /// <param name="force">The force.</param>
        /// <param name="mass">The mass.</param>
        /// <param name="tstep">The time step.</param>
        private void ApplyForces(List<Vector3> velocity, List<Vector3> force, List<float> mass, float tstep)
        {
            float factor;
            for (int i = 0; i < velocity.Count; i++)
            {
                factor = tstep * 0.5f / mass[i];
                velocity[i] += factor * force[i];
            }
        }

        /// <summary>
        /// Finds the first value in the vector that is NaN. Returns -1 if none found.
        /// </summary>
        /// <param name="vector"></param>
        /// <returns> Index of first NaN, or -1 if none found </returns>
        private int CheckForNaNs(List<Vector3> vector)
        {
            for (int index = 0; index < vector.Count; index++)
            {
                if (InvalidFloat(vector[index].X) || InvalidFloat(vector[index].Y) || InvalidFloat(vector[index].Z)) return index;
            }
            return -1;
        }

        private bool InvalidFloat(float x)
        {
            return float.IsInfinity(x) || float.IsNaN(x);
        }

        /// <summary>
        /// Propagates the positions.
        /// </summary>
        /// <param name="sys">The system.</param>
        private void PropagatePositions(AtomicSystem sys)
        {
            if (!(sys.Boundary is SimulationBoundary simBoundary))
                throw new Exception($"The Velocity Verlet integrator is currently only compatible with the {typeof(SimulationBoundary)} boundary type, due to box resizing logic that it implements.");

            BoundingBox box = simBoundary.SimulationBox;
            float dt = timeStep;
            bool periodic = simBoundary.PeriodicBoundary;

            bool resizing = simBoundary.Resizing;

            if (periodic)
            {
                simBoundary.GetBoxLengths();
            }

            if (processCollisions)
                WallCollisions.Clear();

            float factor = 0.5f * dt * dt;
            for (int i = 0; i < sys.NumberOfParticles; i++)
            {
                Vector3 currentPosition = sys.Particles.Position[i];

                Vector3 v = sys.Particles.Velocity[i];

                //Candidate new position
                Vector3 newPosition = currentPosition + v * dt + sys.Particles.Force[i] * factor / sys.Particles.Mass[i];

                if (periodic == false || resizing)
                {
                    //The following if statements reset the position if it has crossed the boundary
                    //and inverts the relevant velocity
                    //TODO there must be a more elegant way to do this?

                    if (resizing)
                    {
                        box = simBoundary.DesiredSimulationBox;
                    }

                    bool collided = false;
                    if (newPosition.X <= (box.Minimum.X) && v.X <= 0.0f)
                    {
                        newPosition.X = currentPosition.X;
                        v.X *= -1.0f;
                        collided = true;
                    }
                    if (newPosition.X >= (box.Maximum.X) && v.X >= 0.0f)
                    {
                        newPosition.X = currentPosition.X;
                        v.X *= -1.0f;
                        collided = true;
                    }

                    if (newPosition.Y <= (box.Minimum.Y) && v.Y <= 0.0f)
                    {
                        newPosition.Y = currentPosition.Y;
                        v.Y *= -1.0f;
                        collided = true;
                    }
                    if (newPosition.Y >= (box.Maximum.Y) && v.Y >= 0.0f)
                    {
                        newPosition.Y = currentPosition.Y;
                        v.Y *= -1.0f;
                        collided = true;
                    }
                    if (newPosition.Z <= (box.Minimum.Z) && v.Z <= 0.0f)
                    {
                        newPosition.Z = currentPosition.Z;
                        v.Z *= -1.0f;
                        collided = true;
                    }
                    if (newPosition.Z >= (box.Maximum.Z) && v.Z >= 0.0f)
                    {
                        newPosition.Z = currentPosition.Z;
                        v.Z *= -1.0f;
                        collided = true;
                    }
                    sys.Particles.Velocity[i] = v;
                    if (processCollisions && collided)
                    {
                        WallCollisions.Add((ushort)i);
                    }
                }

                sys.Particles.Position[i] = newPosition;
            }
        }

        /// <summary>
        /// Gets the properties of this integrator.
        /// </summary>
        /// <returns>IIntegratorProperties.</returns>
        public IIntegratorProperties GetProperties()
        {
            VelocityVerletProperties properties = new VelocityVerletProperties()
            {
                TimeStep = timeStep,
                RemoveCmMotion = removeCMMotion
            };
            return properties;
        }

        /// <summary>
        /// Indicates whether this integrator will remove centre of mass motion.
        /// </summary>
        /// <returns><c>true</c> if centre of mass motion will be removed, <c>false</c> otherwise.</returns>
        public bool RemovesCmMotion()
        {
            return removeCMMotion;
        }

        #endregion Private Methods
    }
}