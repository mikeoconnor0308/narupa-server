﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Integrator;
// ReSharper disable SuspiciousTypeConversion.Global

namespace Simbox.MD.Dynamics.Integrator
{
    /// <summary>
    /// Represents the properties of a velocity verlet integrator.
    /// </summary>
    /// <seealso cref="Nano.Science.Simulation.Integrator.IIntegratorProperties" />
    [XmlName("VelocityVerlet")]
    public class VelocityVerletProperties : IntegratorPropertiesBase
    {
        /// <summary>
        /// Whether to remove CM motion.
        /// </summary>
        public bool RemoveCmMotion;

        /// <inheritdoc />
        public override string Name
        {
            get
            {
                return "VelocityVerlet";
            }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>IIntegratorProperties.</returns>
        public override IIntegratorProperties Clone()
        {
            return new VelocityVerletProperties()
            {
                TimeStep = TimeStep,
                RemoveCmMotion = RemoveCmMotion
            };
        }

        /// <summary>
        /// Initialises the integrator.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="reporter">The reporter.</param>
        /// <returns>IIntegrator.</returns>
        /// <exception cref="Exception">The system utilises constraints, which are not currently supported by the Velocity Verlet integrator. Try running with the Verlet integrator instead.</exception>
        public override IIntegrator InitialiseIntegrator(IAtomicSystem system, IReporter reporter)
        {
            if (system is AtomicSystem == false)
                throw new Exception($"VelocityVerlet integrator requires the type {typeof(AtomicSystem)} ");
            reporter?.PrintNormal("Initialising Velocity Verlet Integrator with timestep {0} fs", TimeStep * Units.FsPerPs);
            //Perform sanity checking to make sure velocity verlet integrator is compatible with system.
            foreach (IForceField f in system.ForceFields)
            {
                if (f is IConstraintImplementation)
                {
                    if ((f as IConstraintImplementation).HasConstraints())
                        throw new Exception("The system utilises constraints, which are not currently supported by the Velocity Verlet integrator. Try running with the Verlet integrator instead.");
                }
            }
            return new VelocityVerletIntegrator((AtomicSystem)system, TimeStep, RemoveCmMotion, reporter);
        }

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
            RemoveCmMotion = Helper.GetAttributeValue(node, "RemoveCMMotion", false);
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            base.Save(context, element);
            Helper.AppendAttributeAndValue(element, "RemoveCMMotion", false);
            return element;
        }

        /// <inheritdoc />
        public override bool Equals(IIntegratorProperties other)
        {
            if (other is VelocityVerletProperties == false) return false;
            VelocityVerletProperties otherV = other as VelocityVerletProperties;
            if (otherV.Name != Name) return false;
            if (otherV.RemoveCmMotion != RemoveCmMotion) return false;
            if (Math.Abs(otherV.TimeStep - TimeStep) > 0.0000001f) return false;
            return true;
        }
    }
}