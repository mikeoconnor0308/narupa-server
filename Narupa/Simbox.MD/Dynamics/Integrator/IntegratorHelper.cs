﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Threading.Tasks;
using Nano.Science.Simulation;
using SlimMath;

namespace Simbox.MD.Dynamics.Integrator
{
    internal static class IntegratorHelper
    {
   
        public static void WrapMoleculesIntoPeriodicBox(IAtomicSystem system, Vector3[] periodicBoxSize)
        {
            //since each molecule has different atoms in it, can safely do this in parallel.
            Parallel.ForEach(system.Topology.Molecules, (m) =>
            {
                Vector3 center = m.GetCentre(system.Particles);
                // Find the displacement to move it into the first periodic box.
                //TODO it might be better to just switch to box with corner at (0,0,0).
                int xcell = (int) Math.Floor((center[0] + 0.5f * periodicBoxSize[0][0]) / periodicBoxSize[0][0]);
                int ycell = (int) Math.Floor((center[1] + 0.5f * periodicBoxSize[1][1]) / periodicBoxSize[1][1]);
                int zcell = (int) Math.Floor((center[2] + 0.5f * periodicBoxSize[2][2]) / periodicBoxSize[2][2]);
                float dx = xcell * periodicBoxSize[0][0];
                float dy = ycell * periodicBoxSize[1][1];
                float dz = zcell * periodicBoxSize[2][2];

                // Translate all the particles in the molecule.

                foreach (int atomIndex in m.AtomIndexes)
                {
                    system.Particles.Position[atomIndex] -= new Vector3(dx, dy, dz);
                }
            });
        }
    }
}