﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Integrator;

namespace Simbox.MD.Dynamics.Integrator
{
    [XmlName("Verlet")]
    internal class VerletIntegratorProperties : IntegratorPropertiesBase
    {
        public bool RemoveCmMotion;

        public override string Name
        {
            get
            {
                return "Verlet";
            }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>IIntegratorProperties.</returns>
        public override IIntegratorProperties Clone()
        {
            return new VerletIntegratorProperties()
            {
                TimeStep = TimeStep,
                RemoveCmMotion = RemoveCmMotion
            };
        }

        /// <summary>
        /// Initialises the integrator.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="reporter">The reporter.</param>
        /// <returns>IIntegrator.</returns>
        public override IIntegrator InitialiseIntegrator(IAtomicSystem system, IReporter reporter)
        {
            reporter?.PrintNormal("Initialising Verlet Integrator");
            if (system is AtomicSystem == false)
                throw new Exception($"Verlet integrator requires the type {typeof(AtomicSystem)} ");

            return new VerletIntegrator((AtomicSystem)system, TimeStep, RemoveCmMotion, reporter);
        }

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
            RemoveCmMotion = Helper.GetAttributeValue(node, "RemoveCMMotion", false);
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            base.Save(context, element);
            Helper.AppendAttributeAndValue(element, "RemoveCMMotion", false);
            return element;
        }

        public override bool Equals(IIntegratorProperties other)
        {
            if (other is VerletIntegratorProperties == false) return false;
            VerletIntegratorProperties otherV = other as VerletIntegratorProperties;
            if (otherV.Name != Name) return false;
            if (otherV.RemoveCmMotion != RemoveCmMotion) return false;
            if (Math.Abs(otherV.TimeStep - TimeStep) > 0.00001f) return false;
            return true;
        }
    }
}