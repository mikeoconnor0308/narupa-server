﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Integrator;

namespace Simbox.MD.Dynamics.Thermostat
{
    [XmlName("BerendsenThermostat")]
    internal class BerendsenThermostatProperties : ThermostatPropertiesBase
    {
        public float BerendsenCoupling;
        public float RescaleFreq;

        public override string Name
        {
            get
            {
                return "BerendsenThermostat";
            }
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);

            BerendsenCoupling = Helper.GetAttributeValue(node, "BerendsenCoupling", 0.5f);
            RescaleFreq = Helper.GetAttributeValue(node, "RescaleFrequency", 25);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            element = base.Save(context, element);
            Helper.AppendAttributeAndValue(element, "RescaleFrequency", RescaleFreq);
            Helper.AppendAttributeAndValue(element, "BerendsenCoupling", BerendsenCoupling);

            return element;
        }

        public override IThermostatProperties Clone()
        {
            BerendsenThermostatProperties b = new BerendsenThermostatProperties()
            {
                EquilibriumTemperature = EquilibriumTemperature,
                MinimumTemperature = MinimumTemperature,
                MaximumTemperature = MaximumTemperature,
                BerendsenCoupling = BerendsenCoupling,
                RescaleFreq = RescaleFreq
            };
            return b;
        }

        public override IThermostat InitialiseStat(IAtomicSystem system, IIntegrator integrator)
        {
            BerendsenThermostat b = new BerendsenThermostat();
            b.MaximumTemperature = MaximumTemperature;
            b.MinimumTemperature = MinimumTemperature;
            b.RescaleFreq = RescaleFreq;
            b.EquilibriumTemperature = EquilibriumTemperature;
            b.BerendsenCoupling = BerendsenCoupling;
            b.SetTimeStep(integrator.GetTimeStep());
            return b;
        }

        public override bool Equals(IThermostatProperties other)
        {
            if (other is BerendsenThermostatProperties == false) return false;
            BerendsenThermostatProperties otherV = other as BerendsenThermostatProperties;
            if (otherV.Name != Name) return false;
            if (otherV.EquilibriumTemperature != EquilibriumTemperature) return false;
            if (otherV.MaximumTemperature != MaximumTemperature) return false;
            if (otherV.MinimumTemperature != MinimumTemperature) return false;
            if (otherV.RescaleFreq != RescaleFreq) return false;
            return true;
        }
    }
}