﻿/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2008-2010 Stanford University and the Authors.      *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

using System;
using System.Collections.Generic;
using Nano;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Transport.Agents;
using SlimMath;

namespace Simbox.MD.Dynamics.Thermostat
{
    /// <summary>
    /// Represents an Andersen thermostat.
    /// </summary>
    public class AndersenThermostat : Nano.Science.Simulation.Thermostat
    {

        /// <summary> Rate of collision with bath in 1/ps.</summary>
        public float CollisionRate;

        /// <summary>
        /// Initialises this instance's IVariables.
        /// </summary>
        /// <param name="variableManager">The variable manager.</param>
        public override void InitialiseVariables(VariableManager variableManager)
        {
            base.InitialiseVariables(variableManager);
            ResetVariableDesiredValues();
        }

        private List<List<int>> atomGroups = new List<List<int>>();
        
        /// <summary>
        /// Generates sets of atoms that are grouped together by constraints. 
        /// </summary>
        /// <param name="system">Atomic system.</param>
        internal void GenerateGroups(IAtomicSystem system)
        {
            //Get all the constraints. 
            List<Constraint> constraints = new List<Constraint>();
            foreach (IConstraintImplementation constraintImplementation in system.Constraints)
            {
                constraints.AddRange(constraintImplementation.GetConstraints());
            }
            
            //Make a list for each particle detailing which atom it is in a constraint with. 
            List<List<int>> particleConstraints = new List<List<int>>(system.NumberOfParticles);
            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                particleConstraints.Add(new List<int>());
            }
            
            foreach (var constraint in constraints)
            {
                particleConstraints[constraint.A].Add(constraint.B);
                particleConstraints[constraint.B].Add(constraint.A);
            }
            
            //Initialize group of each atom to -1
            List<int> particleGroup = new List<int>(system.NumberOfParticles);
            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                particleGroup.Add(-1);
            }

            
            //Use the constraint information to group atoms. 
            int numGroups = 0;
            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                if (particleGroup[i] == -1)
                {
                    TagParticlesInGroup(i, numGroups++, particleGroup, particleConstraints);
                }
            }
            
            //Make lists of all the atoms in each group
            List<List<int>> particleIndices = new List<List<int>>(numGroups);
            for (int i = 0; i < numGroups; i++)
            {
                particleIndices.Add(new List<int>());
            }
            
            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                particleIndices[particleGroup[i]].Add(i);
            }

            atomGroups = particleIndices;
        }

        private void TagParticlesInGroup(int atomIndex, int currentGroupIndex, List<int> particleGroup, List<List<int>> particleConstraints)
        {
            //Recursively tag particles as belonging to a particular group.
            particleGroup[atomIndex] = currentGroupIndex;
            foreach (var constrained in particleConstraints[atomIndex])
            {
                if(particleGroup[constrained] == -1)
                    TagParticlesInGroup(constrained, currentGroupIndex, particleGroup, particleConstraints);
            }
        }

        /// <summary>
        /// Updates this instance's IVariables.
        /// </summary>
        public override void UpdateVariableValues()
        {
            if (EquilibriumTemperatureIVar.DesiredValue < MinimumTemperature)
            {
                EquilibriumTemperatureIVar.DesiredValue = MinimumTemperature;
            }
            else if (EquilibriumTemperatureIVar.DesiredValue >= MaximumTemperature)
            {
                EquilibriumTemperatureIVar.DesiredValue = MaximumTemperature;
            }
            if (Math.Abs(EquilibriumTemperatureIVar.DesiredValue - EquilibriumTemperature) > 0.001f)
            {
                SetEquilibriumTemperature(EquilibriumTemperatureIVar.DesiredValue);
            }
            EquilibriumTemperatureIVar.Value = InstantaneousTemperature;
        }

        /// <summary>
        /// Applies the thermostat to the system it was instantiated in.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <exception cref="Exception">Berendsen Rescale factor set to NaN. Check input! </exception>
        /// <exception cref="NotImplementedException"></exception>
        public override void ApplyStat(IAtomicSystem system)
        {
            float timeStep = system.Integrator.GetTimeStep();

            double collisionProbability = 1.0 - Math.Exp(-CollisionRate * timeStep);

            Vector2 keTemp = system.CalculateKineticEnergyAndTemperature();
            InstantaneousTemperature = keTemp[1];
            
            foreach (var group in atomGroups)
            {
                if (MathUtilities.Rand.NextDouble() < collisionProbability)
                {
                    foreach (var atom in group)
                    {
                        if (system.Particles.Mass[atom] < 0.00001f)
                            continue;
                        double boltz = PhysicalConstants.MolarGasConstantR / 1000.0;
                        double velocityScale = Math.Sqrt(boltz * EquilibriumTemperature / system.Particles.Mass[atom]);
                        
                        system.Particles.Velocity[atom] = (float) velocityScale * new Vector3(MathUtilities.GenerateGaussianNoise(0, 1),MathUtilities.GenerateGaussianNoise(0, 1), MathUtilities.GenerateGaussianNoise(0, 1));
                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <returns>IThermostatProperties.</returns>
        public override IThermostatProperties GetProperties()
        {
            throw new NotImplementedException(); 
        }

        #region Private Fields

        private int step;
        private float timeStep;

        /// <summary>
        /// Gets the name of this thermostat.
        /// </summary>
        /// <value>The name.</value>
        public override string Name
        {
            get
            {
                return "Andersen Thermostat";
            }
        }

        #endregion Private Fields
    }
}