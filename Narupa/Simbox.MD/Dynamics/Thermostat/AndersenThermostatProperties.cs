﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Integrator;

namespace Simbox.MD.Dynamics.Thermostat
{
    [XmlName("AndersenThermostat")]
    internal class AndersenThermostatProperties : ThermostatPropertiesBase
    {
        /// <summary>
        /// Frequency of collisions with the bath.
        /// </summary>
        public float CollisionFreq;


        /// <inheritdoc />
        public override string Name
        {
            get
            {
                return "AndersenThermostat";
            }
        }

        /// <inheritdoc />
        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
            CollisionFreq = Helper.GetAttributeValue(node, "CollisionFrequency", 0.5f);
        }

        /// <inheritdoc />
        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            element = base.Save(context, element);
            Helper.AppendAttributeAndValue(element, "CollisionFrequency", CollisionFreq);

            return element;
        }

        /// <inheritdoc />
        public override IThermostatProperties Clone()
        {
            AndersenThermostatProperties b = new AndersenThermostatProperties()
            {
                EquilibriumTemperature = EquilibriumTemperature,
                MinimumTemperature = MinimumTemperature,
                MaximumTemperature = MaximumTemperature,
                CollisionFreq = CollisionFreq
            };
            return b;
        }

        /// <inheritdoc />
        public override IThermostat InitialiseStat(IAtomicSystem system, IIntegrator integrator)
        {
            AndersenThermostat b = new AndersenThermostat();
            b.MaximumTemperature = MaximumTemperature;
            b.MinimumTemperature = MinimumTemperature;
            b.CollisionRate = CollisionFreq;
            b.EquilibriumTemperature = EquilibriumTemperature;
            b.GenerateGroups(system);
            return b;
        }

        /// <inheritdoc />
        public override bool Equals(IThermostatProperties other)
        {
            if (other is AndersenThermostatProperties == false) return false;
            AndersenThermostatProperties otherV = other as AndersenThermostatProperties;
            if (otherV.Name != Name) return false;
            if (otherV.EquilibriumTemperature != EquilibriumTemperature) return false;
            if (otherV.MaximumTemperature != MaximumTemperature) return false;
            if (otherV.MinimumTemperature != MinimumTemperature) return false;
            if (otherV.CollisionFreq != CollisionFreq) return false;
            return true;
        }
    }
}