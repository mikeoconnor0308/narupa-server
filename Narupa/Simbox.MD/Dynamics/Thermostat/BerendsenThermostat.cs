﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Agents;
using Nano.Science.Simulation;
using SlimMath;

namespace Simbox.MD.Dynamics.Thermostat
{
    /// <summary>
    /// Represents the properties of the Berendsen thermostat.
    /// </summary>
    public class BerendsenThermostat : Nano.Science.Simulation.Thermostat
    {

        #region Public Properties

        /// <summary> Coupling between target temperature and current temperature.</summary>
        public float BerendsenCoupling;

        /// <summary>
        /// Gets the rescale frequency.
        /// </summary>
        /// <value>The rescale frequency.</value>
        public float RescaleFreq;

        #endregion Public Properties

        /// <summary>
        /// Sets the time step.
        /// </summary>
        /// <param name="v">The v.</param>
        internal void SetTimeStep(float v)
        {
            if (BerendsenCoupling < timeStep)
            {
                throw new Exception("Attempted to set the Berendsen coupling to less than the timestep, which can result in instability!");
            }
            timeStep = v;
        }

        /// <summary>
        /// Disposes this instance's IVariables.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public override void Dispose()
        {
            base.Dispose();
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialises this instance's IVariables.
        /// </summary>
        /// <param name="variableManager">The variable manager.</param>
        public override void InitialiseVariables(VariableManager variableManager)
        {
            base.InitialiseVariables(variableManager);
            ResetVariableDesiredValues();
        }

        /// <summary>
        /// Updates this instance's IVariables.
        /// </summary>
        public override void UpdateVariableValues()
        {
            if (EquilibriumTemperatureIVar.DesiredValue < MinimumTemperature)
            {
                EquilibriumTemperatureIVar.DesiredValue = MinimumTemperature;
            }
            else if (EquilibriumTemperatureIVar.DesiredValue >= MaximumTemperature)
            {
                EquilibriumTemperatureIVar.DesiredValue = MaximumTemperature;
            }
            if (Math.Abs(EquilibriumTemperatureIVar.DesiredValue - EquilibriumTemperature) > 0.001f)
            {
                SetEquilibriumTemperature(EquilibriumTemperatureIVar.DesiredValue);
            }
            EquilibriumTemperatureIVar.Value = InstantaneousTemperature;
        }

        /// <summary>
        /// Applies the thermostat to the system it was instantiated in.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <exception cref="Exception">Berendsen Rescale factor set to NaN. Check input! </exception>
        /// <exception cref="NotImplementedException"></exception>
        public override void ApplyStat(IAtomicSystem system)
        {
            step++;
            if ((step - 1) < RescaleFreq)
            {
                return;
            }
            step = 1;
            //Apply berendsen velocity rescaling
            Vector2 keTemp = system.CalculateKineticEnergyAndTemperature();
            if (keTemp[0] < 0.001)
            {
                throw new Exception("Shockingly low kinetic energy");
            }
            float temperature = keTemp.Y;
            InstantaneousTemperature = temperature;
            float berendsenFactor = (float)Math.Sqrt(1f + timeStep / BerendsenCoupling * (EquilibriumTemperature / temperature - 1f));

            float maxAllowedDeviation = 4f;
            if (temperature > maxAllowedDeviation * MaximumTemperature)
            {
                throw new Exception($"System instantaneous temperature has exceeded maximum temperature of {MaximumTemperature}K, it is: {temperature}");
            }
            if (!float.IsNaN(berendsenFactor))
            {
                system.Particles.RescaleVelocities(berendsenFactor);
            }
            else
            {
                throw new Exception("Berendsen Rescale factor set to NaN. Check input! ");
            }
        }

        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <returns>IThermostatProperties.</returns>
        public override IThermostatProperties GetProperties()
        {
            return new BerendsenThermostatProperties()
            {
                EquilibriumTemperature = EquilibriumTemperature,
                BerendsenCoupling = BerendsenCoupling,
                MaximumTemperature = MaximumTemperature,
                MinimumTemperature = MinimumTemperature,
                RescaleFreq = RescaleFreq
            };
        }

        #region Private Fields

        private int step;
        private float timeStep;

        /// <summary>
        /// Gets the name of this thermostat.
        /// </summary>
        /// <value>The name.</value>
        public override string Name
        {
            get
            {
                return "Berendsen Thermostat";
            }
        }

        #endregion Private Fields
    }
}