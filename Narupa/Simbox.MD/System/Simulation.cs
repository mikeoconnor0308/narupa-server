﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using Simbox.MD.Dynamics.Integrator;

namespace Simbox.MD
{
    /// <summary>
    /// Class that encapsulates an MD simulation
    /// </summary>
    [XmlName("Simulation")]
    public class Simulation : SimulationBase
    {
        #region Public Fields

        /// <inheritdoc />
        public override string Name { get; set; }

        /// <summary>
        /// The atomic system.
        /// </summary>
        public override IAtomicSystem ActiveSystem => activeSystem;

        /// <summary>
        /// The active topology.
        /// </summary>
        public override InstantiatedTopology ActiveTopology { get; set; }

        /// <summary>
        /// The simulation file.
        /// </summary>
        public SimulationFile SimulationFile;

        #endregion Public Fields

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether this instance has reset.
        /// </summary>
        /// <value><c>true</c> if this instance has reset; otherwise, <c>false</c>.</value>
        public override bool HasReset { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="T:Nano.Transport.Variables.IVariableProvider" />  is initialised
        /// </summary>
        /// <value><c>true</c> if the IVariableProvider; otherwise, <c>false</c>.</value>
        public override bool VariablesInitialised => ActiveSystem.VariablesInitialised;

        /// <summary>
        /// Gets the simulation time.
        /// </summary>
        /// <value>The simulation time.</value>
        public override float SimulationTime
        {
            get { return simulationTime; }
        }
        
        /// <inheritdoc />
        public override bool EndOfSimulation { get; } = false;

        /// <summary>
        /// Gets or sets the address of the command.
        /// </summary>
        /// <value>The address.</value>
        public override string Address
        {
            get
            {
                return "/sim";
            }
        }

        #endregion Public Properties

        #region Public Constructors

        public Simulation(IReporter reporter) : base(reporter)
        {
        }

        public Simulation(string simulationFile, IReporter reporter) : base(reporter)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Helper.ResolvePath(simulationFile));
            reporter?.PrintEmphasized("Loading simulation from file {0}", simulationFile);
            LoadContext context = new LoadContext(reporter);
            Load(context, doc.DocumentElement);
            Reporter = reporter;
        }

        public Simulation(XmlDocument xmlDoc, IReporter reporter) : base(reporter)
        {
            Reporter = reporter;
            LoadContext context = new LoadContext(reporter);
            Load(context, xmlDoc.DocumentElement);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            element.AppendChild(SimulationFile.Save(context, element));
            return element;
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, nameof(Name), "Simulation");
            Reporter = context.Reporter;
            SimulationFile = new SimulationFile();
            SimulationFile.Load(context, node);

            if (SimulationFile.LoadedSimulation == false)
            {
                throw new Exception("Failed to load simulation.");
            }
            SimulationName = SimulationFile.SimulationName;

            Reporter?.PrintNormal("Loaded simulation file. Proceeding to instantiate topology...");
            try
            {
                ActiveTopology = SimulationFile.InstantiateTopology();
            }
            catch (Exception e)
            {
                context.Reporter.PrintException(e, "Exception thrown when spawning loaded templates into simulation.");
                throw;
            }

            Reporter?.PrintNormal("Instantiated topology, proceeding to initialise simulation...");
            try
            {
                InitialiseSimulation();
            }
            catch (Exception e)
            {
                context.Reporter.PrintException(e, "Exception thrown when attemping to create active atomic system.");
                throw;
            };
            Reporter?.PrintEmphasized("Successfully initialised simulation.");

        }

        /// <summary>
        /// Attaches the transport context.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        public override void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        /// <summary>
        /// Detaches the transport context.
        /// </summary>
        /// <param name="transportContext">The transport context.</param>
        public override void DetachTransportContext(TransportContext transportContext)

        {
        }

        #endregion Public Constructors

        #region Public Methods

        private IVariable<float> computationTimeIVar;
        private IVariable<string> simulationNameIVar;
        private float frameComputeTime;
        private float averageComputeTime;
        private Stopwatch clock = new Stopwatch();

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public override void Reset()
        {
            DetachTransportContext(transportContext);
            ActiveSystem.Dispose();
            ActiveTopology = SimulationFile.InstantiateTopology();

            InitialiseSimulation();

            AttachTransportContext(transportContext);
            ActiveTopology.AttachTransportContext(transportContext);
            simulationTime = 0f;

            HasReset = true;
        }

        private void InitialiseSimulation()
        {
            activeSystem = new AtomicSystem(ActiveTopology, SimulationFile.SimulationSettings.Clone(), Reporter);
            activeSystem.CalculateForceFields();
            activeSystem.InitializeVelocities();
            activeSystem.ResetAveragePressure();
            Reporter?.PrintEmphasized("Created a system of {0} particles with potential energy: {1} kJ/mol", ActiveSystem.NumberOfParticles, activeSystem.PotentialEnergy);
        }

        /// <summary>
        /// Runs the specified steps.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="interactionPoints">The interaction points.</param>
        public override void Run(int steps, List<Interaction> interactionPoints = null)
        {
            paused = false;
            ActiveSystem.Interactions.SetInteractions(interactionPoints);
            /*
            if (ActiveSystem.Interactions.Count == 0)
                ActiveSystem.Particles.RemoveCentreOfMassMotion();
            */

            clock.Restart();
            ActiveSystem.Integrator.Propagate(steps);
            clock.Stop();
            frameComputeTime = (float)clock.ElapsedTicks / Stopwatch.Frequency * 1000;

            simulationTime += steps * ActiveSystem.Integrator.GetTimeStep();
            HasReset = false;
        }

        #endregion Public Methods

        #region Private Fields

        private IReporter Reporter;
        private TransportContext transportContext;
        private string SimulationName;
        private float steps;
        private AtomicSystem activeSystem;
        private float simulationTime;
        private bool paused;

        #endregion Private Fields

        #region Private Methods

        /// <summary>
        /// Initialises the specified variable manager.
        /// </summary>
        /// <param name="variableManager">The variable manager.</param>
        public override void InitialiseVariables(VariableManager variableManager)
        {
            computationTimeIVar = variableManager.Variables.Add(VariableName.ComputationTime, VariableType.Float, true) as IVariable<float>;

            simulationNameIVar = variableManager.Variables.Add(VariableName.SimulationName, VariableType.String, true) as IVariable<string>;
            simulationNameIVar.Value = SimulationName;
            simulationNameIVar.DesiredValue = SimulationName;
            //SimulationTimeIVar = variableManager.Variables.Add(VariableName.SimulationTime, VariableType.Float, true) as IVariable<float>;

            ActiveSystem.InitialiseVariables(variableManager);
            ActiveSystem.ResetVariableDesiredValues();
            ActiveSystem.UpdateVariableValues();
        }

        /// <summary>
        /// Populates the collisions stream.
        /// </summary>
        /// <param name="atomCollision">The atom collision.</param>
        public void PopulateCollisions(IDataStream<ushort> atomCollision)
        {
            if (ActiveSystem.Integrator is VelocityVerletIntegrator)
            {
                List<ushort> collisions = (ActiveSystem.Integrator as VelocityVerletIntegrator).WallCollisions;
                for (int i = 0; i < collisions.Count; i++)
                {
                    if (i == atomCollision.Data.Length)
                        return;
                    atomCollision.Data[i] = collisions[i];
                }
                atomCollision.Count = collisions.Count;
                //TODO add particle collisions.
            }
        }

        /// <summary>
        /// Update input IVariables
        /// </summary>
        public override void UpdateVariableValues()
        {
            averageComputeTime = (frameComputeTime + steps * averageComputeTime) / (steps + 1);
            
            steps++;
            
            if (steps > 100)
            {
                steps = 0;
                Reporter.PrintDetail($"Avg Compute time:{averageComputeTime}");
                averageComputeTime = 0f;
            }
            
            computationTimeIVar.Value = averageComputeTime;
            if (simulationNameIVar != null)
            {
                if (SimulationName != simulationNameIVar.Value)
                    simulationNameIVar.Value = SimulationName;
            }
            //SimulationTimeIVar.Value = SimulationTime;
            ActiveSystem.UpdateVariableValues();
        }

        /// <summary>
        /// Update output IVariables and broadcast values, overriding desired values
        /// </summary>
        public override void ResetVariableDesiredValues()
        {
            ActiveSystem.ResetVariableDesiredValues();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            ActiveSystem.Dispose();
        }

        /// <summary>
        /// Sets the simulation state to paused.
        /// </summary>
        public override void Pause()
        {
            paused = true;
        }
        
        #endregion Private Methods
    }
}