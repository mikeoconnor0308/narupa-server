﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Rug.Osc;
using Simbox.MD.ForceField.External.ForceField;
using Simbox.MD.Selection;

namespace Simbox.MD
{
    /// <summary>
    /// Class representing the atomic system, including atom positions and masses
    /// Methods to determine properties such as kinetic energy and temperature.
    /// <remarks>
    /// Atomic variables are stored in Structure of Arrays format (SoA) for
    /// performance purposes.
    /// </remarks></summary>
    /// <seealso cref="Nano.Transport.Variables.IVariableProvider" />
    public partial class AtomicSystem : AtomicSystemBase
    {
        #region Public Fields

        public Barostat Barostat;

        /// <summary>
        /// The thermostat.
        /// </summary>
        public Thermostat Thermostat;

        #endregion Public Fields

        #region Public Properties

        /// <summary>
        /// Gets the average pressure of the system in kJ/mol/nm^3 .
        /// </summary>
        /// <value>The pressure.</value>
        public float AveragePressure { get; private set; }

        /// <summary>
        /// Gets the average pressure in bars.
        /// </summary>
        /// <value>The average pressure in bars.</value>
        public float AveragePressureInBars => AveragePressure / (PhysicalConstants.AvogadroC * 1e-25f);

        public float NumberOfMoleculesPerMol => Topology.Molecules.Count / PhysicalConstants.AvogadroC;

        #endregion Public Properties

        #region Private Fields

        public float Virial;

        private float pressureCalculations;

        #endregion Private Fields

        /// <summary>
        /// Handles the <see cref="E:BoundaryChange" /> event.
        /// </summary>
        /// <param name="boundary">The boundary.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnBoundaryChange(object boundary, EventArgs e)
        {
            pressureCalculations = 0;
        }

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AtomicSystem" /> class.
        /// </summary>
        /// <param name="highTopology">The high topology.</param>
        /// <param name="systemProperties">The system properties.</param>
        /// <param name="reporter">The reporter.</param>
        /// <param name="integrator">Whether to construct with an integrator</param>
        public AtomicSystem(InstantiatedTopology highTopology, SystemProperties systemProperties, IReporter reporter, bool integrator = true) : base(highTopology, systemProperties, reporter, integrator)
        {
            AtomSelections = new AtomSelectionPool(new OscAddress("/top"), Topology, reporter);
            if (Boundary != null) Boundary.BoxChange += OnBoundaryChange;

            if (integrator)
            {
                Integrator = systemProperties.IntegratorProperties?.InitialiseIntegrator(this, reporter);

                foreach (IThermostatProperties thermostatProperties in systemProperties.ThermostatProperties.Values)
                {
                    if (Integrator == null)
                        throw new Exception("Attempted to create thermostat with no integrator.");
                    IThermostat stat = thermostatProperties.InitialiseStat(this, Integrator);
                    if (stat is Thermostat)
                    {
                        Thermostat = stat as Thermostat;
                        reporter?.PrintNormal("Added thermostat ({0}) with equilibrium temperature {1} Kelvin", Thermostat.Name, Thermostat.EquilibriumTemperature);
                    }
                    if (stat is Barostat)
                    {
                        Barostat = stat as Barostat;
                        reporter?.PrintNormal("Added barostat ({0}) with equilibrium pressure {1} bars", Barostat.Name, Barostat.EquilibriumPressure);
                    }
                }
            }

           
            bool containsParticleRestraintForce = false;
            foreach (IForceField forcefield in ForceFields)
            {
                if (forcefield is ParticleRestraintForce)
                {
                    containsParticleRestraintForce = true;
                }
                
            }
            if(!containsParticleRestraintForce) ForceFields.Add(new ParticleRestraintForce());
            InitializeVelocities();

            AtomSelections.UpdateVisibleAtoms();
        }

        #endregion Public Constructors

        /// <summary>
        /// Initialize this system's velocities, based on thermostat in use
        /// </summary>
        public void InitializeVelocities()
        {
            if (Thermostat != null)
            {
                InitializeVelocitiesToTemperature(Thermostat.EquilibriumTemperature);
            }
            else
            {
                Particles.ZeroVelocities();
            }
        }

        public override float CalculateForceFields()
        {
            Virial = 0f;
            return base.CalculateForceFields();
        }
    }
}