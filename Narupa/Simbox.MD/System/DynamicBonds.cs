﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Simbox.MD
{
    /// <summary>
    /// Implements dynamic generation of bonds from atomic positions.
    /// </summary>
    /// <remarks>
    /// The implementation is naive and should not be used for large systems.
    /// </remarks>
    [XmlName("DynamicBonds")]
    public class DynamicBonds : IDynamicBonds
    {
        /// <summary>
        /// Frequency with which bonds will be updated.
        /// </summary>
        public int UpdateFrequency = 1;

        /// <summary>
        /// Length that defines a bond in nm.
        /// </summary>
        public float BondLength = 0.12f;

        private BondList bonds = new BondList();

        private int step;
        private bool firstCalculation = true;

        /// <summary>
        /// Given current state of atomic system, calculates bonds based on the update frequency.
        /// </summary>
        /// <param name="system"></param>
        /// <param name="forceCalculation"></param>
        /// <returns></returns>
        /// <remarks>
        /// TODO need cell list for this to be efficient.
        /// TODO should be able to only do dynamic bonds on a particular residue etc.
        /// </remarks>
        public BondList CalculateBonds(IAtomicSystem system, out bool bondsChanged, bool forceCalculation = false)
        {
            bondsChanged = false;
            if (step < UpdateFrequency && !forceCalculation && !firstCalculation)
            {
                step++;
                return bonds;
            }
            if (firstCalculation)
                firstCalculation = false;

            var bondCopy = bonds.Clone();
            bonds.Clear();

            //Loop over all particles
            //if

            Vector3 posI;
            Vector3 posJ;
            for (int i = 0; i < system.Particles.NumberOfParticles; i++)
            {
                posI = system.Particles.Position[i];
                for (int j = i + 1; j < system.Particles.NumberOfParticles; j++)
                {
                    posJ = system.Particles.Position[j];
                    if (ShouldBond(posI, posJ, BondLength))
                    {
                        bonds.AddBond(new BondPair(i, j));
                    }
                }
            }

            if (bonds.Equals(bondCopy) == false)
                bondsChanged = true;
            return bonds;
        }

        private static bool ShouldBond(Vector3 one, Vector3 two, float bondLength = 0.16f)
        {
            float dist = (one - two).Length;

            return (dist <= bondLength);
        }

        public void Load(LoadContext context, XmlNode node)
        {
            UpdateFrequency = Helper.GetAttributeValue(node, "UpdateFreq", 1);
            BondLength = Helper.GetAttributeValue(node, "BondLength", BondLength);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "UpdateFreq", UpdateFrequency);
            Helper.AppendAttributeAndValue(element, "BondLength", BondLength);
            return element;
        }
    }
}