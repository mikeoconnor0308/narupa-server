﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano;
using Nano.Transport.Variables;
using Nano.Science;

// ReSharper disable once CheckNamespace
namespace Simbox.MD
{
    public partial class AtomicSystem
    {
        /// <summary>
        /// Calculates the pressure in the system in bar.
        /// </summary>
        /// <remarks>
        /// de Miguel, Enrique, and George Jackson.
        /// "The nature of the calculation of the pressure in molecular
        /// simulations of continuous models from volume perturbations."
        /// The Journal of chemical physics 125.16 (2006): 164109.
        /// </remarks>
        public float CalculateInstantaneousPressure()
        {
            if (Thermostat == null)
                return 0f;
            float v = (MathUtilities.CalculateVolume(Boundary.SimulationBox));
            float n = NumberOfParticles;

            //Pressure in kJ/mol/nm^3
            //TODO not convinced the ideal gas contribution is correct...
            //this is calculated as 1/V * N * R * T. Divide through by 1000 to convert to kJ
            float pressure = 1 / v * (n * PhysicalConstants.MolarGasConstantR / 1000 * Thermostat.EquilibriumTemperature + 0.33f * Virial);

            AveragePressure = (pressure + pressureCalculations * AveragePressure) / (pressureCalculations + 1);
            pressureCalculations++;
            //Convert to bar
            return pressure / (PhysicalConstants.AvogadroC * 1e-25f);
        }

        /// <summary>
        /// Resets the average pressure.
        /// </summary>
        public void ResetAveragePressure()
        {
            AveragePressure = 0;
            pressureCalculations = 0;
            CalculateInstantaneousPressure();
            (this as IVariableProvider).ResetVariableDesiredValues();
        }
    }
}