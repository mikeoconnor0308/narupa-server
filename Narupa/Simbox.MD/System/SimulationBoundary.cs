﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Xml;
using Nano.Loading;
using Nano.Transport.Agents;
using Nano.Transport.Variables;
using SlimMath;
using Nano.Science.Simulation;
using Nano;

namespace Simbox.MD
{
    /// <summary> Class managing boundary of simulation. </summary>
    [XmlName("SimulationBoundary")]
    public class SimulationBoundary : ISimulationBoundary
    {
        /// <summary>
        /// Modes in which the box may resize, either by rescaling positions relative to zero, or by allowing the particles to transition into a new box.
        /// </summary>
        public enum ResizeMode { RescalePositions, Transition }

        #region Public Fields

        /// <summary> Minimum box volume for simulation. </summary>
        public float MinimumBoxVolume { get; set; }

        /// <summary> Whether this simulation is using periodic boundary conditions. </summary>
        public bool PeriodicBoundary { get; set; }

        /// <summary> Whether this simulation boundary is currently undergoing resizing. </summary>
        public bool Resizing { get; set; }

        public ResizeMode ActiveResizeMode = ResizeMode.Transition;

        #endregion Public Fields

        #region Public Properties

        /// <summary> Target dimensions of simulation box, in Angstroms. </summary>
        public BoundingBox DesiredSimulationBox { get; set; }

        /// <summary>
        /// Whether this boundary is variable or not.
        /// </summary>
        public bool IsVariableBox { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="IVariableProvider" />  is initialised
        /// </summary>
        /// <value><c>true</c> if the IVariableProvider; otherwise, <c>false</c>.</value>
        public bool VariablesInitialised { get { return iVarsInitialised; } }

        /// <summary>
        /// Occurs when [box change].
        /// </summary>
        public event EventHandler BoxChange;

        /// <summary> 3D dimensions of box, in Angstroms. </summary>
        public BoundingBox SimulationBox
        {
            get { return simulationBox; }
            set
            {
                simulationBox = value;
                ComputeBoxLengths();
                SetPeriodicBoxVectorsToLengths();
                if (BoxChange != null) BoxChange(this, null);
            }
        }

        private BoundingBox simulationBox;

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SimulationBoundary(IReporter reporter)
        {
            this.reporter = reporter;
            SetDefaults();
        }

        /// <summary> Construct boundary from passed BoundingBox </summary>
        public SimulationBoundary(BoundingBox boundingBox, bool periodic = false, bool resizable = false, float minVolume = 0f)
        {
            SimulationBox = boundingBox;
            DesiredSimulationBox = SimulationBox;
            PeriodicBoundary = periodic;
            ComputeBoxLengths();
            IsVariableBox = resizable;
            if (resizable)
            {
                if (minVolume <= 0f)
                    throw new Exception("Invalid minimum box volume for resizable box.");
                MinimumBoxVolume = minVolume;
            }
            else
            {
                MinimumBoxVolume = 0f;
            }
        }

        public SimulationBoundary()
        {
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Sets the desired simulation box to the current simulation box
        /// </summary>
        private void ClampDesiredToCurrentBox()
        {
            DesiredSimulationBox = SimulationBox;
        }

        /// <summary>
        /// Gets the periodic box vector.
        /// </summary>
        /// <returns>Vector3.</returns>
        /// <exception cref="Exception">Attempted to get periodic box vector when periodic booundary is not enforced!</exception>
        public Vector3 GetBoxLengths()
        {
            if (boxLengths == null)
                ComputeBoxLengths();
            return boxLengths;
        }

        /// <summary>
        /// Gets the periodic box vector recipricol.
        /// </summary>
        /// <returns>Vector3.</returns>
        /// <exception cref="Exception">Attempted to get periodic box vector when periodic booundary is not enforced!</exception>
        public Vector3 GetBoxLengthsRecipricol()
        {
            if (!PeriodicBoundary)
                throw new Exception("Attempted to get periodic box vector when periodic booundary is not enforced!");
            if (boxLengths == null)
                ComputeBoxLengths();
            return boxLengthsRecip;
        }

        /// <summary>
        /// Saves this instance to the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        /// <autogeneratedoc />
        /// TODO Edit XML Comment Template for Save
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "SimulationBox", SimulationBox.Maximum - SimulationBox.Minimum);
            Helper.AppendAttributeAndValue(element, "PeriodicBoundary", PeriodicBoundary);
            Helper.AppendAttributeAndValue(element, "MinimumBoxVolume", MinimumBoxVolume);

            return element;
        }

        internal void FitBoundaryToPositions(List<Vector3> atomPositions, float padPercentage = 0.1f)
        {
            BoundingBox boundingBox = ComputeBoundingSimulationBox(atomPositions);
            BoundingBox boundingCube = GetBoundingCube(boundingBox);

            Vector3 newMax = boundingCube.Maximum + boundingCube.Maximum * 0.1f;
            BoundingBox newBox = new BoundingBox(-newMax, newMax);
            SimulationBox = newBox;
        }

        private BoundingBox GetBoundingCube(BoundingBox box)
        {
            float length = box.Maximum.X;
            if (Math.Abs(box.Maximum.Y) > length)
                length = Math.Abs(box.Maximum.Y);
            if (Math.Abs(box.Maximum.Z) > length)
                length = Math.Abs(box.Maximum.Z);
            if (Math.Abs(box.Minimum.X) > length)
                length = Math.Abs(box.Minimum.X);
            if (Math.Abs(box.Minimum.Y) > length)
                length = Math.Abs(box.Minimum.Y);
            if (Math.Abs(box.Minimum.Z) > length)
                length = Math.Abs(box.Minimum.Z);

            BoundingBox cube = new BoundingBox(-length, -length, -length, length, length, length);
            return cube;
        }

        /// <summary>
        /// Loads boundary information from the specified node.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        /// <exception cref="SimboxException">
        /// </exception>
        public void Load(LoadContext context, XmlNode node)
        {
            SetDefaults();

            Vector3 size = Helper.GetAttributeValue(node, "SimulationBox", Vector3.Zero);

            Vector3 simBoxMin = Helper.GetAttributeValue(node, "SimulationBoxMin", Vector3.Zero);
            Vector3 simBoxMax = Helper.GetAttributeValue(node, "SimulationBoxMax", Vector3.Zero);

            if (size != Vector3.Zero)
            {
                if (simBoxMin != Vector3.Zero || simBoxMax != Vector3.Zero)
                {
                    string error = "Attributes SimulationBox and SimulationBoxMin/Max were both specified, only one is required. Check input file.";
                    context.Error(error);
                    throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, error);
                }
                SimulationBox = new BoundingBox(size * -0.5f, size * 0.5f);
            }
            else
            {
                if (simBoxMax != Vector3.Zero || simBoxMin != Vector3.Zero)
                {
                    SimulationBox = new BoundingBox(simBoxMin, simBoxMax);
                }
                else
                {
                    string error = "No valid simulation boundary dimensions detected. Either the attribute SimulationBox or both SimulationBoxMin and SimulationBoxMax should be specified. Check input file.";
                    context.Error(error);
                    throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, error);
                }
            }

            PeriodicBoundary = Helper.GetAttributeValue(node, "PeriodicBoundary", false);

            IsVariableBox = Helper.GetAttributeValue(node, "IsResizeable", true);

            if (IsVariableBox)
                MinimumBoxVolume = Helper.GetAttributeValue(node, "MinimumBoxVolume", 0f);
            DesiredSimulationBox = SimulationBox;
            ComputeBoxLengths();
            SetPeriodicBoxVectorsToLengths();
        }

        /// <summary> Load default initial values for simulation bounding box </summary>
        public void SetDefaults()
        {
            float defaultLength = 10000f;
            PeriodicBoundary = false;
            Resizing = false;
            IsVariableBox = false;
            MinimumBoxVolume = 0f;
            SimulationBox = new BoundingBox(new Vector3(-defaultLength), new Vector3(defaultLength));
            DesiredSimulationBox = SimulationBox;
        }

        /// <summary>
        /// Sets the periodic box vectors to the lengths of the box.
        /// </summary>
        private void SetPeriodicBoxVectorsToLengths()
        {
            //Assume a cubic periodic box for now.
            //TODO generalise to orthorombic.
            Vector3 a = new Vector3(boxLengths.X, 0, 0);
            Vector3 b = new Vector3(0, boxLengths.Y, 0);
            Vector3 c = new Vector3(0, 0, boxLengths.Z);
            SetPeriodicBoxVectors(a, b, c);
        }

        /// <summary>
        /// Sets the simulation box to a specific bounding box.
        /// </summary>
        /// <param name="box">The box.</param>
        public void SetSimulationBox(BoundingBox box)
        {
            if (MathUtilities.CalculateVolume(box) < MinimumBoxVolume)
                throw new Exception("Attempted to set box to volume smaller than the minimum volume!");
            SimulationBox = box;

            ClampDesiredToCurrentBox();
            (this as IVariableProvider).ResetVariableDesiredValues();
        }


        public void SetSimulationBox(Vector3 a, Vector3 b, Vector3 c, bool centerAboutZero = true)
        {
            if(a[1] != 0f || a[2] != 0f || b[0] != 0f || b[2] != 0f || c[0] != 0f || c[1] != 0f)
            {
                throw new ArgumentException($"Nano Simbox currently only supports rectangular boxes, the periodic boundary vectors supplied ({a},{b},{c}) do not form a rectangle aligned with xyz.");
            }

            BoundingBox box = centerAboutZero == false ? 
                new BoundingBox(Vector3.Zero, new Vector3(a[0], b[1], c[2])) : 
                new BoundingBox(-0.5f * new Vector3(a[0], b[1], c[2]), 0.5f * new Vector3(a[0], b[1], c[2]));

            SimulationBox = box;
            ClampDesiredToCurrentBox();
            (this as IVariableProvider).ResetVariableDesiredValues();
        }

        /// <summary> Given list of positions, updates bounding box towards desired size. </summary>
        public void UpdateBoundingBox(IAtomicSystem system)
        {
            float desiredVolume = MathUtilities.CalculateVolume(DesiredSimulationBox);
            float currentVolume = MathUtilities.CalculateVolume(SimulationBox);

            if (DesiredSimulationBox == SimulationBox)
                return;

            if (ActiveResizeMode == ResizeMode.RescalePositions)
            {
                float originalVolume = MathUtilities.CalculateVolume(SimulationBox);
                SimulationBox = DesiredSimulationBox;
                float lengthScale = (float)Math.Pow(desiredVolume / originalVolume, 1.0 / 3.0);
                float length = (float)Math.Pow(desiredVolume, 1.0 / 3.0);
                system.ScaleMoleculeCenters(lengthScale);
            }
            else
            {
                if (desiredVolume < currentVolume)
                {
                    BoundingBox positionBox = ComputeBoundingSimulationBox(system.Particles.Position);
                    SimulationBox = new BoundingBox(Vector3.Min(DesiredSimulationBox.Minimum, positionBox.Minimum), Vector3.Max(DesiredSimulationBox.Maximum, positionBox.Maximum));
                    Resizing = true;
                }

                if (desiredVolume > currentVolume || (Resizing && desiredVolume == currentVolume))
                {
                    SimulationBox = DesiredSimulationBox;
                    Resizing = false;
                    if (PeriodicBoundary)
                        ComputeBoxLengths();
                }
            }
        }

        #endregion Public Methods

        public bool IsCube()
        {
            if (boxLengths[0] == boxLengths[1] && boxLengths[1] == boxLengths[2])
            {
                return true;
            }
            return false;
        }

        #region Private Fields

        private bool iVarsInitialised;
        private IVariable<bool> PeriodicBoundaryIVal;
        private Vector3 boxLengths;
        private Vector3 boxLengthsRecip;

        private IReporter reporter;
        private IVariable<BoundingBox> SimulationBoxIVar;
        //private IVariable<float> SimulationBoxVolumeIVar;

        /// <summary>
        /// The periodic box vectors, referred to as a, b, and c.
        /// </summary>
        /// <remarks>
        /// There are certain requirements on the vectors a, b and c.
        /// For most cases a, b and c will be orthogonal and will refer to the lengths of the box
        /// in the x, y and z axis.
        /// However, the method can handle orthorombic boxes, as long as a is parallel to x-axis,
        /// b is on x and y axis, and c is on z.
        /// </remarks>
        private readonly Vector3[] periodicBoxVectors = new Vector3[3];

        #endregion Private Fields

        #region Private Methods

        /// <summary>
        /// Computes and updates the simulation bounding box from the given list of positions
        /// </summary>
        /// <param name="positions"></param>
        /// <returns></returns>
        public static BoundingBox ComputeBoundingSimulationBox(List<Vector3> positions)
        {
            Vector3 max = new Vector3(float.MinValue);
            Vector3 min = new Vector3(float.MaxValue);
            foreach (Vector3 position in positions)
            {
                if (position.X > max.X)
                    max.X = (position.X);
                if (position.Y > max.Y)
                    max.Y = (position.Y);
                if (position.Z > max.Z)
                    max.Z = (position.Z);
                if (position.X < min.X)
                    min.X = (position.X);
                if (position.Y < min.Y)
                    min.Y = (position.Y);
                if (position.Z < min.Z)
                    min.Z = (position.Z);
            }
            return new BoundingBox(min, max);
        }

        private void ComputeBoxLengths()
        {
            boxLengths = SimulationBox.Maximum - SimulationBox.Minimum;
            boxLengthsRecip = new Vector3(1.0f / boxLengths.X, 1.0f / boxLengths.Y, 1.0f / boxLengths.Z);
        }

        /// <summary>
        /// Sets the periodic box vectors.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        public void SetPeriodicBoxVectors(Vector3 a, Vector3 b, Vector3 c)
        {
            periodicBoxVectors[0] = a;
            periodicBoxVectors[1] = b;
            periodicBoxVectors[2] = c;
        }

        /// <summary>
        /// Gets the periodic box vectors.
        /// </summary>
        /// <returns>Vector3[].</returns>
        public Vector3[] GetPeriodicBoxVectors()
        {
            return periodicBoxVectors;
        }

        /// <summary>
        /// Gets the periodic difference of a given vector.
        /// </summary>
        /// <param name="diff">The difference.</param>
        public void GetPeriodicDifference(ref Vector3 diff)
        {
            //Based from wikipedia:
            //https://en.wikipedia.org/wiki/Periodic_boundary_conditions#Practical_implementation:_continuity_and_the_minimum_image_convention

            for (int i = 0; i < 3; i++)
            {
                //Funky math that either subtracts box length from difference or does nothing.
                //TODO generalise to orthorombic.
                int cell = (int)Math.Floor((boxLengthsRecip[i] * diff[i]) + 0.5f);
                float d = boxLengths[i] * cell;
                diff[i] -= d;
            }
        }

        #endregion Private Methods

        void IVariableProvider.InitialiseVariables(VariableManager variableManager)
        {
            SimulationBoxIVar = variableManager.Variables.Add(VariableName.SimBoundingBox, VariableType.BoundingBox, !IsVariableBox) as IVariable<BoundingBox>;
            PeriodicBoundaryIVal = variableManager.Variables.Add(VariableName.PeriodicBox, VariableType.Bool, true) as IVariable<bool>;
            (this as IVariableProvider).ResetVariableDesiredValues();
            iVarsInitialised = true;
        }

        /// <summary>
        /// Sets the desired box.
        /// </summary>
        /// <param name="newBox">The new box.</param>
        /// <exception cref="Exception">Attempt to set a desired box size for a fixed box.</exception>
        public void SetDesiredBox(BoundingBox newBox)
        {
            if (IsVariableBox == false)
                throw new Exception("Attempt to set a desired box size for a fixed box.");
            float volume = MathUtilities.CalculateVolume(newBox);
            if (volume > MinimumBoxVolume)
            {
                DesiredSimulationBox = newBox;
            }
            else
            {
                BoundingBox minBox = MathUtilities.CreateCube(MinimumBoxVolume);
                DesiredSimulationBox = minBox;
                if (SimulationBoxIVar != null) SimulationBoxIVar.DesiredValue = minBox;
            }
        }

        void IVariableProvider.UpdateVariableValues()
        {
            if (!VariablesInitialised) return;

            if (SimulationBoxIVar.Readonly == false)
            {
                if (DesiredSimulationBox != SimulationBoxIVar.DesiredValue)
                {
                    SetDesiredBox(SimulationBoxIVar.DesiredValue);
                }
            }
            else
            {
                ClampDesiredToCurrentBox();
                SimulationBoxIVar.DesiredValue = SimulationBox;
            }
            if (SimulationBoxIVar.Value != SimulationBox)
                SimulationBoxIVar.Value = SimulationBox;
        }

        public ISimulationBoundary Clone()
        {
            SimulationBoundary simbound = new SimulationBoundary()
            {
                PeriodicBoundary = PeriodicBoundary,
                MinimumBoxVolume = MinimumBoxVolume,
                DesiredSimulationBox = DesiredSimulationBox,
                Resizing = Resizing,
                SimulationBox = SimulationBox,
                IsVariableBox = IsVariableBox
            };
            simbound.PeriodicBoundaryIVal = PeriodicBoundaryIVal;
            simbound.SimulationBoxIVar = SimulationBoxIVar;
            simbound.boxLengths = boxLengths;
            for (int i = 0; i < 3; i++)
            {
                simbound.periodicBoxVectors[i] = periodicBoxVectors[i];
            }
            return simbound;
        }

        void IDisposable.Dispose()
        {
        }

        void IVariableProvider.ResetVariableDesiredValues()
        {
            if (iVarsInitialised == false)
                return;
            SimulationBoxIVar.Value = SimulationBox;
            SimulationBoxIVar.DesiredValue = SimulationBox;

            PeriodicBoundaryIVal.Value = PeriodicBoundary;
            PeriodicBoundaryIVal.DesiredValue = PeriodicBoundary;
        }

        public bool Equals(ISimulationBoundary otherBoundary)
        {
            var other = (SimulationBoundary)otherBoundary;
            if (other == null) return false;
            if (other.boxLengths != boxLengths) return false;
            if (other.boxLengthsRecip != boxLengthsRecip) return false;
            if (other.DesiredSimulationBox != DesiredSimulationBox) return false;
            if (other.IsVariableBox != IsVariableBox) return false;
            if (other.VariablesInitialised != VariablesInitialised) return false;
            if (other.MinimumBoxVolume != MinimumBoxVolume) return false;
            if (other.PeriodicBoundary != PeriodicBoundary) return false;
            for (int i = 0; i < 3; i++)
            {
                if (other.periodicBoxVectors[i] != periodicBoxVectors[i]) return false;
            }
            if (other.Resizing != Resizing) return false;
            if (other.SimulationBox != SimulationBox) return false;
            return true;
        }
    }
}