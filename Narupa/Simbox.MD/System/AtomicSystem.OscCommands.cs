﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using Rug.Osc;

namespace Simbox.MD
{
    public partial class AtomicSystem
    {
        private TransportContext transportContext;

        [OscCommand(Help = "Initialises Velocities.")]
        public void InitialiseVelocitiesCommand(OscMessage message)
        {
            InitializeVelocities();
            transportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, new[] {message});
        }
    }
}