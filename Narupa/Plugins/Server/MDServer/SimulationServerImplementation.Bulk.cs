// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano;
using Nano.Commands;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using Nano.Server;
using Nano.Server.Basic;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using Nano.Transport.Streams;
using Nano.Transport.Streams.SpecialStreams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Rug.Osc;
using Simbox.MD;
using SlimMath;
using Interaction = Nano.Transport.Variables.Interaction.Interaction;

namespace MDServer
{
    public partial class SimulationServerImplementation
    {
        
        [OscCommand(Help = "Initialises a new simulation from the specified XML string", TypeTagString = "s")]
        public void SendSimulation(OscMessage message)
        {
            IReporter reporter = ServerContext.ReportManager.BeginReport(ReportContext.Load, "load-command", out Uri logIdentifier);

            XmlDocument simulationXmlDoc = new XmlDocument();

            try
            {
                reporter.PrintEmphasized($"Load simulation via OSC from {message.Origin}");

                //TODO @review @mike This method is messy.
                OscCommandAttribute attribute = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>();

                if (message[0] is string == false)
                {
                    OscCommandHelper.SendTypeTagString(TransportContext, message.Origin, attribute);
                    TransportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, new OscMessage("/error", "Invalid XML string for simulation."));

                    return;
                }

                string simulationXml = (string)message[0];

                try
                {
                    reporter.PrintDebug(simulationXml);

                    simulationXmlDoc.LoadXml(simulationXml);
                }
                catch
                {
                    OscCommandHelper.SendTypeTagString(TransportContext, message.Origin, attribute);

                    TransportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, new OscMessage("/error", "Invalid XML string for simulation."));

                    return;
                }

                TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(message.Address, "New simulation received, reconnect signal incoming"));
            }
            catch
            {
                ServerContext.ReportManager.EndReport(logIdentifier);

                throw;
            }
            finally
            {
                // Hmm?!
            }

            SimulationLoadRequest newLoadRequest = new SimulationLoadRequest(
                ServerContext,
                SimulationLoadRequest.SimulationLoadType.Command,
                new Uri($"osc://{message.Origin}", UriKind.RelativeOrAbsolute),
                logIdentifier,
                simulationXmlDoc,
                reporter);

            SimulationLoadRequest oldSLoadRequest = Interlocked.Exchange(ref simulationLoadRequest, newLoadRequest);

            if (oldSLoadRequest != null && oldSLoadRequest.Completed == false)
            {
                oldSLoadRequest.CompleteLoad(false);
            }
        }

        internal void PopulateAtomPositions()
        {
            if (sim == null)
            {
                return;
            }

            if (!(sim.ActiveSystem is AtomicSystemBase atomicSystem))
            {
                throw new Exception("Cannot populate atom positions, the simulation does not implement AtomicSystem!");
            }

            atomicSystem.PopulatePositionStream(atomPositions);

            (atomPositions as DataStream_CompressVector3ToHalf3)?.FlushToDestinationStream();

            OutgoingStreams.Flush(atomPositions.ID);
        }

        internal void PopulateAtomTypes()
        {
            if (sim == null)
            {
                return;
            }

            if (!(sim.ActiveSystem is AtomicSystemBase atomicSystem))
            {
                throw new Exception("Cannot populate atom types, the simulation does not implement AtomicSystem!");
            }

            atomicSystem.PopulateElementStream(atomTypes);

            OutgoingStreams.Flush(atomTypes.ID);
        }

        internal void PopulateAtomVelocities()
        {
            if (sim == null)
            {
                return;
            }

            if (atomVelocities == null)
            {
                return;
            }

            if (!(sim.ActiveSystem is AtomicSystemBase atomicSystem))
            {
                throw new Exception("Cannot populate atom velocities, the simulation does not implement AtomicSystem!");
            }

            atomicSystem.PopulateVelocityStream(atomVelocities);

            (atomVelocities as DataStream_CompressVector3ToHalf3)?.FlushToDestinationStream();

            OutgoingStreams.Flush(atomVelocities.ID);
        }

        internal void PopulateBonds(bool firstFrame)
        {
            if (sim == null)
            {
                return;
            }

            if (sim.ActiveTopology.DynamicBonds == null && firstFrame == false)
            {
                return;
            }

            if (!(sim.ActiveSystem is AtomicSystemBase atomicSystem))
            {
                throw new Exception("Cannot populate bonds, the simulation does not implement AtomicSystem!");
            }

            atomicSystem.PopulateBondStream(bonds);

            OutgoingStreams.Flush(bonds.ID);
        }

        internal void PopulateForces()
        {
            if (sim == null)
            {
                return;
            }

            if (forces == null)
            {
                return;
            }

            for (int i = 0; i < sim.ActiveSystem.NumberOfParticles; i++)
            {
                forces.Data[i] = sim.ActiveSystem.Particles.Force[i];
            }

            forces.Count = forces.MaxCount;

            (forces as DataStream_CompressVector3ToHalf3)?.FlushToDestinationStream();

            OutgoingStreams.Flush(forces.ID);
        }

        internal void PopulateSelectedAtoms()
        {
            if (selectedAtoms == null)
                return;

            int numberOfSelectedAtoms = 0;
            var numberOfActiveInteractions = 0;
            for (var j = 0; j < sim.ActiveSystem.Interactions.Count; j++)
            {
                var interaction = sim.ActiveSystem.Interactions[j];
                if (interaction.SelectedAtoms.Count == 0) continue;

                foreach (var atomIndex in interaction.SelectedAtoms)
                    if (atomIndex != -1)
                    {
                        //TODO loss of precision.
                        selectedAtoms.Data[numberOfSelectedAtoms] =
                            (ushort)sim.ActiveSystem.AtomSelections.VisibleAtoms.IndexOf(atomIndex);
                        numberOfSelectedAtoms++;
                    }
                //Populate the more detailed information about interactions.
                var interactionInfo = new InteractionForceInfo
                {
                    Force = interaction.NetForce,
                    InputID = (byte)interaction.InputId,
                    PlayerID = (byte)interaction.PlayerId,
                    Position = interaction.Position,
                    SelectedAtomsCentre = interaction.SelectedAtomsCentreOfMass
                };
                interactionForceOutput.Data[numberOfActiveInteractions] = interactionInfo;
                numberOfActiveInteractions++;
            }

            selectedAtoms.Count = numberOfSelectedAtoms;
            OutgoingStreams.Flush(selectedAtoms.ID);

            interactionForceOutput.Count = numberOfActiveInteractions;
            OutgoingStreams.Flush(interactionForceOutput.ID);
        }

        internal void PopulateSimulationTime()
        {
            if (simulationFrame == null)
            {
                return;
            }

            simulationFrame.Count = 1;
            simulationFrame.Data[0] = totalSteps;

            OutgoingStreams.Flush(simulationFrame.ID);
        }

        internal void UpdateVariables()
        {
            if (variableProviders == null)
            {
                return;
            }
            foreach (IVariableProvider provider in variableProviders)
            {
                provider.UpdateVariableValues();
            }

            VariableManager.Flush();
        }

        private void BroadcastPaused(string v)
        {
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(SimboxCommands.GetAddress(SimboxCommand.Pause), v));
        }

        private void BroadcastReconnectCommand(string message)
        {
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(SimboxCommands.GetAddress(SimboxCommand.Reconnect), message));
        }

        private string CleanUrl(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9 ]", "");
        }

       

        private void InitialiseVariables()
        {
            VariableManager.Variables.Clear();
            variableProviders = new List<IVariableProvider> { sim };

            foreach (IVariableProvider provider in variableProviders)
            {
                provider.InitialiseVariables(VariableManager);
            }
        }

        private void LoadNewSimulation(SimulationLoadRequest simulationLoadRequest)
        {
            try
            {
                Restart();

                if (sim != null)
                {
                    OscCommandManager.DetachCommands(oscCommandContext, sim);
                    sim.Dispose();
                }

                VariableManager.Variables.Clear();

                LoadSimulation(simulationLoadRequest);

                State = initialPlayState;

                InitialiseStreams();

                //StartStreamWritePump();
                TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(SimboxCommands.GetAddress(SimboxCommand.SendSimulation), "New simulation started!"));

                //Send a message back to clients to force a reconnect.
                BroadcastReconnectCommand("New simulation");

                simulationLoadRequest.CompleteLoad(true);
            }
            catch (Exception ex)
            {
                simulationLoadRequest.Reporter.PrintException(ex, "Exception thrown while loading simulation");

                simulationLoadRequest.CompleteLoad(false);
                //throw;
            }
        }

        private void LoadSimulation(SimulationLoadRequest simulationLoadRequest)
        {
            LoadContext context = new LoadContext(simulationLoadRequest.Reporter);

            XmlNode simulationNode = simulationLoadRequest.Xml.DocumentElement;

            try
            {
                sim = Loader.LoadRootObject<ISimulation>(context, simulationNode, new[] { typeof(IReporter) }, new object[] { Reporter });
            }
            catch (Exception e)
            {
                context.Reporter.PrintException(e, "Failed to initialise simulation.");

                throw new Exception("Exception thrown while initialising simulation.", e);
            }

            SubscribeToTopologyChange(sim);

            SubscribeToAtomFilterChange(sim);

            //TODO Get plumed simulation initialising variables.
            if (sim is Simulation)
            {
                InitialiseVariables();
            }

            interactions = new InteractionPool();

            OscCommandManager.LoadCommands(oscCommandContext, sim);
        }

        private void OnStreamUpdated(object sender, EventArgs e)
        {
        }

        private void PopulateInteractions()
        {
            interactions.Clear();

            foreach (var inputs in inputFields.Values)
                interactions.CopyLocationsFrom(inputs.Data, inputs.Count);

            foreach (var interactionStream in interactionStream)
            {
                var interaction = interactionStream.Value;
                // If client implements the client-side interactive atoms selection, then use them, otherwise just read them as they are.
                if (clientInteractionAtoms.ContainsKey(interactionStream.Key))
                    interactions.CopyInteractionsFrom(interaction.Data, interaction.Count,
                        clientInteractionAtoms[interactionStream.Key].Data,
                        clientInteractionAtoms[interactionStream.Key].Count);
                else
                    interactions.CopyInteractionsFrom(interaction.Data, interaction.Count);
            }

        }


        /// <summary>
        /// Reroutes incoming steamVR position streams to all subscribed clients.
        /// </summary>
        private void PopulateSteamVrPositions()
        {
            int j = 0;
            foreach (ServerSyncBuffer<VRInteraction> inputs in steamVrInteractions.Values)
            {
                for (int i = 0; i < inputs.Count; i++)
                {
                    steamVrOutputs.Data[j] = inputs.Data[i];
                    j++;
                }
            }
            steamVrOutputs.Count = j;
            OutgoingStreams.Flush(steamVrOutputs.ID);
        }

        private void PopulateVisibleAtomMapping()
        {
            if (sim == null)
            {
                return; 
            }

            var atomicSystem = sim.ActiveSystem as AtomicSystemBase;
            
            if (atomicSystem == null)
            {
                throw new Exception("Cannnot populate visible atom mappings, this simulation does not implement AtomicSystemBase");
            }

            atomicSystem.PopulateVisibleAtomStream(visibleAtomMap);

            switch (OnConnectionState)
            {
                case ServerOnConnectionState.Play:
                    State = ServerPlayState.Play;
                    break;

                case ServerOnConnectionState.Pause:
                    State = ServerPlayState.Pause;
                    break;

                case ServerOnConnectionState.RestartThenPlay:
                    Restart();
                    initialPlayState = ServerPlayState.Play;
                    break;

                case ServerOnConnectionState.RestartThenPause:
                    Restart();
                    initialPlayState = ServerPlayState.Pause;
                    break;

                default:
                    break;
            }

            //currentFPS = framesPerSecond;
            //currentVUPS = variableUpdatesPerSecond;
        }

        public bool FullSpeed { get; set; } = false; 

        protected override async Task StreamWritePump(CancellationToken cancel)
        {
            bool repopulateBonds = true;

            PopulateSimulationTime();

            PopulateInteractions();

            PopulateAtomTypes();
            UpdateTopology(null, null);
            PopulateAtomPositions();
            PopulateAtomVelocities();

            clock.Start();

            float accumulator = 0;

            float delta = clock.Update();

            currentVups = variableUpdatesPerSecond;
            currentFps = framesPerSecond;

            ServerPlayState previousState = State;

            while (shouldExitStream == false && cancel.IsCancellationRequested == false)
            {
                //Reporter?.PrintEmphasized("FRAME " + State + " " + NumberOfClientsConnected + " (" + totalSteps + ")");

                SimulationLoadRequest loadRequest = Interlocked.Exchange(ref simulationLoadRequest, null);

                if (loadRequest != null)
                {
                    LoadNewSimulation(loadRequest);

                    repopulateBonds = true;
                }

                if (NumberOfClientsConnected == 0)
                {
                    GoToSleepState();
                }

                ServerPlayState stateLastFrame = previousState;

                previousState = State;

                if (stateLastFrame == ServerPlayState.Sleep && State != ServerPlayState.Sleep)
                {
                    WakeFromSleepState();

                    TransportContext.Transmitter.Flush();

                    continue;
                }

                PopulateSteamVrPositions();
                
                switch (State)
                {
                    case ServerPlayState.Play:
                        if (stepsToRun > 0 && step > stepsToRun)
                        {
                            State = ServerPlayState.Pause;
                            BroadcastPaused("Play complete");
                        }
                        break;

                    case ServerPlayState.Step:
                        State = ServerPlayState.Pause;
                        break;

                    case ServerPlayState.Pause:
                        delta = await DoClockUpdate(cancel);

                        TransportContext.Transmitter.Flush();
                        continue;

                    case ServerPlayState.Sleep:
                        await Task.Delay(sleepTime, cancel);

                        TransportContext.Transmitter.Flush();
                        continue;

                    case ServerPlayState.Restart:
                        if (sim != null)
                        {
                            OscCommandManager.DetachCommands(oscCommandContext, sim);
                            sim.Reset();
                            OscCommandManager.LoadCommands(oscCommandContext, sim);
                            InitialiseVariables();
                            PopulateAtomPositions();
                            PopulateAtomVelocities();
                            SubscribeToTopologyChange(sim);
                            UpdateTopology(null, null);

                            foreach (IVariableProvider provider in variableProviders)
                            {
                                provider.ResetVariableDesiredValues();
                            }
                        }
                        State = initialPlayState;
                        break;

                    default:
                        break;
                }

                PopulateSimulationTime();
                PopulateInteractions();

                accumulator += delta;

                try
                {
                    sim?.Run(updateFrequency, interactions.Interactions);
                }
                catch (Exception e)
                {
                    Reporter?.PrintException(e, "Exception thrown, resetting!");

                    //TODO better management of simulation resetting.
                    Restart();
                }

                step += updateFrequency;
                totalSteps += updateFrequency;

                if (sim != null && sim.HasReset)
                {
                    PopulateAtomPositions();
                    PopulateAtomVelocities();
                    PopulateBonds(true);
                    SubscribeToTopologyChange(sim);
                }

                float timeTaken = clock.TimeSinceUpdate();

                //serverRecord.Details.ComputationalLoad = timeTaken / (1f / framesPerSecond);
                //Reporter.PrintNormal("ComputationalLoad: " + serverRecord.Details.ComputationalLoad);

                if (FullSpeed == false)
                {
                    delta = await DoClockUpdate(cancel);
                }

                float frameTimeStep = 1.0f / currentFps;

                if (accumulator > variableUpdateRate)
                {
                    accumulator %= variableUpdateRate;

                    UpdateVariables();
                }

                PopulateAtomPositions();
                (sim.ActiveSystem as AtomicSystemBase)?.HasTopologyChanged();
                PopulateBonds(repopulateBonds);
                repopulateBonds = false;
                PopulateAtomVelocities();
                PopulateSelectedAtoms();

                TransportContext.Transmitter.Flush();

                if (sim.EndOfSimulation == true)
                {
                    Reporter?.PrintNormal($"End of simulation.");

                    shouldExitStream = true;
                    
                    return; 
                }

                // send centroids here
            }
        }

        private void SubscribeToAtomFilterChange(ISimulation sim)
        {
            sim.ActiveSystem.AtomSelections.VisibleAtomsChanged += UpdateAtomFilters;
        }

        private void SubscribeToTopologyChange(ISimulation sim)
        {
            try
            {
                sim.ActiveSystem.TopologyChange += UpdateTopology;
            }
            catch (Exception e)
            {
                Reporter.PrintException(e, "Simulation does not implement topology change...");
            }
        }

        private void UpdateAtomFilters(object selectedAtoms, EventArgs e)
        {
            if (atomTypes == null)
            {
                return;
            }

            PopulateAtomTypes();
            PopulateVisibleAtomMapping();
            PopulateAtomPositions();
            PopulateAtomVelocities();
            PopulateBonds(true);
        }

        private void UpdateTopology(object system, EventArgs e)
        {
            PopulateVisibleAtomMapping();
            PopulateAtomTypes();
            PopulateBonds(true);
            topologyChangeStream.Data[0] = topologyChangeStream.Data[0] + 1;
            topologyChangeStream.Count = 1;
            OutgoingStreams.Flush(topologyChangeStream.ID);
        }

        private void WakeFromSleepState()
        {
            Reporter?.PrintEmphasized("New client(s) connected, waking up from sleep.");
        }
     
    }
}