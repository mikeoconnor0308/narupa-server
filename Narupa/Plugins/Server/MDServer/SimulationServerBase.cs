using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano;
using Nano.Science.Simulation.Instantiated;
using Nano.Server;
using Nano.Server.Basic;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Comms.Security;
using Nano.Transport.OSCCommands;
using Nano.Transport.Streams;
using Nano.Transport.Streams.SpecialStreams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Rug.Osc;
using SlimMath;
using AssemblyRoot = Nano.Server.AssemblyRoot;

namespace MDServer
{
    public enum ServerPlayState
    {
        Play,
        Pause,
        Sleep,
        Step,
        Restart
    }

    public class SimulationLoadRequest
    {
        public enum SimulationLoadType
        {
            Asset,
            Command
        }

        private readonly IServer serverContext;

        private readonly TaskCompletionSource<bool> taskCompletionSource = new TaskCompletionSource<bool>();

        public bool Completed { get; private set; }

        public SimulationLoadType LoadType { get; }

        public Uri Log { get; }

        public IReporter Reporter { get; }

        public Uri Source { get; }

        public XmlDocument Xml { get; }

        public SimulationLoadRequest(IServer serverContext, SimulationLoadType loadType, Uri source, Uri log, XmlDocument xml, IReporter reporter)
        {
            this.serverContext = serverContext ?? throw new ArgumentNullException(nameof(serverContext));

            LoadType = loadType;
            Source = source ?? throw new ArgumentNullException(nameof(source));
            Log = log ?? throw new ArgumentNullException(nameof(log));
            Xml = xml ?? throw new ArgumentNullException(nameof(xml));
            Reporter = reporter ?? throw new ArgumentNullException(nameof(reporter));
        }

        public void CompleteLoad(bool success)
        {
            Completed = true;

            if (success)
            {
                Reporter.PrintEmphasized($"Successfully loaded {Source}");
            }
            else
            {
                Reporter.PrintError($"Failed to load {Source}");
            }

            taskCompletionSource.SetResult(success);

            serverContext.ReportManager.EndReport(Log);
        }

        public async Task<bool> WaitForLoad()
        {
            return await taskCompletionSource.Task;
        }
    }
    
    
    public abstract class SimulationServerBase : ServerBase
    {
        public string Name { get; }

        public IServer ServerContext { get; }

        public StreamOptions StreamOptions { get; }

        private const int MaxInteractions = 256;
        public ServerOnConnectionState OnConnectionState = ServerOnConnectionState.Play;
        public ServerPlayState State = ServerPlayState.Play;
        //TODO refactor a lot of this out of base.
        private IDataStream<ushort> atomCollision;
        protected IDataStream<Vector3> atomPositions;
        protected IDataStream<ushort> atomTypes;
        protected IDataStream<Vector3> atomVelocities;
        protected IDataStream<BondPair> bonds;
        protected IDataStream<Vector3> forces;
        protected IDataStream<int> visibleAtomMap;
        protected IDataStream<int> topologyChangeStream;
        protected readonly Dictionary<IPEndPoint, ServerSyncBuffer<Vector3>> inputFields = new Dictionary<IPEndPoint, ServerSyncBuffer<Vector3>>();
        protected InteractionPool interactions = new InteractionPool();
        protected readonly Dictionary<IPEndPoint, ServerSyncBuffer<Interaction>> interactionStream = new Dictionary<IPEndPoint, ServerSyncBuffer<Interaction>>();

        /// <summary>
        /// Stream for client specifying sets of atoms to apply interaction forces to.
        /// </summary>
        protected Dictionary<IPEndPoint, ServerSyncBuffer<int>> clientInteractionAtoms = new Dictionary<IPEndPoint, ServerSyncBuffer<int>>();

        protected readonly Clock clock = new Clock();

        protected float currentFps;
        protected float currentVups;
        protected float framesPerSecond = 30;
        protected ServerPlayState initialPlayState = ServerPlayState.Play;

        protected OscCommandContext oscCommandContext;
        protected IDataStream<ushort> selectedAtoms;
        protected IDataStream<InteractionForceInfo> interactionForceOutput;

        protected bool shouldExitStream = true;

        protected IDataStream<long> simulationFrame;
        protected SimulationLoadRequest simulationLoadRequest;
        protected readonly int sleepTime = 500;

        /// <summary>
        /// Stream for steamvr headset synchronisation
        /// </summary>
        protected readonly Dictionary<IPEndPoint, ServerSyncBuffer<VRInteraction>> steamVrInteractions = new Dictionary<IPEndPoint, ServerSyncBuffer<VRInteraction>>();

        /// <summary>
        /// Stream for sending steamvr positions to all users.
        /// </summary>
        protected IDataStream<VRInteraction> steamVrOutputs;

        protected long step;
        protected int stepsToRun = -1;

        protected long totalSteps;
        protected int updateFrequency = 5;

        protected List<IVariableProvider> variableProviders;
        protected float variableUpdatesPerSecond = 10;

        protected float variableUpdateRate;
        protected float timeSpentSpinning;

        public event EventHandler ForcedReset;
        
        
        /// <inheritdoc />
        protected SimulationServerBase(
            IServer serverContext, 
            string name, 
            StreamOptions streamOptions, 
            ConnectionType connectionType, 
            int port, 
            ITransportSecurityProvider securityProvider, 
            IReporter reporter, 
            ITransportServer transportServer = null) 
            : base(connectionType, port, securityProvider, reporter, transportServer)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            StreamOptions = streamOptions ?? throw new ArgumentNullException(nameof(streamOptions));
            ServerContext = serverContext ?? throw new ArgumentNullException(nameof(serverContext));

            serverContext.SecurityProvider.TransportSecurityProvider.LoadSimulation += SecurityProvider_LoadSimulation;
            serverContext.SecurityProvider.TransportSecurityProvider.ReservedSessionEnded += SecurityProvider_ReservedSessionEnded;
            
            InitialiseCommands();

            InitialiseStreams();
        }
        
        protected void SecurityProvider_ReservedSessionEnded()
        {
            StopAllClientSessions();
        }

        protected void SecurityProvider_LoadSimulation(string simulationPath)
        {
            Task.Run(async () =>
            {
                try
                {
                    await EnqueueLoad(new Uri(simulationPath, UriKind.RelativeOrAbsolute));
                }
                catch (Exception ex)
                {
                    Reporter?.PrintException(ex, $"Exception while loading simulation {simulationPath}");
                }
            });
        }

        /// <inheritdoc />
        public override void DisposeInput(IPEndPoint endPoint)
        {
            ServerSyncBuffer<Vector3> buffer = inputFields[endPoint];

            inputFields.Remove(endPoint);
            buffer?.Detach(this);

            ServerSyncBuffer<Interaction> interaction = interactionStream[endPoint];
            interactionStream.Remove(endPoint);
            interaction?.Detach(this);

            //Detach steamVR buffer.
            ServerSyncBuffer<VRInteraction> steamVRBuffer = steamVrInteractions[endPoint];
            steamVrInteractions.Remove(endPoint);
            steamVRBuffer?.Detach(this);

            ServerSyncBuffer<int> clientSelectedAtoms = clientInteractionAtoms[endPoint];
            clientInteractionAtoms.Remove(endPoint);
            clientSelectedAtoms?.Detach(this);
        }
       
        public override void InitialDataDump(IPEndPoint endPoint)
        {
            Version serverVersion = Assembly.GetAssembly(typeof(AssemblyRoot)).GetName().Version;
            Version apiVersion = Assembly.GetAssembly(typeof(Nano.AssemblyRoot)).GetName().Version;

            TransportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, new OscMessage("/server/version", serverVersion.ToString()));
            TransportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, new OscMessage("/api/version", apiVersion.ToString()));

            TransportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, new OscMessage("/mdat/endMetadata"));
        }
        
        public override void InitialiseInput(IPEndPoint endPoint)
        {
            inputFields.Add(endPoint, new ServerSyncBuffer<Vector3>(endPoint, 1));
            inputFields[endPoint].Attach(this);

            interactionStream.Add(endPoint, new ServerSyncBuffer<Interaction>(endPoint, (ushort)StreamID.Interaction));
            interactionStream[endPoint].Attach(this);

            steamVrInteractions.Add(endPoint, new ServerSyncBuffer<VRInteraction>(endPoint, (ushort)StreamID.VRInteraction));
            steamVrInteractions[endPoint].Attach(this);

            clientInteractionAtoms.Add(endPoint, new ServerSyncBuffer<int>(endPoint, (ushort)StreamID.InteractionSelectedAtoms));
            clientInteractionAtoms[endPoint].Attach(this);
        }

        public abstract Task EnqueueLoad(Uri simulationPath, bool waitForComplete = true);


        public abstract Task LoadNow(Uri simulationPath);

        public abstract Task LoadNow(XmlDocument simulationXmlDoc, Uri simulationPath);

        public async Task StartAsync(CancellationToken cancel)
        {
            base.Start();
            shouldExitStream = false;
            await StreamWritePump(cancel);
        }

        protected abstract Task StreamWritePump(CancellationToken cancel);
        
        public override void Start()
        {
            throw new NotSupportedException("Use StartAsync");
        }

        /// <inheritdoc />
        protected override IncomingDataAgent CreateDataReader(IPEndPoint clientEndpoint)
        {
            return new ContextDataInputAgent(TransportContext, clientEndpoint);
        }

        /// <inheritdoc />
        protected override void Pause()
        {
            State = ServerPlayState.Pause;
        }

        /// <inheritdoc />
        protected override void Play(int steps = -1, int updateFrequency = 1)
        {
            State = ServerPlayState.Play;
            stepsToRun = steps;
            step = 0;

            if (updateFrequency != 0)
            {
                this.updateFrequency = updateFrequency;
            }
        }

        /// <inheritdoc />
        protected override void Restart()
        {
            State = ServerPlayState.Restart;
            totalSteps = 0;
            step = 0;
        }

        /// <inheritdoc />
        protected override void Step()
        {
            State = ServerPlayState.Step;
        }

        protected void InitialiseCommands()
        {
            oscCommandContext = new OscCommandContext(TransportContext);
            TransportContext.OscManager.Attach("//broadcast/*", BroadcastMessageToClients);
            OscCommandManager.LoadCommands(oscCommandContext, this);
        }

        protected void InitialiseStreams()
        {
            int MaxNumberOfParticles = StreamOptions.MaxNumberOfParticles;
            int MaxNumberOfBonds = 4 * MaxNumberOfParticles;

            if (atomPositions == null)
            {
                atomPositions = StreamOptions.CompressStreams ? new DataStream_CompressVector3ToHalf3(OutgoingStreams.DefineStream<Half3>((int)StreamID.AtomPositions, "Atom Positions", VariableType.Half, 3, MaxNumberOfParticles, TransportDataStreamTransmitMode.OnUpdate)) : OutgoingStreams.DefineStream<Vector3>((int)StreamID.AtomPositions, "Atom Positions", VariableType.Float, 3, MaxNumberOfParticles, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (atomTypes == null)
            {
                atomTypes = OutgoingStreams.DefineStream<ushort>((int)StreamID.AtomTypes, "Atom Types", VariableType.UInt16, 1, MaxNumberOfParticles, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (forces == null && StreamOptions.NoForces == false)
            {
                forces = StreamOptions.CompressStreams ? new DataStream_CompressVector3ToHalf3(OutgoingStreams.DefineStream<Half3>((int)StreamID.Forces, "Forces", VariableType.Half, 3, MaxNumberOfParticles, TransportDataStreamTransmitMode.OnUpdate)) : OutgoingStreams.DefineStream<Vector3>((int)StreamID.Forces, "Forces", VariableType.Float, 3, MaxNumberOfParticles, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (bonds == null)
            {
                bonds = OutgoingStreams.DefineStream<BondPair>((int)StreamID.Bonds, "Bonds", VariableType.UInt16, 2, 2 * MaxNumberOfBonds, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (selectedAtoms == null && StreamOptions.NoSelectedAtoms == false)
            {
                selectedAtoms = OutgoingStreams.DefineStream<ushort>((int)StreamID.SelectedAtoms, "Selected Atoms", VariableType.UInt16, 1, MaxInteractions, TransportDataStreamTransmitMode.OnUpdate);
                interactionForceOutput =
                    OutgoingStreams.DefineStream<InteractionForceInfo>((int)StreamID.InteractionForceInfo,
                        "Interaction Info", VariableType.Byte, Marshal.SizeOf(typeof(InteractionForceInfo)),
                        MaxInteractions, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (simulationFrame == null)
            {
                simulationFrame = OutgoingStreams.DefineStream<long>((int)StreamID.SimulationFrame, "Simulation Frame", VariableType.Int64, 1, 1, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (atomCollision == null && StreamOptions.NoCollisions == false)
            {
                atomCollision = OutgoingStreams.DefineStream<ushort>((int)StreamID.AtomCollision, "Atom Collision", VariableType.UInt16, 1, 32, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (atomVelocities == null && StreamOptions.NoVelocities == false)
            {
                atomVelocities = StreamOptions.CompressStreams ? new DataStream_CompressVector3ToHalf3(OutgoingStreams.DefineStream<Half3>((int)StreamID.AtomVelocities, "Atom Velocities", VariableType.Half, 3, MaxNumberOfParticles, TransportDataStreamTransmitMode.OnUpdate)) : OutgoingStreams.DefineStream<Vector3>((int)StreamID.AtomVelocities, "Atom Velocities", VariableType.Float, 3, MaxNumberOfParticles, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (steamVrOutputs == null && StreamOptions.VRServer)
            {
                const int maxNumberOfSteamVrParticipants = 8 * 4;

                steamVrOutputs = OutgoingStreams.DefineStream<VRInteraction>((int)StreamID.VRPositions, "Steam VR Positions", VariableType.Byte, Marshal.SizeOf(typeof(VRInteraction)), maxNumberOfSteamVrParticipants, TransportDataStreamTransmitMode.OnUpdate);
            }

            if (topologyChangeStream == null)
            {
                topologyChangeStream = OutgoingStreams.DefineStream<int>((int)StreamID.TopologyUpdate, "Topology Update Index", VariableType.Int32, 1, 1, TransportDataStreamTransmitMode.Continuous);
            }

            if (visibleAtomMap == null)
            {
                visibleAtomMap = OutgoingStreams.DefineStream<int>((int)StreamID.VisibleAtomMap, "Visible Atom Map", VariableType.Int32, 1, MaxNumberOfParticles, TransportDataStreamTransmitMode.Continuous);
            }
        }
        
        protected List<string> PollAvailableSimulations(string path, string searchPatterns, SearchOption searchOption=SearchOption.TopDirectoryOnly)
        {
            path = Helper.ResolvePath(path);
            var files = Directory.GetFiles(path, searchPatterns, searchOption);
            List<string> simulationFiles = new List<string>();
            foreach (var file in files)
            {
                simulationFiles.Add(Path.GetFileNameWithoutExtension(file));
            }
            return simulationFiles;
        }
       
        /// <summary>
        /// Initialises a new simulation from the specified URI on the server.
        /// </summary>
        /// <param name="message"></param>
        [OscCommand(Help = "Initialises a new simulation from the specified path on the server's file system", TypeTagString = "s")]
        public void SendSimulationPath(OscMessage message)
        {
            
            IReporter reporter = ServerContext.ReportManager.BeginReport(ReportContext.Load, "load-command", out Uri logIdentifier);
            
            //TODO @review @mike This method is messy.
            OscCommandAttribute attribute = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>();

            string simulationXml = string.Empty;
            XmlDocument simulationXmlDoc = new XmlDocument();

            string simulationPath;
            try
            {
                simulationPath = (string)message[0];
                Reporter?.PrintDebug("Received simulation string: {0}", simulationPath);
                simulationXmlDoc.Load(Helper.ResolvePath(simulationPath));
                simulationXml = simulationXmlDoc.OuterXml;
            }
            catch
            {
                OscCommandHelper.SendTypeTagString(TransportContext, message.Origin, attribute);
                OscMessage error = new OscMessage("/error", "Invalid path for simulation. ");
                TransportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, error);
                return;
            }

            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(message.Address, "New simulation received, reconnect signal incoming"));
            
            SimulationLoadRequest newLoadRequest = new SimulationLoadRequest(
                ServerContext,
                SimulationLoadRequest.SimulationLoadType.Command,
                new Uri($"osc://{message.Origin}", UriKind.RelativeOrAbsolute),
                logIdentifier,
                simulationXmlDoc,
                reporter);

            SimulationLoadRequest oldSLoadRequest = Interlocked.Exchange(ref simulationLoadRequest, newLoadRequest);

            if (oldSLoadRequest != null && oldSLoadRequest.Completed == false)
            {
                oldSLoadRequest.CompleteLoad(false);
            }
        }
        
        [OscCommand(ExampleArguments = "30", Help = "Sets the frame rate", TypeTagString = "i")]
        public void SetFps(OscMessage message)
        {
            OscCommandAttribute attribute = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>();
            int fps = 30;
            OscMessage reply;

            try
            {
                fps = (int)message[0];
            }
            catch
            {
                OscCommandHelper.SendTypeTagString(TransportContext, message.Origin, attribute);
                OscMessage error = new OscMessage("/error", "Invalid FPS. ");
                TransportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, error);
                return;
            }

            framesPerSecond = fps;
            reply = new OscMessage(attribute.Address);
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Important, reply);
            ;
        }

        [OscCommand(ExampleArguments = "0.5", Help = "Sets the scale of the simulation in multiplayer", TypeTagString = "f")]
        public void SetMultiplayerBoxScale(OscMessage message)
        {
            OscCommandAttribute attribute = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>();

            float scale = 1.0f;
            try
            {
                scale = (float)message[0];
                Reporter?.PrintDebug($"Received new box scale: {scale}");
            }
            catch
            {
                OscCommandHelper.SendTypeTagString(TransportContext, message.Origin, attribute);
                OscMessage error = new OscMessage("/error", "Invalid box scale received");
                TransportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, error);
                return;
            }

            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(message.Address, scale));
        }

        [OscCommand(ExampleArguments = "10", Help = "Sets the rate at which variables are updated", TypeTagString = "i")]
        public void SetVariableRate(OscMessage message)
        {
            OscCommandAttribute attribute = MethodBase.GetCurrentMethod().GetCustomAttribute<OscCommandAttribute>();
            int fps = 10;
            OscMessage reply;

            try
            {
                fps = (int)message[0];
            }
            catch
            {
                OscCommandHelper.SendTypeTagString(TransportContext, message.Origin, attribute);
                OscMessage error = new OscMessage("/error", "Invalid update rate. ");
                TransportContext.Transmitter.Transmit(TransportPacketPriority.Normal, message.Origin, error);
                return;
            }

            variableUpdatesPerSecond = fps;
            reply = new OscMessage(attribute.Address);
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Important, reply);
        }


        protected async Task<float> DoClockUpdate(CancellationToken cancel)
        {
            float frameTimeStep = (1.0f / currentFps);

            variableUpdateRate = 1.0f / currentVups;

            timeSpentSpinning = await clock.SpinTillAsync(frameTimeStep, cancel);

            //timeSpentSpinning = Clock.SpinTill(frameTimeStep);

            return clock.Update();
        }

        protected void GoToSleepState()
        {
            if (State == ServerPlayState.Sleep)
            {
                return;
            }

            Reporter?.PrintEmphasized("No clients connected, going to sleep.");
            State = ServerPlayState.Sleep;
            
            //currentFPS = 0.1f;
            //currentVUPS = 0;
        }
        
        protected void BroadcastMessageToClients(OscMessage message)
        {
            TransportContext?.Transmitter.Broadcast(TransportPacketPriority.Normal, message);
        }
    }
}