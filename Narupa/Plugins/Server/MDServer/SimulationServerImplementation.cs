// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using Nano.Server;
using Nano.Server.Basic;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Comms.Security;
using Nano.Transport.OSCCommands;
using Nano.Transport.Streams;
using Nano.Transport.Streams.SpecialStreams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Rug.Osc;
using SlimMath;
using AssemblyRoot = Simbox.MD.AssemblyRoot;

namespace MDServer
{
    public partial class SimulationServerImplementation : SimulationServerBase
    {
        private ISimulation sim;
        
        public SimulationServerImplementation(IServer serverContext, string name, StreamOptions streamOptions, ConnectionType connectionType, int port, ITransportSecurityProvider securityProvider, IReporter reporter, ITransportServer transportServer = null) 
            : base(serverContext, name, streamOptions, connectionType, port, securityProvider, reporter, transportServer)
        {
        }
       
        public override void InitialDataDump(IPEndPoint endPoint)
        {
            base.InitialDataDump(endPoint);
            
            //Transmit server metadata dump.
            if (sim == null)
            {
                TransportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, new OscMessage("/server/status", "There is currently no simulation running."));
                return;
            }

            TransportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, new OscMessage("/server/name", sim.Name));

            var simFiles = PollAvailableSimulations("^/Assets/Simulations/VR Demos");
            foreach (var sim in simFiles)
            {
                TransportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, new OscMessage("/server/demo", sim));
            }

            sim.ActiveTopology?.TransmitTopologyAddresses(endPoint, sim.ActiveSystem.AtomSelections);
            sim.ActiveTopology?.TransmitSelectionData(endPoint);

            TransportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, new OscMessage("/mdat/endMetadata"));
        }

        public override async Task EnqueueLoad(Uri simulationPath, bool waitForComplete = true)
        {
            IReporter reporter = ServerContext.ReportManager.BeginReport(ReportContext.Load, "load-simulation", out Uri logIdentifier);

            XmlDocument simulationXmlDoc = new XmlDocument();

            try
            {
                reporter.PrintEmphasized($"Load simulation {simulationPath}");

                using (Stream stream = await ServerContext.AssetManager.GetAsset(simulationPath, reporter))
                {
                    try
                    {
                        simulationXmlDoc.Load(stream);
                    }
                    catch (Exception ex)
                    {
                        reporter.PrintException(ex, $"Exception while parsing XML file {simulationPath}");

                        TransportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage("/error", $"Failed to load simulation {simulationPath}, see log file on server for details: {logIdentifier}"));
                        TransportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage("/load-error", simulationPath, logIdentifier));

                        ServerContext.ReportManager.EndReport(logIdentifier);

                        throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, $"Could not load {simulationPath}. See {logIdentifier.AbsolutePath} for more details.");
                    }
                }
            }
            finally
            {
                //ServerContext.ReportManager.EndReport(logIdentifier);
            }

            SimulationLoadRequest newLoadRequest = new SimulationLoadRequest(ServerContext, SimulationLoadRequest.SimulationLoadType.Asset, simulationPath, logIdentifier, simulationXmlDoc, reporter);

            SimulationLoadRequest oldLoadRequest = Interlocked.Exchange(ref simulationLoadRequest, newLoadRequest);

            if (oldLoadRequest != null && oldLoadRequest.Completed == false)
            {
                oldLoadRequest.CompleteLoad(false);
            }

            if (waitForComplete == true)
            {
                await newLoadRequest.WaitForLoad();
            }
        }

        public override async Task LoadNow(Uri simulationPath)
        {
            IReporter reporter = ServerContext.ReportManager.BeginReport(ReportContext.Load, "load-simulation", out Uri logIdentifier);

            XmlDocument simulationXmlDoc = new XmlDocument();

            try
            {
                reporter.PrintEmphasized($"Load simulation {simulationPath}");

                using (Stream stream = await ServerContext.AssetManager.GetAsset(simulationPath, reporter))
                {
                    try
                    {
                        simulationXmlDoc.Load(stream);
                    }
                    catch (Exception ex)
                    {
                        reporter.PrintException(ex, $"Exception while parsing XML file {simulationPath}");
                        
                        TransportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage("/error", $"Failed to load simulation {simulationPath}, see log file on server for details: {logIdentifier}"));
                        TransportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, new OscMessage("/load-error", simulationPath, logIdentifier));

                        ServerContext.ReportManager.EndReport(logIdentifier); 
                        
                        throw new SimboxException(SimboxExceptionType.InvalidSimulationInput, $"Could not load {simulationPath}. See {logIdentifier.AbsolutePath} for more details.");
                    }
                }
            }
            finally
            {
                //ServerContext.ReportManager.EndReport(logIdentifier); 
            }

            LoadSimulation(new SimulationLoadRequest(ServerContext, SimulationLoadRequest.SimulationLoadType.Asset, simulationPath, logIdentifier, simulationXmlDoc, reporter)); 
        }
        
        public override async Task LoadNow(XmlDocument simulationXmlDoc, Uri simulationPath)
        {
            IReporter reporter = ServerContext.ReportManager.BeginReport(ReportContext.Load, "load-simulation", out Uri logIdentifier);

            LoadSimulation(new SimulationLoadRequest(ServerContext, SimulationLoadRequest.SimulationLoadType.Asset, simulationPath, logIdentifier, simulationXmlDoc, reporter)); 
        }
        
        private List<string> PollAvailableSimulations(string path)
        {
            path = Helper.ResolvePath(path);
            var files = Directory.GetFiles(path, "*.xml", SearchOption.TopDirectoryOnly);
            List<string> simulationFiles = new List<string>();
            foreach (var file in files)
            {
                simulationFiles.Add(Path.GetFileNameWithoutExtension(file));
            }
            return simulationFiles;
        }
    }
}