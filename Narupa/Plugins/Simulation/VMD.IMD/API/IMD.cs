﻿//Implementation of the VMD IMD API, distributed under the following license: 

//University of Illinois Open Source License
//Copyright 2006 Theoretical and Computational Biophysics Group, 
//All rights reserved.
//
//Developed by:		Theoretical and Computational Biophysics Group
//University of Illinois at Urbana-Champaign
//http://www.ks.uiuc.edu/
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of
//this software and associated documentation files (the Software), to deal with 
//the Software without restriction, including without limitation the rights to 
//use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
//of the Software, and to permit persons to whom the Software is furnished to 
//do so, subject to the following conditions:
//
//Redistributions of source code must retain the above copyright notice, 
//this list of conditions and the following disclaimers.
//
//Redistributions in binary form must reproduce the above copyright notice, 
//this list of conditions and the following disclaimers in the documentation 
//and/or other materials provided with the distribution.
//
//Neither the names of Theoretical and Computational Biophysics Group, 
//University of Illinois at Urbana-Champaign, nor the names of its contributors 
//may be used to endorse or promote products derived from this Software without 
//specific prior written permission.
//
//THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
//THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//OTHER DEALINGS WITH THE SOFTWARE.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace IMDDotNet
{
    public enum ImdType
    {
        /// <summary>
        /// close IMD connection, leaving sim running
        /// </summary>
        ImdDisconnect,

        /// <summary>
        /// Energy data block.
        /// </summary>
        ImdEnergies,

        /// <summary>
        /// The atom coordinates.
        /// </summary>
        ImdFcoords,

        /// <summary>
        /// Start the simulation.
        /// </summary>
        ImdGo,

        /// <summary>
        /// Check endianism and version.
        /// </summary>
        ImdHandshake,

        /// <summary>
        /// Kill the simulation job, shutdown IMD.
        /// </summary>
        ImdKill,

        /// <summary>
        /// MDComm style force data.
        /// </summary>
        ImdMdcomm,

        /// <summary>
        /// Pause the running simulation.
        /// </summary>
        ImdPause,

        /// <summary>
        /// Set the IMD update transmission.
        /// </summary>
        ImdTrate,

        /// <summary>
        /// Indicate an I/O Error;
        /// </summary>
        ImdIoerror,

        /// <summary>
        /// Periodic box.
        /// </summary>
        ImdPeriodicBox
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ImdEnergies
    {
        /// <summary>
        /// Time step index.
        /// </summary>
        public int TStep;

        /// <summary>
        /// Temperature in degrees Kelvin.
        /// </summary>
        public float T;

        /// <summary>
        /// Total energy, in Kcal/mol.
        /// </summary>
        public float Etot;

        /// <summary>
        /// Potential energy, in Kcal/mol.
        /// </summary>
        public float Epot;

        /// <summary>
        /// Van der Waals energy, in Kcal/mol.
        /// </summary>
        public float Evdw;

        /// <summary>
        /// Electrostatic energy, in Kcal/mol.
        /// </summary>
        public float Eelec;

        /// <summary>
        /// Bond energy, in Kcal/mol.
        /// </summary>
        public float Ebond;

        /// <summary>
        /// Angle energy, in Kcal/mol.
        /// </summary>
        public float Eangle;

        /// <summary>
        /// Dihedral energy, in Kcal/mol.
        /// </summary>
        public float Edihe;

        /// <summary>
        /// Improper torsion energy, in Kcal/mol.
        /// </summary>
        public float Eimpr;

        /// <summary>
        /// Serialize this instance into an array of bytes.
        /// </summary>
        /// <returns></returns>
        public byte[] Serialize()
        {
            return Serialize(this);
        }

        /// <summary>
        /// Deserialize a byte array into an instance of <see cref="ImdEnergies"/>
        /// </summary>
        /// <param name="byteRepresentation"></param>
        public void Deserialize(byte[] byteRepresentation)
        {
            ImdEnergies energies = new ImdEnergies();
            int size = Marshal.SizeOf(energies);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(byteRepresentation, 0, ptr, size);

            energies = (ImdEnergies)Marshal.PtrToStructure(ptr, energies.GetType());
            Marshal.FreeHGlobal(ptr);
            this = energies;
        }

        /// <summary>
        /// Serialize an instance of <see cref="ImdEnergies"></see> into an array of bytes/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] Serialize(ImdEnergies obj)
        {
            int size = Marshal.SizeOf(obj);
            var byteRepresentation = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, byteRepresentation, 0, size);
            Marshal.FreeHGlobal(ptr);
            return byteRepresentation;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ImdHeader
    {
        /// <summary>
        /// Type of data to be sent/received.
        /// </summary>
        public int Type;

        /// <summary>
        /// Length of the data to be sent/received, in bytes.
        /// </summary>
        public int Length;

        /// <summary>
        /// Size of header. Should be equivalent to Marshal.SizeOf since.
        /// </summary>
        public static int HEADERSIZE = 8;

        /// <summary>
        /// Construct a header of a given <see cref="ImdType"/> and length in bytes./>
        /// </summary>
        /// <param name="type"></param>
        /// <param name="length"></param>
        public ImdHeader(ImdType type, int length)
        {
            Type = IPAddress.HostToNetworkOrder((int)type);
            Length = IPAddress.HostToNetworkOrder(length);
        }

        /// <summary>
        /// Deserialize a byte array into an instance of <see cref="ImdHeader"/>
        /// </summary>
        /// <param name="byteRepresentation"></param>
        public void Deserialize(byte[] byteRepresentation)
        {
            ImdHeader header = new ImdHeader();
            int size = Marshal.SizeOf(header);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(byteRepresentation, 0, ptr, size);

            header = (ImdHeader)Marshal.PtrToStructure(ptr, header.GetType());
            Marshal.FreeHGlobal(ptr);
            Length = header.Length;
            Type = header.Type;
        }

        /// <summary>
        /// Serialize this instance into a byte array.
        /// </summary>
        /// <returns>A byte array containing this instance.</returns>
        public byte[] Serialize()
        {
            return Serialize(this);
        }

        /// <summary>
        /// Serialize a given <see cref="ImdHeader"/> into a byte array.
        /// </summary>
        /// <param name="obj">The instance to be serialized.</param>
        /// <returns>A byte array containing this instance.</returns>
        public static byte[] Serialize(ImdHeader obj)
        {
            int size = Marshal.SizeOf(obj);
            var byteRepresentation = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, byteRepresentation, 0, size);
            Marshal.FreeHGlobal(ptr);
            return byteRepresentation;
        }

        /// <summary>
        /// Swap the order of a header.
        /// </summary>
        public void SwapHeader()
        {
            Type = IPAddress.NetworkToHostOrder(Type);
            Length = IPAddress.NetworkToHostOrder(Length);
        }

        /// <summary>
        /// Transmit a header.
        /// </summary>
        /// <param name="s"></param>
        public void SendHeader(Socket s)
        {
            byte[] bytes = Serialize();
            s.Send(bytes, HEADERSIZE, SocketFlags.None);
        }
    }

    /// <summary>
    /// Implementation of the VMD IMD API.
    /// </summary>
    public class Imd : IDisposable
    {
        /// <summary>
        /// Version of the VMD IMD API this class implements.
        /// </summary>
        public static int IMDVERSION = 2;

        /// <summary>
        /// Time for socket timeouts.
        /// </summary>
        public int TimeoutTime = 5;

        private readonly Socket socket;

        //TODO Implement endian swaps.
        private bool needToSwapEndian = false;

        /// <summary>
        /// Send handshake.
        /// </summary>
        public void Handshake()
        {
            ImdHeader header = new ImdHeader(ImdType.ImdHandshake, 1);
            header.Length = IMDVERSION;
            header.SendHeader(socket);
        }

        /// <summary>
        /// Send T rate.
        /// </summary>
        /// <param name="rate"></param>
        public void TRate(int rate)
        {
            ImdHeader header = new ImdHeader(ImdType.ImdTrate, rate);
            header.SendHeader(socket);
        }

        /// <summary>
        /// Send disconnect signal.
        /// </summary>
        /// <returns>True if error occurs, false otherwise.</returns>
        public bool SendDisconnect()
        {
            ImdHeader header = new ImdHeader(ImdType.ImdDisconnect, 0);
            return (socket.Send(header.Serialize()) != ImdHeader.HEADERSIZE);
        }

        /// <summary>
        /// Send pause signal.
        /// </summary>
        /// <returns>True if error occurs, false otherwise.</returns>
        public bool SendPause()
        {
            ImdHeader header = new ImdHeader(ImdType.ImdPause, 0);
            return (socket.Send(header.Serialize()) != ImdHeader.HEADERSIZE);
        }

        /// <summary>
        /// Send kill signal.
        /// </summary>
        /// <returns>True if error occurs, false otherwise.</returns>
        public bool SendKill()
        {
            ImdHeader header = new ImdHeader(ImdType.ImdKill, 0);
            return (socket.Send(header.Serialize()) != ImdHeader.HEADERSIZE);
        }

        /// <summary>
        /// Send go signal.
        /// </summary>
        /// <returns>True if error occurs, false otherwise.</returns>
        public bool SendGo()
        {
            ImdHeader header = new ImdHeader(ImdType.ImdGo, 0);
            return (socket.Send(header.Serialize()) != ImdHeader.HEADERSIZE);
        }

        /// <summary>
        /// Send MD communication: atom indices and the forces to be applied.
        /// </summary>
        /// <param name="nAtoms"> Number of atoms to apply forces to.</param>
        /// <param name="indices"> Indices of atoms to apply force to.</param>
        /// <param name="forces"> Forces to be applied to atoms.</param>
        /// <returns>True if error occurs, false otherwise.</returns>
        public bool SendMDCommunication(int nAtoms, int[] indices, float[] forces)
        {
            int size = ImdHeader.HEADERSIZE + (nAtoms * sizeof(int)) + (nAtoms * 3 * sizeof(float));
            byte[] buf = new byte[size];

            //Construct a header detailing MDComm and number of atoms.
            byte[] header = (new ImdHeader(ImdType.ImdMdcomm, nAtoms)).Serialize();
            //Copy all the data into the buffer
            Buffer.BlockCopy(header, 0, buf, 0, header.Length);
            Buffer.BlockCopy(indices, 0, buf, ImdHeader.HEADERSIZE, nAtoms * sizeof(int));
            Buffer.BlockCopy(forces, 0, buf, ImdHeader.HEADERSIZE + nAtoms * sizeof(int), nAtoms * 3 * sizeof(float));
            //Return true if failed to send all the bits.
            return socket.Send(buf) != size;
        }

        /// <summary>
        /// Sends an instance of <see cref="ImdEnergies"/> to the machine.
        /// </summary>
        /// <param name="energies"></param>
        /// <returns></returns>
        public bool SendEnergies(ImdEnergies energies)
        {
            int size = ImdHeader.HEADERSIZE + Marshal.SizeOf(energies);
            byte[] buf = new byte[size];

            byte[] header = (new ImdHeader(ImdType.ImdEnergies, 1)).Serialize();

            byte[] energyBuf = energies.Serialize();
            //Get to the data! (Arnie)
            Buffer.BlockCopy(header, 0, buf, 0, header.Length);
            Buffer.BlockCopy(energyBuf, 0, buf, ImdHeader.HEADERSIZE, energyBuf.Length);
            return socket.Send(buf) != size;
        }

        /// <summary>
        /// Sends a specified number of atomic coordinates.
        /// </summary>
        /// <param name="nAtoms">Number of atoms</param>
        /// <param name="coords">Coordinate array, of length 3 * nAtoms.</param>
        /// <returns></returns>
        public bool SendFCoords(int nAtoms, float[] coords)
        {
            int size = ImdHeader.HEADERSIZE + (nAtoms * 3 * sizeof(float));
            byte[] buf = new byte[size];

            //Construct a header detailing MDComm and number of atoms.
            byte[] header = (new ImdHeader(ImdType.ImdFcoords, nAtoms)).Serialize();
            //Copy all the data into the buffer
            Buffer.BlockCopy(header, 0, buf, 0, header.Length);
            Buffer.BlockCopy(coords, 0, buf, ImdHeader.HEADERSIZE, nAtoms * 3 * sizeof(float));
            //Return true if failed to send all the bits.
            return socket.Send(buf) != size;
        }

        /// <summary>
        /// Sends the periodic box.
        /// </summary>
        /// <returns><c>true</c>, if periodic box was sent, <c>false</c> otherwise.</returns>
        /// <param name="dim">Dimensionality of the box.</param>
        /// <param name="periodicBox">Periodic box.</param>
        public bool SendPeriodicBox(int dim, float[] periodicBox)
        {
            int size = ImdHeader.HEADERSIZE + (dim * dim * sizeof(float));
            byte[] buf = new byte[size];

            byte[] header = (new ImdHeader(ImdType.ImdPeriodicBox, dim)).Serialize();
            Buffer.BlockCopy(header, 0, buf, 0 , header.Length);
            Buffer.BlockCopy(periodicBox, 0, buf, ImdHeader.HEADERSIZE, dim * dim * sizeof(float));
            return socket.Send(buf) != size;
        }
        /// <summary>
        /// Receives the header.
        /// </summary>
        /// <returns>The header.</returns>
        /// <param name="length">Length.</param>
        /// <param name="swapLength">Whether to swap the header order.</param>
        public ImdType ReceiveHeader(ref int length, bool swapLength)
        {
            byte[] bytesReceived = new byte[ImdHeader.HEADERSIZE];

            var recv = socket.Receive(bytesReceived, ImdHeader.HEADERSIZE, SocketFlags.None);
            if (recv != ImdHeader.HEADERSIZE) return ImdType.ImdIoerror;

            ImdHeader header = new ImdHeader();
            header.Deserialize(bytesReceived);
            length = header.Length;
            header.SwapHeader();
            if (swapLength)
                length = header.Length;

            return (ImdType)header.Type;
        }

        /// <summary>
        /// Receive handshake.
        /// </summary>
        /// <returns> Returns 0 if successfully received handshake and its the same endianness as us, 1 if it's not the same endianness, and -1 if a failure occurs.</returns>
        public int TryReceiveHandshake()
        {
            needToSwapEndian = false;
            socket.ReceiveTimeout = TimeoutTime;
            int buf = 0;
            ImdType type = ReceiveHeader(ref buf, false);
            if (type != ImdType.ImdHandshake) throw new Exception($"Did not receive handshake, received {type}");

            //Check endianness.
            if (buf == IMDVERSION)
            {
                var error = SendGo();
                if (error) return -1;
                else return 0;
            }
            //Try swapping the bytes.
            var reverseEndian = BitConverter.ToInt32(BitConverter.GetBytes(IMDVERSION).Reverse().ToArray(), 0);
            if (reverseEndian == IMDVERSION)
            {
                var error = SendGo();
                if (error) return -1;
                //return 1 to indicate endian swap.
                else
                {
                    needToSwapEndian = true;
                    return 1;
                }
            }
            //Failed.
            return -1;
        }

        /// <summary>
        /// Receive MD Communication, in the form of atom indices and forces.
        /// </summary>
        /// <param name="numAtoms">Number of atoms to receive indices and forces for.</param>
        /// <param name="indices">Indices of atoms.</param>
        /// <param name="forces">Forces to apply to atoms.</param>
        /// <returns>True if an error occurs, false otherwise.</returns>
        public bool TryReceiveMdCommunication(int numAtoms, int[] indices, float[] forces)
        {
            int indicesLengthInBytes = numAtoms * 4;
            byte[] indicesBytes = new byte[indicesLengthInBytes];
            if (socket.Receive(indicesBytes, 0, indicesLengthInBytes, SocketFlags.None) != indicesLengthInBytes) return true;

            int forcesLengthInBytes = numAtoms * 12;
            byte[] forcesBytes = new byte[forcesLengthInBytes];
            if (socket.Receive(forcesBytes, 0, numAtoms * 12, SocketFlags.None) != forcesLengthInBytes) return true;
            //Convert the data.
            Buffer.BlockCopy(indicesBytes, 0, indices, 0, indicesBytes.Length);
            Buffer.BlockCopy(forcesBytes, 0, forces, 0, forcesBytes.Length);

            return false;
        }

        /// <summary>
        /// Receive and fill <see cref="ImdEnergies"/>.
        /// </summary>
        /// <param name="energies"></param>
        /// <returns>True if an error occurs, false otherwise.</returns>
        public bool TryReceiveEnergies(ref ImdEnergies energies)
        {
            int size = Marshal.SizeOf(typeof(ImdEnergies));
            byte[] buf = new byte[size];
            bool error = (socket.Receive(buf, 0, size, SocketFlags.None) != size);
            if (error) return true;
            energies.Deserialize(buf);
            return false;
        }

        /// <summary>
        /// Receive and fill passed array with coordinates for the given number of atoms.
        /// </summary>
        /// <param name="numAtoms"></param>
        /// <param name="coords"></param>
        /// <returns></returns>
        public bool TryReceiveFCoords(int numAtoms, float[] coords)
        {
            int size = numAtoms * 3 * 4;
            byte[] buf = new byte[size];
            bool error = (socket.Receive(buf, 0, size, SocketFlags.None) != size);
            if (error) return true;
            Buffer.BlockCopy(buf, 0, coords, 0, buf.Length);
            return false;
        }

        /// <summary>
        /// Receive and fill the passed array with the periodic box vectors. 
        /// </summary>
        /// <returns><c>true</c>, if receive periodic box was received successfully, <c>false</c> otherwise.</returns>
        /// <param name="dim">Dimensionality of the box.</param>
        /// <param name="periodicBox">Periodic box.</param>
        public bool TryReceivePeriodicBox(int dim, float[] periodicBox)
        {
            int size = dim * dim * 4;
            byte[] buf = new byte[size];
            bool error = (socket.Receive(buf, 0, size, SocketFlags.None) != size);
            if (error) return true;
            Buffer.BlockCopy(buf, 0, periodicBox, 0, buf.Length);
            return false;
        }

        /// <summary>
        /// Initialises an IMD instance with the default socket structure.
        /// </summary>
        public Imd()
        {
            //Match the VMD IMD API for creating a socket.
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, 0);
        }

        /// <summary>
        /// Connect to the specified host and port.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        public void Connect(IPAddress host, int port)
        {
            socket.Connect(host, port);
        }

        /// <summary>
        /// Binds the server to a given port./>
        /// </summary>
        /// <param name="port"></param>
        public void Bind(int port)
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
            socket.Bind(endPoint);
        }

        /// <summary>
        /// Start listening on the socket.
        /// </summary>
        public void Listen()
        {
            int numConnectionsQueue = 5;
            socket.Listen(numConnectionsQueue);
        }

        /// <summary>
        /// Determine whether this IMD instance is connected.
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            if (socket != null)
            {
                return socket.Connected;
            }
            return false;
        }

        /// <summary>
        /// Determine whether this IMD instance's socket has data to be read.
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns>True if there is data to be read, false otherwise.</returns>
        public bool SelectRead(int timeout = 2)
        {
            List<Socket> sockets = new List<Socket>(1);
            sockets.Add(socket);
            //TODO should I check for errors here?
            Socket.Select(sockets, null, null, timeout);
            if (sockets.Count > 0)
                return true;
            return false;
        }

        /// <summary>
        /// Determine whether this IMD instance's socket has data to be written.
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns>True if there is data to be written, false otherwise.</returns>
        public bool SelectWrite(int timeout = 2)
        {
            List<Socket> sockets = new List<Socket>(1);
            sockets.Add(socket);
            //TODO should I check for errors here?
            Socket.Select(null, sockets, null, microSeconds: timeout);
            if (sockets.Count > 0)
                return true;
            return false;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (socket == null) return;
                if(socket.Connected)
                {
                    socket.Disconnect(false);

                }
                socket.Dispose();
            }
            //Dispose of any unmanaged resources. 
        }
        
        public void Dispose()
        {
            //Dispose of unmanaged resources
            Dispose(true);
            //Suppress garbage collection.
            GC.SuppressFinalize(this);

        }

    }
}