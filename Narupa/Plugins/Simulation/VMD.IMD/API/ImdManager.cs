﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using IMDDotNet;
using Nano;
using Nano.Science.Simulation;
using SlimMath;

namespace Simbox.VMD.IMD.API
{
    internal class ImdManager : IDisposable
    {
        public int SimTimeStep;
        private ImdSettings settings;
        private Imd imdSession;
        private IReporter reporter;

        private float[] imdPositions;
        private bool connected;
        private float[] imdPeriodicBox;
        private Stopwatch clock;
        private int knownAtoms = 0;

        public ImdManager(ImdSettings settings, IReporter reporter)

        {
            this.settings = settings;
            imdSession = new Imd();
            this.reporter = reporter;
            reporter.PrintNormal($"IMD Session: Will connect to {settings.IPAddress} on port {settings.Port}");
            clock = new Stopwatch();
        }

        public void Connect()
        {
            imdSession.Dispose();
            imdSession = new Imd();
            imdSession.Connect(settings.IPAddress, (int)settings.Port);
            connected = true;
            int needToFlip = imdSession.TryReceiveHandshake();
            switch (needToFlip)
            {
                case 0:
                    reporter.PrintEmphasized("Connected to same endian-ness machine");
                    break;

                case 1:
                    reporter.PrintEmphasized("Connected to opposite endianness machine");
                    break;

                default:
                    throw new Exception("Unable to ascertain endianness of target machine");
            }
        }

        public void Update(ImdAtomicSystem system)
        {
            if (imdSession.IsConnected() == false)
            {
                //If last we heard, we were connected, print out a warning.
                if (connected)
                    reporter.PrintWarning(ReportVerbosity.Emphasized,
                        $"IMD Session has disconnected! Will attempt to reconnect again to {settings.IPAddress} on port {settings.Port}");

                try
                {
                    Connect();
                }
                catch (Exception e)
                {
                    if (connected == true) reporter.PrintException(e, "Failed to connect...");
                }
                connected = false;
            }

            if (imdSession.IsConnected() == false)
                return;
            try
            {
                ReceiveCommunications(system);
                SendForces(system);
            }
            catch (Exception e)
            {
                reporter.PrintException(e, "Exception thrown trying to receive IMD communications");
                imdSession.Dispose();
            }
        }

        private void SendForces(ImdAtomicSystem system)
        {
            List<int> selectedIndices = new List<int>();
            List<float> forces = new List<float>();
            foreach (Interaction interaction in system.Interactions)
            {
                if (interaction.SelectedAtoms.Count == 0)
                    continue;

                foreach (int atomIndex in interaction.SelectedAtoms)
                {
                    selectedIndices.Add(atomIndex);
                    Vector3 force = system.Particles.Force[atomIndex] * Nano.Science.Units.KCalPerKJ /
                                    Nano.Science.Units.AngstromsPerNm;
                    for (int j = 0; j < 3; j++)
                    {
                        forces.Add(force[j]);
                    }
                }
            }
            if (forces.Count > 0)
                imdSession.SendMDCommunication(selectedIndices.Count, selectedIndices.ToArray(), forces.ToArray());
        }

        private int framesWithoutComms = 0;
        private bool nonRectangularBox;

        private void ReceiveCommunications(ImdAtomicSystem system)
        {
            bool receivedComms = false;
            while (imdSession.SelectRead())
            {
                receivedComms = true;
                framesWithoutComms = 0;
                int length = -1;
                ImdType type = imdSession.ReceiveHeader(ref length, true);

                switch (type)
                {
                    case ImdType.ImdIoerror:
                        throw new Exception("IOError occurred!");

                    case ImdType.ImdEnergies:
                        ProcessEnergies(system, length);
                        break;

                    case ImdType.ImdFcoords:
                        //reporter.PrintDebug("Received coords");
                        ProcessCoords(system, length);
                        break;

                    case ImdType.ImdPeriodicBox:
                        //reporter.PrintDebug("Received Periodic box...");
                        ProcessPeriodicBox(system, length);
                        break;

                    case ImdType.ImdMdcomm:
                        throw new Exception("Should not have received MD Comms!");
                    default:
                        throw new Exception($"Unknown communications received with type: {type}");
                }
            }

            if (receivedComms == false)
            {
                framesWithoutComms++;
                if (framesWithoutComms > 200)
                {
                    reporter.PrintWarning(ReportVerbosity.Normal, "Haven't received any comms for a while, reconnecting!");
                    Connect();
                    framesWithoutComms = 0;
                }
            }
            
        }

        private void ProcessCoords(ImdAtomicSystem system, int length)

        {
            if (length != knownAtoms)
            {
                reporter.PrintWarning(ReportVerbosity.Normal,
                    $"The topology defined in the input file consists of {system.NumberOfParticles} atoms, and we have received {length}. Additional atoms will be ignored");
                knownAtoms = length;
            }
            if (imdPositions == null || imdPositions.Length != length * 3)
            {
                imdPositions = new float[length * 3];
            }

            imdSession.TryReceiveFCoords(length, imdPositions);

            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                Vector3 pos = system.Particles.Position[i];
                for (int j = 0; j < 3; j++)
                {
                    pos[j] = imdPositions[i * 3 + j];
                }
                system.Particles.Position[i] = pos * Nano.Science.Units.NmPerAngstrom;
            }
        }

        private void ProcessEnergies(ImdAtomicSystem system, int length)
        {
            ImdEnergies energies = new ImdEnergies();
            imdSession.TryReceiveEnergies(ref energies);
            system.PotentialEnergy = energies.Epot;
            SimTimeStep = energies.TStep;
        }

        private void ProcessPeriodicBox(ImdAtomicSystem system, int length)
        {
            if (length != 3) throw new Exception("Received a periodic box of a dimensionality we do not support!");
            if (imdPeriodicBox == null) imdPeriodicBox = new float[3 * 3];
            imdSession.TryReceivePeriodicBox(length, imdPeriodicBox);
            Vector3 a = Vector3.Zero, b = Vector3.Zero, c = Vector3.Zero;
            for (int i = 0; i < 3; i++)
            {
                a[i] = imdPeriodicBox[i];
                b[i] = imdPeriodicBox[3 + i];
                c[i] = imdPeriodicBox[6 + i];
            }
            try
            {
                system.Boundary.SetSimulationBox(a, b, c, false);
            }
            catch (Exception e)
            {
                if (nonRectangularBox == false)
                {
                    reporter.PrintException(e,"Cannot handle box of this shape, will render a large box.");
                    system.Boundary.SetSimulationBox(new BoundingBox(-10000f, -10000f, -10000f, 10000f, 10000f, 10000f));
                    nonRectangularBox = true;
                }
            }

            system.Boundary.PeriodicBoundary = true;
        }

        public void Dispose()
        {
            if (imdSession.IsConnected())
            {
                imdSession.SendDisconnect();
            }
            imdSession.Dispose();
        }

        public void SendPlay()
        {
            if (imdSession != null && imdSession.IsConnected())
            {
                imdSession?.SendGo();                
            }
        }

        public void SendPause()
        {
            if (imdSession != null && imdSession.IsConnected())
            {
                imdSession?.SendPause();                
            }
        }

        public void SendKill()
        {
            if (imdSession != null && imdSession.IsConnected())
            {
                imdSession?.SendKill();                
            }
        }
    }
}