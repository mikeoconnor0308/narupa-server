﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation.Log;

namespace Simbox.VMD.IMD.API
{
    [XmlName("IMDSettings")]
    public class ImdSettings : ILoadable
    {
        /// <summary>
        /// IP Address that will be used to connect to for IMD session.
        /// </summary>
        public IPAddress IPAddress = IPAddress.Loopback;

        /// <summary>
        /// Port of host machine to connect to.
        /// </summary>
        public uint Port = 54321;

        /// <summary>
        /// Command to be launched.
        /// </summary>
        public CommandProcess Command;

        /// <summary>
        /// Loggers attached to this simulation.
        /// </summary>
        public List<ISimulationLogger> Loggers = new List<ISimulationLogger>();

        /// <summary>
        /// Saves this instance.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            if (IPAddress.Equals(IPAddress.Loopback))
            {
                Helper.AppendAttributeAndValue(element, "HostIP", "localhost");
            }
            else
            {
                Helper.AppendAttributeAndValue(element, "HostIP", IPAddress.ToString());
            }
            Helper.AppendAttributeAndValue(element, "Port", Port);
            if (Command != null) Loader.SaveObject(context, element, Command);
            return element;
        }

        /// <summary>
        /// Loads this instance.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        public void Load(LoadContext context, XmlNode node)
        {
            string ip = Helper.GetAttributeValue(node, "HostIP", "localhost");
            if (ip == "localhost")
            {
                IPAddress = IPAddress.Loopback;
            }
            else
            {
                try
                {
                    IPAddress = IPAddress.Parse(ip);
                }
                catch (Exception e)
                {
                    context.Error($"Failed to read HostIP: {ip}", true, true);
                }
            }

            Port = Helper.GetAttributeValue(node, "Port", Port);
            Command = Loader.LoadObject<CommandProcess>(context, node);

            Loggers.AddRange(Loader.LoadObjects<ISimulationLogger>(context, "Logger", node.SelectSingleNode("Logging")));
        }
    }
}