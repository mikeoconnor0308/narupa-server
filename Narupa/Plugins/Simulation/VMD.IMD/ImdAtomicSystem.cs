﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano;
using Nano.Science;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Log;
using Rug.Osc;
using Simbox.MD.ForceField.External.ForceField;
using Simbox.MD.Selection;
using SlimMath;

namespace Simbox.VMD.IMD
{
    internal class ImdAtomicSystem : AtomicSystemBase
    {
        public ImdAtomicSystem(InstantiatedTopology activeTopology, ISimulationBoundary boundary, IReporter reporter, List<ISimulationLogger> loggers)
        {
            AtomSelections = new AtomSelectionPool(new OscAddress("/top"), activeTopology, reporter);
            Topology = activeTopology;
            Boundary = boundary;
            Reporter = reporter;

            InitialiseFields();
            reporter?.PrintEmphasized("Creating atomic system.");
            Particles = new ParticleCollection();
            Reporter = reporter;

            for (int i = 0; i < activeTopology.NumberOfParticles; i++)
            {
                bool isVirtual = activeTopology.Elements[i] == Element.Virtual;
                Particles.AddParticle(PeriodicTable.GetElementProperties(activeTopology.Elements[i]).AtomicWeight, activeTopology.Positions[i], Vector3.Zero, Vector3.Zero, isVirtual);
            }

            //TODO @review this is a hack, IForceField needs it.
            SystemProperties properties = new SystemProperties(reporter);
            properties.SimBoundary = boundary;

            bool hasRestraintForce = false;
            foreach (IInstantiatedForceFieldTerms forceFieldTerm in Topology.ForceFieldTerms)
            {
                IForceField forceField = forceFieldTerm.GenerateForceField(Topology, properties, Reporter);
                ForceFields.Add(forceField);
                // ReSharper disable once SuspiciousTypeConversion.Global, there is this in another solution.
                var item = forceField as IConstraintImplementation;
                if (item != null)
                {
                    // ReSharper disable once SuspiciousTypeConversion.Global this exists in Simbox.OpenMM
                    Constraints.Add(item);
                }

                if (forceField is ParticleRestraintForce)
                {
                    hasRestraintForce = true;
                }
            }

            //ensure all imd atomic systems have the particle restraint force. 
            if (hasRestraintForce == false)
            {
                ForceFields.Add(new ParticleRestraintForce());
            }
            
            GetDegreesOfFreedom();

            foreach (var logger in loggers)
            {
                SimulationLogManager.AddLogger(logger);
            }
            SimulationLogManager.InitialiseActiveLogs();
            AtomSelections.UpdateVisibleAtoms();
        }
    }
}