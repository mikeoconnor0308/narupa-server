﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Diagnostics;
using System.Xml;
using Nano;
using Nano.Loading;

namespace Simbox.VMD.IMD
{
    [XmlName("Command")]
    public class CommandProcess : ILoadable, IDisposable
    {
        /// <summary>
        /// The command to launch.
        /// </summary>
        /// <remarks> Examples: bash, /usr/bin/python. </remarks>
        public string FileName;

        /// <summary>
        /// The arguments to pass to the command. 
        /// </summary>
        public string Arguments;

        /// <summary>
        /// The working directory to run the command from. 
        /// </summary>
        public string WorkingDirectory;

        public bool IsRunning => process != null && process.HasExited == false;
        public bool SupportsPortArgument = false;
        private Process process;
        private IReporter reporter;

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Cmd", FileName);
            Helper.AppendAttributeAndValue(element, "Arguments", Arguments);
            Helper.AppendAttributeAndValue(element, "WorkingDir", WorkingDirectory);
            return element;
        }

        public void Load(LoadContext context, XmlNode node)
        {
            FileName = Helper.GetAttributeValue(node, "FileName", "");
            WorkingDirectory = Helper.GetAttributeValue(node, "WorkingDir", ".");
            Arguments = Helper.GetAttributeValue(node, "Arguments", "");
            SupportsPortArgument = Helper.GetAttributeValue(node, "SupportsPort", SupportsPortArgument);
        }

        /// <summary>
        /// Starts the process.
        /// </summary>
        /// <param name="reporter"></param>
        /// <param name="port">The port to run the process on, if applicable</param>
        /// <exception cref="Exception"></exception>
        public void Start(IReporter reporter, uint port = 0)
        {
            string cmd = "";
            string args = "";

            cmd = FileName;
            args = Arguments;

            if (SupportsPortArgument && port != 0)
            {
                args = args.Replace("${PORT}", port.ToString());
            }
            
            this.reporter = reporter;
            process = new Process
            {
                StartInfo =
                {
                    FileName = cmd,
                    Arguments = args,
                    WorkingDirectory = WorkingDirectory,
                    UseShellExecute =  false,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    
                    
                }
            };
            
            process.OutputDataReceived += OnOutputDataReceived;
            process.ErrorDataReceived += OnErrorDataReceived;
            
            try
            {
                reporter?.PrintEmphasized(
                    $"Launching command {cmd} with arguments {args}, working directory: {WorkingDirectory}");
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
            }
            catch (Exception e)
            {
                reporter?.PrintException(e, "Unable to launch command");
                throw new Exception("Unable to launch command.");
            }
        }

        private void OnErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            //TODO wrap this stuff up and print once per frame.
            reporter?.PrintError($"Process error: {e.Data}");
        }

        private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            //TODO wrap this stuff up and print once per frame.
            reporter?.PrintNormal($"Process output: {e.Data}");
        }

        public void Kill()
        {
            if (process == null) return;
            process.Kill();
            bool success = process.WaitForExit(1000);
            if (!success)
                throw new Exception("Failed to kill existing process!");
            process.Dispose();
        }

        public void Restart(uint port=0)
        {
            Kill();
            //process.Start();
            Start(reporter, port);
        }

        public void Dispose()
        {
            //Dispose of unmanaged resources
            Dispose(true);
            //Suppress garbage collection.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            process?.Kill();
            if (disposing)
            {
                process?.Dispose();
            }
        }
    }
}