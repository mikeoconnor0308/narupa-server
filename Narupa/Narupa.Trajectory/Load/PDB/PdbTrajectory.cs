using System.Collections.Generic;
using Nano;
using Nano.Science;
using SlimMath;

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    /// Class for reading in a trajectory from a PDB file.
    /// </summary>
    public static class PdbTrajectory
    {
        /// <summary>
        ///     Loads a trajectory and topology from PDB file.
        /// </summary>
        /// <param name="trajectoryFile">The PDB file from which to load the trajectory.</param>
        /// <param name="reporter">Reporter to output information to.</param>
        /// <param name="centreAboutZero">Whether to centre coordinates about zero.</param>
        /// <returns></returns>
        public static ITrajectory LoadTrajectory(string trajectoryFile, IReporter reporter,
            bool centreAboutZero = false)
        {
            var PdbFile = new PdbFile(trajectoryFile, reporter, centreAboutZero);

            var topology = GenerateTopology(PdbFile);

            var traj = GenerateTrajectory(PdbFile, topology);
            return traj;
        }

        /// <summary>
        ///     Laods a trajectory from a PDB file, given an existing topology.
        /// </summary>
        /// <param name="trajectoryFile">The PDB file from which to load the trajectory.</param>
        /// <param name="reporter">The reporter to print information to.</param>
        /// <param name="topology">The topoloy to use for the trajectory.</param>
        /// <param name="centreAboutZero">Whether to centre coordinates about zero or not.</param>
        /// <returns></returns>
        public static ITrajectory LoadTrajectory(string trajectoryFile, IReporter reporter, ITopology topology,
            bool centreAboutZero = false)
        {
            var PdbFile = new PdbFile(trajectoryFile, reporter, centreAboutZero);
            var traj = GenerateTrajectory(PdbFile, topology);
            return traj;
        }

        /// <summary>
        ///     Loads a topology from a PDB file.
        /// </summary>
        /// <param name="topologyFile">Path to topology file.</param>
        /// <param name="reporter">Reporter to use.</param>
        /// <returns></returns>
        public static ITopology LoadTopology(string topologyFile, IReporter reporter)
        {
            var PdbFile = new PdbFile(topologyFile, reporter);
            return GenerateTopology(PdbFile);
        }

        private static ITrajectory GenerateTrajectory(PdbFile pdbFile, ITopology topology)
        {
            var traj = new Trajectory();

            var positions = new List<Vector3>(pdbFile.Models[0].AtomsByNumber.Count);
            for (var i = 0; i < pdbFile.Models[0].AtomsByNumber.Count; i++) positions.Add(Vector3.Zero);
            foreach (var model in pdbFile.Models)
            {
                foreach (var atom in model.AtomsByIndex.Values) positions[atom.AbsoluteIndex] = atom.Position * Units.NmPerAngstrom;
                var frame = new Frame(topology, positions);
                traj.AddFrame(frame);
            }

            return traj;
        }

        private static Topology GenerateTopology(PdbFile pdbFile, int frame = 0)
        {
            var top = new Topology();
            var model = pdbFile.Models[frame];
            foreach (var pdbChain in model.Chains)
            {
                var chain = top.AddChain();
                foreach (var pdbResidue in pdbChain.Residues)
                {
                    var residue = top.AddResidue(pdbResidue.Name, chain, pdbResidue.Number);
                    foreach (var atom in pdbResidue.Atoms)
                        top.AddAtom(atom.Name,
                            (Element) PeriodicTable.GetElementPropertiesBySymbol(atom.Element).Number, residue,
                            atom.SerialNumber);
                }
            }

            foreach (var bond in pdbFile.Bonds) top.AddBond(bond.From.AbsoluteIndex, bond.To.AbsoluteIndex);

            return top;
        }
    }
}