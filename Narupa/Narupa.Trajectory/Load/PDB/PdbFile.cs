﻿// Based on the PDB file implementation of OpenMM, distributed under MIT license: 
/*
This is part of the OpenMM molecular simulation toolkit originating from
Simbios, the NIH National Center for Physics-Based Simulation of
Biological Structures at Stanford, funded under the NIH Roadmap for
Medical Research, grant U54 GM072970. See https://simtk.org.
Portions copyright (c) 2012-2018 Stanford University and the Authors.
Authors: Peter Eastman
Contributors:
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Nano;
using SlimMath;

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    ///     Initialises a new PdbFile class.
    /// </summary>
    public class PdbFile
    {
        private readonly IReporter reporter;

        /// <summary>
        ///     The bond data for the residue contained in the xml file.
        /// </summary>
        private readonly Dictionary<string, List<PdbResidueBondData>> residueBondData =
            new Dictionary<string, List<PdbResidueBondData>>();

        /// <summary>
        ///     The bonds contained in the the PDB file.
        /// </summary>
        public HashSet<PdbBond> Bonds = new HashSet<PdbBond>();

        /// <summary>
        ///     The models contained in the the PDB file.
        /// </summary>
        public List<PdbModel> Models = new List<PdbModel>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:Nano.Science.Simulation.PDB.PDBFile" /> class.
        /// </summary>
        /// <param name="pdbFilePath">The PDB file to read.</param>
        /// <param name="reporter">Reporter for printing load information.</param>
        /// <param name="centreAboutZero">If true, will centre protein coordinates around 0. False by default.</param>
        /// <param name="readFirstModelOnly">
        ///     If true, reads in the first model from a pdb file only, otherwise reads in all of
        ///     them.
        /// </param>
        public PdbFile(string pdbFilePath, IReporter reporter, bool centreAboutZero = false,
            bool readFirstModelOnly = false)
        {
            this.reporter = reporter;
            var fullFilePath = Helper.ResolvePath(pdbFilePath);
            reporter?.PrintNormal($"Reading in PDB file from {pdbFilePath}");

            // Read the file and display it line by line.
            var file =
                new StreamReader(fullFilePath);

            //Instead of reading all at once as below. keep reading models until end of file.
            var successfullyReadModel = ReadNextModel(file);

            if (readFirstModelOnly == false)
                while (successfullyReadModel)
                    successfullyReadModel = ReadNextModel(file);

            file.Close();
            ReadResidueBondData();
            reporter?.PrintNormal("Generating bonds using CONECT records and from known residue data");
            GenerateBonds();
            if (centreAboutZero)
            {
                reporter.PrintNormal("Centering coordinates around zero");
                CentreAboutZero();
            }

            //WriteToFile("^/pdb_output_test.pdb");
        }

        /// <summary>
        ///     Read residue bond data from the specified path.
        /// </summary>
        /// <remarks>
        ///     This should point to a residue definition file of the style used by OpenMM.
        /// </remarks>
        /// <param name="filepath"></param>
        public void ReadResidueBondData(string filepath = "^/Assets/Data/residues.xml")
        {
            //Loads xml file and selects residue nodes.
            var path = Helper.ResolvePath(filepath);
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            var residueNodes = xmlDoc.SelectNodes("//Residue");

            //Loops over residue nodes, stores residue name and creates list of residue bonds .
            if (residueNodes != null)
                foreach (XmlNode node in residueNodes)
                    //Gets residue name and creates residueBonds list.
                    if (node.Attributes != null)
                    {
                        var residueStr = node.Attributes["name"].Value;
                        var residueBonds = new List<PdbResidueBondData>();

                        ////Loops over bond nodes and gets attributes "from" and "to" and
                        ////adds them to list of residue bonds.
                        foreach (XmlNode bondNode in node.ChildNodes)
                            if (bondNode.Attributes != null)
                            {
                                var fromStr = bondNode.Attributes["from"].Value;
                                var toStr = bondNode.Attributes["to"].Value;
                                residueBonds.Add(new PdbResidueBondData(fromStr, toStr));
                            }

                        //reporter.PrintNormal($"Residue {residueStr}, From {fromStr}, To{toStr}");

                        //adds the list to the dictionary ResidueBondData,
                        //using the residueStr as the key.
                        residueBondData.Add(residueStr, residueBonds);
                    }
        }

        /// <summary>
        ///     Writes this instance to the given filepath.
        /// </summary>
        /// <param name="filePath"></param>
        public void WriteToFile(string filePath)
        {
            var model = Models[0];
            var recordName = "";

            using (var file =
                new StreamWriter(Helper.ResolvePath(filePath)))
            {
                for (var modelNumber = 0; modelNumber < Models.Count; modelNumber++)
                {
                    var remarkLine = $"{"REMARK",6}{"1",3}";
                    var modelLine = $"{"MODEL",-6}{modelNumber + 1,7}";
                    file.WriteLine(remarkLine);
                    file.WriteLine(modelLine);

                    for (var chainNumber = 0; chainNumber < model.Chains.Count; chainNumber++)
                    {
                        var chain = model.Chains[chainNumber];
                        for (var resId = 0; resId < chain.Residues.Count; resId++)
                        {
                            var residue = chain.Residues[resId];
                            for (var atomNumber = 0; atomNumber < residue.Atoms.Count; atomNumber++)
                            {
                                var atom = residue.Atoms[atomNumber];
                                var charge = atom.Charge.ToString();

                                if (atom.IsHeterogen)
                                    recordName = "HETATM";
                                else if (atom.IsHeterogen == false) recordName = "ATOM";

                                if (atom.Charge == 0) charge = charge.Replace("0", " ");

                                var atomLine = $"{recordName,-6}{atom.SerialNumber,5}{"",1}{atom.Name,-4}{"",1}" +
                                               $"{atom.ResidueName,3}{"",1}{atom.ChainIdentifier,1}{atom.ResidueId,4}" +
                                               $"{atom.ACharCode,1}{"",4}{FormatFloatForPdb(atom.Position.X)}{FormatFloatForPdb(atom.Position.Y)}{FormatFloatForPdb(atom.Position.Z)}" +
                                               $"{atom.Occupancy,6}{atom.TemperatureFactor,6}{atom.SegmentIdentifier,-4}{atom.Element,2}{charge,2}";

                                file.WriteLine(atomLine);
                            }
                        }
                    }

                    var lastModel = Models[modelNumber];
                    var lastChain = lastModel.Chains[lastModel.Chains.Count - 1];
                    var lastResidue = lastChain.Residues[lastChain.Residues.Count - 1];
                    var lastAtom = lastResidue.Atoms[lastResidue.Atoms.Count - 1];

                    var terLine =
                        $"{"TER",-6}{lastAtom.SerialNumber,5}{"",1}{"",-4}{"",1}{lastAtom.ResidueName,3}{"",1}{lastAtom.ChainIdentifier,1}{lastAtom.ResidueId,4}";
                    file.WriteLine(terLine);
                    var endMdlLine = $"{"ENDMDL",-6}";

                    file.WriteLine(endMdlLine);
                }
            }
        }

        private static string FormatFloatForPdb(double x)
        {
            if (x > -999.999 && x < 9999.999)
                return $"{x,8:F3}";
            if (x > -9999999.5 && x < 99999999.5)
                return x.ToString(CultureInfo.InvariantCulture).Substring(0, 8);
            throw new Exception(
                "Structure contains decimal value that cannot be represented in 8 characters");
        }

        //Defines the method AddBonds which will be called in the GenerateBonds method.
        private void AddBonds(int resIdFrom, int resIdTo, string from, string to, PdbChain chain, ISet<PdbBond> bonds)
        {
            if (chain.Residues[resIdFrom].AtomsByName.ContainsKey(from)
                && chain.Residues[resIdTo].AtomsByName.ContainsKey(to))

            {
                var atomFrom = chain.Residues[resIdFrom].AtomsByName[from];
                var atomTo = chain.Residues[resIdTo].AtomsByName[to];
                var newBond = new PdbBond(atomFrom, atomTo);
                bonds.Add(newBond);
            }
        }

        //Defines a GenerateBonds method.
        private void GenerateBonds()
        {
            var model = Models[0];

            //Loops over chains.
            for (var chainNumber = 0; chainNumber < model.Chains.Count; chainNumber++)
            {
                var chain = model.Chains[chainNumber];
                //Loops over residues.
                for (var resId = 0; resId < chain.Residues.Count; resId++)
                {
                    //Gets the current residue and creates a list called bondData.
                    var residue = chain.Residues[resId];

                    if (residueBondData.ContainsKey(residue.Name) == false) continue;
                    var bondData = residueBondData[residue.Name];
                    //Loops over bonds in bond data.
                    for (var bondNumber = 0; bondNumber < bondData.Count; bondNumber++)
                    {
                        //Gets To and From strings.
                        //bondData[bondNumber] = new PdbResidueBondData((bondData[bondNumber].From), (bondData[bondNumber].To));
                        var from = bondData[bondNumber].From;
                        var to = bondData[bondNumber].To;

                        //Prints out to and from strings to check they work.
                        //reporter.PrintNormal($"{bondData[bondNumber].From}, {bondData[bondNumber].To}");

                        if (from.StartsWith("-"))
                        {
                            if (resId == 0) continue;

                            var resIdTo = resId;
                            var resIdFrom = resId - 1;

                            from = from.Substring(1);

                            AddBonds(resIdFrom, resIdTo, from, to, chain, Bonds);
                        }
                        else if (to.StartsWith("-"))
                        {
                            if (resId == 0)
                                continue;

                            var resIdTo = resId;
                            var resIdFrom = resId - 1;

                            to = to.Substring(1);

                            AddBonds(resIdFrom, resIdTo, from, to, chain, Bonds);
                        }
                        else if (from.StartsWith("+"))
                        {
                            if (resId == chain.Residues.Count)
                                continue;

                            from = from.Substring(1);
                            var resIdFrom = resId + 1;
                            var resIdTo = resId;

                            AddBonds(resIdFrom, resIdTo, from, to, chain, Bonds);
                        }
                        else if (to.StartsWith("+"))
                        {
                            if (resId == chain.Residues.Count)
                                continue;

                            to = to.Substring(1);
                            var resIdTo = resId + 1;
                            var resIdFrom = resId;

                            AddBonds(resIdFrom, resIdTo, from, to, chain, Bonds);
                        }
                        else
                        {
                            if (resId == 0) continue;
                            var resIdTo = resId;
                            var resIdFrom = resId;

                            AddBonds(resIdFrom, resIdTo, from, to, chain, Bonds);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     First model centred around zero, all other models centred relative to previous model.
        /// </summary>
        private void CentreAboutZero()
        {
            var positions = new List<Vector3>();

            var model = Models[0];
            for (var chainNumber = 0; chainNumber < model.Chains.Count; chainNumber++)
            {
                var chain = model.Chains[chainNumber];

                for (var resId = 0; resId < chain.Residues.Count; resId++)
                {
                    var residue = chain.Residues[resId];

                    for (var atomNumber = 0; atomNumber < residue.Atoms.Count; atomNumber++)
                    {
                        var currentAtom = residue.Atoms[atomNumber];
                        positions.Add(currentAtom.Position);
                    }
                }
            }

            var centred = new List<Vector3>();
            var centre = Vector3.Zero;

            for (var positionNumber = 0; positionNumber < positions.Count; positionNumber++)
                centre += positions[positionNumber];
            centre = centre / positions.Count;

            foreach (var m in Models)
                for (var chainNumber = 0; chainNumber < model.Chains.Count; chainNumber++)
                {
                    var chain = m.Chains[chainNumber];

                    for (var resId = 0; resId < chain.Residues.Count; resId++)
                    {
                        var residue = chain.Residues[resId];

                        for (var atomNumber = 0; atomNumber < residue.Atoms.Count; atomNumber++)
                        {
                            var currentAtom = residue.Atoms[atomNumber];
                            currentAtom.Position = currentAtom.Position - centre;
                        }
                    }
                }
        }

        private bool ReadNextModel(StreamReader file)
        {
            var addedModel = false;

            PdbResidue residue = null;
            var currentResId = 0;
            PdbModel model = null;
            PdbChain chain = null;
            var currentChainChar = "";
            var atomCount = 0;
            var residueCount = -1;
            string line;
            while ((line = file.ReadLine()) != null)
                //if found a model, create the PDBModel type and add it to the list of Models.
                if (line.StartsWith("MODEL"))
                {
                    var modelNumber = int.Parse(line.Substring(10, 4));
                    model = new PdbModel(modelNumber);
                    Models.Add(model);
                    addedModel = true;
                    atomCount = 0;
                    residueCount = 0;
                }
                else if (line.StartsWith("ENDMDL"))
                {
                    return true;
                }
                else if (line.Contains("CONECT"))
                {
                    var lineParts = line.Split(new[] {" ", "\t"}, StringSplitOptions.RemoveEmptyEntries);
                    var startPoint = int.Parse(lineParts[1]);
                    PdbModel modelReference;
                    if (Models.Count > 0)
                        modelReference = Models[0];
                    else
                        throw new Exception("Invalid CONECT record prior to atoms being read in. Check input.");

                    //use the index map to get the atom.
                    var startAtom = modelReference.AtomsByIndex[startPoint];

                    for (var i = 2; i < lineParts.Length; i++)
                    {
                        var endPoint = int.Parse(lineParts[i]);
                        var endAtom = modelReference.AtomsByIndex[endPoint];
                        Bonds.Add(new PdbBond(startAtom, endAtom));
                    }
                }
                else if (line.Length >= 4)
                {
                    var lineLabel = line.Substring(0, 6);

                    if (lineLabel.Contains("ATOM") || lineLabel.Contains("HETATM"))
                    {
                        if (Models.Count == 0)
                        {
                            model = new PdbModel(1);
                            Models.Add(model);
                            addedModel = true;
                        }

                        if (model == null) throw new Exception("Tried to add a without creating a model, check input");

                        var newChainChar = line.Substring(21, 1);

                        if (newChainChar != currentChainChar || model.Chains.Count == 0)
                        {
                            chain = new PdbChain(newChainChar);
                            model.Chains.Add(chain);
                            currentChainChar = newChainChar;
                            atomCount = 0;
                            residueCount = 0;
                        }

                        if (chain == null)
                            throw new Exception("Tried to add a residue without creating a chain, check input");

                        //check for current residue id. if residue id in line is different, create a new residue and add it to the list of residues.
                        //or if residue.count = 0 create residue and add to the list of residues.
                        var newResId = int.Parse(line.Substring(22, 4).Trim());
                        var newResName = line.Substring(17, 4).Trim();

                        if (newResId != currentResId || chain.Residues.Count == 0)

                        {
                            residueCount++;
                            residue = new PdbResidue(newResId, newResName)
                            {
                                AbsoluteIndex = residueCount
                            };
                            chain.Residues.Add(residue);
                            //residue just added becomes current residue
                            currentResId = newResId;
                        }

                        //add atoms to residue
                        var atom = new PdbAtom(line, this, reporter)
                        {
                            AbsoluteResidueIndex = residueCount,
                            AbsoluteIndex = atomCount
                        };
                        atomCount++;

                        if (residue == null)
                            throw new Exception("Tried to add an atom without creating a residue, check input");
                        residue.Atoms.Add(atom);
                        model.AtomsByIndex[atom.SerialNumber] = atom;
                        //TODO make this not fail. Linked list? 
                        if (residue.AtomsByName.ContainsKey(atom.Name) == false)
                            residue.AtomsByName.Add(atom.Name, atom);

                        model.AtomsByNumber[atom.SerialNumber] = atom;
                    }
                }
                else if (line.Contains("CONECT"))
                {
                    var lineParts = line.Split();
                    var startPoint = int.Parse(lineParts[1]);
                    //use the index map to get the atom.
                    if (model != null)
                    {
                        var startAtom = model.AtomsByIndex[startPoint];

                        for (var i = 2; i < lineParts.Length; i++)
                        {
                            var endPoint = int.Parse(lineParts[i]);
                            var endAtom = model.AtomsByIndex[endPoint];
                            var newBond = new PdbBond(startAtom, endAtom);

                            Bonds.Add(new PdbBond(startAtom, endAtom));
                        }
                    }
                }
                //This should be ok. 
                else if (line.Contains("END"))
                {
                    reporter?.PrintNormal($"Reached end of PDB file at line: {line}");
                    // stop processing the file here
                    break;
                }
                else
                {
                    reporter?.PrintWarning(ReportVerbosity.Detail, $"Found line we could not handle: {line}");
                }

            if (addedModel)
                return true;
            return false;
        }

        //Defines a PrintAtoms method.
        private void PrintAtoms()
        {
            foreach (var model in Models)
            foreach (var chain in model.Chains)
            foreach (var residue in chain.Residues)
            foreach (var atom in residue.Atoms)
                reporter?.PrintNormal(
                    $"model: {model.Number} chain:{chain.Letter} residue: {residue.Number} {residue.Name} atom: {atom.Name}");
        }

        //Defines a PrintBonds() method.
        private void PrintBonds()
        {
            foreach (var newBond in Bonds)
                reporter?.PrintNormal(
                    $"({newBond.From.SerialNumber},{newBond.To.SerialNumber}) - {newBond.From.ResidueId} {newBond.From.Name} - {newBond.To.ResidueId} {newBond.To.Name}");
            var numberOfBonds = Bonds.Count;
            reporter?.PrintNormal($"{numberOfBonds}");
        }
    }
}