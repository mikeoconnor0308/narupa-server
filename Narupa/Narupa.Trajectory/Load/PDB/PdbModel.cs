﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    ///     Initialises the PdbModel class.
    /// </summary>
    public class PdbModel
    {
        /// <summary>
        ///     Model number.
        /// </summary>
        public readonly int Number;

        /// <summary>
        ///     A dictionary of atoms by their index.
        /// </summary>
        public Dictionary<int, PdbAtom> AtomsByIndex = new Dictionary<int, PdbAtom>();

        /// <summary>
        ///     A dictionary of atoms by their number.
        /// </summary>
        public Dictionary<int, PdbAtom> AtomsByNumber = new Dictionary<int, PdbAtom>();

        /// <summary>
        ///     The residues contained in the PDBFile.
        /// </summary>
        public List<PdbChain> Chains = new List<PdbChain>();

        /// <summary>
        ///     Initialises the properties of a model.
        /// </summary>
        /// <param name="modelNumber"></param>
        public PdbModel(int modelNumber)
        {
            Number = modelNumber;
        }
    }
}