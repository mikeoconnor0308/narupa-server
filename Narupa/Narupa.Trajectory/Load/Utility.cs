// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Nano.Science;
using SlimMath;

namespace Narupa.Trajectory.Load
{
    /// <summary>
    ///     Utility functions for loading trajectories.
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Method for extracting the element based on an atom name.
        /// </summary>
        /// <param name="atomName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static Element GetElement(string atomName)
        {
            if (atomName.Length == 1) return (Element) PeriodicTable.GetElementPropertiesBySymbol(atomName).Number;
            //otherwise, have to do our by assuming the characters up to a number is the element.
            var result = Regex.Match(atomName, @"^[^0-9]*").Value;
            if (string.IsNullOrEmpty(result))
                throw new Exception($"Unable to determine element for atom with name {atomName}.");
            return (Element) PeriodicTable.GetElementPropertiesBySymbol(result).Number;
        }

        /// <summary>
        ///     Generates bonds based on simple distance metric.
        /// </summary>
        /// <param name="elements">Elements of the atoms.</param>
        /// <param name="positions">Positions of the atoms.</param>
        /// <param name="cutoff">Cutoff distance to use.</param>
        /// <returns></returns>
        public static List<BondPair> GenerateBondsDistance(List<Element> elements, List<Vector3> positions)
        {
            var bonds = new List<BondPair>();

            for (var i = 0; i < positions.Count; i++)
            for (var j = i + 1; j < positions.Count; j++)
            {
                float radiusI = PeriodicTable.GetElementProperties(elements[i]).VDWRadius;
                float radiusJ = PeriodicTable.GetElementProperties(elements[j]).VDWRadius;
                float cutoff = 0.6f * (radiusI + radiusJ);
                float cutoffSqr = cutoff * cutoff;
                if (Vector3.DistanceSquared(positions[i], positions[j]) < cutoffSqr)
                    bonds.Add(new BondPair(i, j));
            }


            return bonds;
        }

    }
}