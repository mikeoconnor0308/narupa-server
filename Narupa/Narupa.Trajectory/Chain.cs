// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Represents a chain, as in a PDB topology.
    /// </summary>
    public class Chain
    {
        private readonly List<Residue> residues = new List<Residue>();

        /// <summary>
        /// The residues in the chain.
        /// </summary>
        public IEnumerable<Residue> Residues
        {
            get
            {
                foreach (var residue in Residues) yield return residue;
            }
        }

        /// <summary>
        /// The atoms in the chain.
        /// </summary>
        public IEnumerable<Atom> Atoms
        {
            get
            {
                foreach (var residue in Residues)
                foreach (var atom in residue.Atoms)
                    yield return atom;
            }
        }

        /// <summary>
        /// Add a residue to the chain.
        /// </summary>
        /// <param name="res"></param>
        public void AddResidue(Residue res)
        {
            residues.Add(res);
        }
    }
}